<?php

return array(
    'customer' => array(
        'type' => CAuthItem::TYPE_ROLE,
        'description' => 'Customer',
        'bizRule' => null,
        'data' => null
    ),
    'partner' => array(
        'type' => CAuthItem::TYPE_ROLE,
        'description' => 'Partner',
        'children' => array(
            'customer',
        ),
        'bizRule' => null,
        'data' => null
    ),
    'agent' => array(
        'type' => CAuthItem::TYPE_ROLE,
        'description' => 'Agent',
        'children' => array(
            'partner',
        ),
        'bizRule' => null,
        'data' => null
    ),
    'manager' => array(
        'type' => CAuthItem::TYPE_ROLE,
        'description' => 'Manager',
        'children' => array(
            'agent',
        ),
        'bizRule' => null,
        'data' => null
    ),
    'administrator' => array(
        'type' => CAuthItem::TYPE_ROLE,
        'description' => 'Administrator',
        'children' => array(
            'manager',
        ),
        'bizRule' => null,
        'data' => null
    ),
);
