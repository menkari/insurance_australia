<?php $form=$this->beginWidget('CActiveForm', array(
    'id'=>'users-login-form',
    'enableAjaxValidation' => true,
    'enableClientValidation' => true,
    'clientOptions' => array(
        //'errorCssClass' => 'has-error',
        'validateOnSubmit' => true,
//        'beforeValidate' => 'js:function(form, data, hasError) {$("#login-form-enter").html("<img src=\"/themes/themeName/images/preloader.gif\" class=\"preloader\"/>"); return true;}',
        'afterValidate' =>  'js:function(form, data, hasError) {if (!hasError) {location.reload("/");} else {} return false;}'
    ),
//    'clientOptions'=>array(
//        'validateOnSubmit'=>true,
//    ),
    'htmlOptions'=>array(
        'class'=>'p-form-sign',
    ),
    'action' => '/site/login'
)); ?>

<?php echo $form->textField($model, 'email', array('class'=>'p-inputs sign-email', 'placeholder'=>'Email')); ?>
<?php echo $form->error($model,'email', array('style'=>'color: red;')); ?>

<?php echo $form->passwordField($model, 'password', array('class'=>'p-inputs sign-pass', 'placeholder'=>'Password')); ?>
<?php echo $form->error($model,'password', array('style'=>'color: red;')); ?>

<!--label class='p-cbox-label'>
    <input type="checkbox" class='remember'>
    <span>Remember me</span>
</label-->

<div style="color: blue!important; text-align: center; margin-top: 5px;">
    <label class='p-cbox-label'>
        <a href="/site/reset"><span>Reset password</span></a>
    </label>
</div>
<?php echo CHtml::submitButton('Sign in', array('class' => 'p-submit p-sign-submit')); ?>

<?php $this->endWidget(); ?>