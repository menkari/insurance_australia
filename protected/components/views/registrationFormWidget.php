<?php $form=$this->beginWidget('CActiveForm', array(
    'id'=>'users-registration-form',
    'enableAjaxValidation' => true,
    'enableClientValidation' => true,
    'clientOptions'=>array(
        'validateOnSubmit'=>true,
        'afterValidate' =>  'js:function(form, data, hasError) {if (!hasError) {location.reload("/");} else {} return false;}'
    ),
    'htmlOptions'=>array(
        'class'=>'p-form-sign',
    ),
    'action' => '/site/registration'
)); ?>

<?php echo $form->textField($model, 'username', array('class'=>'p-inputs', 'placeholder'=>'Username','size'=>32,'maxlength'=>32)); ?>
<?php echo $form->error($model, 'username', array('style'=>'color: red;')); ?>

<?php echo $form->textField($model, 'email', array('class'=>'p-inputs reg-email', 'placeholder'=>'Email', 'size'=>60,'maxlength'=>255)); ?>
<?php echo $form->error($model, 'email', array('style'=>'color: red;')); ?>

<?php echo $form->passwordField($model, 'password', array('class'=>'p-inputs reg-pass', 'placeholder'=>'Password', 'size'=>60,'maxlength'=>64)); ?>
<?php echo $form->error($model, 'password', array('style'=>'color: red;')); ?>

<?php echo $form->passwordField($model, 'confirm_password', array('class'=>'p-inputs reg-pass', 'placeholder'=>'Re-enter password', 'size'=>60,'maxlength'=>64)); ?>
<?php echo $form->error($model, 'confirm_password', array('style'=>'color: red;')); ?>

<!--select name="country" class='country'>
    <option value="Australia">Australia</option>
    <option value="USA">USA</option>
    <option value="German">German</option>
    <option value="Cannada">Cannada</option>
</select-->

<label class='p-cbox-label'>
    <input type="checkbox" class='receive'>
    <span>Yes, I want to receive news actions</span>
</label>

<?php echo CHtml::submitButton('Join', array('class' => 'p-submit p-join-submit')); ?>
<p class='by-pressing'>By pressing Join you agree with <a href="/pages/tos">Terms & Conditions</a></p>

<?php $this->endWidget(); ?>