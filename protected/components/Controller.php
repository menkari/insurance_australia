<?php
/**
 * Controller is the customized base controller class.
 * All controller classes for this application should extend from this base class.
 */
class Controller extends CController
{
	/**
	 * @var string the default layout for the controller view. Defaults to '//layouts/column1',
	 * meaning using a single column layout. See 'protected/views/layouts/column1.php'.
	 */
	public $layout='//layouts/column1';
	/**
	 * @var array context menu items. This property will be assigned to {@link CMenu::items}.
	 */
	public $menu=array();
	/**
	 * @var array the breadcrumbs of the current page. The value of this property will
	 * be assigned to {@link CBreadcrumbs::links}. Please refer to {@link CBreadcrumbs::links}
	 * for more details on how to specify this property.
	 */
	public $breadcrumbs=array();

    public function init() {
		
        parent::init();
        $serverName = str_replace('www.', '', Yii::app()->request->serverName);
        $template = Templates::model()->find('domain=:domain', array(':domain' => $serverName));
        $template_id = 0;
        if (!empty($template)) {
            $template_id = $template->user_id;
            Yii::app()->theme = $template->theme;
        }

		if($serverName !== 'insurance.iteora.com') { //mytripinsurance.com.au
			if(!isset(Yii::app()->request->cookies['referral']->value)) {
				$referral = Partner::model()->findByAttributes(array('domain_url' => $serverName));

				if(!empty($referral)) {
					$referral->visits++;
					$referral->save();
				} 

				$cookie = new CHttpCookie('referral',$referral->referral_code);
				Yii::app()->request->cookies['referral'] = $cookie;

			}
		}
		//unset(Yii::app()->request->cookies['referral']);
		//var_dump(Yii::app()->request->cookies['referral']->value); exit;
        Yii::app()->session->add("template_id", $template_id);
    }
}