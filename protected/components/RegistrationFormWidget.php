<?php

class RegistrationFormWidget extends CWidget {

    public function init() {

    }

    public function run() {
        $model = new Users;
        $this->render('registrationFormWidget', array('model'=>$model));
    }
}