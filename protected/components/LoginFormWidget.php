<?php

class LoginFormWidget extends CWidget {

    public function init() {

    }

    public function run() {
        $model = new Users;
        $this->render('loginFormWidget', array('model'=>$model));
    }
}