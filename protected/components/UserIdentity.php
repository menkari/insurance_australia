<?php

/**
 * UserIdentity represents the data needed to identity a user.
 * It contains the authentication method that checks if the provided
 * data can identity the user.
 */
class UserIdentity extends CUserIdentity
{
    /**
     * @var string email
     */
    public $email;

    /**
     * @var int id
     */
    protected $_id;

    const ERROR_EMAIL_INVALID=1;

    public function __construct($username, $password, $email)
    {
        parent::__construct($username, $password);
        $this->email=$email;
    }

	public function authenticate()
	{
        $user = Users::model()->find('LOWER(email)=?', array(strtolower($this->email)));
        if(($user===null) || (md5($this->password)!==$user->password)) {
            $this->errorCode = self::ERROR_USERNAME_INVALID;
        } else {
            $this->_id = $user->id;
            $this->username = $user->username;

            $this->errorCode = self::ERROR_NONE;
        }
        return !$this->errorCode;
	}

    public function getId(){
        return $this->_id;
    }
}