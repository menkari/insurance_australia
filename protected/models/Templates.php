<?php

/**
 * This is the model class for table "templates".
 *
 * The followings are the available columns in table 'templates':
 * @property integer $id
 * @property string $domain
 * @property string $theme
 * @property integer $user_id
 */
class Templates extends CActiveRecord
{
    public $image;
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'templates';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('domain', 'unique'),
			array('domain, theme', 'required'),
//			array('domain', 'url'),
			array('user_id', 'numerical', 'integerOnly'=>true),
			array('domain, theme', 'length', 'max'=>255),

            array('domain, theme', 'required', 'except'=> 'admin_domain'),

            // The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, domain, theme, user_id', 'safe', 'on'=>'search'),
            array('image', 'file', 'allowEmpty'=>true, 'types'=>'jpg,jpeg,gif,png'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'domain' => 'Domain',
			'theme' => 'Theme',
			'user_id' => 'User',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('domain',$this->domain,true);
		$criteria->compare('theme',$this->theme,true);
		$criteria->compare('user_id',$this->user_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Templates the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

    public function getThemesList() {
        $directories = glob(Yii::getPathOfAlias('webroot') . '/themes/*' , GLOB_ONLYDIR);

        $themes_list = array();
        foreach ($directories as $result) {
            $themes_list[basename($result)] = basename($result);
        }
        return $themes_list;
    }
}
