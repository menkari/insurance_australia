<?php

/**
 * This is the model class for table "users".
 *
 * The followings are the available columns in table 'users':
 * @property integer $id
 * @property string $username
 * @property string $email
 * @property string $password
 * @property string $confirm_password
 * @property string $token
 * @property string $role
 * @property integer $status
 */
class BaseUsers extends CActiveRecord
{
    const ROLE_ADMINISTRATOR = 'administrator';
    const ROLE_MANAGER = 'manager';
    const ROLE_AGENT = 'agent';
    const ROLE_PARTNER = 'partner';
    const ROLE_CUSTOMER = 'customer';

    private $_identity;
    public $confirm_password;

    public $temp_password;

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'users';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('email, password', 'required', 'on' => 'login'),
			array('email, password, confirm_password', 'required', 'on' => 'registration'),
			array('username, email, password, confirm_password, role, status', 'required', 'on' => 'admin_user_create'),
			array('username, email, role, status', 'required', 'on' => 'admin_user_update'),
			array('status', 'numerical', 'integerOnly'=>true),
			array('username, role', 'length', 'max'=>32),
            array('email, token, name, surname, birth, phone, passport, expiry, country, city, zip_code, street, subscribe_country', 'length', 'max'=>255),

//            array('username', 'unique', 'message' => 'Username already exist', 'on' => 'registration,admin_user_create'),
            array('password', 'length', 'max' => 32, 'min' => 8, 'tooShort' => 'Password is too short (minimum 8 chars)','tooLong' => 'Password is too long (maximum 32 chars)', 'on' => 'registration'),
            array('email', 'email', 'message' => 'Email not valid'),
            array('email', 'unique', 'message' => 'Email already exist', 'on' => 'registration'),
            array('confirm_password', 'compare', 'compareAttribute' => 'password', 'allowEmpty' => FALSE, 'message' => 'Passwords do not match', 'on' => 'registration'),

            array('password, confirm_password', 'changePassword', 'on' => 'profile_user_update, admin_user_update'),

            array('email, password', 'authenticate', 'on' => 'login'),
            array('email', 'exist', 'on' => 'reset-password'),
            array('email', 'required', 'on' => 'reset-password'),
            // The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
            array('id, username, email, password, token, role, status, name, surname, birth, phone, passport, expiry, country, city, zip_code, street, subscribe, subscribe_country', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'username' => 'Username',
			'email' => 'Email',
			'password' => 'Password',
			'confirm_password' => 'Confirm Password',
			'token' => 'Token',
			'role' => 'Role',
			'status' => 'Status',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('username',$this->username,true);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('password',$this->password,true);
		$criteria->compare('confirm_password',$this->confirm_password,true);
		$criteria->compare('role',$this->role,true);
		$criteria->compare('status',$this->status);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Users the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}


    public function authenticate()
    {
        if(!$this->hasErrors())
        {
            $this->_identity=new UserIdentity('', $this->password, $this->email);
            if(!$this->_identity->authenticate())
                $this->addError('password','Incorrect email or password.');
        }
    }


    public function changePassword()
    {
        if(!$this->hasErrors())
        {
            if (!empty($this->password) && (strlen($this->password) < 8 || strlen($this->password) > 32)) {
                $this->addError('password','The password must be 8-32 characters.');
            }
            if ($this->password != $this->confirm_password) {

                $this->addError('confirm_password','Passwords do not match.');
            }
        }
    }


    public function login()
    {
        if($this->_identity===null)
        {
            $this->_identity=new UserIdentity('', $this->password, $this->email);
            $this->_identity->authenticate();
        }
        if($this->_identity->errorCode===UserIdentity::ERROR_NONE)
        {
            $duration=0;
            Yii::app()->user->login($this->_identity,$duration);
            return true;
        }
        else
            return false;
    }


    public function beforeSave() {

        if ($this->getIsNewRecord() && $this->getScenario() != 'admin_user_create') {
            $this->token = md5('iteora' . time());
            $this->role = $this::ROLE_CUSTOMER;
        }

        if($this->getIsNewRecord()) {
            $this->password = $this->confirm_password = md5($this->password);
        }

        return parent::beforeSave();

    }

    public function afterSave() {
        //if ($this->getIsNewRecord() && $this->getScenario() != 'admin_user_create') {
        if ($this->getIsNewRecord() && $this->getScenario() == 'registration') {
            $this->sendMail();
        } elseif($this->getIsNewRecord() && $this->getScenario() == 'registration_by_order') {
            $html = 'Thank you for registration. Your password: ' . $this->temp_password;
            $this->sendMail($html);
        } elseif($this->getScenario() == 'reset-password') {
            $html = 'Your password: ' . $this->temp_password;
            $this->sendMail($html);
        }
        return parent::afterSave();
    }

    protected function sendMail($html = false) {
        if (!$html) {
            $html = $link = CHtml::link('Confirm', Yii::app()->createAbsoluteUrl('site/confirm', array('token' => $this->token)));
        }
        BaseUsers::send($this->email, '', $html, $this->username);
    }


    public static function send($address, $text = '', $html = '', $name = '')
    {
        Yii::import('application.extensions.phpmailer.JPhpMailer');
        $mail = new JPhpMailer;
        $mail->IsSMTP();
        $mail->Host = 'smtp.office365.com';
        $mail->Port = 587;
        $mail->SMTPSecure = 'tls';
        $mail->SMTPAuth = true;
        $mail->Username = 'info@mytripinsurance.com.au';
        $mail->Password = 'Fred1891!';
        $mail->SetFrom('info@mytripinsurance.com.au', 'Mytripinsurance');
        $mail->Subject = 'Mytripinsurance registration';
        //$mail->AltBody = 'To view the message, please use an HTML compatible email viewer!';
        if ($text)
            $mail->Body = $text;
        if ($html)
            $mail->MsgHTML($html);
        $mail->AddAddress($address, $name);
        if ($mail->Send()) {

        } else {

        }
    }

    protected function sendMail2($pass = '') {
        $link = 'Thank you for registration. Your password: ' . $pass;

        Yii::import('application.extensions.phpmailer.JPhpMailer');
        $mail = new JPhpMailer;
        $mail->IsSMTP();
        $mail->Host = 'smtp.gmail.com';
        $mail->Port = 587;
        $mail->SMTPSecure = 'tls';
        $mail->SMTPAuth = true;
        $mail->Username = 'iteorasupp@gmail.com';
        $mail->Password = 'maestro123';
        $mail->SetFrom('info@mytravelinsurance.com.au', 'MyTravelInsurance');
        $mail->Subject = 'Iteora registration';
        //$mail->AltBody = 'To view the message, please use an HTML compatible email viewer!';
        $mail->MsgHTML($link);
        $mail->AddAddress($this->email, $this->username);
        if ($mail->Send()) {

        } else {

        }
    }

    public function getLastOrder($userId){
        $criteria = new CDbCriteria;
        $criteria->order = 'date DESC';
        $criteria->limit = 1;
        $last = Orders::model()->findAllByAttributes(array('user_id'=>$userId),$criteria);
        return $last[0]->date;
    }
}
