<?php

/**
 * This is the model class for table "order_details".
 *
 * The followings are the available columns in table 'order_details':
 * @property integer $id
 * @property string $name
 * @property string $last_name
 * @property string $birth_date
 * @property string $email
 * @property string $phone
 * @property string $health_disorder
 * @property string $address1
 * @property string $address2
 * @property string $region
 * @property string $state
 * @property string $post_code
 * @property string $country
 * @property string $second_name
 * @property string $second_last_name
 * @property string $second_birth_date
 * @property string $kids_info
 * @property integer $save_for_future
 */
class OrderDetails extends CActiveRecord
{
    public $email_confirm;

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'order_details';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name, last_name, birth_date, email, email_confirm, phone, address1, region, state, post_code, country', 'required', 'message'=>'Error: {attribute} cannot be blank.'),
			array('second_name, second_last_name, second_birth_date', 'required', 'on' => 'order-details-form-second', 'message'=>'Error: {attribute} cannot be blank.'),

            array('email, email_confirm', 'email', 'message' => 'Error: Email address is not valid.'),
            array('email_confirm', 'compare', 'compareAttribute' => 'email', 'allowEmpty' => FALSE, 'message' => 'Error: Email addresses do not match.'),
            array('phone', 'numerical', 'integerOnly'=>true),
            array('phone', 'length', 'is' => 12, 'message' => 'Invalid phone number'),

            array('post_code, save_for_future', 'numerical', 'integerOnly'=>true, 'message' => 'Error: {attribute} must be an number.'),
			array('name, last_name, email, email_confirm, phone, health_disorder, address1, address2, region, state, post_code, country, second_name, second_last_name', 'length', 'max'=>255),
            array('post_code', 'length', 'is' => 4, 'message' => 'Error: {attribute} should be {length} digits.'),

			array('birth_date, second_birth_date', 'date', 'format' => 'Y-m-d'),
			array('kids_info', 'safe'),
            // these are not validated client-side and require ajax validation
            //array('birth_date', 'date', 'on' => 'order-details-form'),
            //array('birth_date', 'checkBirthDate', 'on' => 'order-details-form-second'),
            //array('second_birth_date', 'date', 'on' => 'order-details-form-second'),
            //array('kids_info', 'checkKidsInfo', 'on' => 'details-form-second'),
            //array('kids_info', 'checkKidsInfo', 'on' => 'order-details-form-second'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, name, last_name, birth_date, email, email_confirm, phone, health_disorder, address1, address2, region, state, post_code, country, second_name, second_last_name, second_birth_date, kids_info, save_for_future', 'safe', 'on'=>'search'),
		);
	}
    public function checkBirthDate($attr, $params) {
        $birthDate = $this->birth_date;
        if (!empty($birthDate)) {
            $birthDate = explode("/", $birthDate);
            $age_ = (date("md", date("U", mktime(0, 0, 0, $birthDate[0], $birthDate[1], $birthDate[2]))) > date("md")
                ? ((date("Y") - $birthDate[2]) - 1)
                : (date("Y") - $birthDate[2]));


            $age = Yii::app()->session->get("age");
            if ($age_ != $age) {
                $this->addError($attr, 'Error: Date of birth doesn\'t match quoted age'.$attr);
            }
        }

        //}
    }

    public function checkSecondBirthDate($attr, $param) {
        $birthDate = $this->second_birth_date;
        if (!empty($birthDate)) {
            $birthDate = explode("/", $birthDate);
            $age_ = (date("md", date("U", mktime(0, 0, 0, $birthDate[0], $birthDate[1], $birthDate[2]))) > date("md")
                ? ((date("Y") - $birthDate[2]) - 1)
                : (date("Y") - $birthDate[2]));


            $age = Yii::app()->session->get("age2");
            if ($age && $age_ != $age) {
                $this->addError($attr, 'Error: Date of birth doesn\'t match quoted age');
            }
        }
    }

    public function checkKidsInfo($attr, $param) {
        $kidsInfo = $this->kids_info;
        if (isset($kidsInfo) && !empty($kidsInfo)) {
            foreach($kidsInfo as $k => $kid) {
                if (empty($kid['name'])) {
                    $this->addError($attr."[$k][name]", 'Error: Child\'s first name cannot be blank.');
                }
                if (empty($kid['last_name'])) {
                    $this->addError($attr."[$k][last_name]", 'Error: Child\'s last name cannot be blank.');
                }
                if (empty($kid['birth_date'])) {
                    $this->addError($attr."[$k][birth_date]", 'Error: Child\'s date of birth cannot be blank.');
                }
            }
        }
    }


	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'details' => array(self::HAS_ONE, 'Orders', 'detail_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Given Name/First Name',
			'last_name' => 'Surname/Last Name',
			'birth_date' => 'Date of Birth',
			'email' => 'Email Address',
			'phone' => 'Phone Number',
			'health_disorder' => 'Pre-Existing Medical Condition Item Code (if any)',
			'address1' => 'Address Line 1',
			'address2' => 'Address Line 2',
			'region' => 'Suburb',
			'state' => 'State',
			'post_code' => 'Post Code',
			'country' => 'Country',
			'second_name' => 'Given Name/First Name',
			'second_last_name' => 'Surname/Last Name',
			'second_birth_date' => 'Date of Birth',
            'kids_info' => 'Children\'s Details',
			'save_for_future' => 'Save For Next Time',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('last_name',$this->last_name,true);
		$criteria->compare('birth_date',$this->birth_date,true);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('phone',$this->phone,true);
		$criteria->compare('health_disorder',$this->health_disorder,true);
		$criteria->compare('address1',$this->address1,true);
		$criteria->compare('address2',$this->address2,true);
		$criteria->compare('region',$this->region,true);
		$criteria->compare('state',$this->state,true);
		$criteria->compare('post_code',$this->post_code,true);
		$criteria->compare('country',$this->country,true);
		$criteria->compare('second_name',$this->second_name,true);
		$criteria->compare('second_last_name',$this->second_last_name,true);
		$criteria->compare('second_birth_date',$this->second_birth_date,true);
        $criteria->compare('kids_info',$this->kids_info,true);
		$criteria->compare('save_for_future',$this->save_for_future);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return OrderDetails the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
