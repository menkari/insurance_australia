<?php

/**
 * This is the model class for table "settings".
 *
 * The followings are the available columns in table 'settings':
 * @property integer $id
 * @property string $api_data
 * @property string $admin_email
 * @property string $master_pwd
 * @property string $master_login
 * @property string $time_in_day
 * @property string $global_commission
 */
class Settings extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'settings';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('api_data, admin_email, master_pwd, master_login, time_in_day, global_commission', 'required'),
			array('api_data, admin_email, master_pwd, master_login, time_in_day, global_commission', 'length', 'max'=>255),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, api_data, admin_email, master_pwd, master_login, time_in_day, global_commission', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'api_data' => 'Api Data',
			'admin_email' => 'Admin Email',
			'master_pwd' => 'Master Pwd',
			'master_login' => 'Master Login',
			'time_in_day' => 'Time In Day',
			'global_commission' => 'Global Commission',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('api_data',$this->api_data,true);
		$criteria->compare('admin_email',$this->admin_email,true);
		$criteria->compare('master_pwd',$this->master_pwd,true);
		$criteria->compare('master_login',$this->master_login,true);
		$criteria->compare('time_in_day',$this->time_in_day,true);
		$criteria->compare('global_commission',$this->global_commission,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Settings the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
