<?php

/**
 * This is the model class for table "order_details".
 *
 * The followings are the available columns in table 'order_details':
 * @property integer $id
 * @property string $name
 * @property string $surname
 * @property string $birth_date
 * @property string $email
 * @property string $phone
 * @property string $passport
 * @property string $expiry
 * @property string $company
 * @property string $country
 * @property string $city
 * @property string $zip_code
 * @property string $street
 * @property string $medical
 * @property string $medical_explain
 * @property integer $subscribe
 * @property string $interests
 */
class OrderDetails2 extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'order_details';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			//array('name, surname, birth_date, email, phone, passport, expiry, company, country, city, zip_code, street, medical, medical_explain, subscribe, interests', 'required'),
			array('subscribe', 'numerical', 'integerOnly'=>true),
			array('name, surname, email, phone, passport, company, country, city, zip_code, street, medical, medical_explain, interests', 'length', 'max'=>255),

            array('name, surname, birth_date, email, phone, passport, expiry, country, city, zip_code, street, interests', 'required', 'on'=>'step_details'),

			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, name, surname, birth_date, email, phone, passport, expiry, company, country, city, zip_code, street, medical, medical_explain, subscribe, interests', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
            'order' => array(self::BELONGS_TO, 'orders', 'detail_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Name',
			'surname' => 'Surname',
			'birth_date' => 'Birth Date',
			'email' => 'Email',
			'phone' => 'Phone',
			'passport' => 'Passport',
			'expiry' => 'Expiry',
			'company' => 'Company',
			'country' => 'Country',
			'city' => 'City',
			'zip_code' => 'Zip Code',
			'street' => 'Street',
			'medical' => 'Medical',
			'medical_explain' => 'Medical Explain',
			'subscribe' => 'Subscribe',
			'interests' => 'Interests',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('surname',$this->surname,true);
		$criteria->compare('birth_date',$this->birth_date,true);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('phone',$this->phone,true);
		$criteria->compare('passport',$this->passport,true);
		$criteria->compare('expiry',$this->expiry,true);
		$criteria->compare('company',$this->company,true);
		$criteria->compare('country',$this->country,true);
		$criteria->compare('city',$this->city,true);
		$criteria->compare('zip_code',$this->zip_code,true);
		$criteria->compare('street',$this->street,true);
		$criteria->compare('medical',$this->medical,true);
		$criteria->compare('medical_explain',$this->medical_explain,true);
		$criteria->compare('subscribe',$this->subscribe);
		$criteria->compare('interests',$this->interests,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

    protected function afterSave(){
        parent::afterSave();

    }

    protected function beforeSave(){
        parent::beforeSave();
//var_dump($this->birth_date);
        $this->birth_date = date('Y-m-d', strtotime(str_replace('/', '-', $this->birth_date)));
        $this->expiry= date('Y-m-d', strtotime(str_replace('/', '-', $this->expiry)));
        return true;
    }

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return OrderDetails the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
