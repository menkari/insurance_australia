<?php

/**
 * This is the model class for table "orders".
 *
 * The followings are the available columns in table 'orders':
 * @property integer $id
 * @property integer $user_id
 * @property integer $partner_id
 * @property string $date
 * @property string $countries
 * @property string $start_traveling
 * @property string $end_traveling
 * @property string $travellers
 * @property integer $age
 * @property integer $age2
 * @property string $kids
 * @property string $quote
 * @property integer $detail_id
 * @property integer $payment_id
 * @property string $price
 * @property string $pdf
 * @property string $status
 * @property string $status_message
 * @property string $referral
 * @property string $unknown_referral
 */
class Orders extends CActiveRecord
{
    public $minprice;
    public $maxprice;
	public $detailsEmail;
	public $detailsName;
	public $detailsAddress;
	public $date_from;
	public $date_to;
	public $searchByName;
	public $partnerName;
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'orders';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
            array('age', 'required', 'message'=>'Error: {attribute} cannot be blank.'),
            array('user_id, partner_id, date, countries, start_traveling, end_traveling, travellers, age, quote, detail_id, price', 'required', 'message'=>'Error: {attribute} cannot be blank.'),

            array('countries, start_traveling, end_traveling, age', 'required', 'on' => 'orders-base-info', 'message'=>'Error: {attribute} cannot be blank.'),
            array('quote, detail_id, price', 'required', 'on' => 'orders-quote', 'message'=>'Error: {attribute} cannot be blank.'),
            array('countries, start_traveling, end_traveling, age, quote, price', 'required', 'on' => 'orders-create', 'message'=>'Error: {attribute} cannot be blank.'),

            //array('status, status_message', 'on' => 'set-status'),

            array('user_id, age, age2, detail_id, payment_id', 'numerical', 'integerOnly'=>true),
			array('countries, travellers, kids, quote, status, status_message', 'length', 'max'=>255),
			array('price', 'length', 'max'=>11),
			array('referral, unknown_referral', 'length', 'max'=>255),

            array('age, age2', 'checkAge'),

			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, partnerName, searchByName, detailsEmail, detailsName, detailsAddress, date_from, date_to, user_id, partner_id, date, countries, start_traveling, end_traveling, travellers, age, age2, kids, quote, detail_id, payment_id, price, pdf, status, status_message', 'safe', 'on'=>'search'),
		);
	}

    public function checkAge($attr, $params) {
        $age = $this->age;
        $age2 = $this->age2;
        if (!empty($age)) {
            if ($age < 18 || $age > 89) {
                $this->addError($attr, "Error: Adults between 18 and 89 only");
            } elseif (!empty($age2) && ($age2 < 18 || $age2 > 89)) {
                $this->addError($attr, "Error: Adults between 18 and 89 only");
            }
        }
    }

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
            'details' => array(self::HAS_ONE, 'orderDetails', 'id'),
            'payments' => array(self::HAS_ONE, 'orderPayments', 'id'),
			'partner' => array(self::HAS_ONE, 'Partner', array('id' => 'partner_id')),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'user_id' => 'User',
            'partner_id' => 'Partner',
			'date' => 'Date',
			'countries' => 'Countries',
			'start_traveling' => 'Start Traveling',
			'end_traveling' => 'End Traveling',
			'travellers' => 'Travellers',
			'age' => 'First adult age',
            'age2' => 'Second adult age',
            'kids' => 'Children',
			'quote' => 'Quote',
			'detail_id' => 'Detail',
			'payment_id' => 'Payment',
			'price' => 'Price',
            'pdf' => 'Pdf',
            'status' => 'Status',
            'status_message' => 'Status Message',
            'referral' => 'Referral',
			'unknown_referral' => 'Unknown Referral',
			'detailsEmail' => 'Email',
			'detailsName' => 'Name',
			'detailsAddress' => 'Address',
			'date_from' => 'Date from',
			'date_to' => 'Date to',
			'searchByName' => 'Search by name',
			'partnerName' => 'Partner',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;
		$criteria->with = array('details', 'partner');
		
		if($this->date_from && $this->date_to) {
			$criteria->addBetweenCondition('date', $this->date_from, $this->date_to, 'AND');
		}

		if($this->searchByName) {
			$criteria->addSearchCondition('details.name', $this->searchByName, true, 'OR');
			$criteria->addSearchCondition('details.last_name', $this->searchByName, true, 'OR');
			$criteria->addSearchCondition('details.second_name', $this->searchByName, true, 'OR');
			$criteria->addSearchCondition('details.second_last_name', $this->searchByName, true, 'OR');
		}

		$criteria->compare('details.email', $this->detailsEmail, true);
		$criteria->compare('details.name', $this->detailsName, true);
		$criteria->compare('details.address', $this->detailsAddress, true);
		$criteria->compare('partner.name', $this->partnerName, true);
		
		$criteria->compare('id',$this->id);
		$criteria->compare('user_id',$this->user_id);
        $criteria->compare('partner_id',$this->partner_id);
		$criteria->compare('date',$this->date,true);
		$criteria->compare('countries',$this->countries,true);
		$criteria->compare('start_traveling',$this->start_traveling,true);
		$criteria->compare('end_traveling',$this->end_traveling,true);
		$criteria->compare('travellers',$this->travellers,true);
		$criteria->compare('age',$this->age);
        $criteria->compare('age2',$this->age2);
        $criteria->compare('kids',$this->kids,true);
		$criteria->compare('quote',$this->quote,true);
		$criteria->compare('detail_id',$this->detail_id);
		$criteria->compare('payment_id',$this->payment_id);
		$criteria->compare('price',$this->price,true);
        $criteria->compare('pdf',$this->pdf,true);
        $criteria->compare('status',$this->status,true);
        $criteria->compare('status_message',$this->status_message,true);
        $criteria->compare('referral',$this->referral,true);
		$criteria->compare('unknown_referral', $this->unknown_referral, true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Orders the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public static function countPartnerOrders($id) {

		return count(static::model()->findAllByAttributes(array('partner_id' => $id)));
	}

	public static function getPartnerSalesAmount($id) {

		$orders = static::model()->findAllByAttributes(array('partner_id' => $id));
		$price = 0;
		if(!empty($orders)) {
			foreach ($orders as $one) {
				$price = $price + $one->price;
			}
		}
		return $price;
	}

	public static function getPartnerConversion($id, $visits) {
		$orders = static::countPartnerOrders($id);

		$visits = ($visits == 0) ? 1 : $visits;

		return round(($orders / $visits * 100), 2, PHP_ROUND_HALF_EVEN);
	}

	public static function getTouristName($data) {
		return "$data->name $data->last_name";
	}

	public static function getTouristAddress($data) {
		return "$data->country, $data->region, $data->state, $data->post_code, $data->address1";
	}

}
