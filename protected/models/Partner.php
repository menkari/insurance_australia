<?php

/**
 * This is the model class for table "partner".
 *
 * The followings are the available columns in table 'partner':
 * @property integer $id
 * @property integer $user_id
 * @property string $name
 * @property string $domain_url
 * @property string $subdomain_prefix
 * @property string $referral_code
 * @property string $commission_amount
 * @property string $reg_exp
 * @property string $visits
 */
class Partner extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'partner';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('user_id', 'numerical', 'integerOnly'=>true),
			array('name, domain_url, subdomain_prefix, referral_code, reg_exp', 'length', 'max'=>250),
			array('commission_amount', 'length', 'max'=>9),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, visits, user_id, name, domain_url, subdomain_prefix, referral_code, commission_amount, reg_exp', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'orders' => array(self::HAS_MANY, 'Orders', 'partner_id'),
			'user' => array(self::HAS_ONE, 'Users', 'user_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'user_id' => 'User',
			'name' => 'Name',
			'domain_url' => 'Domain Url',
			'subdomain_prefix' => 'Subdomain Prefix',
			'referral_code' => 'Referral Code',
			'commission_amount' => 'Commission Amount',
			'reg_exp' => 'Reg Exp',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('user_id',$this->user_id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('domain_url',$this->domain_url,true);
		$criteria->compare('subdomain_prefix',$this->subdomain_prefix,true);
		$criteria->compare('referral_code',$this->referral_code,true);
		$criteria->compare('commission_amount',$this->commission_amount,true);
		$criteria->compare('reg_exp',$this->reg_exp,true);
		$criteria->compare('visits',$this->visits);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Partner the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public static function findReferralCode($str) {
		$search = array('http://', 'https://', 'www.');
		$str = str_replace($search, '', $str);
		$code = Yii::app()->db->createCommand()
			->select('referral_code')
			->from('partner')
			->where('domain_url=:str or subdomain_prefix=:str or referral_code=:str', array(':str'=>$str))
			->queryRow();
		if(!empty($code)) {
			
			return $code;
		}

		return $str;
	}

	public static function getCommission($data) {
		$model = new Partner($data->partner_id);

		return $model->commission_amount ? $model->commission_amount : 0;
	}

	public static function getPartnerName($data) {
		$model = static::model()->findByPk($data->partner_id);

		if(empty($model)) {
			return 'admin';
		}
		return $model->name;
	}

	public static function getPartnerVisits($data) {
		$model = static::model()->findByAttributes(array('user_id' => $data->id));

		return isset($model->visits) ? $model->visits : 0;
	}
/*
	public function afterSave() {

		parent::afterSave();

		if($this->isNewRecord) {
			Yii::app()->db->createCommand()
				->insert('visits', array(
					'partner_id'=> $this->user_id,
					'counter'=> 0,))
				->execute();
		}


	}
*/
}