<?php
	class TravelRequests
	{
		private $GetTravelPolicyData = array
		(
			'SPName' => 'My Trip Insurance',
			'CustLoginId' => 'mytrip.svc',
			'EncryptionTypeCd' => 'None',
//			'Pswd' => $_SERVER['SERVER_NAME'] == 'staging.mytripinsurance.com.au' ? '$craMBLeD@123!' : 'M3q[OkuR:',
			'Pswd' => '$craMBLeD@123!',
			'ClientDt' => '2013-12-05T12:50:25.2888211-08:00',		//	If value equals 'auto' created curret datetime value
			'CustLangPref' => 'en',
			'Org' => 'My Trip Insurance',
			'Name' => 'My Trip Insurance',
			'Version' => 1,
			'RqUID' => 'f6f71591-7ece-4d18-8426-0f9332cd7df0',
			'TransactionRequestDt' => 'auto',
			'Users' => array
			(
				array
				(
					'Surname' => 'I am',
					'GivenName' => 'TEST',
//					'TitlePrefix' => 'Mr',
					'BirthDt' => '1983-03-01',
					'IdNumberTypeCd' => 'IDNumber',
					'IdNumber' => 'XE362609A',
					'Addresses' => array
					(
						'Addr1' => 'Addr1',
						'Addr2' => 'Addr2',
						'City' => 'City',
						'StateProv' => 'StateProv',
						'PostalCode' => '11111',
						'Country' => 'Australia'
					),
					'Communications' => array
					(
						'Id' => '1',
						'NameInfoRef' => '1',
						'Phones' => array
						(
							array
							(
								'PhoneTypeCd' => 'Telephone',
								'PhoneNumber' => '2222'
							),
							array
							(
								'PhoneTypeCd' => 'Cell',
								'PhoneNumber' => '1234567890'
							)
						),
						'EmailInfo' => array
						(
							'annie.tran@chubb.com'
						)
					)
				),
				array
				(
					'Surname' => 'Cheong',
					'GivenName' => 'Aven',
//					'TitlePrefix' => 'Mr',
					'BirthDt' => '1983-03-01',
					'IdNumberTypeCd' => 'IDNumber',
					'IdNumber' => '8709DS12M'
				),
				array
				(
					'Surname' => 'Billy',
					'GivenName' => 'TEST',
//					'TitlePrefix' => 'Mr',
					'BirthDt' => '1994-05-27',
					'IdNumberTypeCd' => 'IDNumber',
					'IdNumber' => 'XE362609A'
				)
			),
			'Policy' => array
			(
				'CompanyProductCd' => '06E346A0-2D96-42AF-B49E-A62F00719700',
				'EffectiveDt' => '2016-11-30',
				'ExpirationDt' => '2016-12-03',
				'Destination' => array
				(
					'RqUID' => '1bef82fb-6936-4b81-9d30-a62f0073127c',
					'DestinationDesc' => 'AU CTI South America'
				),
				'InsuredPackage' => array
				(
					'RqUID' => '851CFFF2-ED6E-4835-A7FD-9D0D0019C474',
					'InsuredPackageDesc' => 'Individual'
				),
				'Plan' => array
				(
					'RqUID' => '70202bee-d871-434f-8827-a62f007290f1',
					'PlanDesc' => 'Prestige'
				)
			),
			'Extensions' => array
			(
				array
				(
					'Type' => 'System.String',
					'Key' => 'Source_System',
					'Value' => 'Mytripinsurance'
				),
                array
                (
                    'Type' => 'System.String',
                    'Key' => 'ExcessLevel',
                    'Value' => '100'
                ),
				array
				(
					'Type' => 'System.String',
					'Key' => 'TripCost',
					'Value' => '1000'
				),
				array
				(
					'Type' => 'System.String',
					'Key' => 'TripType',
					'Value' => 'Single'
				),
				array
				(
					'Type' => 'Boolean',
					'Key' => 'IsPreExistMedical',
					'Value' => 'false'
				),
				// Extra packages
				array
				(
					'Type' => 'Boolean',
					'Key' => 'IncludeCruise',
					'Value' => 'false'
				),
				array
				(
					'Type' => 'Boolean',
					'Key' => 'IncludeWinterSports',
					'Value' => 'false'
				),
				array
				(
					'Type' => 'Boolean',
					'Key' => 'IncludeGolf',
					'Value' => 'false'
				),
				array
				(
					'Type' => 'Boolean',
					'Key' => 'IncludeExtremeActivities',
					'Value' => 'false'
				),
				array
				(
					'Type' => 'Boolean',
					'Key' => 'IncludeFamily',
					'Value' => 'false'
				),
				array
				(
					'Type' => 'Boolean',
					'Key' => 'IncludeDriving',
					'Value' => 'false'
				),
				array
				(
					'Type' => 'Boolean',
					'Key' => 'IncludeDevice',
					'Value' => 'false'
				),

			),
			'PaymentProcess' => array
			(
				'PaymentRequest' => array
				(
					'PaymentMode' => 'CreditCard',
					'CreditCardDetails' => array
					(
						'NameOnCard' => 'John Smith',
						'CardType' => 'Master',
						'CardNumber' => '5422882800700007',
						'EncryptionType' => 'None',
						'SecurityCode' => '123',
						'ExpiryMonth' => 7,
						'ExpiryYear' => 15
					),
					'TransactionDate' => 'auto',
					'Currency' => 'AUD',
					'Amount' => 175.00
				),
				'PaymentResponse' => array
				(
					'TransactionDate' => 'auto'
				)
			)
		);
		
		public $GetTravelQuoteData = array
		(
			array
			(
				'Type' => 'System.String',
				'Key' => 'Source_System',
				'Value' => 'Mytripinsurance'
			),
            array
            (
                'Type' => 'System.String',
                'Key' => 'ExcessLevel',
                'Value' => '100'
            ),
			array
			(
				'Type' => 'System.String',
				'Key' => 'AdultAge1',
				'Value' => '33'
			),
			array
			(
				'Type' => 'System.String',
				'Key' => 'AdultAge2',
				'Value' => '22'
			),
			array
			(
				'Type' => 'System.String',
				'Key' => 'TripCost',
				'Value' => '1000'
			),
			array
			(
				'Type' => 'System.String',
				'Key' => 'NoOfChildren',
				'Value' => '0'
			),
			array
			(
				'Type' => 'System.String',
				'Key' => 'TripType',
				'Value' => 'Single'
			),
			array
			(
				'Type' => 'System.String',
				'Key' => 'QuoteCode',
				'Value' => 'B2C.SINGLE'
			),
			// Extra packages
			array
			(
				'Type' => 'Boolean',
				'Key' => 'IncludeCruise',
				'Value' => 'true'
			),
			array
			(
				'Type' => 'Boolean',
				'Key' => 'IncludeWinterSports',
				'Value' => 'true'
			),
			array
			(
				'Type' => 'Boolean',
				'Key' => 'IncludeGolf',
				'Value' => 'true'
			),
			array
			(
				'Type' => 'Boolean',
				'Key' => 'IncludeExtremeActivities',
				'Value' => 'true'
			),
			array
			(
				'Type' => 'Boolean',
				'Key' => 'IncludeFamily',
				'Value' => 'true'
			),
			array
			(
				'Type' => 'Boolean',
				'Key' => 'IncludeDriving',
				'Value' => 'true'
			),
			array
			(
				'Type' => 'Boolean',
				'Key' => 'IncludeDevice',
				'Value' => 'true'
			),

		);
		
		private function SendByCurl($SoapVersion = '1.1', $SoapAction, $Contents)
		{
			$options = array
			(
				CURLOPT_URL => $_SERVER['SERVER_NAME'] == 'staging.mytripinsurance.com.au' ? 'http://btsws.atuat.acegroup.com/CRS_ACORD_WS/ACORDService.asmx' : 'https://receive.travel.acegroup.com/CRS_ACORD_WS/ACORDService.asmx',
				CURLOPT_POST => TRUE,
				CURLOPT_FAILONERROR => TRUE,
				CURLOPT_RETURNTRANSFER => TRUE,
				CURLOPT_FOLLOWLOCATION => TRUE,
				CURLOPT_VERBOSE => FALSE,
				CURLOPT_HEADER => TRUE,
				CURLOPT_HTTPHEADER => array
				(
					'POST /CRS_ACORD_WS/ACORDService.asmx HTTP/1.1',
					'Method: Post',
					'Connection: Close',
					'SOAPVersion: '.$SoapVersion,
					$_SERVER['SERVER_NAME'] == 'staging.mytripinsurance.com.au' ? 'Host: btsws.atuat.acegroup.com' : 'Host: receive.travel.acegroup.com',
					'Content-Type: text/xml; charset=utf-8',
					'Content-Length: '.strlen($Contents).'',
					'SOAPAction: "'.$SoapAction.'"'
				),
				CURLOPT_POSTFIELDS => ($Contents)
			);

			$curl = curl_init();
			
			if (!$curl) {return false;}
			
			if (!(curl_setopt_array($curl, $options))) return false;
			
			$response = curl_exec($curl);
			
			if (!$response) {return false;}
			
			if (intval(curl_getinfo($curl, CURLINFO_HTTP_CODE)) >= (int) 400 ||
				(stripos(strtolower(curl_getinfo($curl, CURLINFO_CONTENT_TYPE)), 'application/soap+xml') === false && stripos(strtolower(curl_getinfo($curl, CURLINFO_CONTENT_TYPE)), 'text/xml') === false))
			{
			    $debug = $Contents . "\n\n" . $response;
			    BaseUsers::send('jayen@skedgo.com', $debug);
				return false;
			}
			
			$response = substr($response, strlen($response) - curl_getinfo($curl, CURLINFO_CONTENT_LENGTH_DOWNLOAD), curl_getinfo($curl, CURLINFO_CONTENT_LENGTH_DOWNLOAD));

			curl_close($curl);
			
			return $response;
		}
		
		private function MakeDateTime($CurrentValue, $Time = true, $ISO8601 = false)
		{
			if (strtolower($CurrentValue) != 'auto') return $CurrentValue;
			
			$date = date("Y-m-t");
			
			if ($Time == true)
			{
				$date = $date." ".date("H:i:s");
				
				if ($ISO8601 == true)
				{
					$isof = new DateTime($date);
					$date = $isof->format(DateTime::ISO8601);
					$date[strlen($date) - 0] = $date[strlen($date) - 2];
					$date[strlen($date) - 1] = $date[strlen($date) - 3];
					$date[strlen($date) - 3] = ':';
				}
			}
			
			return $date;
		}
		
		private function PrintUserNames($ItemFormat, $ItemsData)
		{
			$result = '';
            foreach ($ItemsData as $i => $itemsDatum) {
                $result = $result . sprintf($ItemFormat, $i + 1, $itemsDatum['Surname'], $itemsDatum['GivenName'],
//                        $itemsDatum['TitlePrefix'],
                        $itemsDatum['Child'] ? '<TitleRelationshipDesc>Child</TitleRelationshipDesc>' : '', $itemsDatum['BirthDt'],
                        $itemsDatum['IdNumberTypeCd'], $itemsDatum['IdNumber']);
            }

			return $result;
		}
		
		private function PrintUsersAddress($ItemFormat, $ItemsData)
		{
			$result = '';
			$index = 1;
			
			if (count($ItemsData) > 0)
			{
				for ($i = 0; $i < count($ItemsData); $i ++)
				{
					if (isset($ItemsData[$i]['Addresses']))
					{
						$result = $result.sprintf($ItemFormat, $i + 1, $index, $ItemsData[$i]['Addresses']['Addr1'], $ItemsData[$i]['Addresses']['Addr2'], $ItemsData[$i]['Addresses']['City'],
												  $ItemsData[$i]['Addresses']['StateProv'], $ItemsData[$i]['Addresses']['PostalCode'], $ItemsData[$i]['Addresses']['Country']);
						
						$index ++;
					}
				}
			}
			
			return $result;
		}
		
		private function PrintUsersCommunications($ItemFormat, $PhoneFormat, $EmailFormat, $ItemsData)
		{
			$result = '';
			$index = 1;
			
			if (count($ItemsData) > 0)
			{
				for ($i = 0; $i < count($ItemsData); $i ++)
				{
					if (isset($ItemsData[$i]['Communications']))
					{
						$phones = '';
						
						if (isset($ItemsData[$i]['Communications']['Phones']))
						{
							for ($n = 0; $n < count($ItemsData[$i]['Communications']['Phones']); $n ++)
							{
								$phones = $phones.sprintf($PhoneFormat, $ItemsData[$i]['Communications']['Phones'][$n]['PhoneTypeCd'], $ItemsData[$i]['Communications']['Phones'][$n]['PhoneNumber']);
							}
						}
						
						$emails = '';
						
						if (isset($ItemsData[$i]['Communications']['EmailInfo']))
						{
							for ($n = 0; $n < count($ItemsData[$i]['Communications']['EmailInfo']); $n ++)
							{
								$phones = $phones.sprintf($EmailFormat, $ItemsData[$i]['Communications']['EmailInfo'][$n]);
							}
						}
						
						$result = $result.sprintf($ItemFormat, $i + 1, $index, $phones, $emails);
						
						$index ++;
					}
				}
			}
			
			return $result;
		}
		
		private function PrintPolicy($ItemFormat, $ItemsData)
		{
			$result = '';
			
			$result = sprintf($ItemFormat, $ItemsData['CompanyProductCd'], $ItemsData['EffectiveDt'], $ItemsData['ExpirationDt'], $ItemsData['Destination']['RqUID'], $ItemsData['Destination']['DestinationDesc'],
							  $ItemsData['InsuredPackage']['RqUID'], $ItemsData['InsuredPackage']['InsuredPackageDesc'], $ItemsData['Plan']['RqUID'], $ItemsData['Plan']['PlanDesc']);
			
			return $result;
		}
		
		private function PrintExtensions($ItemFormat, $ItemsData)
		{
			$result = '';
			
			if (count($ItemsData) > 0)
			{
				for ($i = 0; $i < count($ItemsData); $i ++) $result = $result.sprintf($ItemFormat, $ItemsData[$i]['Type'], $ItemsData[$i]['Key'], $ItemsData[$i]['Value']);
			}
			
			return $result;
		}
		
		private function PrintPaymentProcess($ItemFormat, $ItemsData)
		{
			$result = '';
			
			$result = sprintf($ItemFormat, $ItemsData['PaymentRequest']['PaymentMode'], $ItemsData['PaymentRequest']['CreditCardDetails']['NameOnCard'],
							  $ItemsData['PaymentRequest']['CreditCardDetails']['CardType'], $ItemsData['PaymentRequest']['CreditCardDetails']['CardNumber'],
							  $ItemsData['PaymentRequest']['CreditCardDetails']['EncryptionType'], $ItemsData['PaymentRequest']['CreditCardDetails']['SecurityCode'],
							  $ItemsData['PaymentRequest']['CreditCardDetails']['ExpiryMonth'], $ItemsData['PaymentRequest']['CreditCardDetails']['ExpiryYear'],
							  $this->MakeDateTime($ItemsData['PaymentRequest']['TransactionDate'], true, true), $ItemsData['PaymentRequest']['Currency'],
							  $ItemsData['PaymentRequest']['Amount'], $this->MakeDateTime($ItemsData['PaymentResponse']['TransactionDate'], true, true));
			
			return $result;
		}
		
		public function ConvertXmlToArray($XmlString)
		{
			if (strlen($XmlString) > 38)
			{
				$pos = stripos($XmlString, "<soap:Envelope", 0);
			
				if ($pos !== false)
				{
					$end = stripos($XmlString, ">", $pos);
					
					if ($end !== false)
					{
						$XmlString = substr_replace($XmlString, "", $pos, $end - $pos + 1);
					}
				}
				
				$XmlString = str_replace('</soap:Envelope>', '', $XmlString);
				$XmlString = str_replace("<soap:Body>", "", $XmlString);
				$XmlString = str_replace("</soap:Body>", "", $XmlString);
				$XmlString = str_replace("<soap:", "<", $XmlString);
				$XmlString = str_replace("</soap:", "</", $XmlString);
				$XmlString = str_replace(' xsi:nil="true"', "", $XmlString);
				
				$XmlString = simplexml_load_string($XmlString);
				$XmlString = json_encode($XmlString);
				$XmlString = json_decode($XmlString, TRUE);
				
				return $XmlString;
			}
			
			return false;
		}
		
		public function GetTravelQuoteArray()
		{
			$soap_action = 'http://ACE.Global.Travel.CRS.Schemas.ACORD.WS/ACORDService/GetTravelQuoteArray';
			
			$extension_format = ''."\n".'<DataItem type="%s" key="%s">'."\n".'<value>%s</value>'."\n".'</DataItem>'."\n";
			$policy_format = '<PersPolicy>'."\n".'<CompanyProductCd>%s</CompanyProductCd>'."\n".'<ContractTerm>'."\n".'<EffectiveDt>%s</EffectiveDt>'."\n".'<ExpirationDt>%s</ExpirationDt>'."\n".'</ContractTerm>'."\n".
							 '<com.acegroup_Destination>'."\n".'<RqUID>%s</RqUID>'."\n".'<DestinationDesc>%s</DestinationDesc>'."\n".'</com.acegroup_Destination>'."\n".'<com.acegroup_InsuredPackage>'."\n".'<RqUID>%s</RqUID>'."\n".
							 '<InsuredPackageDesc>%s</InsuredPackageDesc>'."\n".'</com.acegroup_InsuredPackage>'."\n".'<com.acegroup_Plan>'."\n".'<RqUID>%s</RqUID>'."\n".'<PlanDesc>%s</PlanDesc>'."\n".'</com.acegroup_Plan>'."\n".'</PersPolicy>'."\n";
						   
			$soap_format = '<?xml version="1.0" encoding="utf-8"?>'."\n".
						   '<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">'."\n".
						   '<soap:Body>'."\n".'<GetTravelQuoteArray xmlns="http://ACE.Global.Travel.CRS.Schemas.ACORD.WS/">'."\n".
						   '<ACORD xmlns="http://ACE.Global.Travel.CRS.Schemas.ACORD_QuoteReq">'."\n".
						   '<SignonRq>'."\n".'<SignonPswd>'."\n".'<CustId>'."\n".'<SPName>%s</SPName>'."\n".'<CustLoginId>%s</CustLoginId>'."\n".'</CustId>'."\n".'<CustPswd>'."\n".
						   '<EncryptionTypeCd>%s</EncryptionTypeCd>'."\n".'<Pswd>%s</Pswd>'."\n".'</CustPswd>'."\n".'</SignonPswd>'."\n".'<ClientDt>%s</ClientDt>'."\n".
						   '<CustLangPref>%s</CustLangPref>'."\n".'<ClientApp>'."\n".'<Org>%s</Org>'."\n".'<Name>%s</Name>'."\n".'<Version>%s</Version>'."\n".'</ClientApp>'."\n".
						   '</SignonRq>'."\n".'<InsuranceSvcRq>'."\n".'<RqUID>%s</RqUID>'."\n".'<PersPkgPolicyQuoteInqRq>'."\n".
						   '<TransactionRequestDt>%s</TransactionRequestDt>'."\n".'%s'."\n".'<com.acegroup_DataExtensions>'."\n".'%s'."\n".
						   '</com.acegroup_DataExtensions>'."\n".'</PersPkgPolicyQuoteInqRq>'."\n".'</InsuranceSvcRq>'."\n".'</ACORD>'."\n".'</GetTravelQuoteArray>'."\n".'</soap:Body>'."\n".'</soap:Envelope>';
			
			$soap_data = $this->GetTravelPolicyData;
			
			$request = sprintf($soap_format, $soap_data['SPName'], $soap_data['CustLoginId'], $soap_data['EncryptionTypeCd'], $soap_data['Pswd'], $this->MakeDateTime($soap_data['ClientDt'], true, true),
								$soap_data['CustLangPref'], $soap_data['Org'], $soap_data['Name'], $soap_data['Version'], $soap_data['RqUID'], $this->MakeDateTime($soap_data['TransactionRequestDt'], true, true),
								$this->PrintPolicy($policy_format, $soap_data['Policy']), $this->PrintExtensions($extension_format, $this->GetTravelQuoteData));
			
			$response = $this->SendByCurl('1.1', $soap_action, $request);
			
			if ($response === false) return $response;
			
			return $response;
			
		}
		
		public function GetTravelPolicy()
		{
			$soap_action = 'http://ACE.Global.Travel.CRS.Schemas.ACORD.WS/ACORDService/GetTravelPolicy';
			$user_format = ''."\n".'<NameInfo id="%d">'."\n".'<PersonName>'."\n".'<Surname>%s</Surname>'."\n".'<GivenName>%s</GivenName>'."\n".
//                '<TitlePrefix>%s</TitlePrefix>'."\n".
                '</PersonName>'."\n".
						   '%s<BirthDt>%s</BirthDt>'."\n".'<IdNumberTypeCd>%s</IdNumberTypeCd>'."\n".'<IdNumber>%s</IdNumber>'."\n".'</NameInfo>'."\n";
			$user_address = ''."\n".'<Addr id="%d" NameInfoRef="%d">'."\n".'<Addr1>%s</Addr1>'."\n".'<Addr2>%s</Addr2>'."\n".'<City>%s</City>'."\n".'<StateProv>%s</StateProv>'."\n".'<PostalCode>%s</PostalCode>'."\n".'<Country>%s</Country>'."\n".'</Addr>'."\n";
			$user_communications = ''."\n".'<Communications id="%d" NameInfoRef="%d">'."\n".'%s'."\n".'</Communications>'."\n";
			$user_communications_phone = ''."\n".'<PhoneInfo>'."\n".'<PhoneTypeCd>%s</PhoneTypeCd>'."\n".'<PhoneNumber>%s</PhoneNumber>'."\n".'</PhoneInfo>'."\n";
			$user_communications_email = ''."\n".'<EmailInfo>'."\n".'<EmailAddr>%s</EmailAddr>'."\n".'</EmailInfo>'."\n";
			$policy_format = '<PersPolicy>'."\n".'<CompanyProductCd>%s</CompanyProductCd>'."\n".'<ContractTerm>'."\n".'<EffectiveDt>%s</EffectiveDt>'."\n".'<ExpirationDt>%s</ExpirationDt>'."\n".'</ContractTerm>'."\n".
							 '<com.acegroup_Destination>'."\n".'<RqUID>%s</RqUID>'."\n".'<DestinationDesc>%s</DestinationDesc>'."\n".'</com.acegroup_Destination>'."\n".'<com.acegroup_InsuredPackage>'."\n".'<RqUID>%s</RqUID>'."\n".
							 '<InsuredPackageDesc>%s</InsuredPackageDesc>'."\n".'</com.acegroup_InsuredPackage>'."\n".'<com.acegroup_Plan>'."\n".'<RqUID>%s</RqUID>'."\n".'<PlanDesc>%s</PlanDesc>'."\n".'</com.acegroup_Plan>'."\n".'</PersPolicy>'."\n";
			$extension_format = ''."\n".'<DataItem type="%s" key="%s">'."\n".'<value>%s</value>'."\n".'</DataItem>'."\n";
			$payment_process = '<PaymentProcess xmlns="http://ACE.Global.Travel.CRS.Schemas.PaymentProcess">'."\n".'<PaymentRequest>'."\n".'<PaymentMode>%s</PaymentMode>'."\n".
							   '<CreditCardDetails>'."\n".'<NameOnCard>%s</NameOnCard>'."\n".'<CardType>%s</CardType>'."\n".'<CardNumber>%s</CardNumber>'."\n".'<EncryptionType>%s</EncryptionType>'."\n".'<SecurityCode>%s</SecurityCode>'."\n".
							   '<ExpiryMonth>%d</ExpiryMonth>'."\n".'<ExpiryYear>%d</ExpiryYear>'."\n".'</CreditCardDetails>'."\n".'<ChequeDetails xsi:nil="true" />'."\n".
							   '<TransactionDate>%s</TransactionDate>'."\n".'<Currency>%s</Currency>'."\n".'<Amount>%f</Amount>'."\n".'</PaymentRequest>'."\n".
							   '<PaymentResponse>'."\n".'<TransactionDate>%s</TransactionDate>'."\n".'<Messages xsi:nil="true" />'."\n".'</PaymentResponse>'."\n".'</PaymentProcess>'."\n";
			
			$soap_format = '<?xml version="1.0" encoding="utf-8"?>'."\n".
						   '<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://www.w3.org/2003/05/soap-envelope">'."\n".
						   '<soap:Body>'."\n".'<GetTravelPolicy xmlns="http://ACE.Global.Travel.CRS.Schemas.ACORD.WS/">'."\n".'<ACORD xmlns="http://ACE.Global.Travel.CRS.Schemas.ACORD_PolicyReq">'."\n".
						   '<SignonRq>'."\n".'<SignonPswd>'."\n".'<CustId>'."\n".'<SPName>%s</SPName>'."\n".'<CustLoginId>%s</CustLoginId>'."\n".'</CustId>'."\n".'<CustPswd>'."\n".'<EncryptionTypeCd>%s</EncryptionTypeCd>'."\n".
						   '<Pswd>%s</Pswd>'."\n".'</CustPswd>'."\n".'</SignonPswd>'."\n".'<ClientDt>%s</ClientDt>'."\n".'<CustLangPref>%s</CustLangPref>'."\n".'<ClientApp>'."\n".'<Org>%s</Org>'."\n".'<Name>%s</Name>'."\n".'<Version>%d</Version>'."\n".
						   '</ClientApp>'."\n".'</SignonRq>'."\n".'<InsuranceSvcRq>'."\n".'<RqUID>%s</RqUID>'."\n".'<PersPkgPolicyAddRq>'."\n".'<TransactionRequestDt>%s</TransactionRequestDt>'."\n".'<InsuredOrPrincipal>'."\n".'<GeneralPartyInfo>'."\n".
						   '%s'."\n".'%s'."\n".'%s'."\n".'</GeneralPartyInfo>'."\n".'</InsuredOrPrincipal>'."\n".'%s'."\n".'<com.acegroup_DataExtensions>'."\n".'%s'."\n".'</com.acegroup_DataExtensions>'."\n".
						   '<com.acegroup_PaymentProcess>'."\n".'%s'."\n".'</com.acegroup_PaymentProcess>'."\n".'</PersPkgPolicyAddRq>'."\n".'</InsuranceSvcRq>'."\n".'</ACORD>'."\n".'</GetTravelPolicy>'."\n".'</soap:Body>'."\n".'</soap:Envelope>';
			
			$soap_data = $this->GetTravelPolicyData;
						   
			$request = sprintf($soap_format, $soap_data['SPName'], $soap_data['CustLoginId'], $soap_data['EncryptionTypeCd'], $soap_data['Pswd'], $this->MakeDateTime($soap_data['ClientDt'], true, true),
								$soap_data['CustLangPref'], $soap_data['Org'], $soap_data['Name'], $soap_data['Version'], $soap_data['RqUID'], $this->MakeDateTime($soap_data['TransactionRequestDt'], true, true),
								$this->PrintUserNames($user_format, $soap_data['Users']), $this->PrintUsersAddress($user_address, $soap_data['Users']),
								$this->PrintUsersCommunications($user_communications, $user_communications_phone, $user_communications_email, $soap_data['Users']),
								$this->PrintPolicy($policy_format, $soap_data['Policy']), $this->PrintExtensions($extension_format, $soap_data['Extensions']),
								$this->PrintPaymentProcess($payment_process, $soap_data['PaymentProcess']));

//        echo "<pre>";print_r($request);echo "</pre>";exit;
			$response = $this->SendByCurl('1.1', $soap_action, $request);

			if ($response === false) return $response;

			return $response;
		}

        public function getTravelPolicyDataValue() {
            $this->GetTravelPolicyData['Pswd'] = $_SERVER['SERVER_NAME'] == 'staging.mytripinsurance.com.au' ? '$craMBLeD@123!' : 'M3q[OkuR:';
            return $this->GetTravelPolicyData;
        }

        public function setTravelPolicyDataValue($data = array()) {
            if (!empty($data)) {
                $this->GetTravelPolicyData = $data;
            }
            return true;
        }

        public function getTravelQuoteDataValue() {
            return $this->GetTravelQuoteData;
        }

        public function setTravelQuoteDataValue($data = array()) {
            if (!empty($data)) {
                $this->GetTravelQuoteData = $data;
            }
            return true;
        }

        public function getExtraPackages($data = array()) {
            $packages = array(
                "CruiseAmount" => "IncludeCruise",
                "WinterSportAmount"=> "IncludeWinterSports",
                "GolfCoverAmount"=> "IncludeGolf",
                "ExtremeActivitiesAmount"=> "IncludeExtremeActivities",
                "DeviceAmount"=> "IncludeDevice",
                "DrivingAmount"=> "IncludeDriving",
                "FamilyAmount"=> "IncludeFamily"
            );

            $extraPackages = array();
            foreach ($packages as $key => $item) {
                $check = 'false';
                if (array_key_exists($key, $data)) {
                    $check = 'true';
                }
                array_push($extraPackages, array(
                    'Type' => 'Boolean',
                    'Key' => $item,
                    'Value' => $check
                ));
            }
            return $extraPackages;
        }
	}
//
//	$requests = new TravelRequests();
//
//	$response = $requests->GetTravelQuoteArray();
//	print($response);
//	//if ($response !== false) $response = $requests->ConvertXmlToArray($response);
//////////////////////////////////////////////
//	if ($response !== false) {
//	  echo '<pre>';
//	  //print_r($response);
//	  echo '</pre>';
//	}
//
//
//
//
//      echo '<pre>';
//	  print_r($quotes);
//	  echo '</pre>';
//        exit;
//
//
//        $optionBlocks = $doc->getElementsByTagName( "com.acegroup_DataExtensions" );
//        $options = array();
//        $optionKeys = array('CruiseAmount', 'WinterSportAmount', 'GolfCoverAmount', 'ExtremeActivitiesAmount', 'FamilyAmount', 'DrivingAmount', 'DeviceAmount');
//        //var_dump($optionBlocks);
//
//
//
//        foreach($optionBlocks as $block) {
//			$dataItems = $block->getElementsByTagName( "DataItem" );
//			foreach($dataItems as $item) {
//				if (in_array($item->getAttribute('key'), $optionKeys)) {
//					echo $item->getAttribute('key'); echo ' - ';
//					var_dump($item->nodeValue); echo '<br>';
//				}
//			}
//			echo '<br><br><br><br>';
//        }
//	exit;
//////////////////////////////////////////////
//	$response = $requests->GetTravelPolicy();
//
//	if ($response !== false) echo "Ok\n";
//	else echo "Failed\n";
//
//	if ($response !== false) $response = $requests->ConvertXmlToArray($response);
//
//	if ($response !== false) {print_r($response);}
//	else {echo "Invalid XML structure\n";}
//
//
