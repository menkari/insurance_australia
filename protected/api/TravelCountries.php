<?php

class TravelCountries
{
    public
        $countriesCodes = array(
            // country codes from countries.json
        "Afghanistan" => "AF",
        "Albania" => "AL",
        "Algeria" => "DZ",
        "American Samoa" => "AS",
        "Andorra" => "AD",
        "Angola" => "AO",
        "Anguilla" => "AI",
        "Antigua and Barbuda" => "AG",
        "Argentina" => "AR",
        "Armenia" => "AM",
        "Aruba" => "AW",
        "Australia" => "AU",
        "Austria" => "AT",
        "Azerbaijan" => "AZ",
        "Bahamas" => "BS",
        "Bahrain" => "BH",
        "Bangladesh" => "BD",
        "Barbados" => "BB",
        "Belarus" => "BY",
        "Belgium" => "BE",
        "Belize" => "BZ",
        "Benin" => "BJ",
        "Bermuda" => "BM",
        "Bhutan" => "BT",
        "Bolivia" => "BO",
        "Bosnia and Herzegovina" => "BA",
        "Botswana" => "BW",
        "Brazil" => "BR",
        "British Virgin Islands" => "VG",
        "Brunei Darussalam" => "BN",
        "Bulgaria" => "BG",
        "Burkina Faso" => "BF",
        "Burundi" => "BI",
        "Cambodia" => "KH",
        "Cameroon" => "CM",
        "Canada" => "CA",
        "Cape Verde" => "CV",
        "Cayman Islands" => "KY",
        "Central African Republic" => "CF",
        "Chad" => "TD",
        "Chile" => "CL",
        "China" => "CN",
        "Colombia" => "CO",
        "Comoros" => "KM",
        "Cook Islands" => "CK",
        "Costa Rica" => "CR",
        "Cote d'Ivoire" => "CI",
        "Croatia" => "HR",
        "Cyprus" => "CY",
        "Czech Republic" => "CZ",
        "Democratic Republic of the Congo" => "CD",
        "Denmark" => "DK",
        "Djibouti" => "DJ",
        "Dominica" => "DM",
        "Dominican Republic" => "DO",
        "Ecuador" => "EC",
        "Egypt" => "EG",
        "El Salvador" => "SV",
        "Equatorial Guinea" => "GQ",
        "Eritrea" => "ER",
        "Estonia" => "EE",
        "Ethiopia" => "ET",
        "Federated States of Micronesia" => "FM",
        "Fiji" => "FJ",
        "Finland" => "FI",
        "France" => "FR",
        "French Guiana" => "GF",
        "French Polynesia" => "PF",
        "Gabon" => "GA",
        "Georgia" => "GE",
        "Germany" => "DE",
        "Ghana" => "GH",
        "Gibraltar" => "GI",
        "Greece" => "GR",
        "Greenland" => "GL",
        "Grenada" => "GD",
        "Guadeloupe" => "GP",
        "Guam" => "GU",
        "Guatemala" => "GT",
        "Guinea" => "GN",
        "Guinea-Bissau" => "GW",
        "Guyana" => "GY",
        "Haiti" => "HT",
        "Honduras" => "HN",
        "Hong Kong" => "HK",
        "Hungary" => "HU",
        "Iceland" => "IS",
        "India" => "IN",
        "Indonesia" => "ID",
        "Iran" => "IR",
        "Iraq" => "IQ",
        "Ireland" => "IE",
        "Israel, the Gaza Strip and the West Bank" => "IL",
        "Italy" => "IT",
        "Jamaica" => "JM",
        "Japan" => "JP",
        "Jordan" => "JO",
        "Kazakhstan" => "KZ",
        "Kenya" => "KE",
        "Kiribati" => "KI",
        "Kuwait" => "KW",
        "Kyrgyzstan" => "KG",
        "Laos" => "LA",
        "Latvia" => "LV",
        "Lebanon" => "LB",
        "Lesotho" => "LS",
        "Liberia" => "LR",
        "Libya" => "LY",
        "Liechtenstein" => "LI",
        "Lithuania" => "LT",
        "Luxembourg" => "LU",
        "Macau" => "MO",
        "Macedonia" => "MK",
        "Madagascar" => "MG",
        "Malawi" => "MW",
        "Malaysia" => "MY",
        "Maldives" => "MV",
        "Mali" => "ML",
        "Malta" => "MT",
        "Marshall Islands" => "MH",
        "Martinique" => "MQ",
        "Mauritania" => "MR",
        "Mauritius" => "MU",
        "Mayotte" => "YT",
        "Mexico" => "MX",
        "Moldova" => "MD",
        "Monaco" => "MC",
        "Mongolia" => "MN",
        "Montenegro" => "ME",
        "Montserrat" => "MS",
        "Morocco" => "MA",
        "Mozambique" => "MZ",
        "Myanmar" => "MM",
        "Namibia" => "NA",
        "Nauru" => "NR",
        "Nepal" => "NP",
        "Netherlands" => "NL",
        "Netherlands Antilles" => "AN",
        "New Caledonia" => "NC",
        "New Zealand" => "NZ",
        "Nicaragua" => "NI",
        "Niger" => "NE",
        "Nigeria" => "NG",
        "Niue" => "NU",
        "Norfolk island" => "NF",
        "Northern Mariana Islands" => "MP",
        "Norway" => "NO",
        "Oman" => "OM",
        "Pakistan" => "PK",
        "Palau" => "PW",
        "Panama" => "PA",
        "Papua New Guinea" => "PG",
        "Paraguay" => "PY",
        "Peru" => "PE",
        "Philippines" => "PH",
        "Poland" => "PL",
        "Portugal" => "PT",
        "Puerto Rico" => "PR",
        "Qatar" => "QA",
        "Romania" => "RO",
        "Russia" => "RU",
        "Rwanda" => "RW",
        "Saint Kitts and Nevis" => "KN",
        "Saint Lucia" => "LC",
        "Saint Vincent and the Grenadines" => "VC",
        "Samoa" => "WS",
        "San Marino" => "SM",
        "Sao Tome and Principe" => "ST",
        "Saudi Arabia" => "SA",
        "Senegal" => "SN",
        "Serbia" => "RS",
        "Seychelles" => "SC",
        "Sierra Leone" => "SL",
        "Singapore" => "SG",
        "Slovakia" => "SK",
        "Slovenia" => "SI",
        "Solomon Islands" => "SB",
        "Somalia" => "SO",
        "South Africa" => "ZA",
        "South Korea" => "KR",
        "Spain" => "ES",
        "Sri Lanka" => "LK",
        "Sudan" => "SD",
        "Suriname" => "SR",
        "Swaziland" => "SZ",
        "Sweden" => "SE",
        "Switzerland" => "CH",
        "Syria" => "SY",
        "Taiwan" => "TW",
        "Tajikistan" => "TJ",
        "Tanzania" => "TZ",
        "Thailand" => "TH",
        "The Gambia" => "GM",
        "Timor-Leste" => "TL",
        "Togo" => "TG",
        "Tokelau" => "TK",
        "Tonga" => "TO",
        "Trinidad and Tobago" => "TT",
        "Tunisia" => "TN",
        "Turkey" => "TR",
        "Turkmenistan" => "TM",
        "Turks and Caicos Islands" => "TC",
        "Tuvalu" => "TV",
        "Uganda" => "UG",
        "Ukraine" => "UA",
        "United Arab Emirates" => "AE",
        "United Kingdom" => "GB",
        "United States of America" => "US",
        "Uruguay" => "UY",
        "Uzbekistan" => "UZ",
        "Vanuatu" => "VU",
        "Venezuela" => "VE",
        "Vietnam" => "VN",
        "Virgin Islands (US)" => "VI",
        "Wallis and Futuna" => "WF",
        "Western Sahara" => "EH",
        "Yemen" => "YE",
        "Zambia" => "ZM",
        "Zimbabwe" => "ZW",

        "Bali" => "ID",
        "Ibiza" => "ES",
        "UK" => "GB",
        "USA" => "US",
    );

    public $countries = array(
        "Afghanistan" => "Asia",
        "Albania" => "Europe",
        "Algeria" => "Africa",
        "American Samoa" => "Pacific",
        "Angola" => "Africa",
        "Anguilla" => "South America",
        "Antarctica" => "North America",
        "Antigua and Barbuda" => "South America",
        "Argentina" => "South America",
        "Armenia" => "Asia",
        "Aruba" => "South America",
        "Australia" => "Domestic",
        "Austria" => "Europe",
        "Azerbaijan" => "Asia",
        "Bahamas" => "South America",
        "Bahrain" => "Asia",
        "Bangladesh" => "Asia",
        "Barbados" => "South America",
        "Belarus" => "Europe",
        "Belgium" => "Europe",
        "Belize" => "South America",
        "Benin" => "Africa",
        "Bermuda" => "South America",
        "Bhutan" => "Asia",
        "Bolivia" => "South America",
        "Bosnia and Herzegovina" => "Europe",
        "Botswana" => "Africa",
        "Brazil" => "South America",
        "British Indian Ocean Territory" => "Europe",
        "British Virgin Islands" => "South America",
        "Brunei" => "South East Asia",
        "Bulgaria" => "Europe",
        "Burkina Faso" => "Africa",
        "Burma" => "South East Asia",
        "Burundi" => "Africa",
        "Cambodia" => "South East Asia",
        "Cameroon" => "Africa",
        "Canada" => "North America",
        "Cape Verde" => "Africa",
        "Cayman Islands" => "South America",
        "Central African Republic" => "Africa",
        "Chad" => "Africa",
        "Chile" => "South America",
        "China" => "Asia",
        "Christmas Island" => "Domestic",
        "Cocos (Keeling) Islands" => "Domestic",
        "Colombia" => "South America",
        "Comoros" => "Africa",
        "Congo (Brazzaville)" => "Africa",
        "Congo (Kinshasa)" => "Africa",
        "Cook Islands" => "Pacific",
        "Costa Rica" => "South America",
        "Cote d'Ivoire" => "Africa",
        "Croatia" => "Europe",
        "Cuba" => "#N/A",
        "Cyprus" => "Asia",
        "Czech Republic" => "Europe",
        "Denmark" => "Europe",
        "Djibouti" => "Africa",
        "Dominica" => "South America",
        "Dominican Republic" => "South America",
        "East Timor" => "Pacific",
        "Ecuador" => "South America",
        "Egypt" => "Africa",
        "El Salvador" => "South America",
        "Equatorial Guinea" => "Africa",
        "Eritrea" => "Africa",
        "Estonia" => "Europe",
        "Ethiopia" => "Africa",
        "Falkland Islands" => "South America",
        "Faroe Islands" => "Europe",
        "Fiji" => "Pacific",
        "Finland" => "Europe",
        "France" => "Europe",
        "French Guiana" => "South America",
        "French Polynesia" => "Pacific",
        "Gabon" => "Africa",
        "Gambia" => "Africa",
        "Georgia" => "Asia",
        "Germany" => "Europe",
        "Ghana" => "Africa",
        "Gibraltar" => "Europe",
        "Greece" => "Europe",
        "Greenland" => "Europe",
        "Grenada" => "South America",
        "Guadeloupe" => "South America",
        "Guam" => "Pacific",
        "Guatemala" => "South America",
        "Guernsey" => "Europe",
        "Guinea" => "Africa",
        "Guinea-Bissau" => "Africa",
        "Guyana" => "South America",
        "Haiti" => "South America",
        "Honduras" => "South America",
        "Hong Kong" => "Europe",
        "Hungary" => "Europe",
        "Iceland" => "Europe",
        "India" => "Asia",
        "Indonesia" => "South East Asia",
        "Iran" => "Asia",
        "Iraq" => "Asia",
        "Ireland" => "UKI",
        "Isle of Man" => "Europe",
        "Israel" => "Asia",
        "Italy" => "Europe",
        "Jamaica" => "South America",
        "Japan" => "Asia",
        "Jersey" => "Europe",
        "Johnston Atoll" => "Europe",
        "Jordan" => "Asia",
        "Kazakhstan" => "Asia",
        "Kenya" => "Africa",
        "Kiribati" => "Pacific",
        "Korea" => "Asia",
        "Kuwait" => "Asia",
        "Kyrgyzstan" => "Asia",
        "Laos" => "South East Asia",
        "Latvia" => "Europe",
        "Lebanon" => "Asia",
        "Lesotho" => "Africa",
        "Liberia" => "Africa",
        "Libya" => "Africa",
        "Lithuania" => "Europe",
        "Luxembourg" => "Europe",
        "Macau" => "Europe",
        "Macedonia" => "Europe",
        "Madagascar" => "Africa",
        "Malawi" => "Africa",
        "Malaysia" => "South East Asia",
        "Maldives" => "Asia",
        "Mali" => "Africa",
        "Malta" => "Europe",
        "Marshall Islands" => "Pacific",
        "Martinique" => "South America",
        "Mauritania" => "Africa",
        "Mauritius" => "Europe",
        "Mayotte" => "Africa",
        "Mexico" => "South America",
        "Micronesia" => "Pacific",
        "Midway Islands" => "Pacific",
        "Moldova" => "Europe",
        "Monaco" => "Europe",
        "Mongolia" => "Asia",
        "Montenegro" => "Europe",
        "Montserrat" => "South America",
        "Morocco" => "Africa",
        "Mozambique" => "Africa",
        "Myanmar" => "South East Asia",
        "Namibia" => "Africa",
        "Nauru" => "Pacific",
        "Nepal" => "Asia",
        "Netherlands" => "Europe",
        "Netherlands Antilles" => "Africa",
        "New Caledonia" => "Pacific",
        "New Zealand" => "New Zealand",
        "Nicaragua" => "South America",
        "Niger" => "Africa",
        "Nigeria" => "Africa",
        "Niue" => "Pacific",
        "Norfolk Island" => "Pacific",
        "North Korea" => "Asia",
        "Northern Mariana Islands" => "Pacific",
        "Norway" => "Europe",
        "Oman" => "Asia",
        "Pakistan" => "Asia",
        "Palau" => "Pacific",
        "Palestine" => "Asia",
        "Panama" => "South America",
        "Papua New Guinea" => "South East Asia",
        "Paraguay" => "South America",
        "Peru" => "South America",
        "Philippines" => "South East Asia",
        "Poland" => "Europe",
        "Portugal" => "Europe",
        "Puerto Rico" => "South America",
        "Qatar" => "Europe",
        "Reunion" => "Africa",
        "Romania" => "Europe",
        "Russia" => "Europe",
        "Rwanda" => "Africa",
        "Saint Helena" => "Africa",
        "Saint Kitts and Nevis" => "South America",
        "Saint Lucia" => "South America",
        "Saint Pierre and Miquelon" => "North America",
        "Saint Vincent and the Grenadines" => "South America",
        "Samoa" => "Pacific",
        "Sao Tome and Principe" => "Africa",
        "Saudi Arabia" => "Asia",
        "Senegal" => "Africa",
        "Serbia" => "Europe",
        "Seychelles" => "Europe",
        "Sierra Leone" => "Africa",
        "Singapore" => "South East Asia",
        "Slovakia" => "Europe",
        "Slovenia" => "Europe",
        "Solomon Islands" => "Pacific",
        "Somalia" => "Africa",
        "South Africa" => "Africa",
        "South Georgia and the Islands" => "Europe",
        "South Korea" => "Asia",
        "South Sudan" => "Africa",
        "Spain" => "Europe",
        "Sri Lanka" => "Asia",
        "Sudan" => "Africa",
        "Suriname" => "South America",
        "Svalbard" => "Europe",
        "Swaziland" => "Africa",
        "Sweden" => "Europe",
        "Switzerland" => "Europe",
        "Syria" => "Asia",
        "Taiwan" => "Asia",
        "Tajikistan" => "Asia",
        "Tanzania" => "Africa",
        "Thailand" => "South East Asia",
        "Togo" => "Africa",
        "Tonga" => "Pacific",
        "Trinidad and Tobago" => "South America",
        "Tunisia" => "Africa",
        "Turkey" => "Asia",
        "Turkmenistan" => "Asia",
        "Turks and Caicos Islands" => "South America",
        "Tuvalu" => "Pacific",
        "Uganda" => "Africa",
        "Ukraine" => "Europe",
        "United Arab Emirates" => "Asia",
        "United Kingdom" => "UKI",
        "United States" => "North America",
        "Uruguay" => "South America",
        "Uzbekistan" => "Asia",
        "Vanuatu" => "Pacific",
        "Venezuela" => "South America",
        "Vietnam" => "South East Asia",
        "Virgin Islands" => "North America",
        "Wake Island" => "Pacific",
        "Wallis and Futuna" => "Pacific",
        "West Bank" => "Europe",
        "Western Sahara" => "Africa",
        "Yemen" => "Europe",
        "Zambia" => "Africa",
        "Zimbabwe" => "Africa",

        // missing/differently named countries
        "Andorra" => "Europe", //missing
        "Brunei Darussalam" => "South East Asia",
        "Democratic Republic of the Congo" => "Africa",
        "Federated States of Micronesia" => "Pacific",
        "Israel, the Gaza Strip and the West Bank" => "Asia",
        "Liechtenstein" => "Europe", //missing
        "Norfolk island" => "Pacific",
        "San Marino" => "Europe", //missing
        "The Gambia" => "Africa",
        "Timor-Leste" => "Pacific",
        "Tokelau" => "New Zealand", //missing
        "United States of America" => "North America",
        "Virgin Islands (US)" => "North America",

        // our extras
        "Bali" => "South East Asia",
        "Ibiza" => "Europe",
        "UK" => "UKI",
        "USA" => "North America",
    );

    public $regions = array(
        "North America" => "ACCB9802-E079-4A07-A737-A62F00730DC4",
        "South America" => "1BEF82FB-6936-4B81-9D30-A62F0073127C",
        "Africa" => "D2F90C1B-B391-4DB7-B5C9-A62F0073172E",
        "Europe" => "5DE5B86A-C8E0-4278-BEA6-A62F00731B88",
        "Asia" => "CD0A8806-6180-4073-AC12-A62F00731F76",
        "South East Asia" => "2D3C4E9F-7FA3-4DB3-B8B0-A62F00732408",
        "UKI" => "596BF490-564E-4545-A6AD-A62F00732813",
        "Pacific" => "1D658DF9-F9DF-4C41-87B8-A62F00732C01",
        "New Zealand" => "B996355E-2046-4A35-AC3D-A62F00733031",
        "Domestic" => "93EC7D42-AFF3-4171-94AD-A62F007334DA",
    );

    public function getFirstRegionByCountry($countries)
    {
        foreach ($this->regions as $region => $rquid) {
            foreach ($countries as $country) {
                if ($this->countries[$country] == $region)
                    return array(
                        'RqUID' => $rquid,
                        'DestinationDesc' => 'AU CTI ' . $region,
                    );
            }
        }
        return false;
    }

    public function getCountries() {
        return $this->countries;
    }

    public function getCodeByCountry($country) {
        if (isset($this->countriesCodes[$country])) {
            return $this->countriesCodes[$country];
        }
        return false;
    }

}