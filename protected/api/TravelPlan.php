<?php

class TravelPlan
{
    public $plans = array(
        "Prestige" => "70202BEE-D871-434F-8827-A62F007290F1",
        "Comprehensive" => "0B8425EE-9594-467A-BEF1-A62F00729862",
        "Essential" => "AD6A9A08-E582- 4C2E-9964-A62F00729D27",
        "Comprehensive Annual" => "01E335A2-077A-4CF5-B0DA-A62F0072D842",
        "Essential Annual" => "7B20FA51-7BC4-436B-A1D1-A62F0072DCF4",
        "Domestic Annual" => "1978327E-766C-4E47-970C-A62F0072E1B5"
    );

    public function getPlanKey($plan = false) {
        if ($plan) {
            if (isset($this->plans[$plan])) {
                return array(
                    'RqUID' => $this->plans[$plan],
                    'PlanDesc' => $plan,
                );
            }
        }
        return false;
    }

}