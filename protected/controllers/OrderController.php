<?php

class OrderController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column1';

    public $css;
    public $js;

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules() {
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('quote','details','purchase', 'payment', 'error', 'thanks', 'test'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('index'),
				'roles'=>array(Users::ROLE_CUSTOMER),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('index','customers','statistic','statisticTable'),
				'roles'=>array(Users::ROLE_PARTNER),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

//	/**
//	 * Creates a new model.
//	 * If creation is successful, the browser will be redirected to the 'view' page.
//	 */
//	public function actionCreate()
//	{
//		$model=new Orders;
//
//		// Uncomment the following line if AJAX validation is needed
//		$this->performAjaxValidation($model);
//
//		if(isset($_POST['Orders']))
//		{
//            $order = $_POST['Orders'];
//            $model->user_id = Yii::app()->user->id;
//            $model->date = date('Y-m-d', strtotime($order['date']));
//            $model->countries = $order['countries'];
//            $model->start_traveling = date('Y-m-d', strtotime($order['start_traveling']));
//            $model->end_traveling = date('Y-m-d', strtotime($order['end_traveling']));
//            $model->travellers = $order['travellers'];
//
//			if($model->save())
//				$this->redirect(array('index'));
//		}
//
//		$this->render('create',array(
//			'model'=>$model,
//		));
//	}

	/**
	 * Lists all models.
	 */
	public function actionIndex() {
	   // echo "<pre>";print_r($_POST);echo "</pre>";
        if (Yii::app()->user->checkAccess('partner')) {
            if ($_SERVER['REQUEST_METHOD'] == 'POST' && !empty($_POST)) {
                $condition = 'partner_id=:partner_id';
                $params = array();
                $params[':partner_id'] = Yii::app()->user->id;

                // echo "<pre>";print_r($params);echo "</pre>";exit;

                $search = $_POST['search'];
                if (!empty($search)) {
                    $condition .= ' AND countries LIKE :countries';
                    $params[':countries'] = "%$search%";
                }

                $minVal = $_POST['min-val'];
                if (!empty($minVal)) {
                    $condition .= ' AND price >=:priceMin';
                    $params[':priceMin'] = $minVal;
                }

                $maxVal = $_POST['max-val'];
                if (!empty($maxVal)) {
                    $condition .= ' AND price <=:priceMax';
                    $params[':priceMax'] = $maxVal;
                }

                $from = $_POST['ordersFrom'];
                if (!empty($from)) {
                    $from = DateTime::createFromFormat('d/m/Y', $from)->format('Y-m-d');
                    $condition .= ' AND date >=:dateFrom';
                    $params[':dateFrom'] = $from;
                }

                $to = $_POST['ordersTo'];
                if (!empty($to)) {
                    $to = DateTime::createFromFormat('d/m/Y', $to)->format('Y-m-d');
                    if ($to < $from) {
                        $to = $from;
                    }
                    $condition .= ' AND date <=:dateTo';
                    $params[':dateTo'] = $to;
                }



                $dataProvider = new CActiveDataProvider('Orders', array(
                        'criteria' => array(
                            'condition' => $condition,
                            'params' => $params,
                            'select' => 'id, date, price, countries, start_traveling, end_traveling, travellers',
                            'order' => 'id DESC'
                        ),

                    )
                );


            } else {
                $condition = 'partner_id=:partner_id';
                $params = array(':partner_id' => Yii::app()->user->id);

                $dataProvider = new CActiveDataProvider('Orders', array(
                        'criteria' => array(
                            'condition' => $condition,
                            'params' => $params,
                            'select' => 'id, date, price, countries, start_traveling, end_traveling, travellers, pdf',
                            'order' => 'id DESC'
                        ),
                    )
                );
            }
        } else {
            $condition = 'user_id=:user_id';
            $params = array(':user_id' => Yii::app()->user->id);


            $dataProvider = new CActiveDataProvider('Orders', array(
                    'criteria' => array(
                        'condition' => $condition,
                        'params' => $params,
                        'select' => 'id, date, price, countries, start_traveling, end_traveling, travellers, pdf',
                        'order' => 'id DESC'
                    ),

                )
            );
        }

        $criteria = new CDbCriteria;
        $criteria->select = 'MAX(price) as maxprice, MIN(price) as minprice';
//        $criteria->condition = $condition;
//        $criteria->params = $params;
        $rangePrice = Orders::model()->find($criteria);
        $min = $rangePrice['minprice'];
        $max = $rangePrice['maxprice'];


		$this->render('index',array(
			'dataProvider' => $dataProvider,
            'post' => $_POST,
            'min' => $min,
            'max' => $max,
            'partner' => Partner::model()->findByAttributes(array('user_id' => Yii::app()->user->id))
		));
	}

	public function actionStatistic() {

//        $model = new Orders();
//        $condition = "status='Success' AND partner_id = :partner_id";
//        $params[':partner_id'] = Yii::app()->user->id;
//        if (isset($_POST['date_range_start']) && $_POST['date_range_start']) {
//            $date_range_start = DateTime::createFromFormat('d/m/Y', $_POST['date_range_start'])->format('Y-m-d');//$_POST['date_range_start'];
//            $condition .= " AND date >= :date1";
//            $params[':date1'] = $date_range_start;
//        }
//        if (isset($_POST['date_range_end']) && $_POST['date_range_end']) {
//            $date_range_end = DateTime::createFromFormat('d/m/Y', $_POST['date_range_end'])->format('Y-m-d');//$_POST['date_range_end'];
//            $condition .= " AND date <= :date2";
//            $params[':date2'] = $date_range_end;
//        }
//
//        $orders = Orders::model()->findAll(array(
//            'condition'=>$condition,
//            'params'=>$params,
//        ));
//
//        $table = array();
//        foreach ($orders as $order) {
//
//            if (isset($table[$order->date])) {
//                $table[$order->date]['visits']++;
//                $table[$order->date]['sales'] += $order->price;
//                $table[$order->date]['revenues'] = $table[$order->date]['sales'] * 0.2;
//                $table[$order->date]['conversion'] = $table[$order->date]['sales'] * 0.1;;
//            } else {
//                $table[$order->date]['id'] = $order->date;
//                $table[$order->date]['visits'] = 1;
//                $table[$order->date]['sales'] = $order->price;
//                $table[$order->date]['revenues'] = $table[$order->date]['sales'] * 0.2;
//                $table[$order->date]['conversion'] = $table[$order->date]['sales'] * 0.1;
//            }
//        }
//
//        $table = array_values($table);
//
//        $dataProvider=new CArrayDataProvider($table, array(
//
//            'sort'=>array(
//                'attributes'=>array(
//                    'date', 'visits', 'sales', 'revenues', 'conversion',
//                ),
//            ),
//            'pagination'=>array(
//                'pageSize'=>2,
//            ),
//        ));

        $dataProvider = $this->_statistic();

        $this->render('statistic', array(
            'dataProvider' => $dataProvider,
            'partner' => Partner::model()->findByAttributes(array('user_id' => Yii::app()->user->id))
        ));
    }

    public function actionStatisticTable() {

        if( Yii::app()->request->isAjaxRequest) {

            $format = 'd M';
            if (!isset($_POST['date_range_start']) || !$_POST['date_range_start']) {
                $date_range_start = date('d/m/Y', strtotime('-14day'));

                if (isset($_POST['date_range_end']) && $_POST['date_range_end']) {
                    $date_range_end = DateTime::createFromFormat('d/m/Y', $_POST['date_range_end'])->format('Y-m-d');
                    $date_range_start = date('d/m/Y', strtotime($date_range_end . '-14day'));
                }
                $_POST['date_range_start'] = $date_range_start;
            }
            $dataProvider = $this->_statistic($format);
            echo json_encode($dataProvider);
            Yii::app()->end();
        }
    }

    private function _statistic($dateFormat = false) {

        if (!$dateFormat) {
            $dateFormat = 'Y-m-d';
        }

        $model = new Orders();
//var_dump($_POST); exit;
        $condition = "status='Success' AND partner_id = :partner_id";
        $params[':partner_id'] = Yii::app()->user->id;
        if (isset($_POST['date_range_start']) && $_POST['date_range_start']) {
            $date_range_start = DateTime::createFromFormat('d/m/Y', $_POST['date_range_start'])->format('Y-m-d');//$_POST['date_range_start'];
            $condition .= " AND date >= :date1";
            $params[':date1'] = $date_range_start;
        }
        if (isset($_POST['date_range_end']) && $_POST['date_range_end']) {
            $date_range_end = DateTime::createFromFormat('d/m/Y', $_POST['date_range_end'])->format('Y-m-d');//$_POST['date_range_end'];
            $condition .= " AND date <= :date2";
            $params[':date2'] = $date_range_end;
        }

        $orders = Orders::model()->findAll(array(
            'condition'=>$condition,
            'params'=>$params,
        ));

        $table = array();
        foreach ($orders as $order) {

            if (isset($table[$order->date])) {
                $table[$order->date]['visits']++;
                $table[$order->date]['sales'] += $order->price;
                $table[$order->date]['revenues'] = $table[$order->date]['sales'] * 0.2;
                $table[$order->date]['conversion'] = $table[$order->date]['sales'] * 0.1;;
            } else {
                $table[$order->date]['id'] = date($dateFormat, strtotime($order->date));// $order->date;
                $table[$order->date]['visits'] = 1;
                $table[$order->date]['sales'] = $order->price;
                $table[$order->date]['revenues'] = $table[$order->date]['sales'] * 0.2;
                $table[$order->date]['conversion'] = $table[$order->date]['sales'] * 0.1;
            }
        }

        $table = array_values($table);

        $dataProvider=new CArrayDataProvider($table, array(

            'sort'=>array(
                'attributes'=>array(
                    'date', 'visits', 'sales', 'revenues', 'conversion',
                ),
            ),
            'pagination'=>array(
                'pageSize'=>10,
            ),
        ));

        return $dataProvider;
    }

    public function actionCustomers() {


        if($_SERVER['REQUEST_METHOD'] == 'POST' && !empty($_POST)){
            $search = $_POST['search'];
            $orders = array();
            $criteria = new CDbCriteria;

            // Order
            $criteriaOrder = new CDbCriteria;
            $criteriaOrder->group = 'user_id';

            $date_condition = array();
            $from = $_POST['ordersFrom'];
            if (!empty($from)){
                $from = DateTime::createFromFormat('d/m/Y', $from)->format('Y-m-d');
                $date_condition['date >'] = $from;
            }
            $to = $_POST['ordersTo'];
            if ($to < $from) {
                $to = $_POST['ordersTo'] = $_POST['ordersFrom'];
            }
            if (!empty($to)){
                $to = DateTime::createFromFormat('d/m/Y', $to)->format('Y-m-d');
                $date_condition['date <'] = $to;
            }
            if (!empty($date_condition)) {
                $criteriaOrder->addColumnCondition($date_condition, 'AND');
            }
            $orders = Orders::model()->findAllByAttributes(array('partner_id'=>Yii::app()->user->id),$criteriaOrder);
            // /order
            if (!empty($search)){
                $criteria->addSearchCondition('username', $search, true);
                $criteria->addSearchCondition('surname', $search, true, 'OR');
            }

            $id = array();
            if (!empty($orders)){
                foreach ($orders as $order){
                    $id[] = $order->user_id;
                }
            }
            $criteria->addInCondition('id',$id);
            $users = Users::model()->findAll($criteria);
        } else {
            $criteria = new CDbCriteria;
            $criteria->group = 'user_id';

            $orders = Orders::model()->findAllByAttributes(array('partner_id'=>Yii::app()->user->id),$criteria);

            $id = array();

            if (!empty($orders)){
                foreach ($orders as $order){
                    $id[] = $order->user_id;
                }
            }

            $users = Users::model()->findAllByPk($id);


        }

        $dataProvider =  new CArrayDataProvider('User');
        $dataProvider->setData($users);

        $this->render('customers',array(
            'dataProvider'=>$dataProvider,
            'post' => $_POST
        ));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionQuote() {
        $model=new Orders;
        $model->scenario = 'orders-base-info';

        if(isset($_POST['ajax']) && $_POST['ajax'] === 'orders-base-info') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }

        if (isset($_POST['Orders'])) {
            Yii::app()->session->add("countries", $_POST['Orders']['countries']);
            Yii::app()->session->add("start_traveling", strtotime($_POST['Orders']['start_traveling']));
            Yii::app()->session->add("end_traveling", strtotime($_POST['Orders']['end_traveling']));
            Yii::app()->session->add("age", $_POST['Orders']['age']);
            Yii::app()->session->add("age2", $_POST['Orders']['age2']);
            if (isset($_POST['Orders']['kids'])) {
                Yii::app()->session->add("kids", $_POST['Orders']['kids']);
            }
            $this->refresh();
        }

        $sessionData = $this->getSessionData(array('countries', 'start_traveling','end_traveling', 'age'));
        $sessionData['age2'] = Yii::app()->session->get("age2");
        $sessionData['kids'] = Yii::app()->session->get("kids");
        // GET QUOTES
        $quotes = $this->parseTravelQuoteArray();

        $start_traveling = date('Y-m-d', $sessionData['start_traveling']);
        $end_traveling = date('Y-m-d', $sessionData['end_traveling']);

        foreach ($quotes as $item) {
            if ($item['quote']['clear_price'] < 0) {
                Yii::app()->user->setFlash('orders_base_error', "Error: Details are invalid");
                $this->redirect('/');
            }
        }

        $this->render('quote',array(
            'model' => $model,
            'quotes' => $quotes,
            'start_traveling' => $start_traveling,
            'end_traveling' => $end_traveling,
            'sessionData' => $sessionData
        ));
    }


    public function actionDetails()
    {
        $model=new Orders;
        $orderDetails = new OrderDetails;
//        $orderPayments = new OrderPayments;
        $model->scenario = 'orders-quote';

        $userInfo = array(
            'name' => '',
            'surname' => '',
            'email' => '',
            'birth' => '',
            'phone' => '',
            'street' => '',
            'city' => '',
            'zip_code' => '',
            'state' => '',
        );

        // User Info
        $userID = Yii::app()->user->getId();
        $user = false;
        if ($userID) {
            $user = Users::model()->findByPk($userID);
            $userInfo = array(
                'name' => $user->name,
                'surname' => $user->surname,
                'email' => $user->email,
                'birth' => $user->birth,
                'phone' => $user->phone,
                'street' => $user->street,
                'city' => $user->city,
                'zip_code' => $user->zip_code,
                'state' => $user->state,
            );
        }
        // /User Info

//        if(isset($_POST['ajax']) && $_POST['ajax']==='orders-quote-info') {
//            echo CActiveForm::validate($model);
//            Yii::app()->end();
//        }

//        if (isset($_POST['Orders']['options']) && !empty($_POST['Orders']['options'])) {
//            Yii::app()->session->add("options", $_POST['Orders']['options']);
//        }
        if (isset($_POST['Orders'])) {
            Yii::app()->session->add("quote", $_POST['Orders']['quote']);
            Yii::app()->session->add("price", $_POST['Orders']['price']);
            Yii::app()->session->add("options", $_POST['Orders']['options']);
            $this->refresh();
        }


        $sessionData = $this->getSessionData(array('countries', 'start_traveling','end_traveling', 'age', 'quote', 'price'));
        $sessionData['age2'] = Yii::app()->session->get('age2');
        $sessionData['kids'] = Yii::app()->session->get('kids');

        $sessionData['options'] = array();
        $options = json_decode(Yii::app()->session->get("options"), true);
        if (isset($options)) {
            $sessionData['options'] = $options;
        }

        $start_traveling = date('Y-m-d', $sessionData['start_traveling']);
//        $end_traveling = date('Y-m-d', $sessionData['end_traveling']);

        if(isset(Yii::app()->request->cookies['referral']->value)) {
            $referral = Yii::app()->request->cookies['referral']->value;
            $has_cookie = true;
        } else {
            $referral = '';
            $has_cookie = false;
        }

        $this->render('details',array(
            'orderModel' => $model,
            'detailsModel' => $orderDetails,
//            'paymentsModel' => $orderPayments,
            'sessionData' => $sessionData,
            'start_traveling' => $start_traveling,
//            'end_traveling' => $end_traveling,
            'userInfo' => $userInfo,
            'referral' => $referral,
            'has_cookie' => $has_cookie
        ));
    }

    public function actionPurchase()
    {
        //Yii::import('application.api.*');
        //$a = new TravelRequests();
        //
        $orderDetails = new OrderDetails;
//        $orderPayments = new OrderPayments;
//        $orderPayments->scenario = $orderDetails->scenario = 'step_details';
        $age2 = Yii::app()->session->get('age2');
        if(isset($_POST['ajax']) && $_POST['ajax']==='order-details-form')
        {
            $orderDetails->scenario = 'order-details-form-second';
            if (!empty($age2)) {
                $orderDetails->scenario = 'order-details-form-second';
            }
            echo CActiveForm::validate($orderDetails);
            //echo CActiveForm::validate($orderPayments);
            Yii::app()->end();
        }

        if(isset($_POST['OrderDetails'])) {
            $postOrderDetails = $_POST['OrderDetails'];
            $check_referral = Partner::findReferralCode($postOrderDetails['referral']);
            if(is_array($check_referral)) {
                $postOrderDetails['referral'] = $check_referral['referral_code'];
            } else {
                $postOrderDetails['referral'] = $this->_getReferral();
                $postOrderDetails['unknown_referral'] = $check_referral;
            }

            $birth_date = str_replace("/","-",$postOrderDetails['birth_date']);
            $postOrderDetails['birth_date'] = date('Y-m-d', strtotime($birth_date));

            if (Yii::app()->session->get('age2')) {
                $second_birth_date = str_replace("/","-",$postOrderDetails['second_birth_date']);
                $postOrderDetails['second_birth_date'] = date('Y-m-d', strtotime($second_birth_date));
            }
            Yii::app()->session->add("OrderDetails", json_encode($postOrderDetails));

            if(isset($_POST['medical_conditions'])) {
                Yii::app()->session->add("medical_conditions", $_POST['medical_conditions']);
            }

            $this->refresh();
        }


//        if(isset($_POST['OrderPayments'])) {
//            Yii::app()->session->add("OrderPayments", json_encode($_POST['OrderPayments']));
//        }


        $sessionData = $this->getSessionData(array('countries', 'start_traveling','end_traveling', 'age', 'quote', 'price', 'OrderDetails'));
        $sessionData['age2'] = Yii::app()->session->get('age2');
        $sessionData['kids'] = Yii::app()->session->get('kids');


        $orderDetails = Yii::app()->session->get("OrderDetails");
        if (!isset($orderDetails) || empty($orderDetails)) {
            $this->redirect('/');
        } else {
            $sessionData['orderDetails'] = json_decode($orderDetails, true);
        }

        $sessionData['options'] = array();
        $options = json_decode(Yii::app()->session->get("options"), true);
        if (isset($options)) {
            $sessionData['options'] = $options;
        }


        // REMOVE
//        $orderPayments = Yii::app()->session->get("OrderPayments");
//        if (!isset($orderPayments) || empty($orderPayments)) {
//            $this->redirect('/');
//        } else {
//            $sessionData['orderPayments'] = json_decode($orderPayments, true);
//        }

        $this->render('purchase',array(
            'sessionData' => $sessionData
        ));
    }

    public function actionPayment() {
        $order = new Orders;
        $orderDetails = new OrderDetails;

        $cartData = array();
        if (isset($_POST['Cart'])) {
            $cartData['cart_type']      = $_POST['Cart']['card-type'];
            $cartData['cart_name']      = $_POST['Cart']['card-holder-name'];
            $cartData['cart_number']    = $_POST['Cart']['card-holder-number'];
            $cartData['cart_date']      = $_POST['Cart']['card-date'];
            $cartData['cart_cvc']       = $_POST['Cart']['card-cvc'];


            foreach ($cartData as $key => $item) {
                if (empty($item)) {
                    Yii::app()->user->setFlash('error', "Error: All payment fields are required.");
                    $this->redirect(array('order/purchase'));
                }
            }

            preg_match('/^(0|1)[0-9]{1}\/[0-9]{2}$/', str_replace(' ', '', $cartData['cart_date']), $matches);
//            print_r($matches);exit;
            if (empty($matches)) {
                Yii::app()->user->setFlash('cart_date_error', "Error: Please use Month/Year.  For example, " . date("m/y"));
                $this->redirect(array('order/purchase'));
            }
        } else {
            $this->redirect(array('order/purchase'));
        }

        // CHECK SESSION
        $sessionData = $this->getSessionData(array('countries', 'start_traveling','end_traveling', 'age', 'quote', 'price'));

        $sessionData['OrderPayments'] = $cartData;

        $sessionData['age2'] = Yii::app()->session->get('age2');
        $sessionData['kids'] = Yii::app()->session->get('kids');
        $sessionData['medical_conditions'] = Yii::app()->session->get('medical_conditions');

        $sessionData['options'] = array();
        $options = json_decode(Yii::app()->session->get("options"), true);
        if (isset($options)) {
            $sessionData['options'] = $options;
        }

        $order->countries       = $sessionData['countries'];
        $order->start_traveling = date('Y-m-d', $sessionData['start_traveling']);
        $order->end_traveling   = date('Y-m-d', $sessionData['end_traveling']);
        $order->age             = $sessionData['age'];
        $order->age2            = $sessionData['age2'];
        $order->kids            = $sessionData['kids'];
        $order->quote           = $sessionData['quote'];
        $order->price           = $sessionData['price'];

        $order->date           = date('Y-m-d');

        $order->travellers     = 1;
        if (!empty($order->age2)) {
            $order->travellers++;
        }
        if ($order->kids) {
            $order->travellers += count(explode(',', $order->kids));
        }
        $sessOrderDetails = Yii::app()->session->get('OrderDetails');

        if (!isset($sessOrderDetails) || empty($sessOrderDetails)) {
            $this->redirect(array('site/index'));
        }
        $sessionData['OrderDetails'] = json_decode($sessOrderDetails, true);
        unset($sessionData['OrderDetails']['referral']);
        $sessionData['OrderDetails']['kids_info'] = json_encode($sessionData['OrderDetails']['kids_info']);

        $orderDetails->attributes    = $sessionData['OrderDetails'];

        $save = false;
        $transaction = Yii::app()->db->beginTransaction();
        try {
            if ($orderDetails->validate()) {

                $orderDetails->save();
                $order->detail_id = $orderDetails->id;

                $order->user_id = 0;
                if (isset($sessionData['OrderDetails']['save_for_future']) && $sessionData['OrderDetails']['save_for_future']) {
                    $order->user_id = $this->userAutoRegistration($sessionData['OrderDetails']);
                }
                $order->partner_id = Yii::app()->session->get('template_id');

                if (isset($sessionData['OrderDetails']['referral']) && !empty($sessionData['OrderDetails']['referral'])) {
                    $order->referral = $sessionData['OrderDetails']['referral'];
                    if(isset($sessionData['OrderDetails']['unknown_referral'])) {
                        $order->unknown_referral = $sessionData['OrderDetails']['unknown_referral'];
                    }
                } else {
                    $order->referral = $this->_getReferral();
                }
                if ($order->save()) {
                    $save = $this->getTravelPolicy($sessionData, $order->id);

                    $cookie = new CHttpCookie('referral',$order->referral);
                    Yii::app()->request->cookies['referral'] = $cookie;

                    if ($save) {
                        Yii::app()->session->remove("OrderDetails");
//                        Yii::app()->session->remove("OrderPayments");
                        Yii::app()->session->remove("countries");
                        Yii::app()->session->remove("start_traveling");
                        Yii::app()->session->remove("end_traveling");
                        Yii::app()->session->remove("age");
                        Yii::app()->session->remove("age2");
                        Yii::app()->session->remove("kids");
                        Yii::app()->session->remove("quote");
//                        Yii::app()->session->remove("price");


                    }
                } else {

                }
            } else {
//                $error = 'Details';
//                echo CActiveForm::validate($orderDetails);
            }

            $transaction->commit();
//            exit;
//            var_dump($save);
            if ($save) {
                $this->redirect(array('order/thanks'));
            } else {
                $this->redirect(array('order/error'));
            }
        } catch (Exception $e) {
            $transaction->rollBack();
            Yii::app()->user->setFlash('error', "{$e->getMessage()}");
            $this->refresh();
        }

        $this->redirect(array('order/error'));
    }


    public function actionError() {

        $error = Yii::app()->session->get("status");
        $error_message = Yii::app()->session->get("status_message");
        $this->render('error',array(
            'error' => $error,
            'error_message' => $error_message
        ));
    }


    public function actionTest()
    {
        $this->parseTravelQuoteArray();
    }

    public function actionThanks() {

        $price = Yii::app()->session->get("price");
        $pdf = Yii::app()->session->get("pdf");
        $PolicyNumber = Yii::app()->session->get("PolicyNumber");
        if (empty($price)) {
            $this->redirect('/');
        }
        $this->render('thanks',array(
            'price' => '$' . $price,
            'pdf' => $pdf,
            'PolicyNumber' => $PolicyNumber
        ));
    }


    /**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Orders the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Orders::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Orders $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='orders-form')
		{

            $_POST['Orders']['user_id'] = Yii::app()->user->id;
            echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}

    private function parseTravelQuoteArray() {
        Yii::import('application.api.*');
        //$travelCountries = TravelCountries::getRegionByCountry($country);
        $requests = new TravelRequests();
        $AdultAge1 = Yii::app()->session->get("age");
        $AdultAge2 = Yii::app()->session->get("age2");

        $destinationData = array(
            'RqUID' => '3812A460-38E0-4874-AB4E-A62F007338C8',
            'DestinationDesc' => 'AU CTI Worldwide'
        );
        $countries = Yii::app()->session->get("countries");
        $travelCountries = new TravelCountries;
        if ($countries) {
//            $travelCountries = new TravelCountries;
            $destinationData = $travelCountries->getFirstRegionByCountry(explode('|', $countries));
        }

        $kidsCSV = Yii::app()->session->get("kids");
        $kidsArray = empty($kidsCSV) ? NULL : explode(',', $kidsCSV);

        $referral = $this->_getReferral();
        $GetTravelQuoteData = array
        (
            array
            (
                'Type' => 'System.String',
                'Key' => 'Source_System',
                'Value' => $referral
            ),
            array
            (
                'Type' => 'System.String',
                'Key' => 'ExcessLevel',
                'Value' => '100'
            ),
            array
            (
                'Type' => 'System.String',
                'Key' => 'AdultAge1',
                'Value' => $AdultAge1//'33'
            ),
            array
            (
                'Type' => 'System.String',
                'Key' => 'TripCost',
                'Value' => '1000'
            ),
            array
            (
                'Type' => 'System.String',
                'Key' => 'NoOfChildren',
                'Value' => count($kidsArray)
            ),
            array
            (
                'Type' => 'System.String',
                'Key' => 'TripType',
                'Value' => 'Single'
            ),
            array
            (
                'Type' => 'System.String',
                'Key' => 'QuoteCode',
                'Value' => 'B2C.SINGLE'
            ),
        );
            // Extra packages
        $packagesArray = array('IncludeCruise', 'IncludeWinterSports', 'IncludeGolf', 'IncludeExtremeActivities', 'IncludeFamily', 'IncludeDriving', 'IncludeDevice');

        foreach ($packagesArray as $packagesItem) {
            if (($AdultAge1 >= 70 || (!empty($AdultAge2) && $AdultAge2 >= 70)) &&
                ($packagesItem == 'IncludeExtremeActivities' || $packagesItem == 'IncludeWinterSports')) {
                $GetTravelQuoteData[] = array
                (
                    'Type' => 'Boolean',
                    'Key' => $packagesItem,
                    'Value' => 'false'
                );
            } else {
                $GetTravelQuoteData[] = array
                (
                    'Type' => 'Boolean',
                    'Key' => $packagesItem,
                    'Value' => 'true'
                );
            }
        }

        $countriesCodes = array();
        $countriesArray = explode('|', $countries);
        foreach ($countriesArray as $countryItem) {
            $countryCode = $travelCountries->getCodeByCountry($countryItem);
            if ($countryCode) {
                $countriesCodes[] = $countryCode;
            }
        }
        $countriesCodes = implode('|', $countriesCodes);
        $GetTravelQuoteData[] = array
            (
                'Type' => 'System.String',
                'Key' => 'TravelCountries',
                'Value' => $countriesCodes
            );
//        array
//        (
//            array
//            (
//                'Type' => 'Boolean',
//                'Key' => 'IncludeCruise',
//                'Value' => 'true'
//            ),
//            array
//            (
//                'Type' => 'Boolean',
//                'Key' => 'IncludeWinterSports',
//                'Value' => 'true'
//            ),
//            array
//            (
//                'Type' => 'Boolean',
//                'Key' => 'IncludeGolf',
//                'Value' => 'true'
//            ),
//            array
//            (
//                'Type' => 'Boolean',
//                'Key' => 'IncludeExtremeActivities',
//                'Value' => 'true'
//            ),
//            array
//            (
//                'Type' => 'Boolean',
//                'Key' => 'IncludeFamily',
//                'Value' => 'true'
//            ),
//            array
//            (
//                'Type' => 'Boolean',
//                'Key' => 'IncludeDriving',
//                'Value' => 'true'
//            ),
//            array
//            (
//                'Type' => 'Boolean',
//                'Key' => 'IncludeDevice',
//                'Value' => 'true'
//            ),
//        );
        if (!empty($AdultAge2)) {
            $GetTravelQuoteData[] = array(
                'Type' => 'System.String',
                'Key' => 'AdultAge2',
                'Value' => $AdultAge2//'33'
            );
        }

        $requests->setTravelQuoteDataValue($GetTravelQuoteData);


        $start_traveling = Yii::app()->session->get("start_traveling");
        $end_traveling = Yii::app()->session->get("end_traveling");

        $dataPolicy = $requests->getTravelPolicyDataValue();
        //RqUID = 3812A460-38E0-4874-AB4E-A62F007338C8
        //DestinationDesc AU CTI Worldwide

        $dataPolicy['Policy']['EffectiveDt'] = date('Y-m-d', $start_traveling);
        $dataPolicy['Policy']['ExpirationDt'] = date('Y-m-d', $end_traveling);

        $dataPolicy['Policy']['Destination']['RqUID'] = $destinationData['RqUID'];   //'3812A460-38E0-4874-AB4E-A62F007338C8';
        $dataPolicy['Policy']['Destination']['DestinationDesc'] = $destinationData['DestinationDesc'];   //'AU CTI Worldwide';

        $requests->setTravelPolicyDataValue($dataPolicy);

        $response = $requests->GetTravelQuoteArray();
//        echo "<pre>";print_r($response);echo "</pre>";exit;

        //if ($response !== false) //print($response);

        $doc = new DOMDocument();
        $doc->loadXML( $response );

        $optionKeys = array('CruiseAmount', 'WinterSportAmount', 'GolfCoverAmount', 'ExtremeActivitiesAmount', 'FamilyAmount', 'DrivingAmount', 'DeviceAmount');

        $packageNames = array(
            "CruiseAmount" => "Cruise Pack",
            "WinterSportAmount" => "Winter Sports Pack",
            "GolfCoverAmount" => "Golf Pack",
            "ExtremeActivitiesAmount" => "Adventure Sports Pack",
            "DeviceAmount" => "Increased Electronic Equipment Pack",
            "DrivingAmount" => "Driving Holiday Pack",
            "FamilyAmount" => "Family Protection Pack"
        );

        $quoteBlocks = $doc->getElementsByTagName('PersPkgPolicyQuoteInqRs');
        $quotes = array();
        foreach($quoteBlocks as $block) {
            $quote = array();
            $quoteBlock = $block->getElementsByTagName( "PersPolicy" );

            $plan = $quoteBlock->item(0)->getElementsByTagName("com.acegroup_Plan")->item(0);
            $quote['quote']['type'] = $plan->getElementsByTagName('PlanDesc')->item(0)->nodeValue;

            $quoteInfo = $quoteBlock->item(0)->getElementsByTagName("QuoteInfo")->item(0);
            $quote['quote']['clear_price'] = $quote['quote']['price'] = $quoteInfo->getElementsByTagName('InsuredFullToBePaidAmt')->item(0)
                ->getElementsByTagName('Amt')->item(0)->nodeValue;

            $optionBlock = $block->getElementsByTagName( "com.acegroup_DataExtensions" );
            $dataItems = $optionBlock->item(0)->getElementsByTagName( "DataItem" );
            foreach($dataItems as $item) {
                if (in_array($item->getAttribute('key'), $optionKeys)) {
                    $quote['options'][$item->getAttribute('key')]['value'] = number_format($item->nodeValue, 2);

                    $quote['options'][$item->getAttribute('key')]['key'] = $item->getAttribute('key');
                    if (isset($packageNames[$item->getAttribute('key')])) {
                        $quote['options'][$item->getAttribute('key')]['key'] = $packageNames[$item->getAttribute('key')];
                    }
                    $quote['quote']['clear_price'] -= $item->nodeValue;
                }
            }
            $quotes[$quote['quote']['price']] = $quote;
            //array_push($quotes, $quote);
        }
        ksort($quotes);

        foreach ($quotes as $item) {
            if ($item['quote']['clear_price'] < 0) {
                BaseUsers::send('jayen@skedgo.com', print_r($dataPolicy, true) . "\n\n" . $response);
                break;
            }
        }

        return $quotes;
    }


    private function getTravelPolicy($sessionData, $id) {
        Yii::import('application.api.*');
        $requests = new TravelRequests();
        $data = $requests->getTravelPolicyDataValue();
        if (isset($sessionData['OrderDetails']['referral']) && !empty($sessionData['OrderDetails']['referral'])) {
            $data['CustLoginId'] = $sessionData['OrderDetails']['referral'];
        } elseif ($this->_getReferral() != 'Mytripinsurance') {
            $data['CustLoginId'] = $this->_getReferral();
        }
        $data['CustLoginId'] = 'mytrip.svc';
        $data['Users'][0]['Surname'] = $sessionData['OrderDetails']['last_name'];
        $data['Users'][0]['GivenName'] = $sessionData['OrderDetails']['name'];
        $data['Users'][0]['BirthDt'] = $sessionData['OrderDetails']['birth_date'];
        $data['Users'][0]['Child'] = 'false';

        $data['Users'][0]['Addresses']['Addr1'] = $sessionData['OrderDetails']['address1'];
        $data['Users'][0]['Addresses']['Addr2'] = $sessionData['OrderDetails']['address2'];
        $data['Users'][0]['Addresses']['City'] = $sessionData['OrderDetails']['region'];
        $data['Users'][0]['Addresses']['StateProv'] = $sessionData['OrderDetails']['state'];
        $data['Users'][0]['Addresses']['PostalCode'] = $sessionData['OrderDetails']['post_code'];
        $data['Users'][0]['Addresses']['Country'] = $sessionData['OrderDetails']['country'];

//        if (!empty($sessionData['age2'])) {
//            $data['Users'][1]['Surname'] = $sessionData['OrderDetails']['second_name'];
//            $data['Users'][1]['GivenName'] = $sessionData['OrderDetails']['second_last_name'];
//            $data['Users'][1]['BirthDt'] = $sessionData['OrderDetails']['second_birth_date'];
//            $data['Users'][1]['Child'] = false;
//        } else {
//            unset($data['Users'][1]);
//        }
        $data['Users'][1]['Surname'] = $sessionData['OrderDetails']['last_name'];
        $data['Users'][1]['GivenName'] = $sessionData['OrderDetails']['name'];
        $data['Users'][1]['BirthDt'] = $sessionData['OrderDetails']['birth_date'];
        $data['Users'][1]['Child'] = 'false';

        $polisy_user = $data['Users'][2];
        $user_iterator = 2;
        unset($data['Users'][2]);
        if (!empty($sessionData['age2'])) {
            $second_user = $polisy_user;
            $second_user['Surname'] = $sessionData['OrderDetails']['second_last_name'];
            $second_user['GivenName'] = $sessionData['OrderDetails']['second_name'];
            $second_user['BirthDt'] = $sessionData['OrderDetails']['second_birth_date'];
            $second_user['Child'] = 'false';
            $data['Users'][$user_iterator] = $second_user;
            $user_iterator++;
        }
//        $user_iterator = 3; // children start at 4, so we put 3 here ???!!!???
        if (!empty($sessionData['OrderDetails']['kids_info'])) {
            foreach($sessionData['OrderDetails']['kids_info'] as $kid) {
                $user_kid= $polisy_user;
                $user_kid['Surname'] = $kid['last_name'];
                $user_kid['GivenName'] = $kid['name'];
                $user_kid['BirthDt'] = DateTime::createFromFormat('d/m/Y', $kid['birth_date'])->format('Y-m-d');; //$kid['birth_date'];
                $user_kid['Child'] = 'true';
                $data['Users'][$user_iterator] = $user_kid;
                $user_iterator++;
            }
        }

        $data['Users'][0]['Communications']['Phones'][0]['PhoneNumber'] = $sessionData['OrderDetails']['phone'];
        $data['Users'][0]['Communications']['EmailInfo'][0] = $sessionData['OrderDetails']['email'];

        $data['Policy']['EffectiveDt'] = date('Y-m-d', $sessionData['start_traveling']);
        $data['Policy']['ExpirationDt'] = date('Y-m-d', $sessionData['end_traveling']);

        $destinationData = array(
            'RqUID' => '3812A460-38E0-4874-AB4E-A62F007338C8',
            'DestinationDesc' => 'AU CTI Worldwide'
        );
        $countries = Yii::app()->session->get("countries");
        $travelCountries = new TravelCountries;
        if ($countries) {
//            $travelCountries = new TravelCountries;
            $destinationData = $travelCountries->getFirstRegionByCountry(explode('|', $countries));
        }
        $data['Policy']['Destination']['RqUID'] = $destinationData['RqUID'];   //'3812A460-38E0-4874-AB4E-A62F007338C8';
        $data['Policy']['Destination']['DestinationDesc'] = $destinationData['DestinationDesc'];   //'AU CTI Worldwide';

        $quote = Yii::app()->session->get("quote");
        if ($quote) {
            $travelPlan = new TravelPlan();
            $planData = $travelPlan->getPlanKey($quote);
            $data['Policy']['Plan']['RqUID'] = $planData['RqUID'];   //'3812A460-38E0-4874-AB4E-A62F007338C8';
            $data['Policy']['Plan']['PlanDesc'] = $planData['PlanDesc'];   //'AU CTI Worldwide';
        }

        // Extensions
        $medical_conditions = 'false';
//        var_dump($sessionData['medical_conditions']);
        if ($sessionData['medical_conditions'] == 'on') {
            $medical_conditions = 'true';
        }

        $referral = $this->_getReferral();

        $data['Extensions'] = array(
            array
            (
                'Type' => 'System.String',
                'Key' => 'Source_System',
                'Value' => $referral
            ),
            array
            (
                'Type' => 'System.String',
                'Key' => 'ExcessLevel',
                'Value' => '100'
            ),
            array
            (
                'Type' => 'System.String',
                'Key' => 'TripCost',
                'Value' => '1000'
            ),
            array
            (
                'Type' => 'System.String',
                'Key' => 'TripType',
                'Value' => 'Single'
            ),
            array
            (
                'Type' => 'Boolean',
                'Key' => 'IsPreExistMedical',
                'Value' => $medical_conditions
            ));

        $extraPackages = $requests->getExtraPackages($sessionData['options']);
        foreach ($extraPackages as $item) {
            $data['Extensions'][] = $item;
        }

        $countriesCodes = array();
        $countriesArray = explode('|', $countries);
        foreach ($countriesArray as $countryItem) {
            $countryCode = $travelCountries->getCodeByCountry($countryItem);
            if ($countryCode) {
                $countriesCodes[] = $countryCode;
            }
        }
        $countriesCodes = implode('|', $countriesCodes);
        $data['Extensions'][] = array
        (
            'Type' => 'System.String',
            'Key' => 'TravelCountries',
            'Value' => $countriesCodes
        );
        // /Extensions

        // Payment Process
        $data['PaymentProcess']['PaymentRequest']['CreditCardDetails']['NameOnCard'] = $sessionData['OrderPayments']['cart_name'];
        $data['PaymentProcess']['PaymentRequest']['CreditCardDetails']['CardType'] = $sessionData['OrderPayments']['cart_type'];
        $data['PaymentProcess']['PaymentRequest']['CreditCardDetails']['CardNumber'] = str_replace(' ', '', $sessionData['OrderPayments']['cart_number']);

        $date = $sessionData['OrderPayments']['cart_date'];
        $date = str_replace(' ', '', $date);
        $date_arr = explode('/', $date);

        $data['PaymentProcess']['PaymentRequest']['CreditCardDetails']['ExpiryMonth'] = substr($date_arr[0], 0, 2);
        $data['PaymentProcess']['PaymentRequest']['CreditCardDetails']['ExpiryYear'] = substr($date_arr[1], 0, 2);

        $data['PaymentProcess']['PaymentRequest']['CreditCardDetails']['SecurityCode'] = $sessionData['OrderPayments']['cart_cvc'];

        $data['PaymentProcess']['PaymentRequest']['Amount'] = $sessionData['price'];
        // Payment Process
//        echo '<pre>';
//        print_r($sessionData);
//        echo '</pre><br><br>';
//        echo "<pre>";print_r($data);echo "</pre>";exit;

        $requests->setTravelPolicyDataValue($data);

        $response = $requests->GetTravelPolicy();
//        echo "<pre>";print_r($response);echo "</pre>";exit;
//        print($response);exit;

        $order = Orders::model()->findByPk($id);

        $status = 'Failed send';
        $status_message = '';
        if ($response !== false) {
            $doc = new DOMDocument();
            $doc->loadXML( $response );
            $msgStatus = $doc->getElementsByTagName("MsgStatus");
            $msgStatus = $msgStatus->item(0);
            $status_message = $msgStatus->getElementsByTagName('MsgStatusDesc')->item(0)->nodeValue;
            $MsgStatusCd = $msgStatus->getElementsByTagName( "MsgStatusCd" );
            $status = $MsgStatusCd->item(0)->nodeValue;

            $dataItems = $doc->getElementsByTagName( "DataItem" );
            foreach($dataItems as $item) {
                if ($item->getAttribute('key') == 'PolicySchedule') {
                    $order->pdf = $item->nodeValue;
                    Yii::app()->session->add("pdf", $order->pdf);
                }
            }
            $PolicyNumber = $doc->getElementsByTagName( "PolicyNumber" );
            $PolicyNumber =  $PolicyNumber->item(0)->nodeValue;
            Yii::app()->session->add("PolicyNumber",  $PolicyNumber);
        }

        //$order->scenario = 'set-status';
        Yii::app()->session->add("status", $status);
        Yii::app()->session->add("status_message", $status_message);
        $order->status = $status;
        $order->status_message = $status_message;


        if ($order->save()) {
            if (strtolower($order->status) != 'success') {
                BaseUsers::send('jayen@skedgo.com', print_r($data, true) . "\n\n" . $response);
                return false;
            }
            return true;
        }
        return false;
    }

    private function userAutoRegistration($data) {
        $user = new Users;
        $user_id = 0;
        $userFind = $user->find('email=:email', array(':email' => $data['email']));
        if ($userFind) {
            $user_id = $userFind->id;
        } else {
            $user->scenario = 'registration_by_order';
            $user->username = $data['name'];
            $user->email = $data['email'];
            $user->surname = $data['last_name'];
            $user->phone = $data['phone'];
            $user->country = $data['country'];
            $user->street = $data['address1'];
            $user->birth = $data['birth_date'];
            $user->referral = $data['referral'];
            //$user->subscribe = $data['birth_date'];
            $user->status = 1;
            $user->temp_password = $user->password = substr(md5(time()), 0, 8);

            if ($user->save(false)) {
                $user_id = $user->id;
            }
        }
        return $user_id;
    }

    private function getSessionData($sessionKeys = array()) {
        //$sessionKeys = array('countries', 'start_traveling','end_traveling', 'age');
        $sessionData = array();
        foreach ($sessionKeys as $sKey) {
            $sValue = Yii::app()->session->get($sKey);
            if (!isset($sValue) || empty($sValue)) {
                $this->redirect('/');
            }
            $sessionData[$sKey] = $sValue;
        }

        return $sessionData;
    }

    private function _getReferral() {

        $template = Templates::model()->findByAttributes(array('domain' => $serverName = Yii::app()->request->serverName));

        $referral = 'Mytripinsurance';
        if ($template) {
            $partner = Partner::model()->findByAttributes(array('user_id' => $template->user_id));

            if ($partner && !empty($partner->referral_code)) {
                $referral = $partner->referral_code;
            }
        }

        return $referral;
    }
}
