<?php

class SiteController extends Controller
{

    public $css;
    public $js;

	/**
	 * Declares class-based actions.
	 */
	public function actions()
	{
		return array(
			// captcha action renders the CAPTCHA image displayed on the contact page
			'captcha'=>array(
				'class'=>'CCaptchaAction',
				'backColor'=>0xFFFFFF,
			),
			// page action renders "static" pages stored under 'protected/views/site/pages'
			// They can be accessed via: index.php?r=site/page&view=FileName
			'page'=>array(
				'class'=>'CViewAction',
			),
		);
	}

	/**
	 * This is the default 'index' action that is invoked
	 * when an action is not explicitly requested by users.
	 */
	public function actionIndex()
	{
        $order_model=new Orders;
		// renders the view file 'protected/views/site/index.php'
		// using the default layout 'protected/views/layouts/main.php'

        $countries = Yii::app()->session->get("countries");
        $age = Yii::app()->session->get("age");
        $age2 = Yii::app()->session->get("age2");

        $start_traveling = Yii::app()->session->get("start_traveling");
        $end_traveling = Yii::app()->session->get("end_traveling");
        $start_traveling = (!empty($start_traveling)) ? date('Y-m-d', $start_traveling) : date('Y-m-d');
        $end_traveling = (!empty($end_traveling)) ? date('Y-m-d', $end_traveling) : date('Y-m-d', strtotime('+7day'));

		$this->render('index', array(
            'order_model' => $order_model,
            'age' => $age,
            'age2' => $age2,
            'countries' => $countries,
            'start_traveling' => $start_traveling,
            'end_traveling' => $end_traveling
        ));
	}

	/**
	 * This is the action to handle external exceptions.
	 */
	public function actionError()
	{
		if($error=Yii::app()->errorHandler->error)
		{
			if(Yii::app()->request->isAjaxRequest)
				echo $error['message'];
			else
				$this->render('error', $error);
		}
	}

	/**
	 * Displays the contact page
	 */
	public function actionContact()
	{
		$model=new ContactForm;
		if(isset($_POST['ContactForm']))
		{
			$model->attributes=$_POST['ContactForm'];
			if($model->validate())
			{
				$name='=?UTF-8?B?'.base64_encode($model->name).'?=';
				$subject='=?UTF-8?B?'.base64_encode($model->subject).'?=';
				$headers="From: $name <{$model->email}>\r\n".
					"Reply-To: {$model->email}\r\n".
					"MIME-Version: 1.0\r\n".
					"Content-Type: text/plain; charset=UTF-8";

				mail(Yii::app()->params['adminEmail'],$subject,$model->body,$headers);
				Yii::app()->user->setFlash('contact','Thank you for contacting us. We will respond to you as soon as possible.');
				$this->refresh();
			}
		}
		$this->render('contact',array('model'=>$model));
	}

	/**
	 * Displays the login page
	 */
	public function actionLogin()
	{
        if (!Yii::app()->user->isGuest) {
            $this->redirect(array('index'));
        }
		$model=new Users();
        $model->scenario = 'login';
        if (Yii::app()->request->isAjaxRequest) {
            // if it is ajax validation request
            if (isset($_POST['ajax']) && $_POST['ajax'] === 'users-login-form' && isset($_POST['Users'])) {
                //echo 11; exit;
                $model->attributes = $_POST['Users'];
                if ($model->validate() && $model->login()) {
                    if(!empty($model->referral)) {
                        $cookie = new CHttpCookie('referral',$model->referral);
                        Yii::app()->request->cookies['referral'] = $cookie;
                    }
                    $array = array("login" => "success");
                    Yii::app()->user->setFlash("success", "Successfully logged in.");
                    $json = json_encode($array);
                    echo $json;
                    Yii::app()->end();
                } else {
                    echo CActiveForm::validate($model);
                    Yii::app()->end();
                }
            }
        } else {
            $this->redirect(Yii::app()->homeUrl);
        }
	}

	/**
	 * Logs out the current user and redirect to homepage.
	 */
	public function actionLogout()
	{
		Yii::app()->user->logout();
		$this->redirect(Yii::app()->homeUrl);
	}

	/**
	 * Displays the registration page
     */
    public function actionRegistration()
    {
        if (!Yii::app()->user->isGuest) {
            $this->redirect(array('index'));
        }
        $model=new Users();
        $model->scenario = 'registration';
        if (Yii::app()->request->isAjaxRequest) {
            // if it is ajax validation request
            if (isset($_POST['ajax']) && $_POST['ajax'] === 'users-registration-form' && isset($_POST['Users'])) {
                $model->attributes = $_POST['Users'];
                $password = $model->password;
                if ($model->validate() && $model->save()) {

                    $model->password = $password;
                    $model->login();
                    $array = array("registration" => "success");
                    Yii::app()->user->setFlash("success", "Successfully registered.");
                    $json = json_encode($array);
                    echo $json;
                    Yii::app()->end();
                } else {
                    echo CActiveForm::validate($model);
                    Yii::app()->end();
                }
            }

//            // collect user input data
//            if (isset($_POST['Users'])) {
//                $model->attributes = $_POST['Users'];
//                $password = $model->password;
//                // validate user input and redirect to the previous page if valid
//                if ($model->save()) {
//                    $model->password = $password;
//                    $model->login();
//                    $this->redirect(array('site/thanks_registration'));
//                }
//            }
            // display the login form
            $this->render('registration', array('model' => $model));
        } else {
            $this->redirect(Yii::app()->homeUrl);
        }
    }

    public function actionThanks_registration()
    {
        $this->render('thanks_registration');
    }

    public function actionConfirm($token)
    {
        $user = Users::model()->find('token=:token', array(':token'=>$token));
        if (isset($user)) {
            $user->token = '';
            $user->status = 1;

            if ($user->save()) {
                Yii::app()->user->setFlash('success_confirm', "Email address was confirmed");
            } else {
                Yii::app()->user->setFlash('error_confirm', "Error confirmed");
            }
        }
        Yii::app()->user->setFlash('success_confirm', "Email address was confirmed");
        $this->redirect(array('index'));
    }
    /*
    public function actionTest()
    {
        $client = new SoapClient("http://btsws.atuat.acegroup.com/CRS_ACORD_WS/ACORDService.asmx?WSDL");
        //echo "<pre>";print_r($client->__getFunctions());echo "</pre>";
        $result = $client->GetTravelPolicy();
        echo "<pre>";var_dump($result);echo "</pre>";exit;
    }
    */
    public function actionReset()
    {
        $model = new Users();
        $model->scenario = 'reset-password';

        if(isset($_POST['ajax'])) {//echo 12312;
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }

        if (isset($_POST['Users'])) {
            var_dump($_POST);
            $userFind = $model->find('email=:email', array(':email' => $_POST['Users']['email']));
            if ($userFind) {

                $userFind->temp_password = substr(md5(time()), 0, 8);
                $userFind->password = md5($userFind->temp_password);
                $userFind->scenario = 'reset-password';

                if($userFind->save()) {
                    $this->redirect(array('index'));
                }
            }
        }



        $this->render('reset', array('model' => $model));
    }
}