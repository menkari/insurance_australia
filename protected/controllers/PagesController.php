<?php

class PagesController extends Controller
{
	public function actionIndex()
	{
		$this->render('index');
	}

    public function actionView($slug = '')
    {

        $page = false;
        if (!empty($slug)) {
            $page = Pages::model()->find('slug=:slug', array(':slug' => $slug));
        }
        if (!$page) {
            throw new CHttpException(404,Yii::t('site', 'Page not found'));
        } else {
            $this->render('view', array('page' => $page));
        }
    }

	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}