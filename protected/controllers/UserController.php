<?php

class UserController extends Controller
{

    public function filters()
    {
        return array(
            'accessControl',
        );
    }

    public function accessRules()
    {
        return array(
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions'=>array('createdomain', 'profile'),
                'users'=>array('@'),
            ),
            array('deny',  // deny all users
                'users'=>array('*'),
            ),
        );
    }

	public function actionProfile()
	{
        $model = $this->loadModel(Yii::app()->user->getId())->with('partner');

        $company = Partner::model()->findByAttributes(array('user_id' => Yii::app()->user->getId()));

        if (empty($company)) {
            $company = new Partner();
            $company->user_id = Yii::app()->user->getId();
            $company->save();
        }

      //  $model->scenario = 'profile_user_update';

        $this->performAjaxValidation($model);

       // echo "<pre>";print_r($model->company);echo "</pre>";exit;

        if(!empty($_POST))
        {
           /// echo "<pre>";print_r($_POST);echo "</pre>";exit;
            $model->attributes = $_POST['Users'];
            $model->subscribe = $_POST['Users']['subscribe'];

            if($model->save()){
                $company->attributes = $_POST['Partner'];
                $clean_url = str_replace(array('http://', 'https://', 'www.', '/'), '', $company->domain_url);
                $company->domain_url = $clean_url;
                if ($company->save()){
                    Yii::app()->user->setFlash("access", 'Your profile has been saved!');
                    $this->redirect(array('user/profile'));
                }

            }

        }

        $this->render('profile', array('model' => $model));
	}


    public function actionOrder()
    {
//        $model = new Orders();
//
//        $model->scenario = 'create order';
//
//        $this->performAjaxValidation($model);
//
//        if(isset($_POST['Orders']))
//        {
//            $model->name = $_POST['Orders']['name'];
//
//
//            if($model->save())
//                $this->redirect(array('user/profile'));
//        }
//
//        $this->render('profile', array('model' => $model));
    }


    public function loadModel($id)
    {
        $model=Users::model()->findByPk($id);
        if($model===null)
            throw new CHttpException(404,'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param Users $model the model to be validated
     */
    protected function performAjaxValidation($model)
    {
        if(isset($_POST['ajax']) && $_POST['ajax']==='users-form')
        {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}