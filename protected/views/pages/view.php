<?php
/* @var $this PagesController */

$this->breadcrumbs=array(
	'Pages'=>array('/pages'),
	$page->title,
);
?>
<div class="iteora-page-information">
<h1><?php echo $page->title; ?></h1>

<p>
	<?= $page->content ?>
</p>
</div>