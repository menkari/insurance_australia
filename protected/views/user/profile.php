<?php
/* @var $this UserController */

?>
<h1>My Profile</h1>


<div class="form">
    <?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm', array(
        'id'=>'users-form',
        //'enableClientValidation'=>true,
        'enableAjaxValidation'=>true,
        'clientOptions'=>array(
            'validateOnSubmit'=>true,
        ),
    )); ?>

    <div class="control-group">
        <label for="Users_name">Username</label>
        <input size="60" maxlength="255" name="Users[username]" id="Users_Username" type="text" value="<?= $model->username; ?>" disabled>
    </div>

    <div class="control-group">
        <label for="Users_name">Email</label>
        <input size="60" maxlength="255" name="Users[username]" id="Users_Username" type="text" value="<?= $model->email; ?>" disabled>
    </div>

    <div class="control-group">
        <?php echo $form->labelEx($model,'name'); ?>
        <?php echo $form->textField($model,'name',array('size'=>60,'maxlength'=>255)); ?>
        <?php echo $form->error($model,'name'); ?>
    </div>

    <div class="control-group">
        <?php echo $form->labelEx($model,'password'); ?>
        <?php echo $form->passwordField($model,'password',array('size'=>60,'maxlength'=>64, 'value' => '')); ?>
        <?php echo $form->error($model,'password'); ?>
    </div>
    <div class="control-group">
        <?php echo $form->labelEx($model,'confirm_password'); ?>
        <?php echo $form->passwordField($model,'confirm_password',array('size'=>60,'maxlength'=>64, 'value' => '')); ?>
        <?php echo $form->error($model,'confirm_password'); ?>
    </div>

    <div class="buttons">
        <?php $this->widget(
            'bootstrap.widgets.TbButton',
            array('buttonType' => 'submit', 'label' => 'Update')
        ); ?>
    </div>

    <?php $this->endWidget(); ?>
</div><!-- form -->