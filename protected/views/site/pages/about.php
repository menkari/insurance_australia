<?php
/* @var $this SiteController */

$this->pageTitle=Yii::app()->name . ' - About';
$this->breadcrumbs=array(
	'About',
);
?>
<div class="iteora-page-information about-page">
<h1>About</h1>

<style type="text/css">
    @page {  }
    table { border-collapse:collapse; border-spacing:0; empty-cells:show }
    td, th { vertical-align:top; font-size:12pt;}
    h1, h2, h3, h4, h5, h6 { clear:both }
    ol, ul { margin:0; padding:0;}
    li { list-style: none; margin:0; padding:0;}
    <!-- "li span.odfLiEnd" - IE 7 issue-->
                                   li span. { clear: both; line-height:0; width:0; height:0; margin:0; padding:0; }
    span.footnodeNumber { padding-right:1em; }
    span.annotation_style_by_filter { font-size:95%; font-family:Arial; background-color:#fff000;  margin:0; border:0; padding:0;  }
    * { margin:0;}
    .fr1 { color:#000000; font-size:11pt; font-family:Calibri; text-align:center; vertical-align:top; writing-mode:lr-tb; margin-left:0cm; margin-right:0.026cm; margin-top:0cm; margin-bottom:0.026cm; background-color:transparent; padding:0cm; border-style:none; }
    .P1 { color:#000000; font-size:11pt; line-height:100%; margin-bottom:0cm; margin-top:0cm; text-align:left ! important; font-family:Calibri; writing-mode:lr-tb; padding:2px; }
    .P2 { color:#000000; font-size:14pt; line-height:100%; margin-bottom:0cm; margin-top:0cm; text-align:left ! important; font-family:Calibri; writing-mode:lr-tb; font-weight:bold; }
    .P3 { color:#000000; font-size:11pt; line-height:100%; margin-bottom:0cm; margin-top:0cm; text-align:left ! important; font-family:Calibri; writing-mode:lr-tb; font-weight:bold; }
    .P4 { color:#000000; font-size:11pt; line-height:100%; margin-bottom:0cm; margin-top:0cm; text-align:left ! important; font-family:Calibri; writing-mode:lr-tb; }
    .P5 { color:#000000; font-size:11pt; line-height:100%; margin-bottom:0cm; margin-top:0cm; text-align:left ! important; font-family:Calibri; writing-mode:lr-tb; margin-left:0.501cm; margin-right:0cm; text-indent:-0.501cm; }
    .P6 { color:#000000; font-size:11pt; line-height:100%; margin-bottom:0cm; margin-top:0cm; text-align:left ! important; font-family:Calibri; writing-mode:lr-tb; margin-left:0.25cm; margin-right:0cm; text-indent:-0.25cm; }
    .Standard { color:#000000; font-size:11pt; font-family:Calibri; writing-mode:lr-tb; margin-top:0cm; margin-bottom:0.353cm; line-height:115%; text-align:left ! important; }
    .T1 { font-size:14pt; font-weight:bold; }
    .T2 { font-size:14pt; }
    .T3 { font-weight:bold; }
    <!-- ODF styles with no properties representable as CSS -->
    .T4  { }
    .bold-font-size-fix {
        font-weight: bold;
        font-size: 12pt;
    }
</style>
<!--formatting-->
<div class="P4"><a id="__DdeLink__297_935328195"/><!--Next '
			span' is a draw:frame.
		--><span style="height:1.561cm;width:7.117cm; padding:0; " class="fr1" id="Picture_1"><img
                style="height:1.561cm;width:7.117cm;" alt=""
                src="data:image/*;base64,iVBORw0KGgoAAAANSUhEUgAAAQ0AAAA7CAIAAADuJEhbAAAAAXNSR0IArs4c6QAAHmdJREFUeF7tXQd8FNXWvzOzvWfTO0lIQgIhhISE3iFBgqGFpqKANAWx4Hv4AEG6jWLhEwVUUISH0hQBQRAEVERKIHSSkJCEkL69zc53ZjfZbOruhgR5Mve3P9hy59x7zr3/e+pMMIqiENMYCTASaFICOCMfRgKMBBxKgMGJQxExHRgJIAYnzCZgJOBYAgxOHMuI6cFIgMEJswcYCTiWAIMTxzJiejASYHDC7AFGAo4lwODEsYyYHowEGJwwe4CRgGMJYA+YjzeYqVyN6baWvKk0ZBmJ+ySmNGNKCtcizEzRUWc+RolxSoyZvVhUKAeFC/AwAStIwOLgmOPZMT0YCTwaEmgmTm6pjL+WG48rqQta7LaJUFE4IghkLYFpuBAGQ1ZcmM0ijAxjmTvxqD4S1EvOaStkPxqiYGbBSKBRCbiGkzsa0/dFut0K/IyOUCEWvfVpaFCNYKORUTELYuBfihIhsgvXNEJGDfPktRGymIViJPBoSsBZnJwo1W8u0H+v5ZYBPJqBjca4B7RYYONGmVIFhud92b09eI+mpJhZPc4ScIyTw/e1a/KNBw0CCsMRZXZNdTgvWhowOGYmk/mGV/2IQZ4MWpyXHdOz1SXQFE7OV+iX5hp3a7gIx8GvaPW5WAewjDWcr18YxO7sxn1IgzLDMBJoUgJNxYV/KTPu1vGtzvfDE6NlrD16/tEy48MblBmJkUCzceLGxjCSBI/7b5AhaZYSf8OwzJCMBBqUQFP6RMjCOTj1d8AEY2NmEfcfGP66oaWOVlKlpmbuRgi5/3U7/8TVnAqNtpkkmMuaJYGmcCInkNBG1OJn07Gp1noBcQt9aBgSYEhOPLo3JG8tNr+Ubd5R4po5elZNJV8hR18mX8wyNY+3b/+4lPbmhpFvfPz2nhPNWm7momZKoCmceHMJKQZ2F53r4Bh0HFKHWHhrvCDQRVBGjNKzTAYrH1JEerH/DnvPCTHmG6j38swf3iGX5JnLXdEMdw0oR0OVk9QZFWoeUK7mF+cXl5dWKK/lFUG5A9MemgSaincV6kzDM3VnTHxEYD53s9I3rQy8eUXDlZgh9d4ijaLYRgNGGhVuEo3c28sk2D128vnYTshIxhH6ve15gfwWGqhFZltNJNdAPXGFzFRSEWLsZAeWp9Pm4XEFNeiyyWhE3dyxXzuwmsHbJz+dmbl6G9Lppo0dtGHmqBZliyHWlASawglJURMvq7ZphchMIjZLXl4w7It1A7dt4lFqLRI3W66Qh2cjPQvpVUh8p21kaURMIMnrkVP8+dixnz+ZaoKTFsNHcVXbYsSPZg0Y4GToFfKykmonxk64ghMNiQ5Xmu8ZUYIIxQubU4FarFAfvnBDbTD2ig5p5+fZ7CVgLnRVAg7yjCtvKv5TDjixGOIsNhKg0Izzoz9e3unkYSPiGbg82PTODYlhZjPbqGfDYcgWZEXFZPToa/Jr2zmvPPX3c2F5uXP+9crXw5KRykTnMQlikUixOErmHOWH3euuRZ9csuAE9Im70/rkYU+UGa/lJOAAJwfuaZ64w6mVg+dB2aK516Edwz953y/nho4QkYCfxtGCkyTbBNoD/BvRncjoPwek3Ejq5a/Dhv/ye8qJ08F5d8vFoudWLPqpTzdUWZ0wwfE9/to0f5GrbGqNJoOJlPLp7GRRpUqtM0gEPA+xAD4CmosrVRq9kc0i3MQCAbvu7lbrjUao0eRyWPUKmYGmxmDksVnwAlKFRiolk8xQUtES7HQHVv34tdqMSApJLHZVkRFpzJQHC4kJ2t0yUPARSXBkP4iRQnAJ0IEeChKBAwO6Rs7CGlQ5oExIyzxxa8zD0nRGE7xkArqIoUylrdTouCzCXSKEfxuTYYlSA/KBpK5MyBfz6uZzSTOl1Bl4bMLKsq2BKGACEh6HgCvtmkpvgMJzK53CCqXeaPKSCAVcjq1LhRpmpYePUiHPOs/6rVKrZxO4gMMGQ+ZehdJoIsV8rruIXr7GWqlKo9TqCQyHnjJhUzUcIBOYAExSwGN7imviU00Qt//JAU7uak0dLpoqMajpstMbEJgSEaKy8iHb1g/Y/rms4r4OE5BsNmVZOaAIqoMwmcC4go8KicedqA7neg+Cl87Tt++FzGe/29fz7DmOXg+bpUQmnbz0zZNd4pCyGiQYJkSmjPZ4qKhGys4wU67Szt24N7uobHDX9pSJ3HrsXH6FMtxdOmd4794d2q7Zc/xIxs38SrWYw+7Sxue5Id2GxUfZyN4uKpv/+feFpZX9YyP+NWYgn1OzOe6VK1//4ofc/OLOkcErnx2SS3HW3CV3llGlBsqNi03wwCE0JyLQHB9MyqLZv6NHs7JMOhKN98LAcf9vCZWjo+b644sDCcDMvBxTtgY944NP8a7aZ4CcBbnk75VULxkWzse23Tef01Kw3RIEWLoX/rRHre14IafwrS0/qjS62en9n6yef7lGu+Dz/VdyCwckxbgLuFt+PnutqBQOi+6hAdNSu/dtH1ZHesVK9f/tP73vTGZWSQWPze7o7zm0a4cZyUnsarfTTFHr95/aeeyvtm181z6fBlvQSgGOkmXfHDp58WafxOgl4wbbgPpXVsGiLftJkpw2rNcfN3K/O5lRUqF8Ma33svGD4aq7ZZUbDvx+4vKtK/dK4WOUl3tqUvT0lG7S2mg5dunWsq8PSt3EE/rGH7tw48dzNxQ6fZiHbHDnyFmpPX2kdQ/NvNKKTw78fuTCjVvF5Swcb+ctT0mImp7SVV4PV2Csbjj429GLN6/eK4UzJkgu7hsVMimla0ygjzP7ytrHcX1X3/Pq40Ze3ZQ8oAZOGgHmlZU9YNcXCT//4JGXx6LovU5iLK1QXOofkBXV6Vrn7pcTe1SEBrqV6sbvPzBh3/7o21lmHFPz+UKtNt/La9LyRefat0Mqu9Q7QXSllKe6SOzPS2f4uZxb1HPu2soyJZIIkcGAVDp6hnADjIc03Ncj41o2MmPIZEQ4AbxI3CQ7F04e3LGtlfKd4vIur64tzi6QB3gdfXt2bHCNBLefvDh+8adIb0zul7B34aQPStj/umoE+5NuIASId8G/GPotjugqovc0OOt9L9JRMBYLmcBchbckeiYA3xJBXFBTcRdMyIDS/PA9kVUnvZJEMRdNd5QUgmMB+oMkgAy8KDqy+FE4Md2rBiobj/w5deUXyGB844XRKyy7ENrVgpIBr39UWHgfyUTIaEKVagQ4h+HNlNxdunvBpN7tQ20ChDN7xsc7t/94GoGqgbMPJg+qDcfnPTNkxVMpVg2lN5FpSzcd+ukPXoD3hQ9eifStcoRySyuT/7P+WsatgNiIGx++xq9WNVtOnH928UbEJoSeMnWpAukMSG8YNqzXvjeevXmvZNK6HafOXqWD/lbrnT5MqfTkrp+9mG7V/Na2dMfhNz/+FokFHLHQUFwOkweCiMBB8iMGJ302K91esWTfLx/z7tazF2/R3ay2DK3fqNS+8V+/Mk7Cr1EseWWKaR/+9+CvF6oXDJjFEGkODfXbMe+5hDB/Z7YWTd5hv34CU1Vaw74rMGsyIYXxvl/IN6+9tXDrz6s27lq/csP6FZ+8t3774q8OLfri4MZVH5ycOE6CcZe8+8mpcc+sfH9t29xcpVCgEgjEavXtwID0NavORber0STV9PuKgBeXg8I4jglEfAQHj4mUySRzJ6fOn5TqHeSjVWszbt0F5yoiInDVC+kjB3aBxVBUKNfu+UUFK2ppwZ5uMRHBSCYu1+gu5hTYM3rk0m0EyykW9O4UzmWzQ7gUKG3r5GARZTAgH0WJkbdFmdBfwv8ATzYNEikbPR+ArYrAZ/nRcgae6FsH4Hixkzp0py00yz04cKC/EIyvbkv0l8P9PDSF9++SEDawzYfDZrHgFJAI7TUeARpYxEMSYNwsk4rmTBr27uyx3TpHIiGvrFy5cvdxe3b+e/Li9p/PIhG/fXTox689tWzmKI8AL9jBGw+czswrsvXkAcsSoUjEI+x2CAwkACUgFkqFXHv7grbuxHz4Xl2h9PJzn/108vuvP71o7ECgtuHwn6f+vIoEvLDwwCXTRiyemhYIW5PH3Xn4z62/nLWfGMgWRsREAoNSndQl+r2Xx74yfrDE0w2JBLt/vXgsM8vWGdD96pc/0CDhcRISIt95cfQbk1L94Wjj8344fv7DH3+zJ/vpgd8OHj8PE4iIClk8bcTKmaPi4yJgoKycwje27K/Q6Bzuf2sHxzhJ9uASTdR3GYyw0TV86dX4nqdSx5waNjaj+4DCoHCjRBCUXbhq8Ye/PDd1ztZvZEpluVii53DANoP3GRHh6avfvhEcTGuS2ogAm22Ip2sWVw2rsKP0BrGQt3n2mHcnDl02IfntZ4bgoEAoKiLI+9u5T/97ZN+tc8b2iGoDJ+7x63dy4NyqbskxYbCRKSN55Px1A12tQ7dytfZ45m045rlCQWLbIPhmpBw7FsMK52Bw8IdwsBPR7Cux7FMxrBCIaNg3MwrlY/ujWJ+Gsf4dQCSKHMMe0kWfh+EfhxCv+OF7I4mhbnDyoZs69AeoGvtGK4F6sRP4wmCUiQVbXh6/dlLq3LTe216Z0KmNL1D47XrO9YJiKwGT2fwrzQ5J8Lgbpo94IaXb/FH9XhnWE06WCoUajM96AzW0ixp7ILXBGBrke2TR1A+mpL06om98qD8cQ3/A/tYbOBz25lnpC8cMWDR20K65Twf6ugPdTw6dAa+m9oiI0ukHJEQdWDjptbQ+qycNWzEhGYPTyGT69fJt24231wrun4bDC8c6hvhtf3XC6yP6giZcP30ELRa19nDGTRvNgnLlztMZsNYw4r43Ji4aM3DeqH4/zp8UFeSL2Owzt/NLFOoWw0m8GzcGB1+iSUSRJqQ10pseXmCbY2j69l0/TZs1c/u3HIOhXCI2suC8pVdXplCcjIsds3pVrrcPUtcFCYzSDtcnyptdJozBdukc4j8oNtzKf6iPnDavDcakdiExQd7wDTiX/TuEgebVaPT5ZZU2MQ1Pai+SiUH6Z2/nWz1OaOezC4oASyZTlzD/zqF+8A1IwZeNeBZhwM4O5CI/DnKz+Oh1cDJYivUQO60WzSiCh9Lcq4QMDs9TnhaaFMqpmoujBTWa2gX7psRFWvu18ZQNjm8HZolOrYPspPVLWACSBOMHY+OER7XFP21g4spZoz+YObpHZLCjMZr4HWxa05OJ0TFBNSYraPgof08/L9pz6NmujfXiuDD/XnBOUea8ckWpQlObIgXrMq5XJzdhle8+qmsHuZsE5n0rv1ilrzLOz2cXgi8Kh1pyl+gwL7mVApx9k/vHJ/eLnz4oyUYTzOkbAH4KgS8aWR1G95KI4tr4gCCMFAVhCSdZdqxP2Dg2StqQ6dXgCBiGs7F316x9/501EpW6XCohiZpNJFMoD/bsNuHd5felchpX9Q9ZDB8pNvLgCGlmo6EolgiE3Kp7ic3WrHXtIjUuh03bQGbKANZ8dQv1kkf5ecCn7JKK/NIq/FzOK4KgEHSOjQiUg1FnaZDgsdarAGkIVTXcKORagoQCYwyeKFAjES5dxUOP4XzmHqwy+zCUgMcB2INTDlE+6yTZOB4XFgCugk6rf2nTvit378OXHmLhvJH9Zw7pZg0MPkir421D5GrFpNR97720ceYoW3wP1ILBQG8nykwZwXS3b7BYBO4urnHZuWyWgF5KCiKZoAytfSHGZYSFI3A/eU0Sz13IXz8r/cDS6eN7xtpIKrR6Cs4FDHmCsVrdQOtoLRNwrOLt5ubUjhzjzRWAxnCGsoA17sdDM3Z8VyaRQAgWogRVY1GUXKH4bnD/ycsWK7giCGQ2BBKMRxkn+D7oHVq0Z2e3fa1v7R+XUfPejiOCwHvBgUpROoX6yPkb1ouOZ2aZ9UaCy+kUTCsTl5prtV/1Iuu2y11ZTuCshvOqM8LqOVe3p/rFd+sQijTan37LSFn02eIdhyFg6hJfTXS2bWVbHw+RID7ED7bp3dLKz4+dnfHp7kFLNh+6fBschsbMNxLuBaxuAHIrR9VPV7AfvO525NI2S61mCb/SgQqL/Zy17NujEz/a2Wfxp4cv30JcNoDN+b9p4hROIqS8YXytA9OLnhOG68kJBw7puFxrjNjaAC0ypWrziCenLpqvIWgrqOEaZJx4glBHSx8UJ85mPuvJNDUxGufB6Ni+c9fgR4gjnb+eC28CvOUDwHv5RzRfqWjD7DHdEqJwHM/LuwdR5v4LPvnx3PVWYg5SPbv+uDxi+Rdxc9dNfu/rDd8e/eXUBSVYTS1R+uTULodOAt6nR88OWPjpwo17t+498eupDFVxBQTlQj1kkiZTLvYycQoncMFsXwKnFV+TpxtBeFRUtCkoMEDsorrhFCVVqddOHD/n33NJM04HLhtpoIlf8m+uB98S6xzm6x7oIQWVcq2wBOz4nPvld8sUwHFYgFeIl1tLjPBI0IgJ9jm4aMpHs9IjQgNgQueu5ox/e8vuM5mtMbnPDp9JX/HlnhPnSwpLIVTt7iWPiQrxdpcgME8eWsNxnUpDGo18idDX3zO+Y9sxg7u+PXPUt/MmBsolTs7CWZz08BINIRR0KLSJhmF8nY5rMNqUCUGSEAJeNm3yotkzkIGEuEqjV+PEYI6mj4/LiVIn+XSmm7dElBQeCEHSygrlmVt3bxeUGOE2D4LoHkFHuv4ZDUIXWUVlYi5nZkq37xc9P3f0ALaQp6hQLdl2qBSq9lq03SmteH/3L2YIaon5o/on7Jg3ce/CKXsWTOkfHYLUOmeseJemYyLNUGSgBWvFvsEwekP3DmGbXhm/d8HkfW9O+eHN53e8On7KwC5+bhLnH17nLE5g6Pn+BAtyZk3yBxFki9qhG8tk4hkM/3n5xXenTkRqEkIZTbBNYNRCv2ZU0LokSQedwWuEyAxEDMG/33j0T8j1ggknkAhGJrVv8ErQrS04Y4shXTOOLfrhZP2cM4L4Kyu/17yP+722bi8k/hCK8HV/Z1LqmN5x4Bxl3Su9mF2VOKJnUW3TQMzKRpl+b/nNmbGgz+U7RbeKSsFcH5HYfsvL48Z0i+kRGRTqJaMJOEujgaGqJ1TX7Fqw/afus94b8faWUmVVtJceBHrpDSmx4ZP7Jwzq2DYh1N9HJlqx8+fOU5b3nv8JxGyc5MUFnHTzlT6DV9CJ3sabGcodIZQB96sYjThlfvmN19Y/lW6pbmzSrWWxnsLLe3g9aLzFSZ6b6Bbbxs8XoigUtfP3S0evZkNGP9LfC760vwSCwpA+gQap9Aoo5GqRhqESyNTbCakCsowW2gK4pbSFmkpryC4oyS0oycytSikCH/EQAcNxlcEEhWHWcTgsQkpXQGE6rRb8b9vg9xWqfEi3N7kB7GdKH+0gH4oK8ZZD7Mv6U15pJUTekV3pl6vMyUR8FmQ2SfJWYYntWogaHzl37WZ2wV838yq1VZEJUJu0U40TGbn37EfZf+FGbknF1cJifXUk0OEcXMAJ0HorUupF0nHSRuhSJhZhYrF4Oh2Eg6cvnv9V2hNIYSkBbqJhuAdlXBLR/EJ9h0w63yHU262NjzuoPqVGrwSDwWzuHOJXpzDSnYXJYdExdFdPrSs039BRhUZL/cSDNBxl66m38kgoCdOa0XUt2lBEUxSwUKzQlYhXk3OI9PcM8veCPfr18XPnsgshPJpdXP49eCZms5zPDQLfrLrFhvhCYQtUkn6w/xQUiUBYNru44p1dx4vAYauOuTtkF5w9qByA3bL798unbuQCEUhlvrz5+5sQj+axnfLCGxqjU7CvlCaL7zt96eCFm1B1Bgjf9PMfmXeKoAQhyNc9QF7FSJivR3s61k/t//PqVycuQFoMIsV7/rxy6U4hcAF+vFuTRZb2g7uGk0ARZ4mHrtHAlxnRqVeTCeJdzy1ftG9gHyhscaxiCWKJuCxY3OzcYgOytG1aV3cvlLL27hhuMYAsLxYxIrFDnQHA1kqWWZIbCH1QaE69Qg6/YvpVWVdhNjF0Yz99VGBOvWp68hqZes1Ep+FJNECGJ1rKxqqapSCrfquKfdf+oUYI1e+8pMIRidHgQ1+9c2/Uqi/Slm8etnTzMchtIywpKqRjsK+NQGpcuzYBnpAn+u63jNSlm9OWQc9N2w6foUvmwF5oPGtkP4XYYN+hXaLgrrvswpJRq74EIqlLN+06fo4u3KqHfcckq3u08/fsCeFHioIoy3Prtqct2wRzm/flAZ0ldTG+RydOtcbzlgqfHZAAp55Wp5/+f989uWwzvJ5Zs10J+U0zNXNId/u8StOwdw0nQGtaqGQ4XtmI8qXA6MoO8Bv37oojPZIsIHHUWOwnUemMdi5UbjZGERJHJOQEIZVOkjaFB7I1glrQ6Ax2KS0zeEqQNNDpG1zwlE7hdH043JqmM0QEeHeJCKw/4gs++HRfjHZOzOimijpTTtmy5vQGAMiAH1dv5enJWCojG9hnFHJnY24cdLGCOlJivqWiQdJRgr0djNsXg8H2IrV64NFU294jwX7Q6EkTMF6zAaE2Htg0afW276Bq7o1R/Qf36Ij0+pys/MMnL2ZezYE+sdFt1k5Nsy+hj/DzWDdtuAcEpnSGK9dzDp+8kJl5OyjQyx9KFipVkHmwL8CjdSk9K12dzQ8Elz6dkhjbFmoZiwpKDp+6eO1aDhQ9+LqJkUKjhXWxu4BeC1g7be1KFoTRuWC1DgrAbZ2hOvjD59O6QHGn0ViUX/zzb5dO/3VNV6kC8M0Z3X/WkG726/XCEz2mj+oHPrOmrPLEmczjf2SqSiogBjtzRJ9ZT3QnnH4YPLF48WJHe7nW78BaLwm+s9igwKEYrvYhSieq8YM9e1xpG4Y0ToCEIAKQYVckS+q0Km9iqlCnDTL29/d8PjkJCoStPWUCPtxR4uPvObFfvO1LIZ+rxbHkxPbpPWKFkLSu3bxloutFpZdyCmEz9Y+LHN87DhamTh+oWBnqhqe44WF8FCjAusmwpz1xmSXNBfeZQDFGtAhN9sb9rX5MdQPFAJtezsOm+WCh1fVgUOW46b75vg7FirCt4Swe3KmDo2A+9rwPvjqUCOXWogBzM+F4VHjgU73j/KtjmiIeBxj39PWYOjgpGooaqxtkso0Ea1BC1DN9Owur/QHonJbUIdjPA37iSkThIb4zhvZYPeXJIPcao8tKAAo9hiW2ZwMRFsvTWz5hYOKmF9Pb+rjjPM60wUmdQ2sqbYU8tgnDOka2mZrc1b12Ul8u5I/o2sFTLlFTSOouTUmK2TBj5ND4dlCLM7p7DFTZ2Ir53SRCBaIGJkSN6xlrq+QHpHEJlthdMiWla6cQP5ssYK1h7YJ93MHO50lF/n4eQ5M6vD9p2IzkrlCSYC9zDkE80bldl4hgCooVhHwfX/eB8ZHvTR42O7Vn/WVtYnc5rqtv8OKjd8uHFgp1ZigxrA0VOLsghVSnHqFBEjjOpQw/+OsHBshcAupD6Lzgm5+Wf30QVPOaF0a9nNqzVUdUkaj7ZdMlBZUgxY51YAGWQOcA4lo8bFqfC6hnsVSFOGiQKwTVBM69o44OfoebtyCi+IBE6l8OpgJgow48GhwFJgBIg+qeZszBZbvLOkb/ALfVIgj51VtP0J7OgMQCp9WiikcQJLAnfoSIsJH08nLrE1N1g0ozJNuMS6ylXOzqxzM1g4JLlzgDEiAINWMPDhKg0xogAbI8cEecKwiECTQPJDBKM3ECV86M8p0vKrcUILgekCGI/0i1L7SvcRxdWuDW6wwVSlCYfQvuxDCbYwK9IcPQemMxlP+HJNB8nACTy6Lc5+D3rLcNutC47Jeo/OVtq8pvXbiwNbuCoz9jw67Or65Nf/9rpU4P91t1bR8qrI76t+bIDO3/AQk8EE6Av7WdfV8WquhIsTMGteVZki8JNOu60JVFj1SDkocvjv516UqOqlIF0Zi4yOCpAxMfwgzB1NKDi2dGulb7kxkPgYt//BAPihMQ0Jp2ojdlaku+oUkDzAKSBW7adZF/f969/rqCsR7u58GTibzl0pEDEr6c+3SwXd6t9fYB1Kf4chDEmOGWr0f1CZitx/3/DOVmxrvq87cxW/FSMV183/AfgcBxHmVa46mfEepsheZDFiGc67nF5UqtAQDjA08uelgWF4wLDwSDJ3N7s5EP/IGAh8w2M5xzEmgxnMBwx0v007JMNzA+ZPpqjY4TYZj+0zaov+ej5ZM4JyKmFyMBJ55L5JKQ7mmNr93UbdPSVT20YrGcj+kC/Zq2XH++i+6+SwMznRkJtKYEWlKf2Ob5VZ56wT38DuL6I+NSL8Ok4EeixrE1xcjQ/odLoFVwAjLL15p2FmiG+/DbMH8e/h++hR4L9loLJ4+F8BgmHxsJtEBc+LGRFcPo4ysBBieP79oznDsvAQYnzsuK6fn4SoDByeO79gznzkuAwYnzsmJ6Pr4SYHDy+K49w7nzEvh/Xs7incHtNQkAAAAASUVORK5CYII="/></span>
</div>
<div style="clear:both; line-height:0; width:0; height:0; margin:0; padding:0;"> 
</div>
    <p class="P1"><span class="T1">Our Plans</span></p>
    <p class="P1">Select the Mytripinsurance plan that best suits you. Compare Mytripinsurance plans to find the policy that is right for your travel plans and budget.</p>
    <br />
    <p class="P1"><span class="T1">Overseas & Domestic</span></p>
    <p class="P1"> </p>
    <p class="P1"><br/>
        <span class="T3">Prestige:</span>
    </p>
    <p class="P1">Our most comprehensive travel insurance plan, which has been developed to offer you with broad travel insurance including extensive cover on your trip with high benefit limits.</p>
    <p class="P1"> </p>
    <p class="P1"><br/>
        <span class="T3">Comprehensive:</span>
    </p>
    <p class="P1">Offers extensive international coverage with lower limits than the Platinum plan at a competitive price.</p>
    <p class="P1"> </p>
    <p class="P1"><br/>
        <span class="T3">Essential:</span>
    </p>
    <p class="P1">Provides mid-level international travel cover for the essentials on your trip abroad, but without the high benefits so it’s more economical price.</p>
    <p class="P1"> </p>
    <p class="P1"><br/>
        <span class="T3">Domestic:</span>
    </p>
    <p class="P1">Suitable for frequent travel within Australia.</p>
    <p class="P1"> </p>
    <p class="P1"> </p>
    <p class="P1"><span class="T1">Why choose Mytripinsurance?</span></p>
    <p class="P1">Everyone plans their trips with the best intentions but sometimes the unexpected happens. Bags can be lost, accidents can happen. Mytripinsurance is designed to protect you when you’re travelling – and be user-friendly should you need it.</p>
    <p class="P1"> </p>
    <p class="P1">In just three easy steps you can get a quote and book your plan, so you can get back to the exciting part of arranging your travel plans. We know how to help you when you need it the most and our policy holders receive:</p>
    <p class="P1"> </p>
    <p class="P1">• Quality travel insurance at affordable prices</p>
    <p class="P1">• Straight-forward, fair and effective handling of all your claims</p>
    <p class="P1">• A global team of travel experts that put your health and safety first</p>
    <p class="P1">• 24-hour emergency assistance from a team of experienced travel assistance providers</p>
    <p class="P1"> </p>
    <p class="P1"><span class="T1">How can Mytripinsurance help me?</span></p>
    <p class="P1">Mytripinsurance was designed by travel and insurance industry veterans to offer good value insurance for individuals, families and businesses of all sizes.</p>
    <p class="P1"> </p>
    <p class="P1"><br/>
        <span class="T3">We’re here when you call</span>
    </p>
    <p class="P1">You can be assured that when you choose Mytripinsurance you will receive the best possible service as swiftly as possible. We are committed to providing our customers with excellence in handling your claims. We understand you want to protect yourself and your travel companions, and are here to help.</p>
    <p class="P1"> </p>
    <p class="P1"><br/>
        <span class="T3">A local presence and a globally partner with strong financial strength</span>
    </p>
    <p class="P1">Chubb has local operations in 54 countries and territories. And in many of those countries, we have extensive branch office networks. While we have a global footprint, we’re really a local company everywhere we do business. Our people are local, with local knowledge that informs the products and services they provide.</p>
    <p class="P1"> </p>
    <p class="P1">All of those products and services are backed by our strong financial strength. Chubb is one of the world’s largest publicly traded P&C company, and are rated AA- by Standard & Poor’s* and A++ from A.M. Best. When you are insured by Mytripinsurance, you’re insured with confidence.</p>
    <p class="P1"> </p>
    <p class="P1"><span class="T1">What you get by choosing Mytripinsurance</span></p>
    <p class="P1">We treat all customers as individuals, not a policy number.  Working with one of the world’s largest insurers, Chubb Insurance, you can be assured that we have arranged the best underwriting service to back-up our travel insurance products.</p>
    <p class="P1"> </p>
    <p class="P1">We want to help protect travellers from the full range of risks – from the basic to the complex.  With high standards we have developed the best possible insurance coverage and service to cover your trip.</p>
    <p class="P1"> </p>
    <p class="P1"><br/>
        <span class="T3">a. Product</span>
    </p>
    <p class="P1">Travelling without insurance is a massive risk – and could be very costly if something goes wrong. There is a whole raft of potential mishaps – from lost baggage to cancelled flights, or at the worst case if you or a family member gets sick or is injured.</p>
    <p class="P1"> </p>
    <p class="P1">Mytripinsurance provides cover for risks including medical expenses overseas, cancellation expenses and additional trip expenses due to a variety of circumstances outside your control, including flight or baggage delay and personal accidents. You can choose from <!--Single Trip and Annual Multi-Trip plans for--> international and domestic trips.</p>
    <p class="P1"> </p>
    <p class="P1">Mytripinsurance includes access to a range of online tools as well as a global support network of medical experts who can assist with a referral service if you become sick or injured overseas. No matter where you are in the world, all you need to do is make a reverse charge call to receive 24 hour access to our assistance hotline.</p>
    <p class="P1"> </p>
    <p class="P1"><br/>
        <span class="T3">b. Service</span>
    </p>
    <p class="P1">We treat every customer and partner with the highest regard and aim to offer the best possible service.  Our claims service offers exceptional quality and dependability. We understand that when you make a claim it’s usually been a challenging time for you, so we try to make difficult situation as stress-free as possible. Our claims team works to pay all covered claims fairly and promptly, while navigating coverage terms and complex laws and regulations.</p>
    <p class="P1"> </p>
    <p class="P1"><br/>
        <span class="T3">c. Simplicity</span>
    </p>
    <p class="P1">Mytripinsurance understands that insurance can be complicated – so we aim to simplify the experience. We aim to instil a positive attitude in our team to ensure that you benefit. We understand we need to be accountable and efficient, so you feel protected even when you’re far from home.</p>
    <p class="P1"> </p>
    <p class="P1"><span class="T1">What to do in the event of a claim</span></p>
    <p class="P1">By submitting your claim online, you receive more control and convenience.</p>
    <p class="P1"> </p>
    <p class="P1">The Mytripinsurance portal enables you to process your claim online, and receive ongoing updates that aim to reduce the steps and time to completion. You can opt into SMS updates or just simply receive emailed updates.</p>
    <p class="P1"> </p>
    <p class="P1">To ensure your claim is handled as efficiently as possible, please follow the steps below:</p>
    <p class="P1"> </p>
    <p class="P1"><a style="color:deepskyblue" href="https://mytripinsurance.com.au/pages/claim">Online Claim Form</a></p>
    <p class="P1"> </p>
    <p class="P1">For further questions on your Mytripinsurance claims, please call Customer Service at Tel: 1 800 061 568 (Mon-Fri: 8.30am to 5pm only)</p>
    <p class="P1"> </p>
    <p class="P1"><br/>
        <span class="T3">To submit a claim online you need:</span>
    </p>
    <p class="P1"> </p>
    <p class="P1">• Your contact details enables Mytripinsurance to validate your claim. You must have an email address to submit your claim online.</p>
    <p class="P1">• Supporting documents vary based on claim type, but any relevant receipts, photographs or quotes need to be attached to your online submission to accelerate assessment.</p>
    <p class="P1">• Intended payee information allows Mytripinsurance to quickly make approved payments.</p>
    <p class="P1"> </p>
    <p class="P1">You may either click on the Online Claim Form to file it online or download the claim form and send to the below address.</p>
    <p class="P1"> </p>
    <p class="P1">• Advise all claims to us within 30 days of a claim event or as soon as possible.</p>
    <p class="P1">• You must submit all supporting documentation, such as medical reports, police reports, declarations, receipts, valuations or other such evidence we may request to assist us to promptly find a resolution of your claim.</p>
    <p class="P1">• For liability claims, do not make any admission or offer. Request the claim against you be put in writing.</p>
    <p class="P1">• All losses under baggage, personal effects, travel documents and money must be reported to local authorities and/or your transport provider and written acknowledgment obtained.</p>
    <p class="P1">• In respect of medical expenses overseas, you should submit claims to your private health insurance provider prior to lodgement with Mytripinsurance.</p>
    <p class="P1">• Use the Mytripinsurance hotline +61 2 8907 5666 for specific assistance on all travel emergency matters whilst travelling overseas.</p>
    <p class="P1">Please send your completed claim form to travelclaims.au@chubb.com or post to:</p>
    <p class="P1">Chubb Claims Accident & Health</p>
    <p class="P1">GPO Box 4065</p>
    <p class="P1">Sydney NSW 2001</p>
    <p class="P1"> </p>
    <p class="P1"><span class="T1">Cruise Insurance with Mytripinsurance</span></p>
    <p class="P1"> </p>
    <p class="P1">We cater specifically for cruise holidays by adding a Cruising Trip for an extra premium to your Mytripinsurance policy</p>
    <p class="P1"> </p>
    <p class="P1">Our Cruising Trip will ensure your policy includes the cover you need under any relevant benefit section.</p>
    <p class="P1"> </p>
    <p class="P1">In addition, the definition for Cancellation Expenses is extended to include pre-paid, non-refundable and unused portion of day excursions and cruise activities and we will provide a daily contribution to the additional expenses when a doctor considers it necessary for you to be confined to your cabin on your cruise for more than a day due to an accidental injury or sickness.</p>
    <p class="P1"> </p>
    <p class="P1"><br/>
        <span class="T3">Onboard medical costs</span>
    </p>
    <p class="P1">Medical facilities onboard aren’t usually on people’s checklist when reviewing the right ship for you. While we hope you never have to use them, if you do you’ll be charged separately by the cruise line for those services. Then, if you have a condition serious enough to require evacuation, an air ambulance evacuation from the ship or back home to Australia can cost up to $85,000 – and that’s before the medical bills start adding up. A Cruising Trip with Mytripinsurance will give you peace of mind when on your cruise holiday.</p>
    <p class="P1"> </p>
    <p class="P1"><br/>
        <span class="T3">Your luggage doesn’t arrive</span>
    </p>
    <p class="P1">Delayed baggage can become a major issue on a cruise. When on a usual trip, a lost bag by an airline can be an inconvenience but not a major hassle. However on a cruise, you may be at sea for days and the cost to get your bag to the next port can be much higher. Remember to check your policy limits and keep receipts to submit with your claim.</p>
    <p class="P1"> </p>
    <p class="P1"><br/>
        <span class="T3">Unexpectedly, you can’t go on your cruise</span>
    </p>
    <p class="P1">Bad things can happen, and if for any reasons you need to cancel your dream cruise, travel insurance could help you recover your non-refundable pre-paid costs including pre-paid deposits. Mytripinsurance provides cover for identified reasons that are outside of your control such as if you are rendered unfit to travel due to an accidental injury or sickness on the opinion of a doctor.</p>
    <p class="P1"> </p>
    <p class="P1"><br/>
        <span class="T3">Activities and sport participation</span>
    </p>
    <p class="P1">Mytripinsurance caters to a wide variety of activities and sports.</p>
    <p class="P1"> </p>
    <p class="P1">If you intend to participate in activities and sports on your trip you should ensure that you understand what you will be covered for and what you will not before you go away.</p>
    <p class="P1"> </p>
    <p class="P1">Tourist activities that can be undertaken without age restriction, specific training or requiring specialised equipment will be covered under your Mytripinsurance policy (including those activities with height or general health warnings, e.g. whale watching).</p>
    <p class="P1"> </p>
    <p class="P1">We also cover you for tourist activities provided by a recognised tour operator or commercial operator, providing you are acting under the guidance and supervision of qualified guides/ instructors, where relevant, whilst carrying out such activities (e.g. theme park rides or bungee jumping).</p>
    <p class="P1"> </p>
    <p class="P1"><br/>
        <span class="T3">You need to be aware of the following general exclusions:</span>
    </p>
    <p class="P1">General exclusion 10 excludes any claim that arises from you or your travel companion not taking all reasonable efforts or your carelessness, negligence or recklessness avoiding Your Accidental Injury.</p>
    <p class="P1">General exclusion 18 relates to air travel which means we exclude claims directly or indirectly related to activities and sport that involve air travel to participate in such as gliding or sky diving (but not hot air ballooning when carried out by a licensed operator).</p>
    <p class="P1"> </p>
    <p class="P1"><br/>
        <span class="T3">General exclusion 23 excludes any claim that arises directly or indirectly due to you participating in:</span>
    </p>
    <p class="P1"><span class="bold-font-size-fix">a.</span> Extreme Sports and Activities; or</p>
    <p class="P1"><span class="bold-font-size-fix">b.</span> Winter Sports unless you have purchased the Winter Sports Pack; or</p>
    <p class="P1"><span class="bold-font-size-fix">c.</span> Adventure Sports unless You have purchased the Adventure Sports Pack; or</p>
    <p class="P1"><span class="bold-font-size-fix">d.</span> scuba diving unless you hold a PADI certification (or similar recognised qualification)
    or when diving with a qualified instructor. In these situations the maximum depth that this Policy         covers is as specified under Your PADI certification (or similar recognised qualification) but no   deeper than thirty (30) metres and You must not be diving alone unless you have purchased the Adventure Sports Pack; or</p>
    <p class="P1"><span class="bold-font-size-fix">e.</span> Mountaineering; or</p>
    <p class="P1"><span class="bold-font-size-fix">f.</span> any professional competitions or sports in which you receive payment, sponsorships or any forms        of financial rewards; or</p>
    <p class="P1"><span class="bold-font-size-fix">g.</span> Racing of any kind, other than on foot but this does not include ultra-marathons, biathlons and triathlons; or</p>
    <p class="P1"><span class="bold-font-size-fix">h.</span>Motorcycling for any purpose except involving the use of a motorcycle with an engine capacity of    200cc or less, provided that the driver holds a current Australian motorcycle license and you wear a helmet whilst in use.</p>
    <p class="P1"> </p>
    <p class="P1"><br/>
        <span class="T3">Where:</span>
    </p>
    <p class="P1">Mountaineering means the ascent or descent of a mountain ordinarily necessitating the use of specified equipment including but not limited to crampons, pickaxes, anchors, bolts, carabineers and lead-rope or top-rope anchoring equipment.</p>
    <p class="P1">Extreme Sports and Sporting Activities means any sports or sporting activities that present a high level of inherent danger (i.e. involve a high level of expertise, exceptional physical exertion, highly specialised gear or stunts).</p>
    <p class="P1"> </p>
    <p class="P1">By way of example, this means that:</p>
    <p class="P1">You will be covered for things such as:</p>
    <p class="P1">• amateur golf or tennis</p>
    <p class="P1">• bungee jumping</p>
    <p class="P1">• camel or elephant riding</p>
    <p class="P1">• dragon boating</p>
    <p class="P1">• go-karting</p>
    <p class="P1">• inline skating, skateboarding or ice skating</p>
    <p class="P1">• parasailing</p>
    <p class="P1">• riding a motorcycle or moped if less than 200cc, You are wearing a helmet and hold an Australia motorcycle license</p>
    <p class="P1">• rowing</p>
    <p class="P1">• snorkelling</p>
    <p class="P1">• surfing</p>
    <p class="P1"> </p>
    <p class="P1">You will not be covered for things such as:</p>
    <p class="P1">• big wave surfing</p>
    <p class="P1">• canoeing down rapids</p>
    <p class="P1">• cliff jumping</p>
    <p class="P1">• gliding</p>
    <p class="P1">• horse jumping</p>
    <p class="P1">• hunting</p>
    <p class="P1">• mountain biking</p>
    <p class="P1">• Mountaineering</p>
    <p class="P1">• sky diving</p>
    <p class="P1">• stunt riding</p>
    <p class="P1">• skiing or snowboarding</p>
    <p class="P1">• ultra marathons, biathlons, triathlons</p>
    <p class="P1"> </p>
    <p class="P1">In addition you may be offered the option to add-on a benefit pack that removes the associated activity exclusion and extend cover to include:</p>
    <p class="P1"> </p>
    <p class="P1"><br/>
        <span class="T3">Adventure Sports Pack</span>
    </p>
    <p class="P1">Included sports and/or activities:</p>
    <p class="P1">• diving up to forty (40) metres (subject to PADI Certification),</p>
    <p class="P1">• expedition bicycle touring,</p>
    <p class="P1">• mountain biking,</p>
    <p class="P1">• motorcycling (if more than 200cc, you are wearing a helmet and hold an Australia motorcycle license),</p>
    <p class="P1">• quad biking,</p>
    <p class="P1">• trekking/mountain trekking up to four thousand (4,000) metres,</p>
    <p class="P1">• white water rafting of Grade 4 and above,</p>
    <p class="P1">• 4 wheel driving adventure.</p>
    <p class="P1"> </p>
    <p class="P1"> </p>
    <p class="P1"><br/>
        <span class="T3">Winter Sports Pack</span>
    </p>
    <p class="P1">Included sports and/or activities (but not when Backcountry/Off-piste where relevant):</p>
    <p class="P1">• glacier walking with a qualified guide,</p>
    <p class="P1">• skiing,</p>
    <p class="P1">• snowboarding,</p>
    <p class="P1">• snow tubing/rafting,</p>
    <p class="P1">• snow mobiling.</p>
    <p class="P1"> </p>
    <p class="P1"> </p>
    <p class="P1"> </p>
    <p class="P1"><span class="T1">Winter Sports</span></p>
    <p class="P1">Embarking on a winter escape? For an extra premium, you can add ski, snowboarding and snowmobiling cover to any Mytravelinsurance policy during the quote process.</p>
    <p class="P1"> </p>
    <p class="P1">For an extra premium, you can add cover for Winter Sports to any Mytripinsurance policy during the quote process.</p>
    <p class="P1">Winter Sports means the following (but not when Backcountry/Off-piste where relevant):</p>
    <p class="P1">• glacier walking with a qualified guide</p>
    <p class="P1">• skiing</p>
    <p class="P1">• snowboarding</p>
    <p class="P1">• snow tubing/rafting</p>
    <p class="P1">• snow mobiling</p>
    <p class="P1">And skiing isn't the only adventure sport we cover, check out Adventure Sports and Activities and Sport.</p>
    <p class="P1"> </p>
    <p class="P1"><br/>
        <span class="T3">Understand what’s covered and what’s not</span>
    </p>
    <p class="P1">Before you leave on your winter sports holiday, make sure you understand when you are and are not covered. As always, the best way is to read the Product Disclosure Statement for full details.</p>
    <p class="P1">For example, skiers and snowboarders will not be covered if they are backcountry/off-piste, racing or participating in a professional capacity. Snowmobilers will not be covered for personal liability as a snowmobile is considered to be a mechanically propelled vehicle.</p>
    <p class="P1"> </p>
    <p class="P1"><br/>
        <span class="T3">Benefits included in the Winter Sports Pack</span>
    </p>
    <p class="P1">General Exclusion for participation in Winter Sports is removed.</p>
    <p class="P1">Own Winter Sport Equipment</p>
    <p class="P1">Cover for when Your Winter Sports Equipment is lost, stolen or damaged.</p>
    <p class="P1">Winter Sport Equipment Hire</p>
    <p class="P1">Cover for the cost of hiring alternative Winter Sports Equipment when Your Winter Sports Equipment is lost, stolen or damaged or is delayed by Your Public Transportation by more than 24 hours.</p>
    <p class="P1">Unused Winter Sports Costs</p>
    <p class="P1">Cover in the Event You suffer an Accidental Injury or Sickness and a Doctor certifies that you are unable to use the unused pre-booked and pre-paid ski passes, ski hire, tuition fees or lift passes we will reimburse You for the non-refundable amount.</p>
    <p class="P1">Piste Closure</p>
    <p class="P1">Cover for the cost of transport and additional ski passes at the nearest alternative resort when all the lift systems are closed for more than 24 hours due to insufficient snow, severe weather or power failure at Your pre-booked ski holiday resort.</p>
    <p class="P1">Weather and Avalanche Closure</p>
    <p class="P1">Cover for Your reasonable Additional Trip Expenses due to You being delayed from leaving Your ski holiday resort for more than 12 hours due to severe weather or an avalanche that has been confirmed in writing by the relevant authority.</p>
    <p class="P1"> </p>
    <p class="P1"><span class="T1">Seniors Travel Insurance</span></p>
    <p class="P1">Mytripinsurance wants to ensure that travel insurance for seniors is easy. We provide cover options for indiviudals under 90 years of age, whether you are travelling in Australia or taking an overseas holiday.</p>
    <p class="P1"> </p>
    <p class="P1">Travel insurance can help when unexpected and unwanted mishaps like lost travel documents, travel delays or unexpected medical problems that arise during a trip. Mytripinsurance offers travel insurance options for people aged up to 90 years of age.</p>
    <p class="P1"> </p>
    <p class="P1"><!--For frequent travellers under 80 years of age, we offer annual multi-trip travel insurance policies. -->Get in touch with our experts with any questions about travel insurance for seniors by calling us on 1800 061 568  or get a quote online today.</p>
    <p class="P1"> </p>
    <p class="P1">Receive better value for money with our travel insurance benefits.</p>
    <p class="P1">Travel knowing your trip is protected by Mytripinsurance - including a number of benefits that are popular with senior travellers including:</p>
    <p class="P1">• Round-the-clock emergency medical and travel assistance from Mytripinsurance travel assistance experts</p>
    <p class="P1">• Medical expenses covered overseas when you suffer an accidental injury or sickness</p>
    <p class="P1">• Lost luggage and personal effects cover when lost, stolen or damaged</p>
    <p class="P1">• Lost travel documents, including credit cards and travellers cheques replacement</p>
    <p class="P1">• Public transportation delays due to severe weather or mechanical breakdowns</p>
    <p class="P1"> </p>
    <p class="P1">If you are travelling on a cruise you need to choose our Cruising Trip to ensure you have cover under your policy including an extended definition of cancellation expenses which includes cover pre-paid, non-refundable and unused portion of cruise activities and day excursions.</p>
    <p class="P1"> </p>
    <p class="P1"><br/>
        <span class="T3">Automatic cover for select approved medical conditions</span>
    </p>
    <p class="P1">Mytripinsurance offers cover for 54 automatically approved medical conditions without additional premium. See Pre-existing Medical Conditions for an overview and ensure you review the
        <a style="color:deepskyblue" href="/source/My%20Trip%20Insurance%20Australia%20-%20Single%20Trip%20PDS.pdf" target="_blank">PDS</a> for full details.</p>
    <p class="Standard"/>
</div>