<?php
/* @var $this OrderController */
/* @var $model Orders */

$this->breadcrumbs=array(
	'Orders'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Orders', 'url'=>array('index')),
	array('label'=>'Manage Orders', 'url'=>array('admin')),
);
?>

<h1>Create Orders</h1>


<div class="form">

    <?php $form=$this->beginWidget('CActiveForm', array(
        'id'=>'orders-form',
        // Please note: When you enable ajax validation, make sure the corresponding
        // controller action is handling ajax validation correctly.
        // There is a call to performAjaxValidation() commented in generated controller code.
        // See class documentation of CActiveForm for details on this.
        'enableAjaxValidation'=>false,
    )); ?>

    <p class="note">Fields with <span class="required">*</span> are required.</p>

    <?php echo $form->errorSummary($model); ?>

    <div class="row">
        <?php echo $form->labelEx($model,'user_id'); ?>
        <?php echo $form->textField($model,'user_id'); ?>
        <?php echo $form->error($model,'user_id'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model,'date'); ?>
        <?php echo $form->textField($model,'date'); ?>
        <?php echo $form->error($model,'date'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model,'countries'); ?>
        <?php echo $form->textField($model,'countries',array('size'=>60,'maxlength'=>255)); ?>
        <?php echo $form->error($model,'countries'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model,'start_traveling'); ?>
        <?php echo $form->textField($model,'start_traveling'); ?>
        <?php echo $form->error($model,'start_traveling'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model,'end_traveling'); ?>
        <?php echo $form->textField($model,'end_traveling'); ?>
        <?php echo $form->error($model,'end_traveling'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model,'travellers'); ?>
        <?php echo $form->textField($model,'travellers',array('size'=>60,'maxlength'=>255)); ?>
        <?php echo $form->error($model,'travellers'); ?>
    </div>

    <div class="row buttons">
        <?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- form -->