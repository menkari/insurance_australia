<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id),array('view','id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('api_data')); ?>:</b>
	<?php echo CHtml::encode($data->api_data); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('admin_email')); ?>:</b>
	<?php echo CHtml::encode($data->admin_email); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('master_pwd')); ?>:</b>
	<?php echo CHtml::encode($data->master_pwd); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('master_login')); ?>:</b>
	<?php echo CHtml::encode($data->master_login); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('time_in_day')); ?>:</b>
	<?php echo CHtml::encode($data->time_in_day); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('global_commission')); ?>:</b>
	<?php echo CHtml::encode($data->global_commission); ?>
	<br />


</div>