<?php
$this->breadcrumbs=array(
	'Settings'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List Settings','url'=>array('index')),
	array('label'=>'Create Settings','url'=>array('create')),
	array('label'=>'Update Settings','url'=>array('update','id'=>$model->id)),
	array('label'=>'Delete Settings','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
);
?>

<h1>View Settings #<?php echo $model->id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'api_data',
		'admin_email',
		'master_pwd',
		'master_login',
		'time_in_day',
		'global_commission',
	),
)); ?>
