<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<?php echo $form->textFieldRow($model,'id',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'api_data',array('class'=>'span5','maxlength'=>255)); ?>

	<?php echo $form->textFieldRow($model,'admin_email',array('class'=>'span5','maxlength'=>255)); ?>

	<?php echo $form->textFieldRow($model,'master_pwd',array('class'=>'span5','maxlength'=>255)); ?>

	<?php echo $form->textFieldRow($model,'master_login',array('class'=>'span5','maxlength'=>255)); ?>

	<?php echo $form->textFieldRow($model,'time_in_day',array('class'=>'span5','maxlength'=>255)); ?>

	<?php echo $form->textFieldRow($model,'global_commission',array('class'=>'span5','maxlength'=>255)); ?>

	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>'Search',
		)); ?>
	</div>

<?php $this->endWidget(); ?>
