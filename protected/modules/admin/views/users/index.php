<?php
$this->breadcrumbs=array(
	'Users'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List Users','url'=>array('index')),
	array('label'=>'Create Users','url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('users-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Users</h1>

<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('bootstrap.widgets.TbGridView',array(
	'id'=>'users-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		'username',
		'email',
        array(
            'header' => 'Orders',
            'class' => 'CButtonColumn',
            'template' => '{orders}',
            'buttons' => array(
                'orders' => array(
                    'url'=> '"/admin/users/orders/id/$data->id"',
                    'options' => array('target' => '_new'),
                ),
            ),
        ),
//        array(
//            'header' => 'Domain',
//            'class' => 'CButtonColumn',
//            'template' => '{domain}',
//            'buttons' => array(
//                'domain' => array(
//                    'url'=> '"/admin/users/CreateDomain/id/$data->id"',
//                    'options' => array('target' => '_new'),
//                ),
//            ),
//        ),
//		'password',
//		'confirm_password',
//		'token',
		/*
		'role',
		'status',
		*/
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
		),
	),
)); ?>
