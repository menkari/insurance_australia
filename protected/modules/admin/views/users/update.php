<?php
/* @var $this UsersController */
/* @var $model Users */

$this->breadcrumbs=array(
	'Users'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Users', 'url'=>array('index')),
	array('label'=>'Create Users', 'url'=>array('create')),
	array('label'=>'View Users', 'url'=>array('view', 'id'=>$model->id)),
);
?>

<h1>Update Users <?php echo $model->id; ?></h1>

<?php //$this->renderPartial('_form', array('model'=>$model)); ?>


<div class="form">
    <?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm', array(
        'id'=>'users-form',
        //'enableClientValidation'=>true,
        'enableAjaxValidation'=>true,
        'clientOptions'=>array(
            'validateOnSubmit'=>true,
        ),
    )); ?>

    <p class="note">Fields with <span class="required">*</span> are required.</p>

    <div class="control-group">
        <?php echo $form->labelEx($model,'username'); ?>
        <?php echo $form->textField($model,'username',array('size'=>32,'maxlength'=>32)); ?>
        <?php echo $form->error($model,'username'); ?>
    </div>

    <div class="control-group">
        <?php echo $form->labelEx($model,'name'); ?>
        <?php echo $form->textField($model,'name',array('size'=>255,'maxlength'=>255)); ?>
        <?php echo $form->error($model,'name'); ?>
    </div>

    <div class="control-group">
        <?php echo $form->labelEx($model,'email'); ?>
        <?php echo $form->textField($model,'email',array('size'=>60,'maxlength'=>255)); ?>
        <?php echo $form->error($model,'email'); ?>
    </div>

    <div class="control-group">
        <?php echo $form->labelEx($model,'password'); ?>
        <?php echo $form->passwordField($model,'password',array('size'=>60,'maxlength'=>64, 'value' => '')); ?>
        <?php echo $form->error($model,'password'); ?>
    </div>

    <div class="control-group">
        <?php echo $form->labelEx($model,'confirm_password'); ?>
        <?php echo $form->passwordField($model,'confirm_password',array('size'=>60,'maxlength'=>64, 'value' => '')); ?>
        <?php echo $form->error($model,'confirm_password'); ?>
    </div>

    <div class="control-group">
        <?php echo $form->labelEx($model,'role'); ?>
        <?php echo $form->dropDownList($model, 'role', array(Users::ROLE_CUSTOMER => 'Customer', Users::ROLE_PARTNER => 'Partner', Users::ROLE_AGENT => 'Agent', Users::ROLE_MANAGER => 'Manager', Users::ROLE_ADMINISTRATOR => 'Administrator')); ?>
        <?php echo $form->error($model,'role'); ?>
    </div>

    <div class="control-group">
        <?php echo $form->labelEx($model,'status'); ?>
        <?php echo $form->dropDownList($model, 'status', array('0' => 'Off', '1' => 'On')); ?>
        <?php echo $form->error($model,'status'); ?>
    </div>
    <?php if ($model->role == 'partner') : ?>
        <br>
        <br>
        <div class="control-group">
            <label for='referral'>Referral:</label>
            <?php echo $form->textField($model,'referral'); ?>
            <?php echo $form->error($model,'referral'); ?>
        </div>
    <?php endif; ?>
    <div class="buttons">
        <?php $this->widget(
            'bootstrap.widgets.TbButton',
            array('buttonType' => 'submit', 'label' => 'Update')
        ); ?>
    </div>

    <?php $this->endWidget(); ?>
</div><!-- form -->