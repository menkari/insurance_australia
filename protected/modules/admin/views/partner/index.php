<?php
$this->breadcrumbs=array(
	'Users'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List Users','url'=>array('index')),
	array('label'=>'Create Users','url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('users-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Partners</h1>

<div class="search-form" style="display:none">
<?php
$this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php
$this->widget('bootstrap.widgets.TbGridView',array(
	'id'=>'users-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		array('name' => 'name', 'header' => 'Partner'),
		'username',
		'email',
        array(
            'header' => 'Visits',
            'value' => '$data->partner["visits"] ? $data->partner["visits"] : 0',
        ),
        array(
            'header' => 'Order count',
            'value' => 'Orders::countPartnerOrders($data["id"])',
        ),
        array(
            'header' => 'Sales amount',
            'value' => 'Orders::getPartnerSalesAmount($data["id"])',
        ),
        array(
            'header' => 'Commission amount',
            'value' => '$data->partner["commission_amount"]',
        ),
        array(
            'header' => 'Conversion',
            'value' => 'Orders::getPartnerConversion($data["id"], $data->partner["visits"])',
            ),
        array(
            'header' => 'Orders',
            'class' => 'CButtonColumn',
            'template' => '{orders}',
            'buttons' => array(
                'orders' => array(
                    'url'=> '"/admin/orders/?Orders[partner_id]=$data->id"',
                    'options' => array('target' => '_new'),
                ),
            ),
        ),
//        array(
//            'header' => 'Domain',
//            'class' => 'CButtonColumn',
//            'template' => '{domain}',
//            'buttons' => array(
//                'domain' => array(
//                    'url'=> '"/admin/users/CreateDomain/id/$data->id"',
//                    'options' => array('target' => '_new'),
//                ),
//            ),
//        ),
//		'password',
//		'confirm_password',
//		'token',
		/*
		'role',
		'status',
		*/
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
		),
	),
)); ?>
