<?php
$this->breadcrumbs=array(
	'Partner'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List Users','url'=>array('index')),
	array('label'=>'Create Users','url'=>array('create')),
	array('label'=>'Update Users','url'=>array('update','id'=>$model->id)),
	array('label'=>'Delete Users','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
);
?>

<h1>View User #<?php echo $model->id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'username',
		'email',
		'token',
		'role',
		'name',
		'surname',
		'birth',
		'phone',
		'passport',
		'expiry',
		'country',
		'city',
		'zip_code',
		'street',
		'subscribe',
		'subscribe_country',
		'referral',
		'state',
	),
)); ?>
