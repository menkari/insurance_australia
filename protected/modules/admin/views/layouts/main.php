<?php /* @var $this Controller */ ?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="language" content="en" />

    <link href='https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i' rel='stylesheet'>
    <link rel="stylesheet" type="text/css" href="<?php echo $this->module->getAssetsUrl(); ?>/css/styles.css" />

    <title><?php echo CHtml::encode($this->pageTitle); ?></title>

    <?php Yii::app()->bootstrap->register(); ?>
</head>

<body>

<?php $this->widget('bootstrap.widgets.TbNavbar',array(
    'items'=>array(
        array(
            'class'=>'bootstrap.widgets.TbMenu',
            'items'=>array(
                array('label'=>'Site', 'url'=>array('/site/index')),
                array('label'=>'Orders', 'url'=>array('/admin/orders'), 'visible'=>(Yii::app()->user->role == Users::ROLE_ADMINISTRATOR)),
                array('label'=>'Partners', 'url'=>array('/admin/partner'), 'visible'=>(Yii::app()->user->role == Users::ROLE_ADMINISTRATOR)),
                array('label'=>'Users', 'url'=>array('/admin/users'), 'visible'=>(Yii::app()->user->role == Users::ROLE_ADMINISTRATOR)),
                array('label'=>'Templates', 'url'=>array('/admin/templates'), 'visible'=>(Yii::app()->user->role == Users::ROLE_ADMINISTRATOR)),
                array('label'=>'Pages', 'url'=>array('/admin/pages'), 'visible'=>(Yii::app()->user->role == Users::ROLE_ADMINISTRATOR)),
//                array('label'=>'Settings', 'url'=>array('/admin/settings'), 'visible'=>(Yii::app()->user->role == Users::ROLE_ADMINISTRATOR)),
                array('label'=>'Login', 'url'=>array('/site/login'), 'visible'=>Yii::app()->user->isGuest),

                array('label'=>'Logout ('.Yii::app()->user->name.')', 'url'=>array('/site/logout'), 'visible'=>!Yii::app()->user->isGuest)
            ),
        ),
    ),
)); ?>

<div class="container" id="page">

    <?php if(isset($this->breadcrumbs)):?>
        <?php $this->widget('bootstrap.widgets.TbBreadcrumbs', array(
            'links'=>$this->breadcrumbs,
        )); ?><!-- breadcrumbs -->
    <?php endif?>

    <?php echo $content; ?>

    <div class="clear"></div>

    <div id="footer">
        Copyright &copy; <?php echo date('Y'); ?> by My Company.<br/>
        All Rights Reserved.<br/>
        <?php echo Yii::powered(); ?>
    </div><!-- footer -->

</div><!-- page -->

</body>
</html>
