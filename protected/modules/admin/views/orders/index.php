<?php
$this->breadcrumbs=array(
	'Orders'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List Orders','url'=>array('index')),
	//array('label'=>'Create Orders','url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('users-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Orders</h1>

<div class="search-form" style="">
<?php
$this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php
$this->widget('bootstrap.widgets.TbGridView',array(
	'id'=>'users-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		'date',
		array('name' => 'detailsEmail', 'value' => function($data) {
			echo isset($data->details->email) ? $data->details->email : 'none';
		}),
		array('name' => 'detailsName', 'value' => function($data) {
			echo Orders::getTouristName($data->details);
		}),
		array('name' => 'detailsAddress', 'value' => function($data) {
			echo Orders::getTouristAddress($data->details);
		}),
		'travellers',
		'kids',
		'price',
		array('header' => 'Commission', 'value' => function($data) {
			echo Partner::getCommission($data);
		}),
		'quote',
		array('header' => 'PDF', 'value' => function($data) {
			echo "<a href='$data->pdf'>Link</a>";
		}),
		'referral',
		array('name' => 'unknown_referral', 'value' => function($data) {
			echo substr($data->unknown_referral, 0, 10);
		}),
		array('name' => 'partnerName', 'value' => function($data) {
			echo isset($data->partner->name) ? $data->partner->name : 'none';
		}),
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
			'template'=>'{delete}{view}',
		),
	),
)); ?>
