<?php
$this->breadcrumbs=array(
	'Orders'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List Orders','url'=>array('index')),
	//array('label'=>'Create Users','url'=>array('create')),
	//array('label'=>'Update Order','url'=>array('update','id'=>$model->id)),
	array('label'=>'Delete Order','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
);
?>

<h1>View Order #<?php echo $model->id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		array('label' => 'Order date', 'format' => 'html', 'value' => $model->date),
		array('label' => 'Partner', 'format' => 'html', 'value' => $model->partner->name),
		array('label' => 'Name', 'format' => 'html', 'value' => "{$model->details->name} {$model->details->last_name}"),
		array('label' => 'Birth date', 'format' => 'html', 'value' => $model->details->birth_date),
		array('label' => 'Second tourist', 'format' => 'html', 'value' => "{$model->details->second_name} {$model->details->second_last_name}"),
		array('label' => 'Birth date', 'format' => 'html', 'value' => $model->details->second_birth_date),
		array('label' => 'Children', 'format' => 'html', 'value' => count(explode(',', $model->kids))),
		array('label' => 'Children age', 'format' => 'html', 'value' => $model->kids),
		array('label' => 'Email', 'format' => 'html', 'value' => $model->details->email),
		array('label' => 'Phone', 'format' => 'html', 'value' => $model->details->phone),
		array('label' => 'Address 1', 'format' => 'html', 'value' => Orders::getTouristAddress($model->details)),
		array('label' => 'Address 2', 'format' => 'html', 'value' => $model->details->address2),
		array('label' => 'Health disorder', 'format' => 'html', 'value' => $model->details->health_disorder),


		'countries',
		'start_traveling',
		'end_traveling',
		'price',
		array('label' => 'PDF', 'format' => 'html', 'type' => 'raw', 'value' => "<a href='$model->pdf'>LINK</a>"),
		'status',
		'status_message',
		'referral',
		'unknown_referral',
	),
)); ?>

<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm', array(
	'id'=>'order-form',
	//'enableClientValidation'=>true,
	'enableAjaxValidation'=>false,
	'clientOptions'=>array(
		'validateOnSubmit'=>true,
	),
)); ?>
<div class="control-group">
	<?php echo $form->labelEx($model,'partner_id'); ?>
	<?php echo $form->dropDownList($model,'partner_id', $partners); ?>
</div>
<div class="buttons">
	<?php $this->widget(
		'bootstrap.widgets.TbButton',
		array('buttonType' => 'submit', 'label' => 'Update')
	); ?>
</div>

<?php $this->endWidget(); ?>
