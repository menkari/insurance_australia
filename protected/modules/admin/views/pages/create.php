<?php
$this->breadcrumbs=array(
	'Pages'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Pages','url'=>array('index')),
);
?>

<h1>Create Pages</h1>
<?php Yii::import('ext.imperavi-redactor-widget-master.ImperaviRedactorWidget'); ?>


<?php /* echo $this->renderPartial('_form', array('model'=>$model)); */ ?>


<div class="form">
    <?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm', array(
        'id'=>'pages-form',
        //'enableClientValidation'=>true,
        'enableAjaxValidation'=>true,
        'clientOptions'=>array(
            'validateOnSubmit'=>true,
        ),
    )); ?>

    <p class="note">Fields with <span class="required">*</span> are required.</p>

    <div class="control-group">
        <?php echo $form->labelEx($model,'slug'); ?>
        <?php echo $form->textField($model,'slug'); ?>
        <?php echo $form->error($model,'slug'); ?>
    </div>
    <div class="control-group">
        <?php echo $form->labelEx($model,'title'); ?>
        <?php echo $form->textField($model,'title'); ?>
        <?php echo $form->error($model,'title'); ?>
    </div>

    <div class="control-group">
    <?php
        echo $form->labelEx($model,'Content');
        $form->widget('ext.ckeditor.CKEditorWidget',array(
            "model"=>$model,                 # Data-Model
            "attribute"=>'content',          # Attribute in the Data-Model
            "defaultValue"=>"",              # Optional
            "config" => array(
                "height"=>"300px",
                "width"=>"100%",
                "toolbar"=>"Full",
            ),
        ) );
    ?>
    </div>

    <div class="buttons">
        <?php $this->widget(
            'bootstrap.widgets.TbButton',
            array('buttonType' => 'submit', 'label' => 'Create')
        ); ?>
    </div>

    <?php $this->endWidget(); ?>
</div><!-- form -->