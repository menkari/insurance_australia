<?php
/* @var $this TemplatesController */
/* @var $model Templates */

$this->breadcrumbs=array(
	'Templates'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Templates', 'url'=>array('index')),
	array('label'=>'Create Templates', 'url'=>array('create')),
	array('label'=>'View Templates', 'url'=>array('view', 'id'=>$model->id)),
);
?>

<h1>Update Templates <?php echo $model->id; ?></h1>


<div class="form">
    <?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm', array(
        'id'=>'templates-form',
        //'enableClientValidation'=>true,
        'enableAjaxValidation'=>true,
        'clientOptions'=>array(
            'validateOnSubmit'=>true,
        ),
    )); ?>

    <p class="note">Fields with <span class="required">*</span> are required.</p>


        <?php echo $form->labelEx($model,'domain'); ?>
        <?php echo $form->textField($model,'domain'); ?>
        <?php echo $form->error($model,'domain'); ?>



        <?php /*echo $form->labelEx($model,'theme_id'); ?>
        <?php echo $form->dropDownList($model, 'theme_id', $model->getThemesList(), array(
            'options' => array($model->id => array('selected'=>true)))); ?>
        <?php echo $form->error($model,'theme_id'); */?>
    <?php echo $form->labelEx($model,'theme'); ?>
    <?php echo $form->dropDownList($model, 'theme', $model->getThemesList()); ?>
    <?php echo $form->error($model,'theme'); ?>


    <div class="buttons">
        <?php $this->widget(
            'bootstrap.widgets.TbButton',
            array('buttonType' => 'submit', 'label' => 'Update')
        ); ?>
    </div>

    <?php $this->endWidget(); ?>
</div><!-- form -->