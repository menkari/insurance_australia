<?php
/* @var $this TemplatesController */
/* @var $model Templates */

$this->breadcrumbs=array(
	'Templates'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Templates', 'url'=>array('index')),
);
?>

<h1>Create Templates</h1>

<div class="form">
    <?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm', array(
        'id'=>'templates-form',
        //'enableClientValidation'=>true,
        'enableAjaxValidation'=>true,
        'clientOptions'=>array(
            'validateOnSubmit'=>true,
        ),
    )); ?>

    <p class="note">Fields with <span class="required">*</span> are required.</p>


        <?php echo $form->labelEx($model,'domain'); ?>
        <?php echo $form->textField($model,'domain'); ?>
        <?php echo $form->error($model,'domain'); ?>



        <?php echo $form->labelEx($model,'theme'); ?>
        <?php echo $form->dropDownList($model, 'theme', $model->getThemesList()); ?>
        <?php echo $form->error($model,'theme'); ?>


    <div class="buttons">
        <?php $this->widget(
            'bootstrap.widgets.TbButton',
            array('buttonType' => 'submit', 'label' => 'Create')
        ); ?>
    </div>

    <?php $this->endWidget(); ?>
</div><!-- form -->