<?php

class UsersController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='/layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete', 'orders', 'createDomain'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model = new Users;

        $model->scenario = 'admin_user_create';

		// Uncomment the following line if AJAX validation is needed
		$this->performAjaxValidation($model);

		if(isset($_POST['Users']))
		{
			$model->attributes=$_POST['Users'];
			if($model->save())
				$this->redirect(array('index'));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

        $model->scenario = 'admin_user_update';

		// Uncomment the following line if AJAX validation is needed
		$this->performAjaxValidation($model);

		if(isset($_POST['Users']))
		{

			$model->attributes = $_POST['Users'];

            if (!empty($_POST['Users']['password'])) {
                $model->password = md5($_POST['Users']['password']);
                $model->confirm_password = md5($_POST['Users']['confirm_password']);
            }



			if($model->save())
				$this->redirect(array('index'));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Manages all models.
	 */
	public function actionIndex()
	{
		$model=new Users('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Users']))
			$model->attributes=$_GET['Users'];

		$this->render('index',array(
			'model'=>$model,
		));
	}

    public function actionOrders($id)
    {

        $this->layout = '/layouts/column1';

        if (Yii::app()->user->checkAccess('partner')) {
            if ($_SERVER['REQUEST_METHOD'] == 'POST' && !empty($_POST)) {
                $condition = 'partner_id=:partner_id';
                $params = array();
                $params[':partner_id'] = $id;

                // echo "<pre>";print_r($params);echo "</pre>";exit;

                $search = $_POST['search'];
                if (!empty($search)) {
                    $condition .= ' AND countries LIKE :countries';
                    $params[':countries'] = "%$search%";
                }

                $minVal = $_POST['min-val'];
                if (!empty($minVal)) {
                    $condition .= ' AND price >=:priceMin';
                    $params[':priceMin'] = $minVal;
                }

                $maxVal = $_POST['max-val'];
                if (!empty($maxVal)) {
                    $condition .= ' AND price <=:priceMax';
                    $params[':priceMax'] = $maxVal;
                }

                $from = $_POST['ordersFrom'];
                if (!empty($from)) {
                    $from = DateTime::createFromFormat('d/m/Y', $from)->format('Y-m-d');
                    $condition .= ' AND date >=:dateFrom';
                    $params[':dateFrom'] = $from;
                }

                $to = $_POST['ordersTo'];
                if (!empty($to)) {
                    $to = DateTime::createFromFormat('d/m/Y', $to)->format('Y-m-d');
                    if ($to < $from) {
                        $to = $from;
                    }
                    $condition .= ' AND date <=:dateTo';
                    $params[':dateTo'] = $to;
                }

                $dataProvider = new CActiveDataProvider('Orders', array(
                        'criteria' => array(
                            'condition' => $condition,
                            'params' => $params,
                            'select' => 'id, date, price, countries, start_traveling, end_traveling, travellers',
                            'order' => 'id DESC'
                        ),

                    )
                );
            } else {
                $dataProvider = new CActiveDataProvider('Orders', array(
                        'criteria' => array(
                            'condition' => 'partner_id=:partner_id',
                            'params' => array(':partner_id' => $id),
                            'select' => 'id, date, price, countries, start_traveling, end_traveling, travellers, pdf',
                            'order' => 'id DESC'
                        ),

                    )
                );
            }
        } else {
            $dataProvider = new CActiveDataProvider('Orders', array(
                    'criteria' => array(
                        'condition' => 'user_id=:user_id',
                        'params' => array(':user_id' => $id),
                        'select' => 'id, date, price, countries, start_traveling, end_traveling, travellers, pdf',
                        'order' => 'id DESC'
                    ),

                )
            );
        }

        $criteria = new CDbCriteria;
        $criteria->select = 'MAX(price) as maxprice, MIN(price) as minprice';
//        $criteria->condition = $condition;
//        $criteria->params = $params;
        $rangePrice = Orders::model()->find($criteria);
        $min = $rangePrice['minprice'];
        $max = $rangePrice['maxprice'];

        $this->render('orders',array(
            'dataProvider' => $dataProvider,
            'post' => $_POST,
            'min' => $min,
            'max' => $max
        ));
    }

    public function actionCreateDomain($id) {

        $model = new Templates();

        if(isset($_POST['ajax']) && $_POST['ajax']==='template')
        {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }

        if(!empty($_POST)) {
            $transaction = Yii::app()->db->beginTransaction();
            try {
                $model->scenario = 'admin_domain';
                $model->user_id = $id;
                $model->theme = 'mytripproj';
                $model->domain = $_POST['Templates']['domain'];
                if ($model->save()){
                    $upload_path = dirname(Yii::app()->basePath).'/images/logo/';
                    $ih = new CImageHandler();
                    if (CUploadedFile::getInstance($model,'image')){
                        $ih->load(CUploadedFile::getInstance($model,'image')->getTempName())
                            ->save($upload_path . $id.'.jpg');
                    }
                    Yii::app()->user->setFlash("createDomain", 'Your domain has been created!');

//                    Yii::app()->user->id;
//                    $user = Users::model()->findByPk(Yii::app()->user->id);
//                    $user->role = 'partner';
//                    $user->save();
                    $transaction->commit();

                    $this->redirect(Yii::app()->createAbsoluteUrl('site/index'));
                }


            } catch (Exception $e) {
                $transaction->rollBack();
                Yii::app()->user->setFlash('error', "{$e->getMessage()}");
                $this->refresh();
            }
        }

        $this->render('create_domain',array('model' => $model));
    }

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Users the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Users::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Users $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='users-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
