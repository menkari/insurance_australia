$(function(){

    var currentYear = new Date();

    $('.filter-from-date').datepicker({
        prevText: '‹',
        nextText: '›',
        changeMonth: true,
        yearRange: '1900:' + currentYear,
        changeYear: true,
        dateFormat: 'dd/mm/yy',
        orientation: "bottom",
        showOn: "both"
    }).on('change', function () {
        $('.filter-from-date').attr('value',  $('.filter-from-date').val() );


        $('.filter-to-date').datepicker('option', {
            minDate: $('.filter-from-date').datepicker('getDate')
        });
    });

    $('.filter-to-date').datepicker({
        prevText: '‹',
        nextText: '›',
        changeMonth: true,
        yearRange: '1900:' + currentYear,
        changeYear: true,
        dateFormat: 'dd/mm/yy',
        orientation: "bottom",
        showOn: "both"
    });
    $('.panel-pickers input.autoloadInfo').on('change',function(e){

        $(this).parent().find('input[type=hidden]').val( $(this).val() );

    });

    var minVal = +$('.filter-slider').attr('data-min');
    var maxVal = +$('.filter-slider').attr('data-max');

    $('.filter-slider').slider({
        min: minVal,
        max: maxVal,
        range: true,
        values: [ $('input[name=min-val]').val() || minVal, $('input[name=max-val]').val() || maxVal ],
        create: function(){
            $('.left-handle-controller').text( $( this ).slider( "values" )[0] );
            $('.right-handler-controller').text( $( this ).slider( "values" )[1] );

            if( $('.left-handle-controller').text().length == 4 ){
                $('.left-handle-controller').css({
                    'left': parseInt($('.ui-slider-handle').eq(0).css('left')) - 13
                });
            }else if( $('.left-handle-controller').text().length == 3  ){
                $('.left-handle-controller').css({
                    'left': parseInt($('.ui-slider-handle').eq(0).css('left')) - 6
                });
            }else if( $('.left-handle-controller').text().length < 3  ){
                $('.left-handle-controller').css({
                    'left': parseInt($('.ui-slider-handle').eq(0).css('left')) - 1
                });
            }

            $('.min-span-controlller').text( $('.left-handle-controller').text() );
            $('.max-span-controlller').text( $('.right-handler-controller').text() );



            if( $('.right-handler-controller').text().length == 4 ){
                $('.right-handler-controller').css({
                    'left': parseInt($('.ui-slider-handle').eq(1).css('left')) - 13
                });
            }else if( $('.right-handler-controller').text().length == 3  ){
                $('.right-handler-controller').css({
                    'left': parseInt($('.ui-slider-handle').eq(1).css('left')) - 6
                });
            }else if( $('.right-handler-controller').text().length < 3  ){
                $('.right-handler-controller').css({
                    'left': parseInt($('.ui-slider-handle').eq(1).css('left')) - 3
                });
            }

            $(this).slider( "disable" );

        },
        slide: function() {

            $('.left-handle-controller').text( $( this ).slider( "values" )[0] );
            $('.right-handler-controller').text( $( this ).slider( "values" )[1] );

            if( $('.left-handle-controller').text().length == 4 ){
                $('.left-handle-controller').css({
                    'left': parseInt($('.ui-slider-handle').eq(0).css('left')) - 9
                });
            }else if( $('.left-handle-controller').text().length == 3  ){
                $('.left-handle-controller').css({
                    'left': parseInt($('.ui-slider-handle').eq(0).css('left')) - 6
                });
            }else if( $('.left-handle-controller').text().length < 3  ){
                $('.left-handle-controller').css({
                    'left': parseInt($('.ui-slider-handle').eq(0).css('left')) - 1
                });
            }

            $('.min-span-controlller').text( $('.left-handle-controller').text() );
            $('.max-span-controlller').text( $('.right-handler-controller').text() );

            if( $('.right-handler-controller').text().length == 4 ){
                $('.right-handler-controller').css({
                    'left': parseInt($('.ui-slider-handle').eq(1).css('left')) - 9
                });
            }else if( $('.right-handler-controller').text().length == 3  ){
                $('.right-handler-controller').css({
                    'left': parseInt($('.ui-slider-handle').eq(1).css('left')) - 6
                });
            }else if( $('.right-handler-controller').text().length < 3  ){
                $('.right-handler-controller').css({
                    'left': parseInt($('.ui-slider-handle').eq(1).css('left')) - 3
                });
            }

            $('.min-val').val( +$('.left-handle-controller').text() );
        
            $('.max-val').val( +$('.right-handler-controller').text() );

        },
        change: function( event, ui ) {

            $('.left-handle-controller').text( $( this ).slider( "values" )[0] );
            $('.right-handler-controller').text( $( this ).slider( "values" )[1] );

            if( $('.left-handle-controller').text().length == 4 ){
                $('.left-handle-controller').css({
                    'left': parseInt($('.ui-slider-handle').eq(0).css('left')) - 9
                });
            }else if( $('.left-handle-controller').text().length == 3  ){
                $('.left-handle-controller').css({
                    'left': parseInt($('.ui-slider-handle').eq(0).css('left')) - 6
                });
            }else if( $('.left-handle-controller').text().length < 3  ){
                $('.left-handle-controller').css({
                    'left': parseInt($('.ui-slider-handle').eq(0).css('left')) - 1
                });
            }

            $('.min-span-controlller').text( $('.left-handle-controller').text() );
            $('.max-span-controlller').text( $('.right-handler-controller').text() );


            if( $('.right-handler-controller').text().length == 4 ){
                $('.right-handler-controller').css({
                    'left': parseInt($('.ui-slider-handle').eq(1).css('left')) - 9
                });
            }else if( $('.right-handler-controller').text().length == 3  ){
                $('.right-handler-controller').css({
                    'left': parseInt($('.ui-slider-handle').eq(1).css('left')) - 6
                });
            }else if( $('.right-handler-controller').text().length < 3  ){
                $('.right-handler-controller').css({
                    'left': parseInt($('.ui-slider-handle').eq(1).css('left')) - 3
                });
            }

       
            $('.min-val').val( +$('.left-handle-controller').text() );
        
            $('.max-val').val( +$('.right-handler-controller').text() );
               
            
        }
    });

    $('.ui-slider-handle').on('mouseover',function(){
        $('.filter-slider').slider( "enable" );
    });
    $('.ui-slider-handle').on('mouseout',function(){
        $('.filter-slider').slider( "disable" );
    });


});