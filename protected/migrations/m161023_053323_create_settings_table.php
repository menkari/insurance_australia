<?php

class m161023_053323_create_settings_table extends CDbMigration
{
	public function up()
	{
        $this->createTable('settings', array(
            'id' => 'pk',
            'api_data' => 'string NOT NULL',
            'admin_email' => 'string NOT NULL',
            'master_pwd' => 'string NOT NULL',
            'master_login' => 'string NOT NULL',
            'time_in_day' => 'string NOT NULL',
            'global_commission' => 'string NOT NULL'
        ));
	}

	public function down()
	{
		echo "m161023_053323_create_settings_table does not support migration down.\n";
        $this->dropTable('settings');
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}