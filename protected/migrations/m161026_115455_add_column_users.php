<?php

class m161026_115455_add_column_users extends CDbMigration
{
	public function up()
	{
        $this->addColumn('users', 'name', 'string NOT NULL');

    }

	public function down()
	{
        $this->dropColumn('users', 'name');
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}