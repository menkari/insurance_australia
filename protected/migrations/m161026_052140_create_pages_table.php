<?php

class m161026_052140_create_pages_table extends CDbMigration
{
	public function up()
	{
        $this->createTable('pages', array(
            'id' => 'pk',
            'slug' => 'string NOT NULL',
            'title' => 'string NOT NULL',
            'content' => 'text',
        ));
	}

	public function down()
	{
        $this->dropTable('pages');
		echo "m161026_052140_create_pages_table does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}