<?php

class m161021_182347_create_templates_table extends CDbMigration
{
	public function safeUp()
	{
        $this->createTable('themes', array(
            'id' => 'pk',
            'name' => 'string NOT NULL'
        ));
        $this->insert('themes', array(
            'name' => 'classic'
        ));

        $this->createTable('templates', array(
            'id' => 'pk',
            'domain' => 'string NOT NULL',
            'theme_id' => 'integer NOT NULL'
        ));
        $this->addForeignKey('template_theme_id', 'templates', 'theme_id', 'themes', 'id');
	}

	public function safeDown()
	{
        $this->dropTable('templates');
        $this->dropTable('themes');
	}
}