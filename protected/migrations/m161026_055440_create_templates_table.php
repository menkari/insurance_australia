<?php

class m161026_055440_create_templates_table extends CDbMigration
{
	public function up()
	{
        $this->createTable('templates', array(
            'id' => 'pk',
            'domain' => 'string NOT NULL',
            'theme' => 'string NOT NULL'
        ));
	}

	public function down()
	{
        $this->dropTable('templates');
		echo "m161026_055440_create_templates_table does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}