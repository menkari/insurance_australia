<?php

class m161018_134735_create_users_table extends CDbMigration
{

	public function safeUp()
	{
        $this->createTable('users', array(
            'id' => 'pk',
            'username' => 'varchar(32) NOT NULL',
            'email' => 'string NOT NULL',
            'password' => 'varchar(64) NOT NULL',
            'confirm_password' => 'varchar(64) NOT NULL',
            'token' => 'string NOT NULL',
            'role' => 'varchar(32) NOT NULL',
            'status' => 'boolean DEFAULT 0'
        ));
        $this->insert('users', array(
            'username' => 'admin',
            'email' => 'admin@iteora.mail',
            'password' => '7a7ea6908e33896ddcc8b82f264f4f7a',// maestro
            'confirm_password' => '7a7ea6908e33896ddcc8b82f264f4f7a',
            'token' => '',
            'role' => 'administrator',
            'status' => 1
        ));
	}

	public function safeDown()
	{
        echo "m161018_134735_create_users_table does not support migration down.\n";
        $this->dropTable('users');
	}
}