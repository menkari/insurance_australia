<?php

class m161026_055235_drop_templates_table extends CDbMigration
{

	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
        $this->dropTable('templates');
        $this->dropTable('themes');
	}

	public function safeDown()
	{
        $this->createTable('themes', array(
            'id' => 'pk',
            'name' => 'string NOT NULL'
        ));
        $this->insert('themes', array(
            'name' => 'classic'
        ));

        $this->createTable('templates', array(
            'id' => 'pk',
            'domain' => 'string NOT NULL',
            'theme_id' => 'integer NOT NULL'
        ));
        $this->addForeignKey('template_theme_id', 'templates', 'theme_id', 'themes', 'id');
	}

}