var countries = [
    {"name": "Afghanistan", "code": "AF"},
    {"name": "Albania", "code": "AL"},
    {"name": "Algeria", "code": "DZ"},
    {"name": "American Samoa", "code": "AS"},
    {"name": "Andorra", "code": "AD"},
    {"name": "Angola", "code": "AO"},
    {"name": "Anguilla", "code": "AI"},
    {"name": "Antigua and Barbuda", "code": "AG"},
    {"name": "Argentina", "code": "AR"},
    {"name": "Armenia", "code": "AM"},
    {"name": "Aruba", "code": "AW"},
    {"name": "Australia", "code": "AU"},
    {"name": "Austria", "code": "AT"},
    {"name": "Azerbaijan", "code": "AZ"},
    {"name": "Bahamas", "code": "BS"},
    {"name": "Bahrain", "code": "BH"},
    {"name": "Bangladesh", "code": "BD"},
    {"name": "Barbados", "code": "BB"},
    {"name": "Belarus", "code": "BY"},
    {"name": "Belgium", "code": "BE"},
    {"name": "Belize", "code": "BZ"},
    {"name": "Benin", "code": "BJ"},
    {"name": "Bermuda", "code": "BM"},
    {"name": "Bhutan", "code": "BT"},
    {"name": "Bolivia", "code": "BO"},
    {"name": "Bosnia and Herzegovina", "code": "BA"},
    {"name": "Botswana", "code": "BW"},
    {"name": "Brazil", "code": "BR"},
    {"name": "British Virgin Islands", "code": "VG"},
    {"name": "Brunei Darussalam", "code": "BN"},
    {"name": "Bulgaria", "code": "BG"},
    {"name": "Burkina Faso", "code": "BF"},
    {"name": "Burundi", "code": "BI"},
    {"name": "Cambodia", "code": "KH"},
    {"name": "Cameroon", "code": "CM"},
    {"name": "Canada", "code": "CA"},
    {"name": "Cape Verde", "code": "CV"},
    {"name": "Cayman Islands", "code": "KY"},
    {"name": "Central African Republic", "code": "CF"},
    {"name": "Chad", "code": "TD"},
    {"name": "Chile", "code": "CL"},
    {"name": "China", "code": "CN"},
    {"name": "Colombia", "code": "CO"},
    {"name": "Comoros", "code": "KM"},
    {"name": "Cook Islands", "code": "CK"},
    {"name": "Costa Rica", "code": "CR"},
    {"name": "Cote d'Ivoire", "code": "CI"},
    {"name": "Croatia", "code": "HR"},
    {"name": "Cyprus", "code": "CY"},
    {"name": "Czech Republic", "code": "CZ"},
    {"name": "Democratic Republic of the Congo", "code": "CD"},
    {"name": "Denmark", "code": "DK"},
    {"name": "Djibouti", "code": "DJ"},
    {"name": "Dominica", "code": "DM"},
    {"name": "Dominican Republic", "code": "DO"},
    {"name": "Ecuador", "code": "EC"},
    {"name": "Egypt", "code": "EG"},
    {"name": "El Salvador", "code": "SV"},
    {"name": "Equatorial Guinea", "code": "GQ"},
    {"name": "Eritrea", "code": "ER"},
    {"name": "Estonia", "code": "EE"},
    {"name": "Ethiopia", "code": "ET"},
    {"name": "Federated States of Micronesia", "code": "FM"},
    {"name": "Fiji", "code": "FJ"},
    {"name": "Finland", "code": "FI"},
    {"name": "France", "code": "FR"},
    {"name": "French Guiana", "code": "GF"},
    {"name": "French Polynesia", "code": "PF"},
    {"name": "Gabon", "code": "GA"},
    {"name": "Georgia", "code": "GE"},
    {"name": "Germany", "code": "DE"},
    {"name": "Ghana", "code": "GH"},
    {"name": "Gibraltar", "code": "GI"},
    {"name": "Greece", "code": "GR"},
    {"name": "Greenland", "code": "GL"},
    {"name": "Grenada", "code": "GD"},
    {"name": "Guadeloupe", "code": "GP"},
    {"name": "Guam", "code": "GU"},
    {"name": "Guatemala", "code": "GT"},
    {"name": "Guinea", "code": "GN"},
    {"name": "Guinea-Bissau", "code": "GW"},
    {"name": "Guyana", "code": "GY"},
    {"name": "Haiti", "code": "HT"},
    {"name": "Honduras", "code": "HN"},
    {"name": "Hong Kong", "code": "HK"},
    {"name": "Hungary", "code": "HU"},
    {"name": "Iceland", "code": "IS"},
    {"name": "India", "code": "IN"},
    {"name": "Indonesia", "code": "ID"},
    {"name": "Iran", "code": "IR"},
    {"name": "Iraq", "code": "IQ"},
    {"name": "Ireland", "code": "IE"},
    {"name": "Israel, the Gaza Strip and the West Bank", "code": "IL"},
    {"name": "Italy", "code": "IT"},
    {"name": "Jamaica", "code": "JM"},
    {"name": "Japan", "code": "JP"},
    {"name": "Jordan", "code": "JO"},
    {"name": "Kazakhstan", "code": "KZ"},
    {"name": "Kenya", "code": "KE"},
    {"name": "Kiribati", "code": "KI"},
    {"name": "Kuwait", "code": "KW"},
    {"name": "Kyrgyzstan", "code": "KG"},
    {"name": "Laos", "code": "LA"},
    {"name": "Latvia", "code": "LV"},
    {"name": "Lebanon", "code": "LB"},
    {"name": "Lesotho", "code": "LS"},
    {"name": "Liberia", "code": "LR"},
    {"name": "Libya", "code": "LY"},
    {"name": "Liechtenstein", "code": "LI"},
    {"name": "Lithuania", "code": "LT"},
    {"name": "Luxembourg", "code": "LU"},
    {"name": "Macau", "code": "MO"},
    {"name": "Macedonia", "code": "MK"},
    {"name": "Madagascar", "code": "MG"},
    {"name": "Malawi", "code": "MW"},
    {"name": "Malaysia", "code": "MY"},
    {"name": "Maldives", "code": "MV"},
    {"name": "Mali", "code": "ML"},
    {"name": "Malta", "code": "MT"},
    {"name": "Marshall Islands", "code": "MH"},
    {"name": "Martinique", "code": "MQ"},
    {"name": "Mauritania", "code": "MR"},
    {"name": "Mauritius", "code": "MU"},
    {"name": "Mayotte", "code": "YT"},
    {"name": "Mexico", "code": "MX"},
    {"name": "Moldova", "code": "MD"},
    {"name": "Monaco", "code": "MC"},
    {"name": "Mongolia", "code": "MN"},
    {"name": "Montenegro", "code": "ME"},
    {"name": "Montserrat", "code": "MS"},
    {"name": "Morocco", "code": "MA"},
    {"name": "Mozambique", "code": "MZ"},
    {"name": "Myanmar", "code": "MM"},
    {"name": "Namibia", "code": "NA"},
    {"name": "Nauru", "code": "NR"},
    {"name": "Nepal", "code": "NP"},
    {"name": "Netherlands", "code": "NL"},
    {"name": "Netherlands Antilles", "code": "AN"},
    {"name": "New Caledonia", "code": "NC"},
    {"name": "New Zealand", "code": "NZ"},
    {"name": "Nicaragua", "code": "NI"},
    {"name": "Niger", "code": "NE"},
    {"name": "Nigeria", "code": "NG"},
    {"name": "Niue", "code": "NU"},
    {"name": "Norfolk island", "code": "NF"},
    {"name": "Northern Mariana Islands", "code": "MP"},
    {"name": "Norway", "code": "NO"},
    {"name": "Oman", "code": "OM"},
    {"name": "Pakistan", "code": "PK"},
    {"name": "Palau", "code": "PW"},
    {"name": "Panama", "code": "PA"},
    {"name": "Papua New Guinea", "code": "PG"},
    {"name": "Paraguay", "code": "PY"},
    {"name": "Peru", "code": "PE"},
    {"name": "Philippines", "code": "PH"},
    {"name": "Poland", "code": "PL"},
    {"name": "Portugal", "code": "PT"},
    {"name": "Puerto Rico", "code": "PR"},
    {"name": "Qatar", "code": "QA"},
    {"name": "Romania", "code": "RO"},
    {"name": "Russia", "code": "RU"},
    {"name": "Rwanda", "code": "RW"},
    {"name": "Saint Kitts and Nevis", "code": "KN"},
    {"name": "Saint Lucia", "code": "LC"},
    {"name": "Saint Vincent and the Grenadines", "code": "VC"},
    {"name": "Samoa", "code": "WS"},
    {"name": "San Marino", "code": "SM"},
    {"name": "Sao Tome and Principe", "code": "ST"},
    {"name": "Saudi Arabia", "code": "SA"},
    {"name": "Senegal", "code": "SN"},
    {"name": "Serbia", "code": "RS"},
    {"name": "Seychelles", "code": "SC"},
    {"name": "Sierra Leone", "code": "SL"},
    {"name": "Singapore", "code": "SG"},
    {"name": "Slovakia", "code": "SK"},
    {"name": "Slovenia", "code": "SI"},
    {"name": "Solomon Islands", "code": "SB"},
    {"name": "Somalia", "code": "SO"},
    {"name": "South Africa", "code": "ZA"},
    {"name": "South Korea", "code": "KR"},
    {"name": "Spain", "code": "ES"},
    {"name": "Sri Lanka", "code": "LK"},
    {"name": "Sudan", "code": "SD"},
    {"name": "Suriname", "code": "SR"},
    {"name": "Swaziland", "code": "SZ"},
    {"name": "Sweden", "code": "SE"},
    {"name": "Switzerland", "code": "CH"},
    {"name": "Syria", "code": "SY"},
    {"name": "Taiwan", "code": "TW"},
    {"name": "Tajikistan", "code": "TJ"},
    {"name": "Tanzania", "code": "TZ"},
    {"name": "Thailand", "code": "TH"},
    {"name": "The Gambia", "code": "GM"},
    {"name": "Timor-Leste", "code": "TL"},
    {"name": "Togo", "code": "TG"},
    {"name": "Tokelau", "code": "TK"},
    {"name": "Tonga", "code": "TO"},
    {"name": "Trinidad and Tobago", "code": "TT"},
    {"name": "Tunisia", "code": "TN"},
    {"name": "Turkey", "code": "TR"},
    {"name": "Turkmenistan", "code": "TM"},
    {"name": "Turks and Caicos Islands", "code": "TC"},
    {"name": "Tuvalu", "code": "TV"},
    {"name": "Uganda", "code": "UG"},
    {"name": "Ukraine", "code": "UA"},
    {"name": "United Arab Emirates", "code": "AE"},
    {"name": "United Kingdom", "code": "GB"},
    {"name": "United States of America", "code": "US"},
    {"name": "Uruguay", "code": "UY"},
    {"name": "Uzbekistan", "code": "UZ"},
    {"name": "Vanuatu", "code": "VU"},
    {"name": "Venezuela", "code": "VE"},
    {"name": "Vietnam", "code": "VN"},
    {"name": "Virgin Islands (US)", "code": "VI"},
    {"name": "Wallis and Futuna", "code": "WF"},
    {"name": "Western Sahara", "code": "EH"},
    {"name": "Yemen", "code": "YE"},
    {"name": "Zambia", "code": "ZM"},
    {"name": "Zimbabwe", "code": "ZW"},

    {"name": "Bali", "code": "ID"},
    {"name": "Ibiza", "code": "ES"},
    {"name": "UK", "code": "GB"},
    {"name": "USA", "code": "US"}
];
$(function(){


    $('.where-div').on('click',function(){
        $('.where-inp #where').focus();
    });

    $('.menu-btn').on('click',function(e){
        e.preventDefault();
        $('.left-side-menu').css('position','fixed').animate({
            'left':'0'
        },400);
    });

    $('.close').on('click',function(e){
        e.preventDefault();
        $('.left-side-menu').animate({
            'left':'-256px'
        },400);
    });

    $('.close-popup-mob').on('click',function(e){
        e.preventDefault();
        $('.popup-wrapper').fadeOut();
    });

    $('.p-sign').on('click',function(e){
        e.preventDefault();

        $('.p-content-btns a').removeClass('clicked');
        $(this).addClass('clicked');

        $('.tab').hide().removeClass('visible');

        $('.bottom-line').animate({
            'left':'0'
        },200);

        $(this).addClass('clicked').parent('.p-content-btns').next('.p-tabs').find('.p-sign-tab').fadeIn(200).addClass('visible');
    });

    $('.p-reg').on('click',function(e){
        e.preventDefault();

        $('.p-content-btns a').removeClass('clicked');
        $(this).addClass('clicked');

        $('.tab').hide().removeClass('visible');

        $('.bottom-line').animate({
            'left':'50%'
        },200);

        $(this).addClass('clicked').parent('.p-content-btns').next('.p-tabs').find('.p-reg-tab').fadeIn(200).addClass('visible');
    });

    $('.p-reg-link').on('click',function(e){
        e.preventDefault();
        $('.p-reg').trigger('click');
    });

    $('.p-sign-link').on('click',function(e){
        e.preventDefault();
        $('.p-sign').trigger('click');
    });



    $('.sign').on('click',function(e){
        e.preventDefault();
        if( $('.wrapper').length == 0){
            $('body').wrapInner("<div class='wrapper'></div>");
            $('.popup-wrapper').fadeIn();
            $('.wrapper').css({
                'height': $('.popup').height() + 323,
                'overflow':'hidden',
                'min-height':$(window).height()
            });
        }else{
            $('.popup-wrapper').fadeIn();
            $('.wrapper').css({
                'height': $('.popup').height() + 323,
                'overflow':'hidden',
                'min-height':$(window).height()
            });
        }
    });

    $('.popup-close-bg').on('click',function(){
        $('.popup-wrapper').fadeOut();
        $('.wrapper').css({
            'height':'auto'
        });
    });

    $('.personal-link').on('click',function(e){
        e.preventDefault();

        if($(this).hasClass('clicked-personal')){
            $(this).removeClass('clicked-personal');
            $('.personal-dropdown').hide();
        }else{
            $(this).addClass('clicked-personal');
            $('.personal-dropdown').show();
        }

    });


    $('.main-page-age').on('keydown',function(e){

        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110]) !== -1 ||
                    // Allow: Ctrl+A, Command+A
            (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) ||
                // Allow: home, end, left, right, down, up
            (e.keyCode >= 33 && e.keyCode <= 40)) {
            // let it happen, don't do anything
            return;
        }

        var key = +e.key;
        
        if (!Number.isInteger(key)) {
            e.preventDefault();
        }


    });


    $('.select-autoload').each(function(){
        var inputsVal = $(this).siblings('input[type=hidden]').val();
        if(inputsVal){
            $(this).append('<option>'+ inputsVal +'</option>');
        }
    });


    countries.forEach(function(item){
        $('.form-select').append('<option>'+ item.name +'</option>');
    });

    $('.form-select option').each(function(item){
        $(this).attr('value', $(this).text());
    });
    $('.form-select').styler();

    // убираем повторение стран
    var hiddenElem;
    setTimeout(function(){
        $('div.select-autoload li').each(function(){
            if( $(this).text() == $(this).closest('.input-item').find('.editted-country').val() ){
                hiddenElem = $(this).text();
                $(this).hide();
            }
        });
    },100);

    $('select.select-autoload').on('change',function(){
        //$('div.select-autoload li').each(function(){
        $(this).find('li').each(function(){
            if($(this).text() == hiddenElem){
                $(this).show();
            }
        });
        //console.log($(this).html());
        $(this).closest('.input-item').find('.editted-country').val( $(this).val() );
    });



    /*FORM DATEPICKERS*/

    var currentYear = new Date().getFullYear();

    $('.datepicker-input-item input:not(.editted-date)').not('.statistic-picker').datepicker({
        prevText: '‹',
        nextText: '›',
        changeMonth: true,
        yearRange: '1971:' + currentYear,
        changeYear: true,
        dateFormat: 'dd/mm/yy',
        orientation: "bottom",
        showOn: "both"
    });

    $('.datepicker-input-item .ui-datepicker-trigger,.datepicker-input-item input:not(.editted-date)').on('click',function(){
        if($('#ui-datepicker-div').offset().top < $(this).offset().top){
            $('#ui-datepicker-div').addClass('ui-datepicker-div-higher');
        }else{
            $('#ui-datepicker-div').removeClass('ui-datepicker-div-higher');
        }
    });


    //Statistic pickers

    $('.statistic-picker-from').datepicker({
        prevText: '‹',
        nextText: '›',
        changeMonth: true,
        yearRange: '1971:' + currentYear,
        changeYear: true,
        dateFormat: 'dd/mm/yy',
        orientation: "bottom",
        showOn: "both"
    }).on('change', function () {
        $('.statistic-picker-from').attr('value',  $('.statistic-picker-from').val() );


        $('.statistic-picker-to').datepicker('option', {
            minDate: $('.statistic-picker-from').datepicker('getDate'),
            defaultDate: new Date( new Date( $('.statistic-picker-from').datepicker('getDate') ).getTime() + 3600 * 1000 * 24 *7)
        });
    });

    $('.statistic-picker-to').datepicker({
        prevText: '‹',
        nextText: '›',
        changeMonth: true,
        yearRange: '1971:' + currentYear,
        changeYear: true,
        dateFormat: 'dd/mm/yy',
        orientation: "bottom",
        showOn: "both"
    }).on('change', function () {
        $('.statistic-picker-to').attr('value',  $('.statistic-picker-to').val() );
    });;

    // orders picker
    var currentYear = new Date();

    $('.filter-from-date').datepicker({
        prevText: '‹',
        nextText: '›',
        changeMonth: true,
        yearRange: '1971:' + currentYear,
        changeYear: true,
        dateFormat: 'dd/mm/yy',
        orientation: "bottom",
        showOn: "both"
    }).on('change', function () {
        $('.filter-from-date').attr('value',  $('.filter-from-date').val() );


        $('.filter-to-date').datepicker('option', {
            minDate: $('.filter-from-date').datepicker('getDate')
        });
    });

    $('.filter-to-date').datepicker({
        prevText: '‹',
        nextText: '›',
        changeMonth: true,
        yearRange: '1971:' + currentYear,
        changeYear: true,
        dateFormat: 'dd/mm/yy',
        orientation: "bottom",
        showOn: "both"
    });
    $('.panel-pickers input.autoloadInfo').on('change',function(e){

        $(this).parent().find('input[type=hidden]').val( $(this).val() );

    });
    // /orders picker

    $('.last-month-stat').on('click',function(e){
        e.preventDefault();

        $('.graphic-widget-button').removeClass('graphic-widget-button-active');
        $(this).addClass('graphic-widget-button-active');

        var fromPickerDate = $('.statistic-picker-from').datepicker('getDate');
        $('.statistic-picker-from').datepicker('setDate', new Date(new Date() - 1000 * 60 * 60 * 24 * 30) );
        $('.statistic-picker-to').datepicker('setDate', new Date() );

        $('.statistic-picker-from,.statistic-picker-to').trigger('change');
    });

    $('.last-year-stat').on('click',function(e){
        e.preventDefault();

        $('.graphic-widget-button').removeClass('graphic-widget-button-active');
        $(this).addClass('graphic-widget-button-active');

        var fromPickerDate = $('.statistic-picker-from').datepicker('getDate');

        $('.statistic-picker-from').datepicker('setDate', new Date(new Date - 1000 * 60 * 60 * 24 * 30 * 12) );
        $('.statistic-picker-to').datepicker('setDate', new Date() );

        $('.statistic-picker-from,.statistic-picker-to').trigger('change');
        
    });

    /*END OF FORM DATEPICKERS*/


});



// Slider on orders page

$(function(){
    var minVal = +$('.filter-slider').attr('data-min');
    var maxVal = +$('.filter-slider').attr('data-max');
//console.log(minVal);
//console.log(maxVal);
    $('.filter-slider').slider({
        min: minVal,
        max: maxVal,
        range: true,
        values: [ $('input[name=min-val]').val() || minVal, $('input[name=max-val]').val() || maxVal ],
        create: function(){
            $('.left-handle-controller').text( $( this ).slider( "values" )[0] );
            $('.right-handler-controller').text( $( this ).slider( "values" )[1] );

            if( $('.left-handle-controller').text().length == 4 ){
                $('.left-handle-controller').css({
                    'left': parseInt($('.ui-slider-handle').eq(0).css('left')) - 13
                });
            }else if( $('.left-handle-controller').text().length == 3  ){
                $('.left-handle-controller').css({
                    'left': parseInt($('.ui-slider-handle').eq(0).css('left')) - 6
                });
            }else if( $('.left-handle-controller').text().length < 3  ){
                $('.left-handle-controller').css({
                    'left': parseInt($('.ui-slider-handle').eq(0).css('left')) - 1
                });
            }

            $('.min-span-controlller').text( $('.left-handle-controller').text() );
            $('.max-span-controlller').text( $('.right-handler-controller').text() );



            if( $('.right-handler-controller').text().length == 4 ){
                $('.right-handler-controller').css({
                    'left': parseInt($('.ui-slider-handle').eq(1).css('left')) - 13
                });
            }else if( $('.right-handler-controller').text().length == 3  ){
                $('.right-handler-controller').css({
                    'left': parseInt($('.ui-slider-handle').eq(1).css('left')) - 6
                });
            }else if( $('.right-handler-controller').text().length < 3  ){
                $('.right-handler-controller').css({
                    'left': parseInt($('.ui-slider-handle').eq(1).css('left')) - 3
                });
            }

            $(this).slider( "disable" );

        },
        slide: function() {

            $('.left-handle-controller').text( $( this ).slider( "values" )[0] );
            $('.right-handler-controller').text( $( this ).slider( "values" )[1] );

            if( $('.left-handle-controller').text().length == 4 ){
                $('.left-handle-controller').css({
                    'left': parseInt($('.ui-slider-handle').eq(0).css('left')) - 9
                });
            }else if( $('.left-handle-controller').text().length == 3  ){
                $('.left-handle-controller').css({
                    'left': parseInt($('.ui-slider-handle').eq(0).css('left')) - 6
                });
            }else if( $('.left-handle-controller').text().length < 3  ){
                $('.left-handle-controller').css({
                    'left': parseInt($('.ui-slider-handle').eq(0).css('left')) - 1
                });
            }

            $('.min-span-controlller').text( $('.left-handle-controller').text() );
            $('.max-span-controlller').text( $('.right-handler-controller').text() );

            if( $('.right-handler-controller').text().length == 4 ){
                $('.right-handler-controller').css({
                    'left': parseInt($('.ui-slider-handle').eq(1).css('left')) - 9
                });
            }else if( $('.right-handler-controller').text().length == 3  ){
                $('.right-handler-controller').css({
                    'left': parseInt($('.ui-slider-handle').eq(1).css('left')) - 6
                });
            }else if( $('.right-handler-controller').text().length < 3  ){
                $('.right-handler-controller').css({
                    'left': parseInt($('.ui-slider-handle').eq(1).css('left')) - 3
                });
            }

            $('.min-val').val( +$('.left-handle-controller').text() );
        
            $('.max-val').val( +$('.right-handler-controller').text() );

        },
        change: function( event, ui ) {

            $('.left-handle-controller').text( $( this ).slider( "values" )[0] );
            $('.right-handler-controller').text( $( this ).slider( "values" )[1] );

            if( $('.left-handle-controller').text().length == 4 ){
                $('.left-handle-controller').css({
                    'left': parseInt($('.ui-slider-handle').eq(0).css('left')) - 9
                });
            }else if( $('.left-handle-controller').text().length == 3  ){
                $('.left-handle-controller').css({
                    'left': parseInt($('.ui-slider-handle').eq(0).css('left')) - 6
                });
            }else if( $('.left-handle-controller').text().length < 3  ){
                $('.left-handle-controller').css({
                    'left': parseInt($('.ui-slider-handle').eq(0).css('left')) - 1
                });
            }

            $('.min-span-controlller').text( $('.left-handle-controller').text() );
            $('.max-span-controlller').text( $('.right-handler-controller').text() );


            if( $('.right-handler-controller').text().length == 4 ){
                $('.right-handler-controller').css({
                    'left': parseInt($('.ui-slider-handle').eq(1).css('left')) - 9
                });
            }else if( $('.right-handler-controller').text().length == 3  ){
                $('.right-handler-controller').css({
                    'left': parseInt($('.ui-slider-handle').eq(1).css('left')) - 6
                });
            }else if( $('.right-handler-controller').text().length < 3  ){
                $('.right-handler-controller').css({
                    'left': parseInt($('.ui-slider-handle').eq(1).css('left')) - 3
                });
            }

       
            $('.min-val').val( +$('.left-handle-controller').text() );
        
            $('.max-val').val( +$('.right-handler-controller').text() );
               
            
        }
    });

    $('.ui-slider-handle').on('mouseover',function(){
        $('.filter-slider').slider( "enable" );
    });
    $('.ui-slider-handle').on('mouseout',function(){
        $('.filter-slider').slider( "disable" );
    });
});


// date on main page
$(function(){
    var order_start_date = new Date();
    var order_end_date = "+7d";

    if ($('.order-start-traveling').val()) {
        order_start_date = new Date($('.order-start-traveling').val());
    }
    if ($('.order-end-traveling').val()) {
        order_end_date = new Date($('.order-end-traveling').val());
    }

    if($('.when-inp').length != 0){

        // hack for Chubb using UTC+11:00 (Sydney time?)
        // if it's between 00:00 and 12:59 in UTC, use the UTCDate
        // if it's between 13:00 and 23:59 in UTC, use UTCDate + 1
        var now = new Date();
        var utc_today = new Date(now.getUTCFullYear(), now.getUTCMonth(), now.getUTCDate(), 0, 0, 0, 0);
        var utc_tomorrow = new Date(now.getUTCFullYear(), now.getUTCMonth(), now.getUTCDate() + 1, 0, 0, 0, 0);
        var minDate = now.getUTCHours() < 13 ? utc_today : utc_tomorrow;

        $('.from').datepicker({
            dateFormat: 'dd M yy',
            prevText: '‹',
            nextText: '›',
            minDate:  minDate,
            maxDate: new Date(minDate.getTime() + 3600 * 1000 * 24 * 365),
            altField: $('.order-start-traveling'),
            altFormat: 'yy-mm-dd'
        }).on('change', function () {
            $('.from').datepicker('option', {
                defaultDate: $('.from').datepicker('getDate')
            });
            $('.to').datepicker('option', {
                minDate: $('.from').datepicker('getDate'),
                // placeholder if (adult >= 70) maxDate = order_start_date + 180
                maxDate: new Date( new Date( $('.from').datepicker('getDate') ).getTime() + 3600 * 1000 * 24 *(365 - 1)),
                // this doesn't work. docs say "first opening" but:
                // 1) destroying and recreating doesn't work
                // 2) need to set to selected date (below) to make it appear
                defaultDate: new Date( new Date( $('.from').datepicker('getDate') ).getTime() + 3600 * 1000 * 24 *7)
            });
            
            if( $(".to").datepicker('getDate') && $(".from").datepicker('getDate') ){
                if( $(".to").datepicker('getDate').getTime() - $(".from").datepicker('getDate').getTime() <= 3600 * 1000 * 24 * 7 ){
                    $(".to").datepicker('setDate', new Date( $('.from').datepicker('getDate').getTime() + 3600 * 1000 * 24 *7) );
                }
            }

        });

        $('.to').datepicker({
            dateFormat: 'dd M yy',
            prevText: '‹',
            nextText: '›',
            // dates set when from changes
            altField: $('.order-end-traveling'),
            altFormat: 'yy-mm-dd'
        }).on('change', function () {
            $('.to').datepicker('option', {
                defaultDate: $('.to').datepicker('getDate')
            });
        });


        $('.from,.to').on('click',function(){
            $( this ).datepicker( "show" );
        });

        $('.from').datepicker('setDate', order_start_date).trigger('change');
        $('.to').datepicker('setDate', order_end_date).trigger('change');


        $('.when-inp input').on('click',function(){
            if($('#ui-datepicker-div').offset().top < $(this).offset().top){
                $('#ui-datepicker-div').addClass('ui-datepicker-div-higher');
            }else{
                $('#ui-datepicker-div').removeClass('ui-datepicker-div-higher');
            }
        });
        

    }

});

//autocompleter with countries

$(function(){

    var val = [];
    if($('.where-div').length != 0){

        var options = {

            data: countries,

            getValue: "name",

            list: {
                match: {
                    enabled: true
                },
                maxNumberOfElements: 8,


                onShowListEvent: function(){
                    $('.easy-autocomplete-container').css({
                        'border':'1px solid #dadada'
                    })

                    $('.easy-autocomplete-container .eac-item').each(function(){
                        var currentCountry = $(this).text();
                        var that = this;
                        val.forEach(function(item){
                            if(item == currentCountry){
                                $(that).parent('li').remove();
                            }
                        });
                    });
                },
                onHideListEvent: function(){
                    $('.easy-autocomplete-container').css({
                        'border':'none'
                    })
                },
                onClickEvent: function(){
                    val = [];
                    $(' #where').before('<a class=selected-country>'+ $('#where').val() + '</a>');
                    if($('.selected-country').length != 0){
                        $('.where-div').css('padding','2px 10px 9px 20px');
                    }
                    $('#where').focus();

                    if($('.where-div .selected-country').length != 0){
                        $('.where-div .selected-country').each(function(item,index){
                            val.push( $(this).text().slice(0,$(this).text().length) );
                        });
                    }
                    $('#where').val('');
                    //$('.selected-country').on('click',function(){
                    //    $(this).remove();
                    //    if($('.selected-country').length == 0){
                    //        $('.where-div').css('padding','8px 10px 14px 20px');
                    //    }
                    //    val = [];
                    //    $('.selected-country').each(function(){
                    //        val.push( $(this).text().slice(0,$(this).text().length-1) );;
                    //    });
                    //});
                    var setCountries = val.join("|");
                    $('input#Orders_countries').val(setCountries);


                    $('.selected-country').each(function(){
                        var that = this;
                        countries.forEach(function(item){
                            if( $(that).text() == item.name){
                                $(that).find('span').remove();
                                $(that).prepend('<span class="flag flag-' + item.code.toLowerCase() + ' "></span>');
                            }
                        });
                    });

                }
            },

            template: {
                type: "custom",
                method: function(value, item) {
                    return "<span class='flag flag-" + (item.code).toLowerCase() + "' ></span>" + value;
                }
            },

            theme: "round"
        };

        a = $("#where").easyAutocomplete(options);

    }


    var order_countries = $('.where-values').val();

    $('input#Orders_countries').val(order_countries);
    if (order_countries) {
        order_countries = order_countries.split('|');
        var k_arr = [];
        order_countries.forEach(function(item, k, arr) {
            if (!(item in k_arr)) {
                $(' #where').before('<a class=selected-country>'+ item + '</a>');
                val.push(item);
                k_arr[item] = 1;
            }
        });
    }


    if($('.selected-country').length != 0){
        $('.where-div').css('padding','2px 10px 9px 20px');
    }


    $(document).on('click','.selected-country:not(.confirmation-countries-block .selected-country)',function(){
        $(this).remove();

        if($('.selected-country:not(.confirmation-countries-block .selected-country)').length == 0){
            $('.where-div').css('padding','8px 10px 14px 20px');
        }
        val = [];
        $('.selected-country:not(.confirmation-countries-block .selected-country)').each(function(){
            val.push( $(this).text().slice(0,$(this).text().length) );;
        });

        var setCountries = val.join("|");
        $('input#Orders_countries').val(setCountries);

    });

    $('.selected-country').each(function(){
        var that = this;
        countries.forEach(function(item){
            if( $(that).text() == item.name){
                $(that).find('span').remove();
                $(that).prepend('<span class="flag flag-' + item.code.toLowerCase() + ' "></span>');
            }
        });
    });

});




// INPUTS HIDDEN VAlUES
$(function(){

    $('.country-to-edit').on('change',function(){
        $('input[name="editted-country"]').val( $('.country-to-edit .jq-selectbox__select-text').text() );
    });

    $('.company-country-to-edit').on('change',function(){
        $('input[name="editted-company-country"]').val( $('.company-country-to-edit .jq-selectbox__select-text').text() );
    });

    $('.subscribe-select').on('change',function(){
        $('.interest-inp').val( $(this).val() == 'Interests' ? '' : $(this).val() );
    });

    $('.policy-address-country').on('change',function(){
        var currentVal = $('.policy-address-country .jq-selectbox__select-text').text() == 'Country' ? '' : $('.company-country-to-edit .jq-selectbox__select-text').text();
        $('input[name="OrderDetails[country]"]').val( $('.policy-address-country .jq-selectbox__select-text').text() );

        $('input[name="OrderDetails[country]"]').val($('input[name="OrderDetails[country]"]').val() == 'Country' ? '' : $('input[name="OrderDetails[country]"]').val());
    });

    $('.policy-address-state').on('change',function(){

        $('input[name="OrderDetails[state]"]').val($(this).val());
    });

});

// Blocked interests select
$(function(){


    //$('.form-block .subscribe').on('click',function(){
    //    $(this).toggleClass('clicked-interests');
    //
    //    if($(this).hasClass('clicked-interests')){
    //        $('.interests-block .jq-selectbox').hide();
    //        $('.disabled-select').show();
    //    }else{
    //        $('.interests-block .jq-selectbox').show();
    //        $('.disabled-select').hide();
    //    }
    //});

});

// CARD LISTENER
$(function(){

$.fn.setCursorPosition = function(pos) {
    if ($(this).get(0).setSelectionRange) {

        $(this).get(0).setSelectionRange(pos, pos);

    } else if ($(this).get(0).createTextRange) {
        var range = $(this).get(0).createTextRange();
        range.collapse(true);
        range.moveEnd('character', pos);
        range.moveStart('character', pos);
        range.select();
    }
}

    var cardType;

    $('.card-holder-name').on('change',function(e){
        if(e.target.value.match(/\w+ \w+/)){
            $(this).removeClass('invalid');
            $(this).siblings('.invalid-label').fadeOut();
        }else{
            $(this).addClass('invalid');
            $(this).siblings('.invalid-label').text('Enter first and last names').fadeIn();
        }
    });


    $('.card-holder-name').on('keydown',function(e){
        if(e.target.value.match(/\w+ \w+/)){
            $(this).removeClass('invalid');
            $(this).siblings('.invalid-label').fadeOut();
        }else{
            $(this).addClass('invalid');
            $(this).siblings('.invalid-label').text('Enter first and last names').fadeIn();
        }
    });
    $('.card-holder-name').on('keydown',function(e){

        var probels = e.target.value.match(/ /g) || '';

        if(e.keyCode == 32 && probels.length == 1){
            return false;
        }

        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 32]) !== -1 ||
                // Allow: Ctrl+A, Command+A
            (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) ||
                // Allow: home, end, left, right, down, up
            (e.keyCode >= 33 && e.keyCode <= 40)) {
            // let it happen, don't do anything
            return;
        }

        if((e.keyCode >= 65 && e.keyCode <= 90) || (e.keyCode >= 97 && e.keyCode <= 122)){
            return true;
        }else{
            return false;
        }
    });    

    $('.payment-inp-group .mmyy-card').on('keydown',function(e){

        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110]) !== -1 ||
                    // Allow: Ctrl+A, Command+A
            (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) ||
                // Allow: home, end, left, right, down, up
            (e.keyCode >= 33 && e.keyCode <= 40)) {
            // let it happen, don't do anything
            return;
        }

        var key = +e.key;

        var number = e.target.value.match(/\d/g) || '';

        if(e.keyCode == 8 && number.length == 1){
            e.target.value = '';
        }
        if(e.keyCode == 8 || e.keyCode == 37){
            if(e.target.value.match(/\d\d \/ $/)){
                $(this).setCursorPosition(2);
            }
        }

        if (Number.isInteger(key) || e.keyCode == 39) {
            if(e.target.value.match(/\d\d \/ $/)){
                $(this).setCursorPosition(6);
            }
        }else{
            e.preventDefault();
        }


    });

    $('.payment-inp-group .mmyy-card').on('change',function(e){  

        var matchedVal = e.target.value.match(/\d\d \/ \d\d/) === null ? '' : e.target.value.match(/\n\n \/ \n\n/) === null;       

        if(matchedVal.length == 0 || +e.target.value.split(' / ')[0] > 12 ){
            $(this).addClass('invalid');
            $(this).siblings('.invalid-label').fadeIn();
        }else{
            $(this).removeClass('invalid');
            $(this).siblings('.invalid-label').fadeOut();
        }



        if( e.target.value.match(/\d\d( )?(\/)?( )?\d\d/).length && (e.keyCode != 37 && e.keyCode != 39)){
            var digits = e.target.value.match(/\d/g);

            e.target.value = digits[0] + digits[1] + ' / ' + digits[2] + digits[3];

            if(+e.target.value.split(' / ')[0] < 13){

                $(this).removeClass('invalid').siblings('.invalid-label').fadeOut(); 
            }
        }

    });


    $('.payment-inp-group .mmyy-card').on('keydown',function(e){

        var matchedVal = e.target.value.match(/\d\d \/ \d\d/) === null ? '' : e.target.value.match(/\n\n \/ \n\n/) === null;

        if(matchedVal.length == 0 || +e.target.value.split(' / ')[0] > 12 ){
            $(this).addClass('invalid');
        }else{
            $(this).removeClass('invalid');
            $(this).siblings('.invalid-label').fadeOut();
        }
    });

    $('.payment-inp-group .mmyy-card').on('keydown',function(e){
        var value = e.target.value;

        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110]) !== -1 ||
                // Allow: Ctrl+A, Command+A
            (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) ||
                // Allow: home, end, left, right, down, up
            (e.keyCode >= 33 && e.keyCode <= 40)) {
            // let it happen, don't do anything
            return;
        }

        var key = +e.key;
        
        if (!Number.isInteger(key)) {
            e.preventDefault();
        }

        if ( e.shiftKey || !Number.isInteger(key) ) {
            e.preventDefault();
        } else {

            if(value.length <= 6){
                if( (Number.isInteger(key)) ||  e.keyCode == 8){
                    if(value.length == 2 && e.keyCode != 8){
                        e.target.value += ' / ';
                    }
                }else{
                    return false;
                }
            }else{
                return false
            }
        }
    });

    $('.cvc-card').on('input', function (e) {
        var goodLength = cardType == 'Amex' ? 4 : 3;
        if (e.target.value.length == goodLength) {
            $(this).removeClass('invalid');
            $(this).siblings('.invalid-label').fadeOut();
        }
    });

    $('.cvc-card').on('blur', function (e) {
        var goodLength = cardType == 'Amex' ? 4 : 3;
        if (e.target.value.length != goodLength) {
            $(this).addClass('invalid');
            $(this).siblings('.invalid-label').text('Enter the ' + goodLength + '-digit security code').fadeIn();
        }
    });

    // https://en.wikipedia.org/wiki/Payment_card_number#Issuer_identification_number_.28IIN.29
    // works without the whole number, so the card type is known before the user finishes
    function setCardType(number) {
        cardType = /^4/.test(number) ? 'Visa' :
            // 2221-2720 but we shortcut it
            /^5[1-5]|^2[2-7]/.test(number) ? 'Master' :
                /^3[47]/.test(number) ? 'Amex' :
                    undefined;
    }

    $('.card-holder-number').on('input', function (e) {
        var input = this.value;
        var number = input.replace(/\D/g, '');
        setCardType(number);
        if (cardType) {
            $('[name="Cart[card-type]"]').val(cardType);
            $('.credit-card-image').removeClass('credit-card-possible');
            $('.credit-card-image[data-value="' + cardType + '"]').addClass('credit-card-possible');
            if (luhn(number) &&
                (cardType == 'Amex' && number.length == 15 ||
                cardType == 'Master' && number.length == 16 ||
                cardType == 'Visa' && (number.length == 13 || number.length == 16 || number.length == 19))) {
                $(this).removeClass('invalid');
                $(this).siblings('.invalid-label').fadeOut();
            }
        }
    });

    $('.card-holder-number').on('blur', function (e) {
        var input = this.value;
        var number = input.replace(/\D/g, '');
        setCardType(number);
        if (cardType == 'Amex') {
            if (number.length > 10)
                this.value = number.slice(0, 4) + ' ' + number.slice(4, 10) + ' ' + number.slice(10);
            else if (number.length > 4)
                this.value = number.slice(0, 4) + ' ' + number.slice(4);
            if (number.length != 15) {
                $(this).addClass('invalid');
                $(this).siblings('.invalid-label').text('Enter a 15-digit card number').fadeIn();
            } else if (!luhn(number)) {
                $(this).addClass('invalid');
                $(this).siblings('.invalid-label').text('Please double-check the number').fadeIn();
            }
        } else if (cardType) {
            if (number.length > 13)
                this.value = number.slice(0, 4) + ' ' + number.slice(4, 8) + ' ' + number.slice(8, 12) + ' ' + number.slice(12);
            else if (cardType == 'Visa' && number.length > 12)
                this.value = number.slice(0, 4) + ' ' + number.slice(4, 7) + ' ' + number.slice(7, 10) + ' ' + number.slice(10);
            else if (number.length > 8)
                this.value = number.slice(0, 4) + ' ' + number.slice(4, 8) + ' ' + number.slice(8);
            else if (number.length > 4)
                this.value = number.slice(0, 4) + ' ' + number.slice(4);
            if (number.length != 16 &&
                (cardType != 'Visa' || (number.length != 13 && number.length != 19))) {
                $(this).addClass('invalid');
                $(this).siblings('.invalid-label').text('Enter a 16-digit card number').fadeIn();
            } else if (!luhn(number)) {
                $(this).addClass('invalid');
                $(this).siblings('.invalid-label').text('Please double-check the number').fadeIn();
            }
        } else {
            $(this).addClass('invalid');
            $(this).siblings('.invalid-label').text('Enter an accepted card type').fadeIn();
        }
    });

    function luhn(number) {
        var sum = 0;
        while (number > 0) {
            sum += number % 10;
            number = Math.floor(number / 10);

            var even = 2 * (number % 10);
            if (even >= 10)
                sum += even - 9;
            else
                sum += even;
            number = Math.floor(number / 10);
        }
        return sum % 10 == 0;
    }

    $('.cvc-card,.card-holder-number,.card-holder-name').on('change',function(e){      
        if( $(this).hasClass('invalid') ){
            $(this).siblings('.invalid-label').fadeIn();
        }else{
            $(this).siblings('.invalid-label').fadeOut();        
        }
    });

    if( $('.adult-phone').length ){



   

    function deleteZeroFunc(string){
        var deletedValue = [];
        string.split('').forEach(function(item,index){
            if(index == 0){
                if(item == 0){

                }
            }else{
               deletedValue.push( item ); 
            }
        });

        return deletedValue.join('');
    }



    $(".adult-phone").intlTelInput({
            utilsScript: "/themes/mytripproj/js/utils.js",
    });

    $('.flag-disabled').on('click',function(e){
        e.preventDefault();
    });

    var hiddenNumber = $('.adult-phone').attr('data-value');
    if( hiddenNumber ){

       hiddenNumber = hiddenNumber.replace(/ /g,'');
       var currentCode = '+' + $('.country-list').find('.preferred').attr('data-dial-code');

       var defaultTelVal = hiddenNumber;
       if( defaultTelVal[0] == '+'){
            var telNumber = defaultTelVal.slice(currentCode.length);
       }else {
            var telNumber = defaultTelVal;
       }

       //if( telNumber[0] == '0' && telNumber.length == 10){
           //telNumber = deleteZeroFunc(telNumber);
       //}


       $('.hidden-adult-numb').val(currentCode + telNumber);


        setTimeout(function(){

            $('.adult-phone').val( telNumber );
            $('.adult-phone').trigger('change');
        },400);
    }


    $('.adult-phone').on('change',function(e){
        var val = e.target.value;

        if(val.match(/0?\d{8}\d?/)){

            if( val.length == 10 && val[0] == 0){
                var deletedZero;
                deletedZero = deleteZeroFunc(val);

                var currentCountryCode = $('.country-list').find('.active').attr('data-dial-code');
                var finishValue = '+' + currentCountryCode + deletedZero;
                $('.hidden-adult-numb').val( finishValue );

                $(this).removeClass('invalid');
            }
        }

        if( val.length == 9 && val[0] != 0){
            var currentCountryCode = $('.country-list').find('.preferred').attr('data-dial-code');    console.log(currentCountryCode);
            var finishValue = '+' + currentCountryCode + val;
            $('.hidden-adult-numb').val( finishValue );

            $(this).removeClass('invalid');
        }

    });

    $('.adult-phone').on('keydown',function(e){
        var val = e.target.value;
        $('.hidden-adult-numb').val( '' );
        $(this).addClass('invalid');

        if(val.match(/0?\d{8}\d?/)){

            if( val.length == 10 && val[0] == 0){
                var deletedZero;
                deletedZero = deleteZeroFunc(val);

                var currentCountryCode = $('.country-list').find('.active').attr('data-dial-code');
                var finishValue = '+' + currentCountryCode + deletedZero;
                $('.hidden-adult-numb').val( finishValue );

                $(this).removeClass('invalid');
            }

            if( val.length == 9 && val[0] != 0){
                var currentCountryCode = $('.country-list').find('.active').attr('data-dial-code');    
                var finishValue = '+' + currentCountryCode + val;
                $('.hidden-adult-numb').val( finishValue );

                $(this).removeClass('invalid');
            }

        }else{
            $(this).addClass('invalid');
        }


    });

    $('.adult-phone').on('keydown',function(e){


        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110]) !== -1 ||
                // Allow: Ctrl+A, Command+A
            (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) ||
                // Allow: home, end, left, right, down, up
            (e.keyCode >= 33 && e.keyCode <= 40)) {
            // let it happen, don't do anything
            return;
        }
        // Ensure that it is a number and stop the keypress

        var key = +e.key;
        
        if ( !Number.isInteger(key) ) {
            e.preventDefault();
        }else {
            if( e.target.value[0] == 0 ){
                if( ( Number.isInteger(key) && e.target.value.length < 10 ) ||  e.keyCode == 8){
                    
                }else{
                    return false;
                }
            }else{
                if( ( Number.isInteger(key) && e.target.value.length < 9 ) ||  e.keyCode == 8){
                    
                }else{
                    return false;
                }  
            }
        }
    });

 }
    //$('.card-holder-number').on('keypress',function(e){
    //    var numberMatchVal = e.target.value.match(/\d/g) === null ? '' : e.target.value.match(/\d/g);
    //    if( (e.keyCode >= 48 && e.keyCode <= 57 && e.target.value.length <= 18) ||  e.keyCode == 8){
    //        if( (numberMatchVal.length == 4 || numberMatchVal.length == 8 || numberMatchVal.length == 12) && e.keyCode != 8){
    //            e.target.value += ' ';
    //        }
    //    }else{
    //        return false;
    //    }
    //});

});

// Autocomplete input's values after loading page
$(function(){

    $('.birth-date').val( $('.birth-date').parent().find('.editted-date').val() );
    $('.expiry').val( $('.expiry').parent().find('.editted-date').val() );
    $('.filter-from-date').val( $('.filter-from-date').parent().find('.editted-date').val() );  
    $('.filter-to-date').val( $('.filter-to-date').parent().find('.editted-date').val() );

    $('.autoloadInfo').on('change',function(){
        $(this).siblings('input.editted-date').val( $(this).val() );
    });


});


$(function(){
    if( $('.confirmation-cover .details-pack').length == 0 ){
        $('.confirmation-info-packs .confirmation-cover').append('<h3>None selected</h3>');
    }
    if( $('.details.details-plan-block p').length == 0 ){
        $('.details.details-plan-block').append('<h3 class="undef">None selected</h3>');
    }
    $('.plan-quote').on('click',function(){
        $('.plans-group-block .plan-quote').removeClass('red-quote')
            .parent('.plans-block').removeClass('active-plan-quote')
            .find('.plan-val').removeClass('plan-val-active');

        var packets_class = $(this).attr('data-packets');
        console.log(packets_class);

        $('.option-packets-wrapper').hide();
        $('.option-packets .details-pack').removeClass('added-details-pack');
        $('.confirmation-cover-pack .details-pack').remove();
        if( $('.confirmation-cover .details-pack').length == 0 ){
            $('.confirmation-cover-pack > h3').remove();
            $('.confirmation-info-packs .confirmation-cover').append('<h3 class="undef">None selected</h3>');
        }
        $("." + packets_class).show(400);
        console.log("." + packets_class);
        //if( !$(this).hasClass('red-quote') ){
            $(this).addClass('red-quote')
              .parent('.plans-block').addClass('active-plan-quote')
              .find('.plan-val').addClass('plan-val-active');
        //}else{
        //    $(this).removeClass('red-quote')
        //      .parent('.plans-block').removeClass('active-plan-quote')
        //      .find('.plan-val').removeClass('plan-val-active');
        //}
        calcQuotePrice();
        buy_now();

        $(".confirmation-info,.buyWrapper").show(400);

        var parentsIndex = $(this).parent().index() + 1;
        console.log(parentsIndex);
        $('.benefits-table tr td').removeClass('active-benefits');
        $('.benefits-table tr td:nth-child('+ parentsIndex +')' ).addClass('active-benefits');
    });

    $('.option-packets .details-pack').on('click',function(){


        if( !$(this).hasClass('added-details-pack') ){
            var elem = $(this).clone().prepend('<a class="pack-close" href="#"></a>').wrap('<p/>').parent().html();;
            //console.log(elem);
            $(elem).appendTo('.confirmation-cover-pack');
            $('.confirmation-cover-pack > h3').remove();
            $(this).addClass('added-details-pack');
        }else{
            var pack_class = $(this).attr('data-pack');
            $('.confirmation-cover-pack > .' + pack_class).remove();
            if( $('.confirmation-cover .details-pack').length == 0 ){
                $('.confirmation-cover-pack > h3').remove();
                $('.confirmation-info-packs .confirmation-cover').append('<h3 class="undef">None selected</h3>');
            }

            $(this).removeClass('added-details-pack'); 
        }
        calcQuotePrice();
    });

    //$('.pack-close').on('click',function(e){
    $(document).on('click','.pack-close', function(e){
        e.preventDefault();

        var pack_class = $(this).parent().attr('data-pack');

        $('.'+pack_class+'.added-details-pack').click();

        //$(this).parent().remove();
        if( $('.confirmation-cover .details-pack').length == 0 ){
            $('.confirmation-cover-pack > h3').remove();
            $('.confirmation-info-packs .confirmation-cover').append('<h3 class="undef">None selected</h3>');
        }
        calcQuotePrice();
    });


});

function calcQuotePrice() {
    var quote_price = parseFloat($('.active-plan-quote .quote-packet-price').text().replace(',',''));

    var key = '';
    var option_price = 0.0;
    var options_array = {};
    $('.confirmation-info-packs .details-pack').each(function( index ) {
        key = $( this).find('.packs-name').attr('data-key');
        console.log(key);
        option_price = option_price + parseFloat($( this).find('.option-packet-price').text().replace(',','') );
        options_array[key] = {name : $( this).find('p').text(), value : $( this).find('.option-packet-price').text()};
        //options_array[key]['value'] = $( this).find('.option-packet-price').text();
        //console.log( index + ": " + $( this ).text() );
    });
    quote_price = quote_price + option_price;

    //options_array = JSON.stringify(options_array);
    console.log(options_array);
    var m = JSON.stringify(options_array).toString();
    //console.log(m);

    $('.circle-cover-info span').text(quote_price.toFixed(2));
    $('.total-prem h4').html('<span>$</span>'+ quote_price.toFixed(2) + ' ');

    $('.details-plan-block h2').text($('.active-plan-quote .plan-name').text());
    $('.details-plan-block h3').remove();

    $("input[name='Orders[price]']").val(quote_price.toFixed(2));
    $("input[name='Orders[quote]']").val($('.active-plan-quote .plan-name').text());
    $("input[name='Orders[options]']").val(m);
}


function buy_now() {
    var that = '.buyNow';
    if ($("input[name='Orders[quote]']").val() && $("input[name='Orders[price]']").val()) {
        $(that).prop('disabled', false);
        $(that).css('background-color', '');
    } else {
        $(that).prop('disabled', true);
        $(that).css('background-color', '#888');
    }
}
buy_now('.buyNow');


//STEP3 datepickers

$(function(){

    function min(that){
        var dateMin = $(that).attr('data-min');
        return new Date(+dateMin.split('-')[0], +dateMin.split('-')[1] - 1, +dateMin.split('-')[2]);
    }
    function max(that){
        var dateMax = $(that).attr('data-max');
        return new Date(+dateMax.split('-')[0], +dateMax.split('-')[1] - 1, +dateMax.split('-')[2]);
    }

    $('.traveller').each(function(){
        $(this).datepicker({
            prevText: '‹',
            nextText: '›',
            changeMonth: true,
            minDate: min(this),
            maxDate: max(this),
            changeYear: true,
            dateFormat: 'dd/mm/yy',
            orientation: "bottom",
            showOn: "both"
        });


        if( $(this).val() ){

            var birthday = new Date( $(this).val() );
            var minDate = new Date( $(this).attr('data-min') ).getTime();
            var maxDate = new Date( $(this).attr('data-max') ).getTime();


            if( minDate - birthday.getTime()  > 0){
                $(this).datepicker('setDate', new Date( $(this).attr('data-min')));

            }else if( maxDate - birthday.getTime() < 0){
                $(this).datepicker('setDate', new Date( $(this).attr('data-max')));

            }else{
                $(this).datepicker('setDate', birthday );

            }

        }

    });



    $('.traveller').on('click',function(){
        if($('#ui-datepicker-div').offset().top < $(this).offset().top){
            $('#ui-datepicker-div').addClass('ui-datepicker-div-higher');
        }else{
            $('#ui-datepicker-div').removeClass('ui-datepicker-div-higher');
        }
    });
});

// BENEFITS
$(function(){

    $('.benefits-toggle').on('click',function(e){
        e.preventDefault();
        var toggler = $(this);
        toggler.toggleClass('closed-benefits');
        if( !toggler.hasClass('closed-benefits') ){
            $('.benefits-table').fadeIn();
            toggler.addClass('opened-benefits');
        }else{
            $('.benefits-table').hide(); 
            toggler.removeClass('opened-benefits');
        }

    });

});


$(function(){

    $('[data-toggle="tooltip"]').tooltip({
        html: true
    });

    $('[data-toggle="tooltip"]').on('click',function(e){
        e.preventDefault();
    });

});
