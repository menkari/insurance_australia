$(function(){

    $('.state-select').styler();
    $('.referral-select').styler();

    $('#age').on('change',function() {
        $('input.order-age').val($(this).val());
    });
    $('#age2').on('change',function() {
        $('input.order-age2').val($(this).val());
    });

    var today = new Date();
    var today_day = today.getDate();
    var today_month= today.getMonth() + 1;
    var today_year = today.getFullYear();

    //var today_date = today_day + '-' + today_month + '-' + today_year;
    //$('input#Orders_start_traveling').val(today_date);
    //$('input#Orders_end_traveling').val(today_date);

    var order_age2 = '';
    var kids_array = [];

    $('.add-adult').click(function(e){
        e.preventDefault();
        var count = $(".adult-block").children('.age-inp-wrap').length;

        if (count < 2) {
            $(".adult-block").append('<div class="age-inp-wrap"><input type="text" name="age2" class="added-adult added-age" value="' + order_age2 + '"><a class="wrong wrong-adult"></a></div>');
            $("input[name='Orders[age2]']").val(order_age2);
            if (count == 1) {
                $(this).hide();
            }
        }
    });

    $('.add-kid').click(function(e){
        e.preventDefault();
        var count = $(".kids-block").children('.age-inp-wrap').length;

        if (count < 5) {
            //var kid = "";
            //if (kids_array[count]) {
            //    kid = kids_array[count];
            //}
            $(".kids-block").append('<div class="age-inp-wrap"><input type="text" name="kids[]" class="added-kid added-age" value=""><a class="wrong wrong-kid"></a></div>');
            if (count == 4) {
                $(this).hide();
            }
        }
    });

    //$(document).on('change', '.added-kid', function(){
    //
    //});

    $(document).on('click', '.wrong-adult', function(){
        order_age2 = $(this).closest('.age-inp-wrap').find('input').val();
        $("input[name='Orders[age2]']").val("");
        $(this).closest('.age-inp-wrap').remove();

        if ($(".adult-block").children('.age-inp-wrap').length < 2) {
            $('.add-adult').show();
        } else {
            $('.add-adult').hide();
        }
    });

    $(document).on('click', '.wrong-kid', function(){
        $(this).closest('.age-inp-wrap').remove();

        if ($(".kids-block").children('.age-inp-wrap').length < 6) {
            $('.add-kid').show();
        } else {
            $('.add-kid').hide();
        }
        kids_refresh();
    });

    $('.kids-block').on('change', 'input', function(){
        var kid_age = parseInt($(this).val());
        // if (kid_age > 23) {
        //     $(this).val(23);
        if (kid_age >= 18) {
            $(this).val(18 - 1);
        } else if (kid_age < 0) {
            $(this).val(0);
        } else if (isNaN(kid_age)) {
            $(this).val('');
        }
        kids_refresh();
    });

    function kids_refresh() {
        kids_array = $("input[name='kids[]']")
            .map(function(){return $(this).val();}).get();
        var kids = kids_array.filter(Boolean)
        kids= kids.join();
        $("input[name='Orders[kids]']").val(kids);
    }

    //$('.update-base-order-data').on('submit', function(e) {
    //    e.preventDefault();
    //
    //    $(this).submit();
    //});

    $('.adult-block').on('change', 'input[name=age2]', function(){
        var value = $(this).val();
        $("input[name='Orders[age2]']").val(value);
    });

    $('.reg').on('click',function(e){
        e.preventDefault();
        $('body').wrapInner("<div class='wrapper'></div>");
        $('.wrapper').css({
            'height':'950px',
            'overflow':'hidden'
        });
        $('.popup-wrapper').fadeIn();
        $('.p-reg').click();
    });

    $(document).on('click', '.buyNow', function(){
        if ($("input[name='Orders[quote]']").val() && $("input[name='Orders[price]']").val()) {
            $('.orders-quote-packet').submit();
        } else {
            alert('No select plan!');
        }
    });

    $(document).on('click', '#med-cond', function(){
        med_cond(this);
    });

    $(document).on('click', '#med-cond-bottom', function(){
        med_cond(this);
    });

    function med_cond(that) {
        if($('#med-cond').is(':checked')) {
            $(".medical-conditions").show();
            $(".med-cond-bottom-checkbox").show();

            if ($("#med-cond-bottom").is(':checked')) {
                $('.order-details-submit').prop('disabled', false);
                $('.order-details-submit').css('background-color', '');
            } else {
                $('.order-details-submit').prop('disabled', true);
                $('.order-details-submit').css('background-color', '#888');
            }

        } else {
            $(".medical-conditions").hide();
            $(".med-cond-bottom-checkbox").hide();
            $('.order-details-submit').prop('disabled', false);
            $('.order-details-submit').css('background-color', '');
        }
    }

    med_cond('#med-cond');

    $(document).on('click', '.accept-policy-declaration', function(){
        accept_policy_declaration(this);
        //if($(this).is(':checked')) {
        //    $(this).closest('form').find('button[type="submit"]').prop('disabled', false);
        //
        //    $(that).css('background-color', '');
        //} else {
        //    $(this).closest('form').find('button[type="submit"]').prop('disabled', true);
        //    $(that).css('background-color', '#888');
        //}
    });

    function accept_policy_declaration(that) {
        if($(that).is(':checked')) {
            $(that).closest('form').find('button[type="submit"]').prop('disabled', false);
            $(that).closest('form').find('button[type="submit"]').css('background-color', '');
            $(that).css('background-color', '');
        } else {
            $(that).closest('form').find('button[type="submit"]').prop('disabled', true);
            $(that).closest('form').find('button[type="submit"]').css('background-color', '#888');
        }
    }
    accept_policy_declaration('.accept-policy-declaration');
});

$(function() {
    $('.payment-final-form').on('submit', function(e) {
        //e.preventDefault();



        //$(this).submit();
    });
});


function cardnumber(text)
{
    var cardno = /^(?:5[1-5][0-9]{14})$/;
    if(text.match(cardno))
    {
        return true;
    }
    else
    {
        alert("Not a valid Visa credit card number!");
        return false;
    }
}