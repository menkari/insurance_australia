$(function(){
    var graphicData;

        /*GRAPHIC STATISTIC*/

    $.ajax({
        type: 'POST',
        url: '/order/statisticTable',
        data: {
            date_range_start: $('input[name="date_range_start"]').val(),
            date_range_end: $('input[name="date_range_end"]').val()
        },
        success: function(dataTable){
            graphicData = [];
            //console.log(dataTable);
            var data = jQuery.parseJSON(dataTable);
            var formattedDate;

            data.rawData.forEach(function(element) {
                //console.log(element);

                graphicData.push([element.id, +element.sales, +element.revenues]);
            });

            //console.log( graphicData );
            google.charts.load('current', {'packages':['corechart']});
            google.charts.setOnLoadCallback(drawChart);
        }
    });
        //google.charts.load('current', {'packages':['corechart']});
        //google.charts.setOnLoadCallback(drawChart);



        function drawChart() {
            var data = new google.visualization.DataTable();
            data.addColumn('string', 'Date');
            data.addColumn('number', 'Something1');
            data.addColumn('number', 'Something2');

            data.addRows(graphicData);
            //data.addRows([
            //    ['7 Oct',  400],
            //    ['8 Oct',  450],
            //    ['9 Oct',  400],
            //    ['10 Oct', 350],
            //    ['11 Oct', 400],
            //    ['12 Oct', 450],
            //    ['13 Oct', 500],
            //    ['14 Oct', 700],
            //    ['15 Oct', 800],
            //    ['16 Oct', 700],
            //    ['17 Oct', 550],
            //    ['18 Oct', 350],
            //    ['19 Oct', 300],
            //    ['20 Oct', 450]
            //]);

            var options = {
                chartArea:{
                    left:130,
                    top:10
                },
                //isStacked: true,
                legend: {
                    position: 'top',
                    maxLines: 3
                },
                vAxis: {
                    minValue: 0,
                    format: '$#',
                    textStyle: {
                        color: '#858585',
                        fontName: 'Roboto',
                        fontSize: 15
                    }
                },
                hAxis: {
                    textStyle: {
                        color: '#868585',
                        fontName: 'Roboto',
                        fontSize: 15
                    }
                },
                width: 1125,
                height: 260,
                colors: ['#06c7f9','#e1164b']
            };

            var chart = new google.visualization.AreaChart(document.getElementById('statistic-graphic'));




            chart.draw(data, options);


        }

});