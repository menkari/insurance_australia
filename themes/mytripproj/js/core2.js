$(function(){


    $('.where-div').on('click',function(){
        $('.where-inp #where').focus();
    });

    $('.menu-btn').on('click',function(e){
        e.preventDefault();
        $('.left-side-menu').css('position','fixed').animate({
            'left':'0'
        },400);
    });

    $('.close').on('click',function(e){
        e.preventDefault();
        $('.left-side-menu').animate({
            'left':'-256px'
        },400);
    });

    $('.close-popup-mob').on('click',function(e){
        e.preventDefault();
        $('.popup-wrapper').fadeOut();
    });

    $('.p-sign').on('click',function(e){
        e.preventDefault();

        $('.p-content-btns a').removeClass('clicked');
        $(this).addClass('clicked');

        $('.tab').hide().removeClass('visible');

        $('.bottom-line').animate({
            'left':'0'
        },200);

        $(this).addClass('clicked').parent('.p-content-btns').next('.p-tabs').find('.p-sign-tab').fadeIn(200).addClass('visible');
    });

    $('.p-reg').on('click',function(e){
        e.preventDefault();

        $('.p-content-btns a').removeClass('clicked');
        $(this).addClass('clicked');

        $('.tab').hide().removeClass('visible');

        $('.bottom-line').animate({
            'left':'50%'
        },200);

        $(this).addClass('clicked').parent('.p-content-btns').next('.p-tabs').find('.p-reg-tab').fadeIn(200).addClass('visible');
    });

    $('.p-reg-link').on('click',function(e){
        e.preventDefault();
        $('.p-reg').trigger('click');
    });

    $('.p-sign-link').on('click',function(e){
        e.preventDefault();
        $('.p-sign').trigger('click');
    });



    $('.sign').on('click',function(e){
        e.preventDefault();
        $('body').wrapInner("<div class='wrapper'></div>");
        $('.popup-wrapper').fadeIn();
        $('.wrapper').css({
            'height': $('.popup').height() + 323,
            'overflow':'hidden',
            'min-height':$(window).height()
        });
    });

    $('.popup-close-bg').on('click',function(){
        $('.popup-wrapper').fadeOut();
        $('.wrapper').css({
            'height':'auto'
        });
    });

    $('.personal-link').on('click',function(e){
        e.preventDefault();

        if($(this).hasClass('clicked-personal')){
            $(this).removeClass('clicked-personal');
            $('.personal-dropdown').hide();
        }else{
            $(this).addClass('clicked-personal');
            $('.personal-dropdown').show();
        }

    });

    $('.main-page-age').on('keydown',function(e){

        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110]) !== -1 ||
                    // Allow: Ctrl+A, Command+A
            (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) ||
                // Allow: home, end, left, right, down, up
            (e.keyCode >= 33 && e.keyCode <= 40)) {
            // let it happen, don't do anything
            return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }else if( ((e.keyCode >= 48 && e.keyCode <= 57) || (e.keyCode >= 96 && e.keyCode <= 105)) ||  e.keyCode == 8){

         }else{
            return false;
         }
     });

    $('.select-autoload').each(function(){
        var inputsVal = $(this).siblings('input[type=hidden]').val();
        if(inputsVal){
            $(this).append('<option>'+ inputsVal +'</option>');
        }
    });


    $.get({
        url: '/themes/mytripproj/json/countries.json',
        dataType: 'json',
        success: function(data){
            data.forEach(function(item){
                $('.form-select').append('<option>'+ item.name +'</option>');
            });

            $('.form-select option').each(function(item){
                $(this).attr('value', $(this).text());
            });
        }
    }).done(function(){
        $('.form-select').styler();
    });

    // убираем повторение стран
    var hiddenElem;
    setTimeout(function(){
        $('div.select-autoload li').each(function(){
            if( $(this).text() == $(this).closest('.input-item').find('.editted-country').val() ){
                hiddenElem = $(this).text();
                $(this).hide();
            }
        });
    },100)

    $('.select-autoload').on('change',function(){
        $('div.select-autoload li').each(function(){
            if($(this).text() == hiddenElem){
                $(this).show();
            }
        });
        $(this).closest('.input-item').find('.editted-country').val( $(this).val() );
    });



    /*FORM DATEPICKERS*/

    var currentYear = new Date().getFullYear();

    $('.datepicker-input-item input:not(.editted-date)').datepicker({
        prevText: '‹',
        nextText: '›',
        changeMonth: true,
        yearRange: '1900:' + currentYear,
        changeYear: true,
        dateFormat: 'dd/mm/yy',
        orientation: "bottom",
        showOn: "both"
    });

    $('.datepicker-input-item .ui-datepicker-trigger,.datepicker-input-item input:not(.editted-date)').on('click',function(){
        if($('#ui-datepicker-div').offset().top < $(this).offset().top){
            $('#ui-datepicker-div').addClass('ui-datepicker-div-higher');
        }else{
            $('#ui-datepicker-div').removeClass('ui-datepicker-div-higher');
        }
    });

    /*END OF FORM DATEPICKERS*/


});


// Slider on orders page

$(function(){

    $('.filter-slider').slider({
        min: 0,
        max: 5000,
        range: true,
        values: [ $('input[name=min-val]').val() || 0, $('input[name=max-val]').val() || 0 ],
        create: function(){
            $('.left-handle-controller').text( $( this ).slider( "values" )[0] );
            $('.right-handler-controller').text( $( this ).slider( "values" )[1] );

            if( $('.left-handle-controller').text().length == 4 ){
                $('.left-handle-controller').css({
                    'left': parseInt($('.ui-slider-handle').eq(0).css('left')) - 13
                });
            }else if( $('.left-handle-controller').text().length == 3  ){
                $('.left-handle-controller').css({
                    'left': parseInt($('.ui-slider-handle').eq(0).css('left')) - 6
                });
            }else if( $('.left-handle-controller').text().length < 3  ){
                $('.left-handle-controller').css({
                    'left': parseInt($('.ui-slider-handle').eq(0).css('left')) - 1
                });
            }

            $('.min-span-controlller').text( $('.left-handle-controller').text() );
            $('.max-span-controlller').text( $('.right-handler-controller').text() );



            if( $('.right-handler-controller').text().length == 4 ){
                $('.right-handler-controller').css({
                    'left': parseInt($('.ui-slider-handle').eq(1).css('left')) - 13
                });
            }else if( $('.right-handler-controller').text().length == 3  ){
                $('.right-handler-controller').css({
                    'left': parseInt($('.ui-slider-handle').eq(1).css('left')) - 6
                });
            }else if( $('.right-handler-controller').text().length < 3  ){
                $('.right-handler-controller').css({
                    'left': parseInt($('.ui-slider-handle').eq(1).css('left')) - 3
                });
            }

            $(this).slider( "disable" );

        },
        slide: function() {

            $('.left-handle-controller').text( $( this ).slider( "values" )[0] );
            $('.right-handler-controller').text( $( this ).slider( "values" )[1] );

            if( $('.left-handle-controller').text().length == 4 ){
                $('.left-handle-controller').css({
                    'left': parseInt($('.ui-slider-handle').eq(0).css('left')) - 9
                });
            }else if( $('.left-handle-controller').text().length == 3  ){
                $('.left-handle-controller').css({
                    'left': parseInt($('.ui-slider-handle').eq(0).css('left')) - 6
                });
            }else if( $('.left-handle-controller').text().length < 3  ){
                $('.left-handle-controller').css({
                    'left': parseInt($('.ui-slider-handle').eq(0).css('left')) - 1
                });
            }

            $('.min-span-controlller').text( $('.left-handle-controller').text() );
            $('.max-span-controlller').text( $('.right-handler-controller').text() );

            if( $('.right-handler-controller').text().length == 4 ){
                $('.right-handler-controller').css({
                    'left': parseInt($('.ui-slider-handle').eq(1).css('left')) - 9
                });
            }else if( $('.right-handler-controller').text().length == 3  ){
                $('.right-handler-controller').css({
                    'left': parseInt($('.ui-slider-handle').eq(1).css('left')) - 6
                });
            }else if( $('.right-handler-controller').text().length < 3  ){
                $('.right-handler-controller').css({
                    'left': parseInt($('.ui-slider-handle').eq(1).css('left')) - 3
                });
            }

            $('.min-val').val( +$('.left-handle-controller').text() );

            $('.max-val').val( +$('.right-handler-controller').text() );

        },
        change: function( event, ui ) {

            $('.left-handle-controller').text( $( this ).slider( "values" )[0] );
            $('.right-handler-controller').text( $( this ).slider( "values" )[1] );

            if( $('.left-handle-controller').text().length == 4 ){
                $('.left-handle-controller').css({
                    'left': parseInt($('.ui-slider-handle').eq(0).css('left')) - 9
                });
            }else if( $('.left-handle-controller').text().length == 3  ){
                $('.left-handle-controller').css({
                    'left': parseInt($('.ui-slider-handle').eq(0).css('left')) - 6
                });
            }else if( $('.left-handle-controller').text().length < 3  ){
                $('.left-handle-controller').css({
                    'left': parseInt($('.ui-slider-handle').eq(0).css('left')) - 1
                });
            }

            $('.min-span-controlller').text( $('.left-handle-controller').text() );
            $('.max-span-controlller').text( $('.right-handler-controller').text() );


            if( $('.right-handler-controller').text().length == 4 ){
                $('.right-handler-controller').css({
                    'left': parseInt($('.ui-slider-handle').eq(1).css('left')) - 9
                });
            }else if( $('.right-handler-controller').text().length == 3  ){
                $('.right-handler-controller').css({
                    'left': parseInt($('.ui-slider-handle').eq(1).css('left')) - 6
                });
            }else if( $('.right-handler-controller').text().length < 3  ){
                $('.right-handler-controller').css({
                    'left': parseInt($('.ui-slider-handle').eq(1).css('left')) - 3
                });
            }


            $('.min-val').val( +$('.left-handle-controller').text() );

            $('.max-val').val( +$('.right-handler-controller').text() );


        }
    });

    $('.ui-slider-handle').on('mouseover',function(){
        $('.filter-slider').slider( "enable" );
    });
    $('.ui-slider-handle').on('mouseout',function(){
        $('.filter-slider').slider( "disable" );
    });
});

// date on main page
$(function(){
    var order_start_date = null;
    var order_end_date = "+7d";
    if ($('.order-start-traveling').val()) {
        order_start_date = new Date($('.order-start-traveling').val());
    }
    if ($('.order-end-traveling').val()) {
        order_end_date = new Date($('.order-end-traveling').val());
    }

    if($('.when-inp').length != 0){

        $('.from').datepicker({
            dateFormat: 'dd M yy',
            prevText: '‹',
            nextText: '›',
            minDate: new Date(),
            maxDate: new Date( new Date().getTime() + 3600 * 1000 * 24 *365),
            defaultDate: order_start_date,
            altField: $('.order-start-traveling'),
            altFormat: 'yy-mm-dd'
        }).on('change', function () {
            $('.from').datepicker('option', {
                defaultDate: $('.from').datepicker('getDate')
            });
            $('.to').datepicker('option', {
                minDate: $('.from').datepicker('getDate'),
                maxDate: new Date( new Date( $('.from').datepicker('getDate') ).getTime() + 3600 * 1000 * 24 *365),
                defaultDate: new Date( new Date( $('.from').datepicker('getDate') ).getTime() + 3600 * 1000 * 24 *7)
            });
        }).datepicker('setDate', order_start_date);

        $('.to').datepicker({
            dateFormat: 'dd M yy',
            prevText: '‹',
            nextText: '›',
            minDate: new Date(),
            // placeholder if (adult >= 70) maxDate = from + 180
            maxDate: new Date( new Date( $('.from').datepicker('getDate') ).getTime() + 3600 * 1000 * 24 *365),
            defaultDate: order_end_date,
            altField: $('.order-end-traveling'),
            altFormat: 'yy-mm-dd'
        }).on('change', function () {
            $('.to').datepicker('option', {
                defaultDate: $('.to').datepicker('getDate')
            });
        }).datepicker('setDate', order_end_date);


        $('.when-inp input').on('click',function(){
            if($('#ui-datepicker-div').offset().top < $(this).offset().top){
                $('#ui-datepicker-div').addClass('ui-datepicker-div-higher');
            }else{
                $('#ui-datepicker-div').removeClass('ui-datepicker-div-higher');
            }
        });


    }

});

//autocompleter with countries

$(function(){

    var val = [];
    if($('.where-div').length != 0){

        var options = {

            url: "/themes/mytripproj/json/countries.json",

            getValue: "name",

            list: {
                match: {
                    enabled: true
                },
                maxNumberOfElements: 8,


                onShowListEvent: function(){
                    $('.easy-autocomplete-container').css({
                        'border':'1px solid #dadada'
                    })

                    $('.easy-autocomplete-container .eac-item').each(function(){
                        var currentCountry = $(this).text();
                        var that = this;
                        val.forEach(function(item){
                            if(item == currentCountry){
                                $(that).parent('li').remove();
                            }
                        });
                    });
                },
                onHideListEvent: function(){
                    $('.easy-autocomplete-container').css({
                        'border':'none'
                    })
                },
                onClickEvent: function(){
                    val = [];
                    $(' #where').before('<a class=selected-country>'+ $('#where').val() + '</a>');
                    if($('.selected-country').length != 0){
                        $('.where-div').css('padding','2px 10px 9px 20px');
                    }
                    $('#where').focus();

                    if($('.selected-country').length != 0){
                        $('.selected-country').each(function(item,index){
                            val.push( $(this).text().slice(0,$(this).text().length) );
                        });
                    }
                    $('#where').val('');
                    //$('.selected-country').on('click',function(){
                    //    $(this).remove();
                    //    if($('.selected-country').length == 0){
                    //        $('.where-div').css('padding','8px 10px 14px 20px');
                    //    }
                    //    val = [];
                    //    $('.selected-country').each(function(){
                    //        val.push( $(this).text().slice(0,$(this).text().length-1) );;
                    //    });
                    //});
                    $('input#Orders_countries').val(val);


                    $.get({
                        url: '/themes/mytripproj/json/countries.json',
                        dataType: 'json',
                        success: function(data){
                            $('.selected-country').each(function(){
                                var that = this;
                                data.forEach(function(item){
                                    if( $(that).text() == item.name){
                                        $(that).find('span').remove();
                                        $(that).prepend('<span class="flag flag-' + item.code.toLowerCase() + ' "></span>');
                                    }
                                });
                            });
                        }
                    });

                }
            },

            template: {
                type: "custom",
                method: function(value, item) {
                    return "<span class='flag flag-" + (item.code).toLowerCase() + "' ></span>" + value;
                }
            },

            theme: "round"
        };

        a = $("#where").easyAutocomplete(options);

    }


    var order_countries = $('.where-values').val();

    $('input#Orders_countries').val(order_countries);
    if (order_countries) {
        order_countries = order_countries.split(',');
        var k_arr = [];
        order_countries.forEach(function(item, k, arr) {
            if (!(item in k_arr)) {
                $(' #where').before('<a class=selected-country>'+ item + '</a>');
                val.push(item);
                k_arr[item] = 1;
            }
        });
    }


    if($('.selected-country').length != 0){
        $('.where-div').css('padding','2px 10px 9px 20px');
    }


    $(document).on('click','.selected-country:not(.confirmation-countries-block .selected-country)',function(){
        $(this).remove();

        if($('.selected-country:not(.confirmation-countries-block .selected-country)').length == 0){
            $('.where-div').css('padding','8px 10px 14px 20px');
        }
        val = [];
        $('.selected-country:not(.confirmation-countries-block .selected-country)').each(function(){
            val.push( $(this).text().slice(0,$(this).text().length) );;
        });
        $('input#Orders_countries').val(val);

    });

    $.get({
        url: '/themes/mytripproj/json/countries.json',
        dataType: 'json',
        success: function(data){
            $('.selected-country').each(function(){
                var that = this;
                data.forEach(function(item){
                    if( $(that).text() == item.name){
                        $(that).find('span').remove();
                        $(that).prepend('<span class="flag flag-' + item.code.toLowerCase() + ' "></span>');
                    }
                });
            });
        }
    });

});




// INPUTS HIDDEN VAlUES
$(function(){

    $('.country-to-edit').on('change',function(){
        $('input[name="editted-country"]').val( $('.country-to-edit .jq-selectbox__select-text').text() );
    });

    $('.company-country-to-edit').on('change',function(){
        $('input[name="editted-company-country"]').val( $('.company-country-to-edit .jq-selectbox__select-text').text() );
    });

    $('.subscribe-select').on('change',function(){
        $('.interest-inp').val( $(this).val() == 'Interests' ? '' : $(this).val() );
    });

    $('.policy-address-country').on('change',function(){
        var currentVal = $('.policy-address-country .jq-selectbox__select-text').text() == 'Country' ? '' : $('.company-country-to-edit .jq-selectbox__select-text').text();
        $('input[name="OrderDetails[country]"]').val( $('.policy-address-country .jq-selectbox__select-text').text() );

        $('input[name="OrderDetails[country]"]').val($('input[name="OrderDetails[country]"]').val() == 'Country' ? '' : $('input[name="OrderDetails[country]"]').val());
    });

    $('.policy-address-state').on('change',function(){

        $('input[name="OrderDetails[state]"]').val($(this).val());
    });

});

// Blocked interests select
$(function(){


    $('.form-block .subscribe').on('click',function(){
        $(this).toggleClass('clicked-interests');

        if($(this).hasClass('clicked-interests')){
            $('.interests-block .jq-selectbox').hide();
            $('.disabled-select').show();
        }else{
            $('.interests-block .jq-selectbox').show();
            $('.disabled-select').hide();
        }
    });

});

// CARD LISTENER
$(function(){

$.fn.setCursorPosition = function(pos) {
    if ($(this).get(0).setSelectionRange) {

        $(this).get(0).setSelectionRange(pos, pos);

    } else if ($(this).get(0).createTextRange) {
        var range = $(this).get(0).createTextRange();
        range.collapse(true);
        range.moveEnd('character', pos);
        range.moveStart('character', pos);
        range.select();
    }
}

    var cardType = $('select.card-type-select').val();
    $('.card-type-select').styler();

    $('.card-type-select ul li').on('click',function(){
        cardType = $('select.card-type-select').val();

        $('div.card-type-select').siblings('.card-type-inp').val(cardType);

        $('.cvc-card').val( null );
    });

    $('.card-holder-name').on('keydown',function(e){

        if(e.target.value.match(/\w+ \w+/)){
            $(this).removeClass('invalid');
        }else{
            $(this).addClass('invalid');
        }
        var probels = e.target.value.match(/ /g) || '';

        if(e.keyCode == 32 && probels.length == 1){
            return false;
        }

        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 32]) !== -1 ||
                // Allow: Ctrl+A, Command+A
            (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) ||
                // Allow: home, end, left, right, down, up
            (e.keyCode >= 33 && e.keyCode <= 40)) {
            // let it happen, don't do anything
            return;
        }

        if((e.keyCode >= 65 && e.keyCode <= 90) || (e.keyCode >= 97 && e.keyCode <= 122)){
            return true;
        }else{
            return false;
        }
    });

    $('.payment-inp-group .mmyy-card').on('keydown',function(e){
        var number = e.target.value.match(/\d/g) || '';
        if(e.keyCode == 8 && number.length == 1){
            e.target.value = '';
        }
        if(e.keyCode == 8 || e.keyCode == 37){
            if(e.target.value.match(/\d\d \/ $/)){
                $(this).setCursorPosition(2);
            }
        }
        if(e.keyCode == 39 || ((e.keyCode >= 48 && e.keyCode <= 57) || (e.keyCode >= 96 && e.keyCode <= 105)) ){
            if(e.target.value.match(/\d\d \/ $/)){
                $(this).setCursorPosition(6);
            }
        }

    });

    $('.payment-inp-group .mmyy-card').on('keyup',function(e){
        var matchedVal = e.target.value.match(/(\d|\d\d) \/ \d\d/) === null ? '' : e.target.value.match(/\n\n \/ \n\n/) === null;

        if(matchedVal.length == 0 || +e.target.value.split(' / ')[0] > 12 ){
            $(this).addClass('invalid');
        }else{
            $(this).removeClass('invalid');
        }
    });

    $('.payment-inp-group .mmyy-card').on('keypress',function(e){
        var value = e.target.value;

        if(value.length <= 6){
            if( (e.keyCode >= 48 && e.keyCode <= 57) ||  e.keyCode == 8){
                if(value.length == 2 && e.keyCode != 8){
                    e.target.value += ' / ';
                }
            }else{
                return false;
            }
        }else{
            return false
        }
    });


    $('.cvc-card').on('keyup',function(e){
        if( cardType.toLowerCase() == 'amex' ){
            if(e.target.value.length <=3){
                $(this).addClass('invalid');
            }else{
                $(this).removeClass('invalid');
            }
        }else{
            if(e.target.value.length <=2){
                $(this).addClass('invalid');
            }else{
                $(this).removeClass('invalid');
            }
        }
    });
    $('.cvc-card').on('keypress',function(e){
        if( cardType.toLowerCase() == 'amex' ){
            if(e.target.value.length <=3){
                if( (e.keyCode >= 48 && e.keyCode <= 57) ||  e.keyCode == 8){

                }else{
                    return false;
                }
            }else{
                return false;
            }
        }else{
            if(e.target.value.length <=2){
                if( (e.keyCode >= 48 && e.keyCode <= 57) ||  e.keyCode == 8){

                }else{
                    return false;
                }
            }else{
                return false;
            }
        }
    });

    $('.card-holder-number').on('keyup',function(e){
        var matchVal = e.target.value.match(/\d\d\d\d \d\d\d\d \d\d\d\d \d\d\d\d/) === null ? '' : e.target.value.match(/\d\d\d\d \d\d\d\d \d\d\d\d \d\d\d\d/);
        if(matchVal.length != 1){
            $(this).addClass('invalid');
        }else{
            $(this).removeClass('invalid');
        }
    });

    $('.card-holder-number').on('keydown',function(e){
        // Allow: backspace, delete, tab, escape, enter
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110]) !== -1 ||
                // Allow: Ctrl+A, Command+A
            (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) ||
                // Allow: home, end, left, right, down, up
            (e.keyCode >= 33 && e.keyCode <= 40)) {
            // let it happen, don't do anything
            return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        } else {
            var numberMatchVal = e.target.value.match(/\d/g) === null ? '' : e.target.value.match(/\d/g);
            if( (((e.keyCode >= 48 && e.keyCode <= 57) || (e.keyCode >= 96 && e.keyCode <= 105)) && e.target.value.length <= 18) ||  e.keyCode == 8){
                if( (numberMatchVal.length == 4 || numberMatchVal.length == 8 || numberMatchVal.length == 12) && e.keyCode != 8){
                    e.target.value += ' ';
                }
            }else{
                return false;
            }
        }
    });

    var hiddenNumber = $('.hidden-adult-numb');

    function deleteZeroFunc(string){
        var deletedValue = [];
        string.split('').forEach(function(item,index){
            if(index == 0){
                if(item == 0){

                }
            }else{
               deletedValue.push( item );
            }
        });

        return deletedValue.join('');
    }

    $('.adult-phone').on('keyup',function(e){
        var val = e.target.value;
        $('.hidden-adult-numb').val( null );
        $(this).addClass('invalid');

        if(val.match(/0?\d{8}\d?/)){

            if( val.length == 10 && val[0] == 0){
                var deletedZero;
                deletedZero = deleteZeroFunc(val);

                var currentCountryCode = $('.country-list').find('.active').attr('data-dial-code');
                var finishValue = '+' + currentCountryCode + deletedZero;
                $('.hidden-adult-numb').val( finishValue );

                $(this).removeClass('invalid');
            }

            if( val.length == 9 && val[0] != 0){
                var currentCountryCode = $('.country-list').find('.active').attr('data-dial-code');
                var finishValue = '+' + currentCountryCode + val;
                $('.hidden-adult-numb').val( finishValue );

                $(this).removeClass('invalid');
            }

        }else{
            $(this).addClass('invalid');
        }


    });

    $('.adult-phone').on('keydown',function(e){


        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110]) !== -1 ||
                // Allow: Ctrl+A, Command+A
            (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) ||
                // Allow: home, end, left, right, down, up
            (e.keyCode >= 33 && e.keyCode <= 40)) {
            // let it happen, don't do anything
            return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }else {
            if( e.target.value[0] == 0 ){
                if( ( ((e.keyCode >= 48 && e.keyCode <= 57) || (e.keyCode >= 96 && e.keyCode <= 105)) && e.target.value.length < 10 ) ||  e.keyCode == 8){

                }else{
                    return false;
                }
            }else{
                if( ( ((e.keyCode >= 48 && e.keyCode <= 57) || (e.keyCode >= 96 && e.keyCode <= 105)) && e.target.value.length < 9 ) ||  e.keyCode == 8){

                }else{
                    return false;
                }
            }
        }
    });
    //$('.card-holder-number').on('keypress',function(e){
    //    var numberMatchVal = e.target.value.match(/\d/g) === null ? '' : e.target.value.match(/\d/g);
    //    if( (e.keyCode >= 48 && e.keyCode <= 57 && e.target.value.length <= 18) ||  e.keyCode == 8){
    //        if( (numberMatchVal.length == 4 || numberMatchVal.length == 8 || numberMatchVal.length == 12) && e.keyCode != 8){
    //            e.target.value += ' ';
    //        }
    //    }else{
    //        return false;
    //    }
    //});

});

// Autocomplete input's values after loading page
$(function(){

    $('.birth-date').val( $('.birth-date').parent().find('.editted-date').val() );
    $('.expiry').val( $('.expiry').parent().find('.editted-date').val() );
    $('.filter-from-date').val( $('.filter-from-date').parent().find('.editted-date').val() );
    $('.filter-to-date').val( $('.filter-to-date').parent().find('.editted-date').val() );

    $('.autoloadInfo').on('change',function(){
        $(this).siblings('input.editted-date').val( $(this).val() );
    });


});


$(function(){
    if( $('.confirmation-cover .details-pack').length == 0 ){
        $('.confirmation-info-packs .confirmation-cover').append('<h3>None selected</h3>');
    }
    if( $('.details.details-plan-block p').length == 0 ){
        $('.details.details-plan-block').append('<h3 class="undef">None selected</h3>');
    }
    $('.plan-quote').on('click',function(){
        $('.plans-group-block .plan-quote').removeClass('red-quote')
            .parent('.plans-block').removeClass('active-plan-quote')
            .find('.plan-val').removeClass('plan-val-active');

        var packets_class = $(this).attr('data-packets');
        console.log(packets_class);

        $('.option-packets-wrapper').hide();
        $('.option-packets .details-pack').removeClass('added-details-pack');
        $('.confirmation-cover-pack .details-pack').remove();
        if( $('.confirmation-cover .details-pack').length == 0 ){
            $('.confirmation-cover-pack > h3').remove();
            $('.confirmation-info-packs .confirmation-cover').append('<h3 class="undef">None selected</h3>');
        }
        $("." + packets_class).show(400);
        console.log("." + packets_class);
        //if( !$(this).hasClass('red-quote') ){
            $(this).addClass('red-quote')
              .parent('.plans-block').addClass('active-plan-quote')
              .find('.plan-val').addClass('plan-val-active');
        //}else{
        //    $(this).removeClass('red-quote')
        //      .parent('.plans-block').removeClass('active-plan-quote')
        //      .find('.plan-val').removeClass('plan-val-active');
        //}
        calcQuotePrice();
        buy_now();

        $(".confirmation-info,.buyWrapper").show(400);

        var parentsIndex = $(this).parent().index() + 1;
        console.log(parentsIndex);
        $('.benefits-table tr td').removeClass('active-benefits');
        $('.benefits-table tr td:nth-child('+ parentsIndex +')' ).addClass('active-benefits');
    });

    $('.option-packets .details-pack').on('click',function(){


        if( !$(this).hasClass('added-details-pack') ){
            var elem = $(this).clone().prepend('<a class="pack-close" href="#"></a>').wrap('<p/>').parent().html();;
            //console.log(elem);
            $(elem).appendTo('.confirmation-cover-pack');
            $('.confirmation-cover-pack > h3').remove();
            $(this).addClass('added-details-pack');
        }else{
            var pack_class = $(this).attr('data-pack');
            $('.confirmation-cover-pack > .' + pack_class).remove();
            if( $('.confirmation-cover .details-pack').length == 0 ){
                $('.confirmation-cover-pack > h3').remove();
                $('.confirmation-info-packs .confirmation-cover').append('<h3 class="undef">None selected</h3>');
            }

            $(this).removeClass('added-details-pack');
        }
        calcQuotePrice();
    });

    //$('.pack-close').on('click',function(e){
    $(document).on('click','.pack-close', function(e){
        e.preventDefault();

        var pack_class = $(this).parent().attr('data-pack');

        $('.'+pack_class+'.added-details-pack').click();

        //$(this).parent().remove();
        if( $('.confirmation-cover .details-pack').length == 0 ){
            $('.confirmation-cover-pack > h3').remove();
            $('.confirmation-info-packs .confirmation-cover').append('<h3 class="undef">None selected</h3>');
        }
        calcQuotePrice();
    });


});

function calcQuotePrice() {
    var quote_price = parseFloat($('.active-plan-quote .quote-packet-price').html());

    var key = '';
    var option_price = 0.0;
    var options_array = {};
    $('.confirmation-info-packs .details-pack').each(function( index ) {
        key = $( this).find('.packs-name').attr('data-key');
        console.log(key);
        option_price = option_price + parseFloat($( this).find('.option-packet-price').text() );
        options_array[key] = {name : $( this).find('p').text(), value : $( this).find('.option-packet-price').text()};
        //options_array[key]['value'] = $( this).find('.option-packet-price').text();
        //console.log( index + ": " + $( this ).text() );
    });
    quote_price = quote_price + option_price;

    //options_array = JSON.stringify(options_array);
    console.log(options_array);
    var m = JSON.stringify(options_array).toString();
    //console.log(m);

    $('.circle-cover-info span').text(quote_price.toFixed(2));
    $('.total-prem h4').html('<span>$</span>'+ quote_price.toFixed(2) + ' ');

    $('.details-plan-block h2').text($('.active-plan-quote .plan-name').text());
    $('.details-plan-block h3').remove();

    $("input[name='Orders[price]']").val(quote_price.toFixed(2));
    $("input[name='Orders[quote]']").val($('.active-plan-quote .plan-name').text());
    $("input[name='Orders[options]']").val(m);
}


function buy_now() {
    var that = '.buyNow';
    if ($("input[name='Orders[quote]']").val() && $("input[name='Orders[price]']").val()) {
        $(that).prop('disabled', false);
        $(that).css('background-color', '');
    } else {
        $(that).prop('disabled', true);
        $(that).css('background-color', '#888');
    }
}
buy_now('.buyNow');


//STEP3 datepickers

$(function(){

    function min(that){
        var dateMin = $(that).attr('data-min');
        return new Date(+dateMin.split('-')[0], +dateMin.split('-')[1] - 1, +dateMin.split('-')[2]);
    }
    function max(that){
        var dateMax = $(that).attr('data-max');
        return new Date(+dateMax.split('-')[0], +dateMax.split('-')[1] - 1, +dateMax.split('-')[2]);
    }

    $('.traveller').each(function(){
        $(this).datepicker({
            prevText: '‹',
            nextText: '›',
            changeMonth: true,
            minDate: min(this),
            maxDate: max(this),
            changeYear: true,
            dateFormat: 'dd/mm/yy',
            orientation: "bottom",
            showOn: "both"
        });

    });



    $('.traveller').on('click',function(){
        if($('#ui-datepicker-div').offset().top < $(this).offset().top){
            $('#ui-datepicker-div').addClass('ui-datepicker-div-higher');
        }else{
            $('#ui-datepicker-div').removeClass('ui-datepicker-div-higher');
        }
    });
});

// BENEFITS
$(function(){

    $('.benefits-toggle').on('click',function(e){
        e.preventDefault();
        var toggler = $(this);
        toggler.toggleClass('closed-benefits');
        if( !toggler.hasClass('closed-benefits') ){
            $('.benefits-table').fadeIn();
            toggler.addClass('opened-benefits');
        }else{
            $('.benefits-table').hide();
            toggler.removeClass('opened-benefits');
        }

    });

});

$(function(){
    if($('.adult-phone').length){
        $(".adult-phone").intlTelInput({
            utilsScript: "/themes/mytripproj/js/utils.js"
        });

        $('.flag-disabled').on('click',function(e){
            e.preventDefault();
        });
    }

});