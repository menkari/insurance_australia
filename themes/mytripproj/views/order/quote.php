<div class="steps-line">
    <div class="step-label-active step-label">
        <span class='fly'></span>
        <p>Trip Details</p>
    </div>
    <div class="step-label-active step-label">
        <span class='fly'></span>
        <p>Plan Options</p>
    </div>
    <div class="step-label">
        <p>Personal Details</p>
    </div>
    <div class="step-label">
        <p>Payment </p>
    </div>
</div>


<div class="steps-wrapper steps-wrapper2">
    <div class="steps-content">
        <div class="back-btn-container">
            <a href="/" class='back-button'>Back</a>
        </div>
        <h5 class="step-info">Step 2  /  Plan Options</h5>
        <h2 class="step-header">Your Trip</h2>
        <?php $form=$this->beginWidget('CActiveForm', array(
            'id' => 'orders-base-info',
            'enableAjaxValidation'=>true,
            'clientOptions'=>array(
                'validateOnChange' => false,
                'validateOnSubmit'=>true,
                'validationDelay'=>2000
            ),
            'htmlOptions'=>array(
                'class'=>'update-base-order-data',
            ),
            'action' => '/order/quote'
        )); ?>
        <div class='head-content-bl-form head-content-bl-container'>

            <div class='where-inp inp steps-inp'>
                <label for='where'>Countries you will be visiting
                    <a href="#" data-toggle="tooltip" title='Please enter all the countries you will visit during your trip and for which you want to be insured. For Domestic, please enter Australia.'></a>
                </label>
                <div class='where-div'>
                    <input type='text' id='where' name='countries-to-visit'>
                    <input type='hidden' class='where-values' value='<?php echo $sessionData['countries']; ?>'>
                </div>
            </div>
            <div class='when-inp inp steps-inp'>
                <label>Choose your travel dates
                    <!-- placeholder <a href="#" data-toggle="tooltip" title='Start Date can be up to 365 days in the future starting from today for Single Trip & up to 45 days for Annual Multi-Trip<br/>
Travel Duration up to a max value of 365 days. Unless there is an Insured Person who is at least 70 years old in which case the max is 180 days.'></a>-->
                    <!--                            formatting-->
                    <a href="#" data-toggle="tooltip" title='Start Date can be up to 365 days in the future starting from today for Single Trip<br/>
Travel Duration up to a max value of 365 days. Unless there is an Insured Person who is at least 70 years old in which case the max is 180 days.'></a>
                </label>
                <input type='button' id='when' class='from step2-when-year' name='time-from-visit'>
                <i class='arr-r'></i>
                <!-- placeholder: fix end date to start date + 1 year for annual multi-trip -->
                <input type='button' class="to step2-when-year" name='time-to-visit'>
            </div>
            <?php echo $form->hiddenField($model, 'countries'); ?>
            <?php echo $form->hiddenField($model, 'start_traveling', array('value' => $start_traveling, 'class' => 'order-start-traveling')); ?>
            <?php echo $form->hiddenField($model, 'end_traveling', array('value' => $end_traveling, 'class' => 'order-end-traveling')); ?>
        </div>
        <div class="who-else-block">
            <h1>Anyone else?</h1>
            <div class="ages-block">
                <div class="adult-block">
                    <p class='qstn-sign children-tooltip'>Adult ages?
                        <!--                            formatting-->
                        <a href="#" data-toggle="tooltip" title='In order to purchase a Policy You must be eighteen (18) years of age when You apply.<br/>
Insured Persons must be under 90 years of age, must have a permanent residence in Australia, unrestricted right of entry into Australia as well as access to long-term medical care in Australia and:
<ul>
    <li>be an Australian resident; or</li>
    <li>be on a skilled working visa (e.g. a 457 visa), but not a working holiday visa; or</li>
    <li>have a partner/spouse visa which allows you to stay in Australia for at least 2 years; or</li>
    <li>have a New Zealand passport.</li></ul>'></a>
                    </p>
                    <button class='add-adult add-age-button' style="<?php echo (!empty($sessionData['age2'])) ? 'display: none;' : '' ?>"></button>
                    <div class='age-inp-wrap'>
                        <input type="text" class='added-adult added-age' name="age" value="<?php echo $sessionData['age']; ?>" disabled>
                    </div>
                    <?php if (!empty($sessionData['age2'])) : ?>
                        <div class='age-inp-wrap'>
                            <input type="text" class='added-adult added-age' name="age2" value="<?php echo $sessionData['age2']; ?>">
                            <a class='wrong wrong-adult'></a>
                        </div>
                    <?php endif; ?>
                </div>

                <div class="kids-block">
<!--                    formatting-->
                    <p class='qstn-sign children-tooltip'>Child ages?
<!--                        <a href="#" data-toggle="tooltip" title="Unmarried child or children of the Insured Persons (including step or legally adopted children) who are:-->
<!--    <br>&nbsp;&nbsp;&nbsp;&nbsp;(a)&nbsp;&nbsp; up to and including eighteen (18) years of age; or-->
<!--    <br>&nbsp;&nbsp;&nbsp;&nbsp;(b)&nbsp;&nbsp; up to and including twenty-three (23) years of age whilst they are full-time students at an accredited institution of higher learning and primarily dependent upon You for maintenance and support."></a>-->
                        <a href="#" data-toggle="tooltip" title="Unmarried child or children of the Insured Persons (including step or legally adopted children) who are up to but excluding eighteen (18) years of age."></a>
                    </p>
                    <button class='add-kid add-age-button'></button>
                    <?php if (isset($sessionData['kids'])) : ?>
                        <?php $kids = explode(',', $sessionData['kids']); ?>
                        <?php $i = 0; ?>
                        <?php foreach ($kids as $k => $kid) : ?>
                            <div class='age-inp-wrap'>
                                <input type="text" class='added-kid added-age' name="kids[]" value="<?php echo $kid; ?>">
                                <?php if ($i > 0) : ?>
                                    <a class="wrong wrong-kid"></a>
                                <?php endif; ?>
                            </div>
                            <?php $i++; ?>
                        <?php endforeach; ?>
                    <?php else : ?>
                        <div class='age-inp-wrap'>
                            <input type="text" class='added-kid added-age' name="kids[]">
                        </div>
                    <?php endif; ?>

                </div>
            </div>
        </div>
        <div style="text-align: center;">
            <?php echo $form->error($model,'age', array('style'=>'color: red; margin-bottom: 20px; font-weight: bold; font-size: 30px;')); ?>
        </div>
        <div class="refresh-button-wrapper">
            <?php echo $form->hiddenField($model, 'age', array('value' => $sessionData['age'], 'class'=>'order-age')); ?>
            <?php echo $form->hiddenField($model, 'age2', array('value' => $sessionData['age2'], 'class'=>'order-age2')); ?>
            <?php echo $form->hiddenField($model, 'kids', array('value' => $sessionData['kids'])); ?>
            <?php echo CHtml::submitButton('Update', array('class' => 'refresh-button')); ?>
        </div>

        <?php $this->endWidget(); ?>


        <div class="plans-group-block">
            <?php foreach($quotes as $pk => $quote) : ?>
            <div class="plans-block">
                <h3 class='plan-name'><?php echo $quote['quote']['type']; ?></h3>
                <h2 class="plan-val"><span>$</span><span class="quote-packet-price"><?php echo number_format($quote['quote']['clear_price'], 2); ?></span> </h2>
                <p class="plan-descr"><?php
                    if ($quote['quote']['type'] =='Essential')
                        echo 'Offers mid-level international travel cover providing essential cover limits on your travels abroad.';
                    else if ($quote['quote']['type'] =='Comprehensive')
                        echo 'Extensive coverage travel plan that will give you wide cover limits on your travels at a competitive price.';
                    else if ($quote['quote']['type'] =='Prestige')
                        echo 'Carefully designed, broad coverage travel plan that will give you extensive cover with high benefit limits.';
                    ?></p>
                <button class="plan-quote" data-packets="option-packets-<?php echo str_replace('.', '-', $pk); ?>">Select</button>
<!--                formatting-->
                <div class="plans-block-a-qroup">
                    <div><a target="_blank" href="/source/20161222%20-%20Mytrip%20-%20Single%20Trip%20-%20Summary%20of%20Benefits%20-%20Final.pdf">Benefits</a></div>
                    <div><a target="_blank" href="/source/20161222%20-%20Mytrip%20-%20Summary%20of%20Exclusions%20(ST).pdf">What's not covered</a></div>
                    <div><a target="_blank" href="/source/My%20Trip%20Insurance%20Australia%20-%20Single%20Trip%20PDS.pdf">Product Disclosure Statement</a></div>
                </div>

            </div>
            <?php endforeach; ?>
        </div>

        <!--div class='benefits-wrapper'>
            <div class="benefits-container">
                <a class='benefits-toggle closed-benefits' href='#'>Show Benefits</a>
                <table class="benefits-table">
                    <thead>
                    <tr>
                        <?php foreach($quotes as $pk => $quote) : ?>
                        <td>
                            <p class="ben-name"><?php echo $quote['quote']['type']; ?></p>
                            <p class="ben-price">$<?php echo number_format($quote['quote']['clear_price'], 2); ?></p>
                        </td>
                        <?php endforeach; ?>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <th colspan="3">
                            <p>Overseas Medical and Accidental Dental Expenses
                                <a href="#" data-toggle="tooltip" title="Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book." class="tooltip-custom"></a>
                            </p>
                        </th>
                    </tr>
                    <tr>
                        <td>Unlimited</td>
                        <td>Unlimited</td>
                        <td>Unlimited</td>
                    </tr>
                    <tr>
                        <th colspan="3">
                            <p>Additional Expenses
                                <a href="#" data-toggle="tooltip" title="Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book." class="tooltip-custom"></a>
                            </p>
                        </th>
                    </tr>
                    <tr>
                        <td>$ 10000</td>
                        <td>$ 10000</td>
                        <td>$ 10000</td>
                    </tr>
                    <tr>
                        <th colspan="3">
                            <p>Overseas Medical and Accidental Dental Expenses
                                <a href="#" data-toggle="tooltip" title="Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book." class="tooltip-custom"></a>
                            </p>
                        </th>
                    </tr>
                    <tr>
                        <td>Unlimited</td>
                        <td>Unlimited</td>
                        <td>Unlimited</td>
                    </tr>
                    <tr>
                        <th colspan="3">
                            <p>Additional Expenses
                                <a href="#" data-toggle="tooltip" title="Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book." class="tooltip-custom"></a>
                            </p>
                        </th>
                    </tr>
                    <tr>
                        <td>$ 10000</td>
                        <td>$ 10000</td>
                        <td>$ 10000</td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div-->

        <?php foreach($quotes as $pk => $quote) : ?>
        <div class="option-packets-wrapper option-packets-<?php echo str_replace('.', '-', $pk); ?>" style="display: none;">
            <div class="option-packets-content">
                <h1>Would you like any benefit packs?</h1>
                <h4><a target="_blank" href="/source/20161222%20-%20Mytrip%20-%20Summary%20of%20Optional%20Add-on%20Packs.pdf">Click here</a> to review the summary of what is included in each of the packs including sum insured.</h4>
                <div class="option-packets">
                    <?php if (isset($quote['options'])) : ?>
                        <?php $index = 0;?>
                        <?php foreach($quote['options'] as $key => $option) : ?>
                        <div class="details-pack details-pack-<?php echo $index?>" data-pack="details-pack-<?php echo $index?>" >
                            <p class="packs-name" data-key="<?php echo $key; ?>"><?php echo $option['key']; ?></p>
                            <h4><span>$</span><span class="option-packet-price"><?php echo $option['value']; ?></span></h4>
                        </div>
                            <?php $index++;?>
                        <?php endforeach; ?>
                    <?php endif; ?>
                </div>
            </div>
        </div>
        <?php endforeach; ?>

        <div class="confirmation-info-wrapper step2-confirmation-info-wrap">
            <div class="confirmation-info" style="display:none;">
                <h1 class='uhaveselected'>What you'll get:</h1>
                <div class="confirmation-info-left">
                    <div class="details confirmation-countries">
                        <div class="details details-plan-block">
                            <h1>Plan</h1>
                            <h2></h2>
<!--                            <p class='trip-type-p'>Single Trip</p>-->
                        </div>
                        <h1>Countries of visit</h1>
                        <div class="confirmation-countries-block">
                            <?php $countries = explode("|", $sessionData['countries']); ?>
                            <?php foreach($countries as $key =>$country) : ?>
                                <a class="selected-country not-x-close"><?php echo $country; ?></a><?php echo ($key < count($countries) - 1) ? ',' : ''; ?>
                            <?php endforeach; ?>
                        </div>
                    </div>
                    <div class="details confirmation-date">
                        <h1>Travel Dates</h1>
                        <div class="confirmation-date-block">
                            <span class="confirmation-date-from"><?php echo date('d M Y', $sessionData['start_traveling']); ?></span>
                            <i class='confirmation-arr'></i>
                            <span class="confirmation-date-to"><?php echo date('d M Y', $sessionData['end_traveling']); ?></span>
                        </div>
                    </div>
                </div>
                <div class="confirmation-info-right">
                    <div class="details confirmation-cover">
                        <h1>&nbsp;</h1>
                        <div class="circle-cover-info">
                            <h2>$<span>0</span></h2>
                        </div>
                    </div>
                </div>
                <div class="confirmation-info-packs">
                    <div class="details confirmation-cover confirmation-cover-pack">
                        <h1>Benefit Packs</h1>
                    </div>
                </div>
            </div>
        </div>
        <div class="change-button-wrapper confirmation-change-button-wrapper buyWrapper" style="display:none;">
            <div class="total-prem">
                <p>Total Premium</p>
                <h4><span>$</span>0 </h4>
            </div>
            <button class='buyNow'>Buy now</button>
        </div>
        <?php $form=$this->beginWidget('CActiveForm', array(
            'id' => 'orders-quote-info',
            'enableAjaxValidation'=>true,
            'clientOptions'=>array(
                'validateOnChange' => false,
                'validateOnSubmit'=>true,
                'validationDelay'=>2000
            ),
            'htmlOptions'=>array(
                'class'=>'orders-quote-packet',
            ),
            'action' => '/order/details'
        )); ?>
        <?php echo $form->hiddenField($model, 'quote', array('value' => '')); ?>
        <?php echo $form->hiddenField($model, 'price', array('value' => '')); ?>
        <?php // echo $form->hiddenField($model, 'options', array('value' => '')); ?>
        <input name="Orders[options]" id="Orders_options" type="hidden">


        <?php //echo CHtml::submitButton('Quote', array('class' => 'buyNow')); ?>
        <?php $this->endWidget(); ?>
    </div>
</div>