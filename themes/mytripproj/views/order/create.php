<?php
/* @var $this OrderController */
/* @var $model Orders */

$this->breadcrumbs=array(
    'Orders'=>array('index'),
    'Create',
);

$this->menu=array(
    array('label'=>'List Orders', 'url'=>array('index')),
    array('label'=>'Manage Orders', 'url'=>array('admin')),
);
?>

<?php Yii::app()->clientScript->registerScriptFile( Yii::app()->theme->getBaseUrl() . '/js/orders.js' );?>
<div class="benefits-bl-wrapper">
    <h1>Create Orders</h1>


    <div style="margin: 0 auto;">
        <div class="tab-form" style="width: 345px; margin: 0 auto;">

        <?php $form=$this->beginWidget('CActiveForm', array(
            'id'=>'orders-form',
            'enableAjaxValidation'=>true,
            'clientOptions'=>array(
                'validateOnChange' => false,
                'validateOnSubmit'=>true,
                'validationDelay'=>2000
            ),
            'htmlOptions'=>array(
                'class'=>'p-form-sign',
            ),
        )); ?>

        <p class="note">Fields with <span class="required">*</span> are required.</p>

        <?php echo $form->textField($model,'date',array('class'=>'p-inputs order-date', 'placeholder'=>'Date', 'size'=>60)); ?>
        <?php echo $form->error($model,'date', array('style'=>'color: red;')); ?>

        <?php echo $form->textField($model,'countries',array('class'=>'p-inputs', 'placeholder'=>'Countries', 'size'=>60,'maxlength'=>255)); ?>
        <?php echo $form->error($model,'countries', array('style'=>'color: red;')); ?>

        <?php echo $form->textField($model,'start_traveling',array('class'=>'p-inputs start-traveling', 'placeholder'=>'Start Traveling', 'size'=>60,'maxlength'=>255)); ?>
        <?php echo $form->error($model,'start_traveling', array('style'=>'color: red;')); ?>

        <?php echo $form->textField($model,'end_traveling',array('class'=>'p-inputs end-traveling', 'placeholder'=>'End Traveling', 'size'=>60,'maxlength'=>255)); ?>
        <?php echo $form->error($model,'end_traveling', array('style'=>'color: red;')); ?>

        <?php echo $form->textField($model,'travellers',array('class'=>'p-inputs', 'placeholder'=>'Travellers', 'size'=>60,'maxlength'=>255)); ?>
        <?php echo $form->error($model,'travellers', array('style'=>'color: red;')); ?>

        <?php echo CHtml::submitButton('Create', array('class' => 'p-submit p-sign-submit')); ?>

        <?php $this->endWidget(); ?>

        </div>
    </div>
</div>