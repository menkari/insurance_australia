<div class="table-wrapper">
    <div class="table-content">
        <h1 class="pages-header">Customers</h1>

        <form method="post" class="customers-panel">
            <div class="search-panel">
                <label for="search-table">Search:</label>
                <input value="<?=isset($post['search']) ? $post['search']:''?>" name="search" type="text" id="search-table">
            </div>

            <div class="panel-pickers">
                <div class="datepicker-input-item input-item">
                    <label for='from-picker-statistic'>Show from:</label>
                    <input type="text" id='from-picker-statistic' placeholder='MM / DD / YYYY' name='ordersFrom' value="<?=isset($post['ordersFrom'])?$post['ordersFrom']:''?>">
                </div>
                <div class="datepicker-input-item input-item">
                    <label for='to-picker-statistic'>To:</label>
                    <input type="text" id='to-picker-statistic' placeholder='MM / DD / YYYY' name='ordersTo' value="<?=isset($post['ordersTo'])?$post['ordersTo']:''?>">
                </div>
            </div>

            <div class="filter-buttons">
                <button type="submit" class='apply-filter filter-button'>Apply</button>
                <button type="reset" class='clear-filter filter-button'>Clear all</button>
            </div>
        </form>

        <div class="customers-table">
            <?php $this->widget('zii.widgets.grid.CGridView', array(
                'itemsCssClass' => 'customers-table-container',
                'dataProvider'=>$dataProvider,
                'summaryText' => '',
                'columns'=>array(
                    array(
                        'header'=>'First name',
                        'value'=>'$data->username',
                        'type' => 'raw'
                    ),
                    array(
                        'header'=>'Last name',
                        'value'=>'$data->surname',
                        'type' => 'raw'
                    ),
                    array(
                        'header'=>'Dates of birth',
                        'value'=>'$data->birth',
                        'type' => 'raw'
                    ),
                    array(
                        'header'=>'City',
                        'value'=>'$data->city',
                        'type' => 'raw'
                    ),
                    array(
                        'header'=>'Address',
                        'value'=>'$data->street',
                        'type' => 'raw'
                    ),
                    array(
                        'header'=>'Zip',
                        'value'=>'$data->zip_code',
                        'type' => 'raw'
                    ),
                    array(
                        'header'=>'Last order',
                        'value'=>'Users::model()->getLastOrder($data->id)',
                        'type' => 'raw'
                    ),
                ),
                'htmlOptions' => array('class' => '')
            )); ?>
        </div>
    </div>
</div>