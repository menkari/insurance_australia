<div class="steps-line">
    <div class="step-label-active step-label">
        <span class='fly'></span>
        <p>Trip Details</p>
    </div>
    <div class="step-label-active step-label">
        <span class='fly'></span>
        <p>Plan Options</p>
    </div>
    <div class="step-label-active step-label">
        <span class='fly'></span>
        <p>Personal Details</p>
    </div>
    <div class="step-label-active step-label">
        <p>Payment </p>
    </div>
</div>


<div class="thanks-wrapper steps-wrapper5">
    <div class="thanks-content steps-content">
        <div class="thanks-logo"></div>
        <div class="thanks-text">
            <h1>Thank you</h1>
            <p>Thank you for purchasing My Trip Insurance Single Trip, underwritten by Chubb Insurance Australia Limited. Your payment of <span class='payment-count'> <?php echo $price; ?></span>   has been processed and your My Trip Insurance Single Trip Policy is now effective.</p>
            <div class="thanks-info">
                <h4>Policy Number</h4>
                <p class='policy-number'><?php echo $PolicyNumber ?></p>
                <?php if (!empty($pdf)) : ?>
                <a href="<?php echo $pdf;?>" class='thanks-download-pdf-btn' name='thanks-download-pdf-btn' target="_blank">Download Pdf</a>
                <?php endif; ?>
                <p>A detailed description of the cover is set out in the benefits sections of the <a href="/source/My%20Trip%20Insurance%20Australia%20-%20Single%20Trip%20PDS.pdf" target="_blank">Single Trip Product Disclosure Documents (PDS)</a><!-- or Annual Trip Product--></p>
            </div>
        </div>
    </div>
</div>


<div class="content-wrapper thanks-content-wrapper">
    <div class="content-bl">
        <div class="blocks-group thanks-blocks-group">
            <div class="blocks-group-bl">
                <div class="blocks-group-bl1">
                    <h2>Emergency Assistance</h2>
                    <a class="blocks-number" href='tel:+61289075666'>+61 2 8907 5666</a>
                    <p class="blocks-text blocks-text1">Available to assist you every hour of every day as part of your cover.</p>
                </div>
                <div class="blocks-group-bl2">
                    <h2>Lodge A claim</h2>
                    <p class="blocks-text blocks-text1">If something goes wrong on your trip and you need to lodge a claim, begin the process by submitting your details here.</p>
                    <a href="/pages/claim" class='group-btn get-quote'>MAKE A CLAIM</a>
                </div>
<!--                <div class="blocks-group-bl3">-->
<!--                    <h2>FAQs</h2>-->
<!--                    <p class="blocks-text blocks-text1"> If you have any questions, please look at our FAQs page.</p>-->
<!--                    <a href="/site/page/view/about" class='group-btn find-out'>Find out more</a>-->
<!--                </div>-->
            </div>
        </div>
    </div>
</div>