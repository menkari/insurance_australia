<?php
/* @var $this OrdersController */
/* @var $model Orders */
/* @var $form CActiveForm */
?>

<div class="wide form">

    <?php $form=$this->beginWidget('CActiveForm', array(
        'action'=>Yii::app()->createUrl($this->route),
        'method'=>'get',
    )); ?>

    <div class="row">
        <?php echo $form->label($model,'id'); ?>
        <?php echo $form->textField($model,'id'); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model,'user_id'); ?>
        <?php echo $form->textField($model,'user_id'); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model,'date'); ?>
        <?php echo $form->textField($model,'date'); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model,'countries'); ?>
        <?php echo $form->textField($model,'countries',array('size'=>60,'maxlength'=>255)); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model,'start_traveling'); ?>
        <?php echo $form->textField($model,'start_traveling'); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model,'end_traveling'); ?>
        <?php echo $form->textField($model,'end_traveling'); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model,'travellers'); ?>
        <?php echo $form->textField($model,'travellers',array('size'=>60,'maxlength'=>255)); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model,'age'); ?>
        <?php echo $form->textField($model,'age'); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model,'age2'); ?>
        <?php echo $form->textField($model,'age2'); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model,'quote'); ?>
        <?php echo $form->textField($model,'quote',array('size'=>60,'maxlength'=>255)); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model,'detail_id'); ?>
        <?php echo $form->textField($model,'detail_id'); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model,'payment_id'); ?>
        <?php echo $form->textField($model,'payment_id'); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model,'price'); ?>
        <?php echo $form->textField($model,'price',array('size'=>11,'maxlength'=>11)); ?>
    </div>

    <div class="row buttons">
        <?php echo CHtml::submitButton('Search'); ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- search-form -->