

<div class="table-wrapper">
    <div class="table-content">
        <h1 class="pages-header">Statistics</h1>
        <div>
            <table>
                <tr>
                    <td>Number of visits:</td><td><?=$partner->visits ?></td>
                </tr>
                <tr>
                    <td>Number of sales:</td><td><?=Orders::countPartnerOrders($partner->id)?></td>
                </tr>
                <tr>
                    <td>Total sales:</td><td><?=Orders::getPartnerSalesAmount($partner->id)?></td>
                </tr>
                <tr>
                    <td>Total revenue:</td><td><?=$partner->commission_amount ?></td>
                </tr>
                <tr>
                    <td>Conversion:</td><td><?=Orders::getPartnerConversion($partner->id, $partner->visits)?></td>
                </tr>

            </table>
        </div>
        <form action="/order/statistic" method="POST">
        <div class="statistic-panel">
            <div class="panel-pickers">
                <div class="datepicker-input-item input-item">
                    <label for='from-picker-statistic'>Show from:</label>
                    <input type="text" id='from-picker-statistic' class='statistic-picker-from statistic-picker' placeholder='MM / DD / YYYY' name="date_range_start" value="<?php echo (isset($_POST['date_range_start'])) ? $_POST['date_range_start'] : '' ?>">
                </div>
                <div class="datepicker-input-item input-item">
                    <label for='to-picker-statistic'>To:</label>
                    <input type="text" id='to-picker-statistic' class='statistic-picker-to statistic-picker' placeholder='MM / DD / YYYY' name="date_range_end" value="<?php echo (isset($_POST['date_range_end'])) ? $_POST['date_range_end'] : '' ?>">
                </div>
            </div>
            <div class="filter-buttons">
                <button class='apply-filter filter-button'>Apply</button>
            </div>
        </div>
        </form>

        <div class="graphic-widget">
            <div class="graphic-widget-buttons">
                <button class="graphic-widget-button graphic-widget-button-active last-month-stat" name='last-month-stat'>Last month</button>
                <button class="graphic-widget-button last-year-stat"  name='last-year-stat'>Last year</button>
            </div>
            <div id="statistic-graphic" class='statistic-graphic'>

            </div>
        </div>
        <div class="statistic-table">
        <?php
            $this->widget('zii.widgets.grid.CGridView', array(
                'id' => 'fruits-grid',
                'dataProvider' => $dataProvider,
                'summaryText' => '',
                'columns' => array(
                    array(
                        'name' => 'Date',
                        'value' => '$data["id"]',
                    ),
                    array(
                        'name' => 'Visits',
                        'value' => '$data["visits"]'
                    ),
                    array(
                        'name' => 'Sales',
                        'value' => '"$" . $data["sales"]'
                    ),
                    array(
                        'name' => 'Revenues',
                        'value' => '"$" . $data["revenues"]'
                    ),
                    array(
                        'name' => 'Conversion',
                        'value' => '"$" . $data["conversion"]'
                    ),
                ),
                'itemsCssClass' => 'statistic-table-container',
                'htmlOptions' => array('class' => 'statistic-table'),
                'pagerCssClass' => 'iteora-pager',
                'pager'=>array("cssFile" => false)
            ));

            ?>
        </div>


        <div class="statistic-total">

        </div>
    </div>
</div>
<script src='<?php echo Yii::app()->theme->baseUrl; ?>/js/graphic.js'></script>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>