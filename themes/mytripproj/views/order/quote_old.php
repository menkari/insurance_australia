<div class="steps-line">
    <div class="step-label-active step-label">
        <span class='fly'></span>
        <p>Details</p>
    </div>
    <div class="step-label-active step-label">
        <span class='fly'></span>
        <p>Choose options</p>
    </div>
    <div class="step-label">
        <p>Summary</p>
    </div>
    <div class="step-label">
        <p>Payment </p>
    </div>
</div>


<div class="steps-wrapper steps-wrapper2">
    <div class="steps-content">
        <h5 class="step-info">2 step   /  choose plan</h5>
        <h2 class="step-header">Your Trip</h2>
        <div class='head-content-bl-form'>

<!--            <form action='#' name='main-page-form'>-->
            <?php $form=$this->beginWidget('CActiveForm', array(
                'id' => 'orders-base-info',
                'enableAjaxValidation' => true,
                'enableClientValidation' => true,
                'clientOptions' => array(
                    //'errorCssClass' => 'has-error',
                    'validateOnSubmit' => true,
                ),
                'htmlOptions'=>array(
                    'class'=>'p-form-sign',
                ),
                'action' => '/order/quote'
            )); ?>
                <div class='where-inp inp steps-inp'>
                    <label for='where'>Countries you will be visiting</label>
                    <div class='where-div'>
                        <input type='text' id='where' name='countries-to-visit'>
                        <input type='hidden' class='where-values' value="<?php echo $countries?>">
                    </div>
                </div>
                <div class='when-inp inp steps-inp'>
                    <label>Choose your travel dates</label>
                    <input type='button' id='when' class='from' name='time-from-visit'>
                    <i class='arr-r'></i>
                    <input type='button' class="to" name='time-to-visit'>
                </div>
                <div class='age-inp inp steps-inp'>
                    <label for='age'>Age</label>
                    <input type='text' id='age' name='age-visit' value="<?php echo $age ?>">
                    <input type='text' id='age2' value="<?php echo $age2 ?>">
                </div>
                <?php echo $form->hiddenField($model, 'countries'); ?>
                <?php echo $form->hiddenField($model, 'start_traveling', array('value' => $start_traveling, 'class' => 'order-start-traveling')); ?>
                <?php echo $form->hiddenField($model, 'end_traveling', array('value' => $end_traveling, 'class' => 'order-end-traveling')); ?>
                <?php echo $form->hiddenField($model, 'age', array('value' => $age, 'class'=>'order-age')); ?>
                <?php echo $form->hiddenField($model, 'age2', array('value' => $age2, 'class'=>'order-age2')); ?>

                <?php echo CHtml::submitButton('Change', array('class' => 'change-button')); ?>
<!--            </form>-->
            <?php $this->endWidget(); ?>
        </div>
        <div class="plans-group-block">
            <?php foreach($quotes as $quote) :?>
            <div class="plans-block">
                <?php $form=$this->beginWidget('CActiveForm', array(
                    'enableAjaxValidation' => true,
                    'enableClientValidation' => true,
                    'clientOptions' => array(
                        //'errorCssClass' => 'has-error',
                        'validateOnSubmit' => true,
                    ),
                    'htmlOptions'=>array(
                        'class'=>'orders-details',
                    ),
                    'action' => '/order/details'
                )); ?>
                <?php echo $form->hiddenField($model, 'quote', array('value' => $quote['quote']['type'])); ?>
                <?php echo $form->hiddenField($model, 'price', array('value' => $quote['quote']['price'])); ?>

                <?php echo $form->hiddenField($model, 'age2', array('value' => $age2)); ?>
                <?php echo $form->hiddenField($model, 'kids', array('value' => $kids)); ?>
                <h3 class='plan-name'><?php echo $quote['quote']['type']; ?></h3>
                <h2 class="plan-val"><span>$</span><?php echo $quote['quote']['price']; ?> </h2>
                <p class="plan-descr">If something goes wrong on your trip and you need to lodge a claim, begin the process by submitting your details here.</p>
                <?php echo CHtml::submitButton('Quote', array('class' => 'plan-quote')); ?>
                <?php $this->endWidget(); ?>
            </div>
            <?php endforeach; ?>
        </div>
        <div class="who-else-block">
            <h1>Tell us who else will go with you</h1>
            <div class="ages-block">
                <div class="adult-block">
                    <p>Age of adults</p>
                    <button class='add-adult add-age-button' style="<?php echo (!empty($age2)) ? 'display: none;' : '' ?>"></button>
                    <div class='age-inp-wrap'>
                        <input type="text" class='added-adult added-age' name="age" value="<?php echo $age ?>" disabled>
                    </div>
                    <?php if (!empty($age2)) : ?>
                    <div class='age-inp-wrap'>
                        <input type="text" class='added-adult added-age' name="age2" value="<?php echo $age2 ?>">
                        <a class='wrong wrong-adult'></a>
                    </div>
                    <?php endif; ?>
                </div>
                <div class="kids-block">
                    <p>Age of kids</p>
                    <button class='add-kid add-age-button'></button>
                    <div class='age-inp-wrap'>
                        <input type="text" class='added-kid added-age' name="kids[]">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>