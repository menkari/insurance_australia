<div class="steps-line">
    <div class="step-label-active step-label">
        <span class='fly'></span>
        <p>Details</p>
    </div>
    <div class="step-label-active step-label">
        <span class='fly'></span>
        <p>Choose options</p>
    </div>
    <div class="step-label-active step-label">
        <span class='fly'></span>
        <p>Summary</p>
    </div>
    <div class="step-label">
        <p>Payment </p>
    </div>
</div>
<?php //echo '<pre>'; print_r($_SESSION); echo '</pre>';?>

<div class="steps-wrapper steps-wrapper3">
    <div class="steps-content">
        <h5 class="step-info">3 step   /  choose plan</h5>
        <h2 class="step-header">Your Trip</h2>
        <div class='head-content-bl-form'>

            <!--            <form action='#' name='main-page-form'>-->
            <?php $form=$this->beginWidget('CActiveForm', array(
                'id' => 'orders-base-info',
                'enableAjaxValidation' => true,
                'enableClientValidation' => true,
                'clientOptions' => array(
                    //'errorCssClass' => 'has-error',
                    'validateOnSubmit' => true,
                ),
                'htmlOptions'=>array(
                    'class'=>'p-form-sign',
                ),
                'action' => '/order/quote'
            )); ?>
            <div class='where-inp inp steps-inp'>
                <label for='where'>Countries you will be visiting</label>
                <div class='where-div'>
                    <input type='text' id='where' name='countries-to-visit'>
                    <input type='hidden' class='where-values' value="<?php echo $countries?>">
                </div>
            </div>
            <div class='when-inp inp steps-inp'>
                <label>Choose your travel dates</label>
                <input type='button' id='when' class='from' name='time-from-visit'>
                <i class='arr-r'></i>
                <input type='button' class="to" name='time-to-visit'>
            </div>
            <div class='age-inp inp steps-inp'>
                <label for='age'>Age</label>
                <input type='text' id='age' name='age-visit' value="<?php echo $sessionData['age']; ?>">
                <input type='text' id='age2' value="<?php echo $sessionData['age2'] ?>">
            </div>
            <?php echo $form->hiddenField($orderModel, 'countries', array('value'=> $sessionData['countries'])); ?>
            <?php echo $form->hiddenField($orderModel, 'start_traveling', array('value' => $start_traveling, 'class' => 'order-start-traveling')); ?>
            <?php echo $form->hiddenField($orderModel, 'end_traveling', array('value' => $end_traveling, 'class' => 'order-end-traveling')); ?>
            <?php echo $form->hiddenField($orderModel, 'age', array('value'=> $sessionData['age'], 'class'=>'order-age')); ?>
            <?php echo $form->hiddenField($orderModel, 'age2', array('value' => $sessionData['age2'], 'class'=>'order-age2')); ?>

            <?php echo CHtml::submitButton('Change', array('class' => 'change-button')); ?>
            <!--            </form>-->
            <?php $this->endWidget(); ?>
        </div>
        <div class="cover-info">
            <h2>Your Cover</h2>
            <div class="circle-cover-info">
                <h2>$<span><?php echo $sessionData['price']; ?></span></h2>
                <h3><?php echo $sessionData['quote']; ?></h3>
            </div>
            <div class="change-button-wrapper">
                <?php echo CHtml::link('Change', array('order/quote'), array('class' => 'change-button change-button-circle')); ?>
            </div>
        </div>
        <div class="details personal-details">
            <h1>Personal detailes</h1>
<!--            <form action="#" name='first-adult-traveller-poilicy-holder'>-->
            <?php $form=$this->beginWidget('CActiveForm', array(
                'id'=>'order-details-form',
                'enableAjaxValidation' => true,
                'enableClientValidation' => true,
                'clientOptions' => array(
                    //'errorCssClass' => 'has-error',
                    'validateOnSubmit' => true,
                    'validateOnChange' => true,
                ),
                'action' => '/order/purchase'
            )); ?>
                <div class="details-form">
                    <h3>First Adult Traveller and Policy Holder</h3>
<!--                    <input type="text" class='adult-name' name='first-adult-name' placeholder='First Name'>-->
                    <?php echo $form->textField($detailsModel, 'name', array('class' => 'adult-name', 'placeholder' => 'First Name')); ?>
                    <?php echo $form->error($detailsModel,'name', array('style'=>'color: red;', 'hideErrorMessage'=>true)); ?>
<!--                    <input type="text" class='adult-last-name' name='first-adult-last-name' placeholder='Last Name'>-->
                    <?php echo $form->textField($detailsModel, 'last_name', array('class' => 'adult-last-name', 'placeholder' => 'Last Name')); ?>
                    <?php echo $form->error($detailsModel,'last_name', array('style'=>'color: red;', 'hideErrorMessage'=>true)); ?>
                    <div class="datepicker-input-item input-item">
<!--                        <input type="text" class='adult-birth' name='first-adult-birth' placeholder='Date of birth'>-->
                        <?php echo $form->textField($detailsModel, 'birth_date', array('class' => 'adult-birth', 'placeholder' => 'Date of birth')); ?>
                        <?php echo $form->error($detailsModel,'birth_date', array('style'=>'color: red;', 'hideErrorMessage'=>true)); ?>
                    </div>

<!--                    <input type="text" class='adult-email' name='first-adult-email' placeholder='Email Address'>-->
                    <?php echo $form->textField($detailsModel, 'email', array('class' => 'adult-email', 'placeholder' => 'Email Address')); ?>
                    <?php echo $form->error($detailsModel,'email', array('style'=>'color: red;', 'hideErrorMessage'=>true)); ?>
<!--                    <input type="text" class='adult-email-confirm' name='first-adult-email-confirm' placeholder='Confirm Email'>-->
                    <?php echo $form->textField($detailsModel, 'email_confirm', array('class' => 'adult-email-confirm', 'placeholder' => 'Confirm Email')); ?>
                    <?php echo $form->error($detailsModel,'email_confirm', array('style'=>'color: red;', 'hideErrorMessage'=>true)); ?>
<!--                    <input type="text" class='adult-phone' name='first-adult-phone' placeholder='Phone number'>-->
                    <?php echo $form->textField($detailsModel, 'phone', array('class' => 'adult-phone', 'placeholder' => 'Phone number')); ?>
                    <?php echo $form->error($detailsModel,'phone', array('style'=>'color: red;', 'hideErrorMessage'=>true)); ?>
<!--                    <input type="text" class='adult-health-disorder-number' name='first-adult-health-disorder-number' placeholder='Existing Health Disorder Number'>-->
                    <?php echo $form->textField($detailsModel, 'health_disorder', array('class' => 'adult-health-disorder-number', 'placeholder' => 'Existing Health Disorder Number')); ?>
                    <?php echo $form->error($detailsModel,'health_disorder', array('style'=>'color: red;', 'hideErrorMessage'=>true)); ?>
                </div>

                <div class="details-form">
                    <h3>Policy Holder's Address</h3>
<!--                    <input type="text" placeholder='Address 1' name='address1' class='address1'>-->
                    <?php echo $form->textField($detailsModel, 'address1', array('class' => 'address1', 'placeholder' => 'Address 1')); ?>
                    <?php echo $form->error($detailsModel,'address1', array('style'=>'color: red;', 'hideErrorMessage'=>true)); ?>
<!--                    <input type="text" placeholder='Address 2' name='address1' class='address2'>-->
                    <?php echo $form->textField($detailsModel, 'address2', array('class' => 'address2', 'placeholder' => 'Address 2')); ?>
                    <?php echo $form->error($detailsModel,'address2', array('style'=>'color: red;', 'hideErrorMessage'=>true)); ?>
<!--                    <input type="text" placeholder='Region' name='region' class='region'>-->
                    <?php echo $form->textField($detailsModel, 'region', array('class' => 'region', 'placeholder' => 'Region')); ?>
                    <?php echo $form->error($detailsModel,'region', array('style'=>'color: red;', 'hideErrorMessage'=>true)); ?>
<!--                    <input type="text" placeholder='State' name='state' class='state'>-->
                    <?php echo $form->textField($detailsModel, 'state', array('class' => 'state', 'placeholder' => 'State')); ?>
                    <?php echo $form->error($detailsModel,'state', array('style'=>'color: red;', 'hideErrorMessage'=>true)); ?>
<!--                    <input type="text" placeholder='Post Code' name='post-code' class='post-code'>-->
                    <?php echo $form->textField($detailsModel, 'post_code', array('class' => 'post-code', 'placeholder' => 'Post Code')); ?>
                    <?php echo $form->error($detailsModel,'post_code', array('style'=>'color: red;', 'hideErrorMessage'=>true)); ?>
                    <select name="policy-holder-country" class='policy-address-country select form-select'>
                        <option>Country</option>
                    </select>
                    <?php echo $form->hiddenField($detailsModel, 'country', array('class' => 'order-detail-country')); ?>
                    <?php echo $form->error($detailsModel,'country', array('style'=>'color: red;', 'hideErrorMessage'=>true)); ?>
<!--                    country-->
                </div>

                <div class="details-form">
                    <h3>Second Adult Traveller</h3>
<!--                    <input type="text" class='adult-name' name='second-adult-name' placeholder='First Name'>-->
                    <?php echo $form->textField($detailsModel, 'second_name', array('class' => 'adult-name', 'placeholder' => 'First Name')); ?>
                    <?php echo $form->error($detailsModel,'second_name', array('style'=>'color: red;', 'hideErrorMessage'=>true)); ?>

<!--                    <input type="text" class='adult-last-name' name='second-adult-last-name' placeholder='Last Name'>-->
                    <?php echo $form->textField($detailsModel, 'second_last_name', array('class' => 'adult-last-name', 'placeholder' => 'Last Name')); ?>
                    <?php echo $form->error($detailsModel,'second_last_name', array('style'=>'color: red;', 'hideErrorMessage'=>true)); ?>

                    <div class="datepicker-input-item input-item">
<!--                        <input type="text" class='adult-birth' name='second-adult-birth' placeholder='Date of birth'>-->
                        <?php echo $form->textField($detailsModel, 'second_birth_date', array('class' => 'adult-birth', 'placeholder' => 'Date of birth')); ?>
                        <?php echo $form->error($detailsModel,'second_birth_date', array('style'=>'color: red;', 'hideErrorMessage'=>true)); ?>
                    </div>
                </div>

                <p class='save-details-for-future-p'><label><?php echo $form->checkBox($detailsModel,'save_for_future',  array('uncheckValue'=>0,'checked'=>'checked')); ?>Save detailes for future</label></p>
<!--            save_for_future-->
                <div class="details-form payment-details">
                    <h1>Payment Detailes</h1>
                    <h3>Enter Credit Card Detailes</h3>
                    <p>Text warning that there will not be immdediate payment and next step will be confirmation step, So person wont hesitate to press the button</p>
<!--                    <input type="text" name='card-holder-name' placeholder='Card Holder Name' class='card-holder-name'>-->
                    <?php echo $form->textField($paymentsModel, 'name', array('class' => 'card-holder-name', 'placeholder' => 'Card Holder Name')); ?>
                    <?php echo $form->error($paymentsModel,'name', array('style'=>'color: red;', 'hideErrorMessage'=>true)); ?>
<!--                    <input type="text" name='card-holder-name' placeholder='Card Number'>-->
                    <?php echo $form->textField($paymentsModel, 'card_number', array('class' => '', 'placeholder' => 'Card Number')); ?>
                    <?php echo $form->error($paymentsModel,'card_number', array('style'=>'color: red;', 'hideErrorMessage'=>true)); ?>
                    <div class="payment-inp-group">

<!--                        <input type="text" class='mmyy-card' name='mmyy-card' placeholder='MM / YY'>-->
                        <?php echo $form->textField($paymentsModel, 'date', array('class' => 'mmyy-card', 'placeholder' => 'MM / YY')); ?>
                        <?php echo $form->error($paymentsModel,'date', array('style'=>'color: red;', 'hideErrorMessage'=>true)); ?>
<!--                        <input type="text" class='cvc-card' name='cvc-card' placeholder='CVC'>-->
                        <?php echo $form->textField($paymentsModel, 'cvc', array('class' => 'cvc-card', 'placeholder' => 'CVC')); ?>
                        <?php echo $form->error($paymentsModel,'cvc', array('style'=>'color: red;', 'hideErrorMessage'=>true)); ?>
                    </div>
                    <?php echo $form->errorSummary($detailsModel); ?>
                    <?php echo $form->errorSummary($paymentsModel); ?>
                    <div class="complete-card-wrapper">
                        <?php echo CHtml::htmlButton('Complete', array('class' => 'complete-card','type'=>'submit')); ?>
                    </div>
                </div>
<!--            </form>-->
            <?php $this->endWidget(); ?>
        </div>
    </div>
</div>