<div class="steps-line">
    <div class="step-label-active step-label">
        <span class='fly'></span>
        <p>Trip Details</p>
    </div>
    <div class="step-label-active step-label">
        <span class='fly'></span>
        <p>Plan Options</p>
    </div>
    <div class="step-label-active step-label">
        <span class='fly'></span>
        <p>Personal Details</p>
    </div>
    <div class="step-label-active step-label">
        <p>Payment </p>
    </div>
</div>
<?php //echo '<pre>'; print_r($_SESSION); echo '</pre>';?>
<div class="steps-wrapper steps-wrapper4 confirmation-steps-wrapper">
    <div class="steps-content">
        <div class="back-btn-container">
            <a href="/order/details" class='back-button'>Back</a>
        </div>
        <h5 class="step-info">Step 4  /  Payment</h5>
        <h2 class="step-header">What you're getting:</h2>
        <div class="confirmation-info-wrapper">
            <div class="confirmation-info">
                <div class="confirmation-info-left">
                    <div class="details confirmation-countries">
                        <h1>Countries of visit</h1>
                        <div class="confirmation-countries-block">
                            <?php $countries = explode("|", $sessionData['countries']); ?>
                            <?php foreach($countries as $key =>$country) : ?>
                                <a class="selected-country not-x-close"><?php echo $country; ?></a><?php echo ($key < count($countries) - 1) ? ',' : ''; ?>
                            <?php endforeach; ?>
                        </div>
                    </div>
                    <div class="details confirmation-date">
                        <h1>Travel Dates</h1>
                        <div class="confirmation-date-block">
                            <span class="confirmation-date-from"><?php echo date('d M Y', $sessionData['start_traveling']); ?></span>
                            <i class='confirmation-arr'></i>
                            <span class="confirmation-date-to"><?php echo date('d M Y', $sessionData['end_traveling']); ?></span>
                        </div>
                    </div>
                </div>
                <div class="confirmation-info-right">
                    <div class="details confirmation-cover">
                        <h1>&nbsp;</h1>
                        <div class="circle-cover-info" style="background-color: #e1164b;">
                            <h2>$<span><?php echo $sessionData['price']; ?></span></h2>
                            <h3><?php echo $sessionData['quote']; ?></h3>
                        </div>
                    </div>
                </div>
                <div class="confirmation-info-packs">
                    <div class="details confirmation-cover">
                        <h1>Benefit Packs</h1>
                        <?php foreach($sessionData['options'] as $option => $item) :?>
                            <div class="details-pack">
                                <p class="packs-name"><?php echo $item['name']; ?></p>
                                <h4><span>$</span><?php echo $item['value']; ?></h4>
                            </div>
                        <?php endforeach; ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="details personal-details">
            <h1>Personal details</h1>
            <div class="details-confirm-columns" data-columns>
                <div class="details-confirm-column details-confirm-column1">
                    <h3>First Adult Traveller</h3>
                    <h4><?php echo $sessionData['orderDetails']['name']; ?> <?php echo $sessionData['orderDetails']['last_name']; ?></h4>
                    <p>
                        <?php echo date('d F Y', strtotime($sessionData['orderDetails']['birth_date'])) ?>
                    </p>
                    <p>
                        Email: <?php echo $sessionData['orderDetails']['email'] ?>
                    </p>
                    <p>
                        Phone: <?php echo $sessionData['orderDetails']['phone'] ?>
                    </p>
                </div>
                <div class="details-confirm-column details-confirm-column2">
                    <h3>Second Adult Traveller</h3>
                    <?php if (!empty($sessionData['age2'])) : ?>
                        <h4><?php echo $sessionData['orderDetails']['second_name']; ?> <?php echo $sessionData['orderDetails']['second_last_name']; ?></h4>
                        <p>
                            <?php echo date('d F Y', strtotime($sessionData['orderDetails']['second_birth_date'])); ?>
                        </p>
                    <?php else : ?>
                        <h4>None<h4>
                    <?php endif; ?>
                </div>

                <div class="details-confirm-column details-confirm-column3">
                    <h3>Policy Holder's Address</h3>
<!--                    <h4>Address 1</h4>-->
                    <p>Address 1: <?php echo $sessionData['orderDetails']['address1']; ?></p>
                    <?php if (!empty($sessionData['orderDetails']['address2'])) : ?>
                    <p>Address 2: <?php echo $sessionData['orderDetails']['address2']; ?></p>
                    <?php endif; ?>

                    <?php if (!empty($sessionData['orderDetails']['region'])) : ?>
                        <p>City: <?php echo $sessionData['orderDetails']['region']; ?></p>
                    <?php endif; ?>
                    <?php if (!empty($sessionData['orderDetails']['state'])) : ?>
                        <p>State: <?php echo $sessionData['orderDetails']['state']; ?></p>
                    <?php endif; ?>
                    <?php if (!empty($sessionData['orderDetails']['post_code'])) : ?>
                        <p>Post Code: <?php echo $sessionData['orderDetails']['post_code']; ?></p>
                    <?php endif; ?>
                    <?php if (!empty($sessionData['orderDetails']['country'])) : ?>
                        <p>Country: <?php echo $sessionData['orderDetails']['country']; ?></p>
                    <?php endif; ?>
                </div>
                <?php if (!empty($sessionData['kids'])) : ?>
                    <?php //var_dump($sessionData['orderDetails']); ?>
                    <div class="details-confirm-column dependents-column">
                        <h3>Dependents</h3>
                        <?php foreach($sessionData['orderDetails']['kids_info'] as $kidItem) : ?>
                            <h4>Name: <?php echo $kidItem['name']; ?> <?php echo $kidItem['last_name']; ?></h4>
                            <p>
                                Birthday: <?php echo date('d F Y', strtotime(str_replace("/","-",$kidItem['birth_date']))); ?>
                            </p>
                        <?php endforeach; ?>
                    </div>
                <?php endif; ?>
            </div>
        </div>
        <div class="pay-complete-wrapper">
            <?php $form=$this->beginWidget('CActiveForm', array(
                'id' => 'order-payment',
                'enableClientValidation'=>true,
                'clientOptions'=>array(
                    'validateOnSubmit'=>true,
                ),
                'htmlOptions'=>array(
                    'class'=>'payment-final-form',
                ),
                'action' => '/order/payment'
            )); ?>
            <div class="details details-form payment-details">
                <h1>Payment Details</h1>
                <h3>Enter Credit Card Details</h3>
                <p>We don't store your payment details. To protect your credit card details we ask that you re-enter
                    your credit card number for each transaction.</p>
                <div class="step3-inp-container payment-container">
                    <div class='step3-inp-wrapper step3-inp-wrapper-verybig'>
                        <div class="credit-card-images"><img class="credit-card-image credit-card-possible" data-value="Visa" src="<?php echo Yii::app()->theme->baseUrl; ?>/img/visa.png"><img class="credit-card-image credit-card-possible" data-value="Master" src="<?php echo Yii::app()->theme->baseUrl; ?>/img/mastercard.png"><img class="credit-card-image credit-card-possible" data-value="Amex" src="<?php echo Yii::app()->theme->baseUrl; ?>/img/amex.png"></div>
                        <input type="hidden" name='Cart[card-type]' class='card-type-inp'>
                    </div>
                    <div class='step3-inp-wrapper step3-inp-wrapper-verybig'>
                        <input type="text" name='Cart[card-holder-name]' placeholder='Name Surname' class='card-holder-name'>
                        <p class='invalid-label'>Error: Invalid name</p>
                    </div>
                    <div class='step3-inp-wrapper step3-inp-wrapper-verybig'>
                        <input type="text" name='Cart[card-holder-number]' placeholder='Card Number' class='card-holder-number'>
                        <p class='invalid-label'>Error: Invalid card number</p>
                    </div>
                    <div class="payment-inp-group">
                        <div class='step3-inp-wrapper step3-inp-wrapper-177px'>
                            <input type="text" class='mmyy-card' name='Cart[card-date]' placeholder='MM / YY'>
                            <p class='invalid-label'>Error: Invalid card date</p>
                        </div>
                        <div class='step3-inp-wrapper step3-inp-wrapper-177px'>
                            <input type="text" class='cvc-card' name='Cart[card-cvc]' placeholder='CVC'>
                            <p class='invalid-label'>Error: Invalid cvc</p>
                        </div>
                    </div>
                </div>
                <?php if(Yii::app()->user->hasFlash('error')):?>
                    <div class="errorMessage">
                        <?php echo Yii::app()->user->getFlash('error'); ?>
                    </div>
                <?php endif; ?>
                <?php if(Yii::app()->user->hasFlash('cart_date_error')):?>
                    <div class="errorMessage">
                        <?php echo Yii::app()->user->getFlash('cart_date_error'); ?>
                    </div>
                <?php endif; ?>

                <div class="policy-declaration">
                    <label><input type="checkbox" name="policy-declaration" class="accept-policy-declaration">
                        I/we have read and understood the <a
                                href="/source/My%20Trip%20Insurance%20Australia%20-%20Single%20Trip%20PDS.pdf"
                                target="_blank">Combined Policy Wording and Product Disclosure Statement</a> and <a
                                href="/source/My%20Trip%20Insurance%20FSG.pdf" target="_blank">Chubb Financial Services
                            Guide</a> made available to me/us and agree to accept the insurance subject to the terms,
                        conditions, exclusions and limitations of <a
                                href="/source/My%20Trip%20Insurance%20Australia%20-%20Single%20Trip%20PDS.pdf"
                                target="_blank">the Combined Policy Wording and Product Disclosure
                            Statement</a>.

                        I/we confirm that I/we are Australian residents.

                        I consent to Chubb providing me with the Combined Policy Wording and Product Disclosure
                        Statement and Certificate of Insurance via email.
                    </label>
                </div>

                <div class="complete-card-wrapper">
<!--                    <button class='complete-card'>Complete</button>-->
                    <?php echo CHtml::htmlButton('Pay and Complete', array('class' => 'complete-card pay-and-complete','type'=>'submit', 'disabled'=>'disabled')); ?>
                </div>
            </div>

            <?php //echo CHtml::htmlButton('Pay and Complete', array('class' => 'pay-and-complete','type'=>'submit')); ?>
            <?php $this->endWidget(); ?>
        </div>
    </div>
</div>