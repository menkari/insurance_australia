<div class="table-wrapper orders-wrapper" xmlns="http://www.w3.org/1999/html">
        <div class="table-content">
                <h1 class="pages-header">Orders List</h1>
                <?php if(Yii::app()->user->checkAccess('partner')) : ?>
                <form class="orders-panel" method="post">
                        <div class="search-panel">
                                <label for="search-table">Search:</label>
                                <input value="<?=isset($post['search'])?$post['search']:''?>" name="search" type="text" id="search-table">
                        </div>

                        <div class="panel-pickers">
                                <div class="datepicker-input-item input-item">
                                        <input type="hidden" name='ordersFrom' class='editted-date' value="<?=isset($post['ordersFrom'])?$post['ordersFrom']:''?>">
                                        <label for='from-picker-statistic'>Show from:</label>
                                        <input type="text" class='dataEdit filter-from-date autoloadInfo' id='from-picker-statistic' placeholder='MM / DD / YYYY'>
                                </div>
                                <div class="datepicker-input-item input-item">
                                        <input type="hidden" name='ordersTo' class='editted-date' value="<?=isset($post['ordersTo'])?$post['ordersTo']:''?>">
                                        <label for='to-picker-statistic'>To:</label>
                                        <input type="text" class='dataEdit filter-to-date autoloadInfo' id='to-picker-statistic' placeholder='MM / DD / YYYY'>
                                </div>
                        </div>

                        <div class="filter-panel">
                                <div class="filter-widget">
                                        <label>Filter by cost
                                                <span>$</span><span class='min-span-controlller'></span>
                                                <span>-</span>
                                                <span>$</span><span class='max-span-controlller'></span>
                                        </label>
                                        <div class='filter-slider' data-min="<?php echo $min ?>" data-max="<?php echo $max ?>">
                                                <input type="hidden" name='min-val' class='min-val' value="<?=isset($post['min-val'])?$post['min-val']:''?>">
                                                <input type="hidden" name='max-val' class='max-val' value="<?=isset($post['max-val'])?$post['max-val']:''?>">
                                                <span class='left-handle-controller handler'></span>
                                                <span class='right-handler-controller handler'></span>
                                        </div>
                                </div>
                                <div class="filter-buttons">
                                        <button type="submit" class='apply-filter filter-button'>Apply</button>
                                        <button type="reset" class='clear-filter filter-button'>Clear all</button>
                                </div>
                        </div>
                </form>
                <?php endif; ?>
                <!--<form class="orders-panel" method="post">
                                <div class="search-panel">
                                        <label for="search-table">Search:</label>
                                        <input value="<?/*=$post['search']*/?>" name="search" type="text" id="search-table">
                                </div>

                                <div class="panel-pickers">
                                        <div class="datepicker-input-item input-item">
                                                <input type="hidden" name='ordersFrom' class='editted-date' value="<?/*=$post['ordersFrom']*/?>">
                                                <label for='from-picker-statistic'>Show from:</label>
                                                <input type="text" class='dataEdit' id='from-picker-statistic' placeholder='MM / DD / YYYY'>
                                        </div>
                                        <div class="datepicker-input-item input-item">
                                                <input type="hidden" name='ordersTo' class='editted-date' value="<?/*=$post['ordersTo']*/?>">
                                                <label for='to-picker-statistic'>To:</label>
                                                <input type="text" class='dataEdit' id='to-picker-statistic' placeholder='MM / DD / YYYY'>
                                        </div>
                                </div>

                                <div class="filter-panel">
                                        <div class="filter-widget">
                                                <label>Filter by cost  <span>$100 - $2540</span></label>
                                                <div class='filter-slider'>
                                                        <input type="hidden" name='min-val' value="<?/*=$post['min-val']*/?>">
                                                        <input type="hidden" name='max-val' value="<?/*=$post['max-val']*/?>">
                                                        <span class='left-handle-controller handler'></span>
                                                        <span class='right-handler-controller handler'></span>
                                                </div>
                                        </div>
                                        <div class="filter-buttons">
                                                <button type="submit" class='apply-filter filter-button'>Apply</button>
                                                <button type="reset" class='clear-filter filter-button'>Clear all</button>
                                        </div>
                                </div>
                </form>-->
                <div class="orders-table">
                        <?php $this->widget('zii.widgets.grid.CGridView', array(
                            'itemsCssClass' => 'orders-table-container',
                            'dataProvider'=>$dataProvider,
                            'summaryText' => '',
                            'columns'=>array(
                                array(
                                    'name'=>'order number',
                                    'value'=>'$data->id',
                                    'type' => 'raw'
                                ),
                                array(
                                    'name'=>'order date',
                                    'value'=> 'date("d M Y", strtotime($data->date))',
                                ),
                                array(
                                    'name'=>'cost',
                                    'value'=> '$data->price',
                                ),
                                array(
                                    'name'=>'countries of visit',
                                    'value'=> '"<b>".$data->countries."</b>"',
                                    'type' => 'raw'
                                ),
                                array(
                                    'name'=>'dates of traveling',
                                    'value'=> '"<b>from</b> " . date("d M Y", strtotime($data->start_traveling)) . "\n <br><b>to</b> " .  date("d M Y", strtotime($data->end_traveling))',
                                    'type' => 'raw'
                                ),
                                array(
                                    'name'=>'Travellers',
                                    'value'=> '"<b>".$data->travellers."</b>"',
                                    'type' => 'raw'
                                ),
                                array(
                                    'name'=>'download',
                                    'value'=> '"<a href=\'".$data->pdf."\' target=\'\'>Download</a>"',
                                    'type' => 'raw'
                                )
                            ),
                            'htmlOptions' => array('class' => 'statistic-table'),
                            'pagerCssClass' => 'iteora-pager',
                            'pager'=>array("cssFile" => false)
                        )); ?>
                </div>
        </div>
</div>