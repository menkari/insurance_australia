<div class="steps-line">
    <div class="step-label-active step-label">
        <span class='fly'></span>
        <p>Trip Details</p>
    </div>
    <div class="step-label-active step-label">
        <span class='fly'></span>
        <p>Plan Options</p>
    </div>
    <div class="step-label-active step-label">
        <span class='fly'></span>
        <p>Personal Details</p>
    </div>
    <div class="step-label">
        <p>Payment </p>
    </div>
</div>

<div class="steps-wrapper steps-wrapper3">
    <div class="steps-content">
        <div class="back-btn-container">
            <a href="/order/quote" class='back-button'>Back</a>
        </div>
        <h5 class="step-info">Step 3  /  Personal Details</h5>
        <h2 class="step-header">What you're getting:</h2>
        <div class='head-content-bl-form'>
            <form action='#' name='main-page-form'>
                <div class="confirmation-info-wrapper">
                    <div class="confirmation-info">
                        <div class="confirmation-info-left">
                            <div class="details confirmation-countries">
                                <div class="details details-plan-block1">
                                    <h1>Plan</h1>
                                    <h2><?php echo $sessionData['quote']; ?></h2>
<!--                                    <p class='trip-type-p'>Single Trip</p>-->
                                </div>
                                <h1>Countries of visit</h1>
                                <div class="confirmation-countries-block">
                                    <?php $countries = explode("|", $sessionData['countries']); ?>
                                    <?php foreach($countries as $key =>$country) : ?>
                                        <a class="selected-country not-x-close"><?php echo $country; ?></a><?php echo ($key < count($countries) - 1) ? ',' : ''; ?>
                                    <?php endforeach; ?>
                                </div>
                            </div>
                            <div class="details confirmation-date">
                                <h1>Travel Dates</h1>
                                <div class="confirmation-date-block">
                                    <span class="confirmation-date-from"><?php echo date('d M Y', $sessionData['start_traveling']); ?></span>
                                    <i class='confirmation-arr'></i>
                                    <span class="confirmation-date-to"><?php echo date('d M Y', $sessionData['end_traveling']); ?></span>
                                </div>
                            </div>
                        </div>
                        <div class="confirmation-info-right">
                            <div class="details confirmation-cover">
                                <h1>&nbsp;</h1>
                                <div class="circle-cover-info">
                                    <h2>$<span><?php echo $sessionData['price']; ?></span></h2>
                                </div>
                            </div>
                        </div>
                        <div class="confirmation-info-packs">
                            <div class="details confirmation-cover">
                                <h1>Benefit Packs</h1>
                                <?php foreach($sessionData['options'] as $option => $item) :?>
                                <div class="details-pack">
                                    <p class="packs-name"><?php echo $item['name']; ?></p>
                                    <h4><span>$</span><?php echo $item['value']; ?></h4>
                                </div>
                                <?php endforeach; ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="change-button-wrapper confirmation-change-button-wrapper">
                    <div class="total-prem">
                        <p>Total Premium</p>
                        <h4><span>$</span><?php echo $sessionData['price']; ?> </h4>
                    </div>
                    <?php echo CHtml::link('Change',array('order/quote'), array('class'=>'change-button change-button-circle')); ?>
                </div>
            </form>
        </div>
        <div class="details personal-details">
            <h1>Personal details</h1>
            <!--            <form action="#" name='first-adult-traveller-poilicy-holder'>-->
            <?php $form=$this->beginWidget('CActiveForm', array(
                'id'=>'order-details-form',
                'enableClientValidation'=>true,
                'clientOptions'=>array(
                    'validateOnSubmit'=>true,
                ),
                'action' => '/order/purchase'
            )); ?>
            <div class="details-form">
                <h3>First Adult Traveller and Policy Holder (age <?php echo $sessionData['age']; ?>)</h3>
                <div class="step3-inp-container">
                    <div class='step3-inp-wrapper step3-inp-wrapper-middle'>
                        <?php echo $form->textField($detailsModel, 'name', array('class' => 'adult-name', 'placeholder' => 'First Name', 'value' => $userInfo['name'])); ?>
                        <?php echo $form->error($detailsModel,'name', array('style'=>'color: red;')); ?>
                    </div>
                    <div class='step3-inp-wrapper step3-inp-wrapper-middle'>
                    <?php echo $form->textField($detailsModel, 'last_name', array('class' => 'adult-last-name', 'placeholder' => 'Last Name', 'value' => $userInfo['surname'])); ?>
                    <?php echo $form->error($detailsModel,'last_name', array('style'=>'color: red;')); ?>
                    </div>
                    <div class='step3-inp-wrapper input-item step3-inp-wrapper-small'>
                        <?php echo $form->textField($detailsModel, 'birth_date', array('class' => 'traveller adult-birth', 'placeholder' => 'Date of birth', 'data-min' => date('Y-m-d', strtotime($start_traveling . ' - ' . ($sessionData['age'] + 1) . 'year + 1day')), 'data-max' => date('Y-m-d', strtotime($start_traveling . ' - ' . $sessionData['age'] . 'year')), 'value' => $userInfo['birth'])); ?>
                        <?php echo $form->error($detailsModel,'birth_date', array('style'=>'color: red;')); ?>
                    </div>

                    <div class='step3-inp-wrapper step3-inp-wrapper-big'>
                        <?php echo $form->textField($detailsModel, 'email', array('class' => 'adult-email', 'placeholder' => 'Email Address', 'value' => $userInfo['email'])); ?>
                        <?php echo $form->error($detailsModel,'email', array('style'=>'color: red;')); ?>
                    </div>
                    <div class='step3-inp-wrapper step3-inp-wrapper-big'>
                        <?php echo $form->textField($detailsModel, 'email_confirm', array('class' => 'adult-email-confirm', 'placeholder' => 'Confirm Email', 'value' => $userInfo['email'])); ?>
                        <?php echo $form->error($detailsModel,'email_confirm', array('style'=>'color: red;')); ?>
                    </div>
                    <div class='step3-inp-wrapper step3-inp-wrapper-big'>

                        <a class="flag-disabled" href='#'>
                            <div class="selected-flag" tabindex="0" title="Australia: +61">
                                <div class="iti-flag au"></div>
                                <div class="iti-arrow"></div>
                            </div>
                        </a>
                        <input type="text" class='adult-phone' name='first-adult-phone' data-value='<?php echo $userInfo['phone'] ?>' value='<?php echo $userInfo['phone'] ?>'>
                        <?php echo $form->hiddenField($detailsModel, 'phone', array('class' => 'hidden-adult-numb', 'placeholder' => 'Phone number')); ?>
                        <?php echo $form->error($detailsModel,'phone', array('style'=>'color: red;')); ?>
                    </div>
                    <div class='step3-inp-wrapper step3-inp-wrapper-big'>
                        <?php //echo $form->textField($detailsModel, 'health_disorder', array('class' => 'adult-health-disorder-number', 'placeholder' => 'Existing Health Disorder Number')); ?>
                        <?php //echo $form->error($detailsModel,'health_disorder', array('style'=>'color: red;')); ?>
                    </div>
                </div>
            </div>
<!--            <div style="text-align: center;">-->
                <?php //echo $form->error($detailsModel,'name', array('style'=>'color: red;')); ?>
                <?php //echo $form->error($detailsModel,'last_name', array('style'=>'color: red;')); ?>
                <?php //echo $form->error($detailsModel,'birth_date', array('style'=>'color: red;')); ?>
                <?php //echo $form->error($detailsModel,'email', array('style'=>'color: red;')); ?>
                <?php //echo $form->error($detailsModel,'email_confirm', array('style'=>'color: red;')); ?>
                <?php //echo $form->error($detailsModel,'phone', array('style'=>'color: red;')); ?>
                <?php //echo $form->error($detailsModel,'health_disorder', array('style'=>'color: red;')); ?>
<!--            </div>-->

            <div class="details-form">
                <h3>Policy Holder's Address</h3>
                <div class="step3-inp-container">
                    <div class='step3-inp-wrapper step3-inp-wrapper-big'>
                        <?php echo $form->textField($detailsModel, 'address1', array('class' => 'address1', 'placeholder' => 'Address 1', 'value' => $userInfo['street'])); ?>
                        <?php echo $form->error($detailsModel,'address1', array('style'=>'color: red;')); ?>
                    </div>
                    <div class='step3-inp-wrapper step3-inp-wrapper-big'>
                        <?php echo $form->textField($detailsModel, 'address2', array('class' => 'address2', 'placeholder' => 'Address 2')); ?>
                        <?php echo $form->error($detailsModel,'address2', array('style'=>'color: red;')); ?>
                    </div>
                    <div class='step3-inp-wrapper step3-inp-wrapper-lessmedium'>
                        <?php echo $form->textField($detailsModel, 'region', array('class' => 'region', 'placeholder' => 'Suburb', 'value' => $userInfo['city'])); ?>
                        <?php echo $form->error($detailsModel,'region', array('style'=>'color: red;')); ?>
                    </div>
                    <div class='step3-inp-wrapper step3-inp-wrapper-lessmedium'>
                        <?php echo $form->dropDownList($detailsModel, 'state',
                            array('ACT' => 'ACT',
                                'NSW' => 'NSW',
                                'NT' => 'NT',
                                'QLD' => 'QLD',
                                'SA' => 'SA',
                                'TAS' => 'TAS',
                                'VIC' => 'VIC',
                                'WA' => 'WA'),
                            array('class' => 'state policy-address-state state-select select',
                                'empty' => 'State',
                                'options' => array($userInfo['state'] => array('selected'=>true))
                            )); ?>

                        <?php echo $form->error($detailsModel,'state', array('style'=>'color: red;')); ?>
                    </div>
                    <div class='step3-inp-wrapper step3-inp-wrapper-lessmedium'>
                        <?php echo $form->textField($detailsModel, 'post_code', array('class' => 'post-code', 'placeholder' => 'Post Code', 'value' => $userInfo['zip_code'])); ?>
                        <?php echo $form->error($detailsModel,'post_code', array('style'=>'color: red;')); ?>
                    </div>
                    <div class='step3-inp-wrapper step3-inp-wrapper-lessmedium'>
                        <?php echo $form->hiddenField($detailsModel, 'country', array('class' => 'order-detail-country', 'value' => 'Australia')); ?>
                        <select name="policy-holder-country" class='policy-address-country select form-select' disabled>
                            <option>Australia</option>
                        </select>
                        <?php echo $form->error($detailsModel,'country', array('style'=>'color: red;')); ?>
                    </div>
                </div>
            </div>
<!--            <div style="text-align: center;">-->
                <?php //echo $form->error($detailsModel,'address1', array('style'=>'color: red;')); ?>
                <?php //echo $form->error($detailsModel,'address2', array('style'=>'color: red;')); ?>
                <?php //echo $form->error($detailsModel,'region', array('style'=>'color: red;')); ?>
                <?php //echo $form->error($detailsModel,'state', array('style'=>'color: red;')); ?>
                <?php //echo $form->error($detailsModel,'post_code', array('style'=>'color: red;')); ?>
                <?php //echo $form->error($detailsModel,'country', array('style'=>'color: red;')); ?>
<!--            </div>-->

            <?php if (!empty($sessionData['age2'])) : ?>
            <div class="details-form">
                <h3>Second Adult Traveller (age <?php echo $sessionData['age2']; ?>)</h3>
                <div class="step3-inp-container">
                    <div class='step3-inp-wrapper step3-inp-wrapper-middle'>
                        <?php echo $form->textField($detailsModel, 'second_name', array('class' => 'adult-name', 'placeholder' => 'First Name')); ?>
                        <?php echo $form->error($detailsModel,'second_name', array('style'=>'color: red;')); ?>
                    </div>
                    <div class='step3-inp-wrapper step3-inp-wrapper-middle'>
                        <?php echo $form->textField($detailsModel, 'second_last_name', array('class' => 'adult-last-name', 'placeholder' => 'Last Name')); ?>
                        <?php echo $form->error($detailsModel,'second_last_name', array('style'=>'color: red;')); ?>
                    </div>
                    <div class='step3-inp-wrapper input-item step3-inp-wrapper-small'>
                        <?php echo $form->textField($detailsModel, 'second_birth_date', array('class' => 'traveller adult-birth', 'placeholder' => 'Date of birth', 'data-min' => date('Y-m-d', strtotime($start_traveling . ' - ' . ($sessionData['age2'] + 1) . 'year + 1day')), 'data-max' => date('Y-m-d', strtotime($start_traveling .' - ' . $sessionData['age2'] . 'year')))); ?>
                        <?php echo $form->error($detailsModel,'second_birth_date', array('style'=>'color: red;')); ?>
                    </div>
                </div>
            </div>
<!--            <div style="text-align: center;">-->
                <?php //echo $form->error($detailsModel,'second_name', array('style'=>'color: red;')); ?>
                <?php //echo $form->error($detailsModel,'second_last_name', array('style'=>'color: red;')); ?>
                <?php //echo $form->error($detailsModel,'second_birth_date', array('style'=>'color: red;')); ?>
<!--            </div>-->
            <?php endif; ?>


            <?php if (!empty($sessionData['kids'])) : ?>
                <?php $kids = explode(',', $sessionData['kids']); ?>
                <?php foreach ($kids as $k => $kid) : ?>
                <div class="details-form">
                    <h3>Child (age <?php echo $kid; ?>)</h3>
                    <div class="step3-inp-container">
                        <div class='step3-inp-wrapper step3-inp-wrapper-middle'>
                            <?php echo $form->textField($detailsModel, "kids_info[$k][name]", array('class' => 'adult-name', 'placeholder' => 'First Name')); ?>
                            <?php echo $form->error($detailsModel, "kids_info[$k][name]", array('style'=>'color: red;')); ?>
                        </div>
                        <div class='step3-inp-wrapper step3-inp-wrapper-middle'>
                            <?php echo $form->textField($detailsModel, "kids_info[$k][last_name]", array('class' => 'adult-last-name', 'placeholder' => 'Last Name')); ?>
                            <?php echo $form->error($detailsModel, "kids_info[$k][last_name]", array('style'=>'color: red;')); ?>
                        </div>
                        <div class='step3-inp-wrapper input-item step3-inp-wrapper-small'>
                            <?php echo $form->textField($detailsModel, "kids_info[$k][birth_date]", array('class' => 'traveller adult-birth', 'placeholder' => 'Date of birth', 'data-min' => date('Y-m-d', strtotime($start_traveling . ' - ' . ($kid + 1). 'year + 1day')), 'data-max' => date('Y-m-d', strtotime($start_traveling . ' - ' . $kid . 'year')))); ?>
                            <?php echo $form->error($detailsModel, "kids_info[$k][birth_date]", array('style'=>'color: red;')); ?>
                        </div>
                    </div>
                </div>
                <div style="text-align: center;">
                    <?php echo $form->error($detailsModel, "kids_info[$k][name]", array('style'=>'color: red;')); ?>
                    <?php echo $form->error($detailsModel, "kids_info[$k][last_name]", array('style'=>'color: red;')); ?>
                    <?php echo $form->error($detailsModel, "kids_info[$k][birth_date]", array('style'=>'color: red;')); ?>
                </div>
                <?php endforeach; ?>
            <?php endif; ?>
            <div class="details-form">
                <div class="step3-inp-wrapper-big referral-select-block" 
                     style="margin: 0 auto;
                     <?=$has_cookie ? 'display: none;' : ''?>
                     width: 257px;
                     ">

                    <h3>Referral:</h3>
                    <input type="text" name="OrderDetails[referral]" class='referral-select' style="width: 235px;" value="<?=$referral?>" />
                </div>
            </div>
            <p class='save-details-for-future-p'><label><?php echo $form->checkBox($detailsModel,'save_for_future',  array('uncheckValue'=>0,'checked'=>'checked')); ?>Save details for next time</label></p>

            <div class="medical-conditions-wrapper">
                <h1>Pre-existing Medical Conditions</h1>
                <p>Do any of you or your fellow travellers have pre-existing medical conditions?</p>
                <label for="med-cond"><input type="checkbox" name="medical_conditions" id='med-cond'> Yes</label>
                <div class="medical-conditions" style="display: none;">
                    <div class="medical-conditions-text-block">
                        <b>Pre-existing medical conditions</b>
                        <p>You will not be covered for any claim that arises directly or indirectly due to a Pre-existing Medical Condition.</p>
                        <p>Pre-existing Medical Condition means any physical defect, condition, illness or disease of You, Your Travel Companion, Relative or Business Partner for which:</p>
                        <ol class="list_alpha">
                            <li>treatment, diagnosis, consultation, advice (including advice for treatment) has been received in the twelve (12) month period immediately preceding the Issue Date; or</li>
                            <li>medication has been prescribed for in the twelve (12) month period immediately preceding the Issue Date; or</li>
                            <li>treatment is planned and yet to be received; or</li>
                            <li>a condition, the manifestation of symptoms of which a reasonable person in the circumstances would be expected to be aware at the Issue Date; and</li>
                            <li>is not an Approved Medical Condition.</li>
                        </ol><br>
                        <b>Approved Medical Conditions</b>
                        <p>Cover is available automatically and without the need to go through an assessment process for the <a href="/source/20161222%20-%20Mytrip%20-%20Approved%20Medical%20Conditions%20(ST).pdf" target="_blank" class="btn-download">approved medical conditions</a> and</p>
                        <ol>
                            <li>the stipulated Requirement and/or Age Limits as set out in the table, where applicable, are met; and</li>
                            <li>You are not travelling against medical advice; and</li>
                            <li>You are not travelling for the purpose of seeking medical attention; and</li>
                            <li>You have not been hospitalised (including day surgery or emergency department attendance) for the Pre-existing Medical Condition at any time during the twelve (12) months prior to the Issue Date (unless a shorter period is specified in the Requirement column); and</li>
                            <li>You are not over the age of seventy six (76) years at any time during the Period of Insurance (or a lower age where specified).</li>
                        </ol><br>
                        <a href="/source/20161222%20-%20Mytrip%20-%20Approved%20Medical%20Conditions%20(ST).pdf" target="_blank" class="btn-download">Click here to view the Approved Medical Conditions table</a>
                    </div>
                </div>
                <label for="med-cond-bottom" class="med-cond-bottom-checkbox" style="display: none;"><input type="checkbox" id='med-cond-bottom'> All Travellers’ Pre-Existing Medical Condition(s) are listed in the attachment or acknowledge that no cover is available. </label>
            </div>

            <div class="complete-card-wrapper">
                <?php echo CHtml::htmlButton('Complete', array('class' => 'complete-card order-details-submit','type'=>'submit')); ?>
            </div>
            <?php $this->endWidget(); ?>
        </div>
    </div>
</div>
