<div class="white-labels-wrapper">
    <h1 class="pages-header">White Labels</h1>
    <div class="white-labels-container">
        <?php $form=$this->beginWidget('CActiveForm', array(
            'id'=>'template',
            'htmlOptions'=>array('enctype'=>'multipart/form-data'),
            //'enableClientValidation'=>true,
            'enableAjaxValidation'=>true,
            'clientOptions'=>array(
                'validateOnChange' => false,
                'validateOnSubmit'=>true,
                'validationDelay'=>2000
            ),
        )); ?>
            <div class="group-header">
                <h3>Edit your white label details</h3>
                <div class="block-input-item input-item">
                    <label for="dom-name">Domain Name:</label>
                    <?php echo $form->textField($model,'domain'); ?>
                    <?php echo $form->error($model,'domain'); ?>
                </div>
                <!--<div class="block-input-item input-item">
                    <label for="email-name">Email:</label>
                    <input type="text" id='email-name' class='group-input'>
                </div>-->
            </div>
            <div class="group-header">
<!--                <h3>Group header 2</h3>-->

                <div class="group-logo-wrapper">
                    <h4 class="group-logo-tip">Logo 1 size 200x400 px:</h4>
                    <p class="group-logo-desctiption">Description if nescessary where this logo will be.</p>
                    <div class="group-logo-icon-wrapper">
                        <img src="/images/wl-logo.png" alt="">
                    </div>
                    <!--<div class="group-checkboxes">
                        <label><input type="checkbox" id='setting1' checked> Some settings if nescessary</label>
                        <label><input type="checkbox" id='setting2' checked> Some settings if nescessary</label>
                    </div>-->
                </div>

                <div class="group-logo-wrapper">
                    <h4 class="group-logo-tip">Logo 1 size 200x400 px:</h4>
                    <p class="group-logo-desctiption">Description if nescessary where this logo will be.</p>
                    <div class="group-logo-icon-wrapper">
                        <span class='group-logo-upload'>Upload</span>
                        <?php echo $form->fileField($model,'image',array('class'=>'group-logo-upload-default')); ?>
                        <!--<input class='group-logo-upload-default' type='file' id='upload-logo' name='upload-logo'>-->
                    </div>
                  <!--  <div class="group-checkboxes">
                        <label><input type="checkbox" id='setting3' checked> Some settings if nescessary</label>
                        <label><input type="checkbox" id='setting4' checked> Some settings if nescessary</label>
                    </div>-->
                </div>
                <?php echo $form->error($model,'image'); ?>
            </div>
            <div class="white-labels-buttons">
<!--                <button class='white-labels-preview-button white-labels-button' disabled>Preview</button>-->
                <button type="submit" class='white-labels-save-button white-labels-button'>Save</button>
            </div>
        <?php $this->endWidget(); ?>
    </div>
</div>