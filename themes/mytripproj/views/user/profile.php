<?php
/* @var $this UserController */

?>
<!--<div class="benefits-bl-wrapper">
    <h1>My Profile</h1>
    <div style="margin: 0 auto;">
        <div class="tab-form" style="width: 345px; margin: 0 auto;">
            <?php /*$form=$this->beginWidget('CActiveForm', array(
                'id'=>'users-form',
                //'enableClientValidation'=>true,
                'enableAjaxValidation'=>true,
                'clientOptions'=>array(

                    'validateOnChange' => false,
                    'validateOnSubmit'=>true,
                ),
                'htmlOptions'=>array(
                    'class'=>'p-form-sign',
                ),
            )); */?>

                <input class="p-inputs" maxlength="255" name="Users[username]" id="Users_Username" type="text" value="<?/*= $model->username; */?>" disabled>
                <input class="p-inputs" maxlength="255" name="Users[username]" id="Users_Username" type="text" value="<?/*= $model->email; */?>" disabled>

                <?php /*echo $form->textField($model,'name',array('class'=>'p-inputs', 'placeholder'=>'Name', 'size'=>60,'maxlength'=>255)); */?>
                <?php /*echo $form->error($model,'name'); */?>

                <?php /*echo $form->passwordField($model,'password',array('class'=>'p-inputs', 'placeholder'=>'New Password', 'size'=>60,'maxlength'=>64, 'value' => '')); */?>
                <?php /*echo $form->error($model,'password'); */?>

                <?php /*echo $form->passwordField($model,'confirm_password',array('class'=>'p-inputs', 'placeholder'=>'Confirm Password', 'size'=>60,'maxlength'=>64, 'value' => '')); */?>
                <?php /*echo $form->error($model,'confirm_password'); */?>

                <?php /*echo CHtml::submitButton('Update', array('class' => 'p-submit p-sign-submit')); */?>

            <?php /*$this->endWidget(); */?>
        </div>
    </div>

</div>-->

<div class="form-wrapper">
    <div class="form-content">
        <h1 class="pages-header">Edit Your Data</h1>
        <?php if(Yii::app()->user->hasFlash('access')):?>
           <h2><?php echo Yii::app()->user->getFlash('access') ?></h2>
        <?php endif; ?>
        <div class="form-cont">
                <?php $form=$this->beginWidget('CActiveForm', array(
                    'id'=>'users-form',
                    'enableClientValidation'=>true,
                    'enableAjaxValidation'=>true,
                    'clientOptions'=>array(
                        'validateOnChange' => false,
                        'validateOnSubmit'=>true,
                        'validationDelay'=>2000
                    ),
                    'htmlOptions'=>array(
                        'class'=>'form',
                    ),
                )); ?>
                <div class="form-block">
                    <div class="form-column form-column-left">
                        <div class="inp-group">
                            <div class="input-item">
                                <label for='name'>First Name:</label>
                                <?php echo $form->textField($model,'name'); ?>
                                <?php echo $form->error($model,'name'); ?>
                            </div>
                            <div class="input-item">
                                <label for='surname'>Surname:</label>
                                <?php echo $form->textField($model,'surname'); ?>
                                <?php echo $form->error($model,'surname'); ?>
                            </div>
                        </div>

                        <div class="datepicker-input-item input-item">
                            <?php echo $form->hiddenField($model,'birth',array('class'=>'editDate editted-birth-date editted-date')); ?>
                            <label for='birth-date'>Date of Birth:</label>
                           <!-- --><?php /*echo $form->textField($model,'birth',array('class'=>'editDate birth-date-to-edit birth-date','placeholder'=>'MM / DD / YYYY')); */?>
                            <input type="text" value="" class='autoloadInfo dataEdit birth-date' placeholder='MM / DD / YYYY'>
                        </div>

                        <div class="block-input-item input-item">
                            <label for='email'>Email:</label>
                            <?php echo $form->textField($model,'email'); ?>
                            <?php echo $form->error($model,'email'); ?>
                        </div>

                        <div class="block-input-item input-item">
                            <label for='phone'>Phone:</label>
                            <?php echo $form->textField($model,'phone'); ?>
                            <?php echo $form->error($model,'phone'); ?>
                        </div>

                        <div class="inp-group">
                            <div class="input-item">
                                <label for='passport'>Passport:</label>
                                <?php echo $form->textField($model,'passport'); ?>
                                <?php echo $form->error($model,'passport'); ?>
                            </div>
                            <div class="datepicker-input-item input-item">
                                <?php echo $form->hiddenField($model,'expiry',array('class'=>'editted-expiry editted-date')); ?>
                                <label for='expiry'>Expiry:</label>
                                <input type="text" class='autoloadInfo expiry dataEdit' placeholder='MM / DD / YYYY'>
                            </div>
                        </div>

                        <div class='input-item'>
                            <?php echo $form->hiddenField($model,'country',array('class'=>'editted-country')); ?>
                            <label for='country'>Country:</label>
                            <select name="country-edit" class='select-autoload form-select country-to-edit'></select>
                        </div>

                        <div class="inp-group">
                            <div class="input-item">
                                <label for='city'>Suburb:</label>
                                <?php echo $form->textField($model,'city'); ?>
                                <?php echo $form->error($model,'city'); ?>
                            </div>
                            <div class="input-item">
                                <label for='zip_code'>Post Code:</label>
                                <?php echo $form->textField($model,'zip_code'); ?>
                                <?php echo $form->error($model,'zip_code'); ?>
                            </div>
                        </div>

                        <div class='block-input-item input-item'>
                            <label for='street'>Street:</label>
                            <?php echo $form->textField($model,'street'); ?>
                            <?php echo $form->error($model,'street'); ?>
                        </div>

                        <?php if ($model->role == 'partner') : ?>
                        <br>
                        <br>
                        <div class='block-input-item input-item'>
                            <label for='referral'>Referral:</label>
                            <?php echo $form->textField($model,'referral'); ?>
                            <?php echo $form->error($model,'referral'); ?>
                        </div>
                        <?php endif; ?>

                        <div class='block-input-item input-item'>
                            <?php echo $form->dropDownList($model, 'state',
                                array('ACT' => 'ACT',
                                    'NSW' => 'NSW',
                                    'NT' => 'NT',
                                    'QLD' => 'QLD',
                                    'SA' => 'SA',
                                    'TAS' => 'TAS',
                                    'VIC' => 'VIC',
                                    'WA' => 'WA'),
                                array('class' => 'state policy-address-state state-select select',
                                    'empty' => 'State')); ?>
                            <?php echo $form->error($model,'state', array('style'=>'color: red;')); ?>
                        </div>
                    </div>
                    <?php if(Yii::app()->user->role == 'admin' || Yii::app()->user->role == 'partner'):  ?>
                    <div class="form-column form-column-right">

                        <div class="block-input-item input-item">
                            <label for='company-name'>Company Name:</label>
                            <?php echo $form->textField($model->partner,'name'); ?>
                            <?php echo $form->error($model->partner,'name'); ?>
                        </div>

                        <div class="block-input-item input-item">
                            <label for='domain_url'>Domain Url:</label>
                            <?php echo $form->textField($model->partner,'domain_url'); ?>
                            <?php echo $form->error($model->partner,'domain_url'); ?>
                        </div>

                        <div class="block-input-item input-item">
                            <label for='subdomain_prefix'>Subdomain prefix:</label>
                            <?php echo $form->textField($model->partner,'subdomain_prefix'); ?>
                            <?php echo $form->error($model->partner,'subdomain_prefix'); ?>
                        </div>
                        <div class="block-input-item input-item">
                            <label for='referral_code'>Referral code:</label>
                            <?php echo $form->textField($model->partner,'referral_code'); ?>
                            <?php echo $form->error($model->partner,'referral_code'); ?>
                        </div>
                        <div class="block-input-item input-item">
                            <label for='commission_amount'>Comission amount:</label>
                            <?php echo $form->textField($model->partner,'commission_amount'); ?>
                            <?php echo $form->error($model->partner,'commission_amount'); ?>
                        </div>

                        <div class="block-input-item input-item">
                            <label for='reg_exp'>Regular expression:</label>
                            <?php echo $form->textField($model->partner,'reg_exp'); ?>
                            <?php echo $form->error($model->partner,'reg_exp'); ?>
                        </div>


                        <!--<?php /*
                        <div class="input-item">
                            <?php echo $form->hiddenField($model->partner,'country',array('class'=>'editted-company-country')); ?>
                            <label for='company-country'>Company Country:</label>
                            <select name="country-edit" id='company-country' class='form-select company-country-to-edit'>
                                <option value="Australia">Australia</option>
                                <option value="USA">USA</option>
                                <option value="German">German</option>
                                <option value="Cannada">Cannada</option>
                            </select>
                        </div>
                        
                        <div class="inp-group">
                            <div class="input-item">
                                <label for='city'>Company Suburb:</label>
                                <?php echo $form->textField($model->partner,'city'); ?>
                                <?php echo $form->error($model->partner,'city'); ?>
                            </div>
                            <div class="input-item">
                                <label for='zip_code'>Company Post Code:</label>
                                <?php echo $form->textField($model->partner,'zip_code'); ?>
                                <?php echo $form->error($model->partner,'zip_code'); ?>
                            </div>
                        </div>

                        <div class='block-input-item input-item'>
                            <label for='sreet'>Company Street:</label>
                            <?php echo $form->textField($model->partner,'sreet'); ?>
                            <?php echo $form->error($model->partner,'sreet'); */ ?>
                        </div>
                        -->
                    </div>
                    <?php endif; ?>
                </div>

            <div class="form-block">
                <div class="input-item bigger-bottom-offset interests-block">
                    <?php echo $form->hiddenField($model,'subscribe_country',array('class'=>'interest-inp')); ?>
                    <label><?php echo $form->checkBox($model,'subscribe',  array('uncheckValue'=>0,'class'=> 'subscribe', 'checked'=>($model->subscribe) ? 'checked' : '')); ?>Subscribe to MyTripInsurance information.</label>
<!--                    <select class='subscribe-select form-select select'>-->
<!--                        <option selected="selected">Interests</option>-->
<!--                        <option value="Australia">Australia</option>-->
<!--                        <option value="USA">USA</option>-->
<!--                        <option value="German">German</option>-->
<!--                        <option value="Cannada">Cannada</option>-->
<!--                    </select>-->
                    <div class="disabled-select">
                        <span>Interests</span>
                    </div>
                </div>
            </div>
                <div class="update-container">
                    <button class='form-button-default update-button' type='submit'>Update</button>
                </div>
            <?php $this->endWidget(); ?>
        </div>
    </div>
</div>