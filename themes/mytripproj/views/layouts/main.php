<!DOCTYPE html>
<html lang='en'>
<head>
    <meta charset='utf-8' />
    <meta name='viewport' content='width=device-width, initial-scale=1'>
    <meta http-equiv='X-UA-Compatible' content='IE=Edge' />
    <meta name="google-site-verification" content="eP1vd_Qw0uHGz1n1yfokC_pWPeSiddOz1Z4cjlfaxoo" />
    <link href='https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i' rel='stylesheet'>
    <link rel='stylesheet' href='<?php echo Yii::app()->theme->baseUrl; ?>/css/intlTelInput.css' />
    <link rel='stylesheet' href='<?php echo Yii::app()->theme->baseUrl; ?>/css/style.css' />
    <link rel='stylesheet' href='<?php echo Yii::app()->theme->baseUrl; ?>/css/supp.css' />
    <link rel='stylesheet' href='<?php echo Yii::app()->theme->baseUrl; ?>/css/tablet.css' />
    <link rel='stylesheet' href='<?php echo Yii::app()->theme->baseUrl; ?>/css/to800px.css' />
    <!-- put in head tag -->
    <link rel="shortcut icon" href="<?php echo Yii::app()->theme->baseUrl; ?>/favicon.ico" type="image/x-icon" />

    <?php if (isset($this->css)) : ?>
        <?php foreach ($this->css as $css_item) : ?>
            <link rel='stylesheet' href='<?php echo Yii::app()->theme->baseUrl . $css_item; ?>' />
        <?php endforeach; ?>
    <?php endif; ?>


    <link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/flags.css">
    <?php if (!Yii::app()->user->isGuest) :?>
    <?php //Yii::app()->clientScript->registerScriptFile('https://code.jquery.com/jquery-3.1.1.min.js');?>
    <?php //Yii::app()->clientScript->registerCoreScript('jquery');?>
    <?php
        Yii::app()->clientScript->registerCoreScript('jquery');
    ?>

    <?php endif; ?>
    <title>mytripinsurance</title>
</head>
<body>
    <?php if (Yii::app()->user->isGuest) :?>
        <div class="popup-wrapper">
            <div class="popup-close-bg"></div>
            <div class="popup">
                <div class="p-logo"></div>
                <div class="p-content">
                    <div class="p-content-btns">
                        <a href="#" class='p-sign clicked'>Sign In</a>
                        <a href="#" class='p-reg'>Registration</a>
                        <div class="bottom-line"></div>
                    </div>
                    <div class="p-tabs">
                        <div class="tab p-sign-tab visible">
                            <div class="tab-header">
                                <h2>Sign in</h2>
                                <p>Are you not registered? <a href="#" class='p-reg-link'>Register</a></p>
                            </div>
                            <div class="tab-form p-sign-form">
                                <?php $this->widget('LoginFormWidget'); ?>
<!--                                <a href="#" class='forgot'>Forgot Password?</a>-->
                            </div>
                        </div>
                        <div class="tab p-reg-tab">
                            <div class="tab-header">
                                <h2>Registration</h2>
                                <p>Are you already registered? <a href="#" class='p-sign-link'>Sign in</a></p>
                            </div>
                            <div class="tab-form p-reg-form">
                                <?php $this->widget('RegistrationFormWidget'); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <?php endif; ?>

    <div class="left-side-menu">
        <a href="#" class='close'></a>

        <?php
        $admins_only = (Yii::app()->user->role == 'administrator' || Yii::app()->user->role == 'partner');
            $menuItems = array(
                array('label'=>'Home', 'url'=>array('/site/index')),
//                array('label'=>'About', 'url'=>array('/site/page', 'view'=>'about')),
                array('label'=>'Contact', 'url'=>array('/site/contact'))
            );
            foreach(Pages::pagesMenuList() as $mItem) {
                $menuItems[] = $mItem;
            }
            $menuItems[] = array('label'=>'Control Panel', 'url'=>array('/admin/'), 'visible'=>(Yii::app()->user->role == Users::ROLE_ADMINISTRATOR));
            $menuItems[] = array('label'=>'Profile',
                'url'=>array('/user/profile'),
                'visible'=>!Yii::app()->user->isGuest
            );
            $menuItems[] = array('label'=>'Orders',
                'url'=>array('/order/index'),
                'visible'=>$admins_only
            );
            $menuItems[] = array('label'=>'Statistic',
                'url'=>array('/order/statistic'),
                'visible'=>$admins_only
            );
            /*$menuItems[] = array('label'=>'Customers',
                'url'=>array('/order/customers'),
                'visible'=>$admins_only
            );*/
            $menuItems[] = array('label'=>'Login',
                'url'=>array('#'),
                'itemOptions' => array('class'=>'sign'),
                'visible'=>Yii::app()->user->isGuest);
            $menuItems[] = array('label'=>'Registration',
                'url'=>array('#'),
                'itemOptions' => array('class'=>'reg'),
                'visible'=>Yii::app()->user->isGuest);
            $menuItems[] = array('label'=>'Logout ('.Yii::app()->user->name.')', 'url'=>array('/site/logout'), 'visible'=>!Yii::app()->user->isGuest);

        ?>
        <?php $this->widget('zii.widgets.CMenu', array(
            'items'=>$menuItems,
            'htmlOptions' => array(
                'class'=>'left-side-nav',
            ),

        ));?>
    </div>
    <?php
        $route = Yii::app()->getRequest()->getPathInfo();
        $header_class = (empty($route) || $route == 'site/index') ? 'header-main-page' : '';
        $logo = (empty($route) || $route == 'site/index') ? false : true;
    ?>

    <div class='header <?php echo $header_class; ?>'>
        <div class='header-left float-left'>
            <div class='menu-btn-bl float-left'>
                <a href='#' class='menu-btn'></a>
            </div>
            <?php if ($logo) : ?>
            <div class='head-logo float-left'>
                <a href='/' class='logo'></a>
            </div>
            <?php endif; ?>
        </div>
        <div class='header-right float-right'>

            <?php if (Yii::app()->user->isGuest) : ?>
                <div class='sign float-left'>
                    <a href='#'>Sign in</a>
                </div>
            <?php else : ?>
                <div class='logout float-left'>
                    <a href='/site/logout'><?php echo 'Logout ('.Yii::app()->user->name.')'; ?></a>
                </div>
            <?php endif; ?>

            <div class='float-left contacts'>
                <p><a href='tel:1800061568' class='number'>1 800 061 568 </a></p>
                <p class='address'>(8:30am-5pm AEST Mon-Fri)</p>
            </div>
        </div>
    </div>
    <?php echo $content; ?>

    <div class="footer-wrapper">
        <div class="footer-top">
            <p class='footer-label'>Supported By</p>
            <div class="companies" >

                <a target="_blank" class="company appGroup" href='http://www.appointmentgroup.com/'><img src="<?php echo Yii::app()->theme->baseUrl; ?>/img/appGroup.png" alt=""></a>
                <a target="_blank" class="company firstCruise" href='http://www.cruise1st.com.au/'><img src="<?php echo Yii::app()->theme->baseUrl; ?>/img/cruise1st.png" alt=""></a>
                <a target="_blank" class="company bayTravel" href='http://www.baytravelgroup.com.au/'><img src="<?php echo Yii::app()->theme->baseUrl; ?>/img/baytravelgroup.png" alt=""></a>

            </div>
            <p class='footer-label'>Supporting</p>
            <div class="companies-supporting">

                <a target="_blank" href='http://smartraveller.gov.au/'><img src="<?php echo Yii::app()->theme->baseUrl; ?>/img/smarttrav.png" alt=""></a>

            </div>
        </div>
        <div class="footer-bottom">
            <div class="footer-bottom-bl-wrap">
                <div class="float-left footer-info">
                    <ul class='foot-nav'>
                        <li><a target="_blank" href="/pages/tos">Terms Of Service</a></li>
                        <li><a target="_blank" href="/pages/privacypolicy">Privacy Policy</a></li>
                        <!--li><a href="/site/page/view/about">About Us</a></li-->
                    </ul>
                    <p class='copyright'>Copyright © B2B Travel Solutions Pty Ltd trading as Mytripinsurance <?php echo date("Y"); ?></p>
                </div>
                <div class="float-right">
                    <div class="socials">
                        <a target="_blank" href='https://www.facebook.com/mytripinsurance.com.au/' class="fb"></a>
                        <a target="_blank" href='https://twitter.com/InsuranceTrip' class="tw"></a>
                        <a target="_blank" href='https://www.instagram.com/mytripinsurance/?hl=en' class="inst"></a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?php //if (!$logo) : ?>
    <div class="chubb-insurance-textblock">
        <p>
            Mytripinsurance is issued and underwritten by Chubb Insurance Australia Limited, ABN 23 001 642 020 AFSL
            239687 (Chubb) and distributed by B2B Travel Solutions Pty Ltd ABN 70 611 652 950 (B2B Travel Solutions) as
            Authorised Representative of Chubb (Authorised Representative Number 001250949). Mytripinsurance is a brand
            and business name of B2B Travel Solutions. Any advice provided is general only and has been prepared without
            taking into account your objectives, financial situation or needs and may not be right for you. Terms,
            conditions, exclusions, limits of cover and eligibility criteria apply. To decide if this product is right
            for you, please read the <a href="/source/My%20Trip%20Insurance%20Australia%20-%20Single%20Trip%20PDS.pdf"
                                        target="_blank">Mytripinsurance Single Trip Product Disclosure Statement</a> and
            <a href="/source/My%20Trip%20Insurance%20FSG.pdf" target="_blank">Financial Services Guide.</a>
        </p>
    </div>
    <?php //endif; ?>


    <script src='<?php echo Yii::app()->theme->baseUrl; ?>/js/jquery-ui.min.js'></script>
    <script src='<?php echo Yii::app()->theme->baseUrl; ?>/js/bootstrap.min.js'></script>
    <script src='<?php echo Yii::app()->theme->baseUrl; ?>/js/jquery.easy-autocomplete.min.js'></script>
    <script src='<?php echo Yii::app()->theme->baseUrl; ?>/js/jquery.creditCardValidator.js'></script>
    <script src='<?php echo Yii::app()->theme->baseUrl; ?>/js/intlTelInput.min.js'></script>
    <script src='<?php echo Yii::app()->theme->baseUrl; ?>/js/core.js'></script>


    <?php if (isset($this->js)) : ?>
        <?php foreach ($this->js as $js_item) : ?>
            <script src='<?php echo Yii::app()->theme->baseUrl . $js_item; ?>'></script>
        <?php endforeach; ?>
    <?php endif; ?>

    <script src='<?php echo Yii::app()->theme->baseUrl; ?>/js/salvattore.min.js'></script>
    <script src='<?php echo Yii::app()->theme->baseUrl; ?>/js/jquery.formstyler.min.js'></script>

    <script src='<?php echo Yii::app()->theme->baseUrl; ?>/js/supp.js'></script>
</body>
</html>