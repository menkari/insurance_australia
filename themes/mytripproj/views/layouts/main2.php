<!DOCTYPE html>
<html lang='ru'>
<head>
    <meta charset='utf-8' />
    <meta name='viewport' content='width=device-width, initial-scale=1'>
    <meta http-equiv='X-UA-Compatible' content='IE=Edge' />
    <link href='https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i' rel='stylesheet'>
    <link rel='stylesheet' href='<?php echo Yii::app()->theme->baseUrl; ?>/css/style.css' />
    <link rel='stylesheet' href='<?php echo Yii::app()->theme->baseUrl; ?>/css/flags.css' />
    <?php if (!Yii::app()->user->isGuest) :?>
        <script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
        <?php Yii::app()->clientScript->scriptMap=array('jquery.js'=>false);?>
    <?php endif; ?>
    <title>mytripinsurance</title>
</head>
<body>
    <?php if (Yii::app()->user->isGuest) :?>
    <div class="popup-wrapper">
        <div class="popup-close-bg"></div>
        <div class="popup">
            <div class="p-logo"></div>
            <div class="p-content">
                <div class="p-content-btns">
                    <a href="#" class='p-sign clicked'>Sign In</a>
                    <a href="#" class='p-reg'>Registration</a>
                    <div class="bottom-line"></div>
                </div>
                <div class="p-tabs">
                    <div class="tab p-sign-tab visible">
                        <div class="tab-header">
                            <h2>Sign in</h2>
                            <p>Are you not registered? <a href="#" class='p-reg-link'>Register</a></p>
                        </div>
                        <div class="tab-form p-sign-form">
                            <?php $this->widget('LoginFormWidget'); ?>
                            <a href="#" class='forgot'>Forgot Password?</a>
                        </div>
                    </div>
                    <div class="tab p-reg-tab">
                        <div class="tab-header">
                            <h2>Registration</h2>
                            <p>Are you already registered? <a href="#" class='p-sign-link'>Sign in</a></p>
                        </div>
                        <div class="tab-form p-reg-form">
                            <?php $this->widget('RegistrationFormWidget'); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php endif; ?>

    <div class="left-side-menu">
        <a href="#" class='close'></a>
        <?php $this->widget('zii.widgets.CMenu', array(
            'items'=>array(
                array('label'=>'Home', 'url'=>array('/site/index')),
                array('label'=>'About', 'url'=>array('/site/page', 'view'=>'about')),
                array('label'=>'Contact', 'url'=>array('/site/contact')),
                Pages::pagesMenuList(),
                array('label'=>'Control Panel', 'url'=>array('/admin/'), 'visible'=>(Yii::app()->user->role == Users::ROLE_ADMINISTRATOR)),
                array('label'=>'Profile',
                    'url'=>array('/user/profile'),
                    'visible'=>!Yii::app()->user->isGuest
                ),
                array('label'=>'Create Order',
                    'url'=>array('/order/create'),
                    'visible'=>!Yii::app()->user->isGuest
                ),
                array('label'=>'My Orders',
                    'url'=>array('/order/index'),
                    'visible'=>!Yii::app()->user->isGuest
                ),
                array('label'=>'Login',
                    'url'=>array('#'),
                    'itemOptions' => array('class'=>'sign'),
                    'visible'=>Yii::app()->user->isGuest),
                array('label'=>'Registration',
                    'url'=>array('#'),
                    'itemOptions' => array('class'=>'reg'),
                    'visible'=>Yii::app()->user->isGuest),
                array('label'=>'Logout ('.Yii::app()->user->name.')', 'url'=>array('/site/logout'), 'visible'=>!Yii::app()->user->isGuest)
            ),
            'htmlOptions' => array(
                'class'=>'left-side-nav',
            ),

        ));?>
    </div>
    <div class='header-wrapper'>

        <div class='header'>
            <div class='header-left float-left'>
                <div class='menu-btn-bl float-left'>
                    <a href='#' class='menu-btn'></a>
                </div>
                <div class='head-logo float-left'>
                    <a href='/' class='logo'></a>
                </div>
            </div>
            <div class='header-right float-right'>

                <?php if (Yii::app()->user->isGuest) : ?>
                    <div class='sign float-left'>
                        <a href='#'>Sign in</a>
                    </div>
                <?php else : ?>
                    <div class='logout float-left'>
                        <a href='/site/logout'><?php echo 'Logout ('.Yii::app()->user->name.')'; ?></a>
                    </div>
                <?php endif; ?>

                <div class='float-left'>
                    <p><a href='tel:1300290217' class='number'>1300 290 217 </a></p>
                    <p class='address'>(8am-6pm AEST Mon-Fri)</p>
                </div>
            </div>
        </div>

        <div class='header-content-wrapper'>
            <div class='header-content'>
                <div class='head-cont-logo'>
                    <img src='<?php echo Yii::app()->theme->baseUrl; ?>/img/logo2.png' alt=''>
                </div>
                <div class='head-content-bl'>
                    <h1>Lorem Ipsum is simply dummy text of the printing and typesetting</h1>
                    <div class='head-content-bl-form'>
                        <form action='#'>
                            <div class='where-inp inp'>
                                <label for='where'>Where are you going?</label>
                                <div class='where-div'>
                                    <input type='text' id='where'>
                                </div>
                            </div>
                            <div class='when-inp inp'>
                                <label for='when'>When?</label>
                                <input type='text' id='when' class='from'>
                                <i class='arr-r'></i>
                                <input type='text' class="to">
                            </div>
                            <div class='age-inp inp'>
                                <label for='age'>Age?</label>
                                <input type='text' id='age'>
                            </div>
                            <div class="submit-bl">
                                <button type='submit' class='submit'>Quote</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="content-wrapper">
        <div class="content-bl">
            <?php echo $content; ?>
        </div>
    </div>

    <div class="footer-wrapper">
        <div class="footer-top">
            <div class="companies"></div>
        </div>
        <div class="footer-bottom">
            <div class="footer-bottom-bl-wrap">
                <div class="float-left">
                    <ul class='foot-nav'>
                        <li><a href="#">Terms Of Service</a></li>
                        <li><a href="#">Privacy Policy</a></li>
                        <li><a href="#">About Us</a></li>
                    </ul>
                    <p class='copyright'>Copyright © Mytripunsurance Agency 2016 - All rights reserved </p>
                </div>
                <div class="float-right">
                    <div class="socials">
                        <a href='#' class="fb"></a>
                        <a href='#' class="tw"></a>
                        <a href='#' class="inst"></a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script src='<?php echo Yii::app()->theme->baseUrl; ?>/js/jquery-ui.min.js'></script>
    <script src='<?php echo Yii::app()->theme->baseUrl; ?>/js/jquery.easy-autocomplete.min.js'></script>
    <script src='<?php echo Yii::app()->theme->baseUrl; ?>/js/core.js'></script>
    <script src='<?php echo Yii::app()->theme->baseUrl; ?>/js/date.js'></script>

    <script src='<?php echo Yii::app()->theme->baseUrl; ?>/js/salvattore.min.js'></script>
    <script src='<?php echo Yii::app()->theme->baseUrl; ?>/js/jquery.formstyler.min.js'></script>
</body>
</html>
