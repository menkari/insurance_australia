<div class="steps-wrapper">
    <div class="steps-content">
        <div class="pay-complete-wrapper">
            <div class="details-form payment-details">
            <?php $form=$this->beginWidget('CActiveForm', array(
                'id' => 'reset-password',
                'enableAjaxValidation' => true,
                'clientOptions' => array(
                    'validateOnChange' => false,
                    'validateOnSubmit' => true,
                    'validationDelay'=>1000
                ),
                'htmlOptions'=>array(
                    'class'=>'',
                ),
                'action' => '/site/reset'
            )); ?>
            <div class="step3-inp-wrapper step3-inp-wrapper-verybig" style="margin: 0 auto; text-align: center;">
                <?php echo $form->textField($model, 'email', array('placeholder' => 'Email')); ?>
                <?php echo $form->error($model,'email', array('style'=>'color: red;')); ?>
            </div>
            <?php echo CHtml::htmlButton('Reset', array('class' => 'reset-password-button', 'type'=>'submit', 'name' => 'Reset')); ?>

            <?php $this->endWidget(); ?>
        </div>
        </div>
    </div>
</div>


