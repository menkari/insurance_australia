<?php if(Yii::app()->user->hasFlash('createDomain')):?>
    <script>
        alert('<?php echo Yii::app()->user->getFlash('createDomain') ?>');
    </script>
<?php endif; ?>

<div class='header-content-wrapper'>
    <div class='header-content'>
        <div class='head-cont-logo'>

        </div>
        <div class='head-content-bl'>
            <h1>Travel Insurance built by the Travel Industry for your holiday</h1>
            <div class='head-content-bl-form head-content-bl-main head-content-bl-container'>
<!--                <form action='#' id="order-quote">-->
                <?php $form=$this->beginWidget('CActiveForm', array(
                    'id' => 'orders-base-info',
                    'enableAjaxValidation' => true,
                    'enableClientValidation' => true,
                    'clientOptions' => array(
                        //'errorCssClass' => 'has-error',
                        'afterValidateAttribute' => 'js:function(form, attribute, data, hasError){if(!hasError){send();}}',
                        'validateOnChange' => false,
                        'validateOnSubmit' => true,
                        'validationDelay'=>2000
                    ),

                    'htmlOptions'=>array(
                        'class'=>'p-form-sign',
                    ),
                    'action' => '/order/quote'
                )); ?>
                    <div class='where-inp inp'>
                        <label for='where'>Where are you going?
                            <a href="#" data-toggle="tooltip" title='Please enter all the countries you will visit during your trip and for which you want to be insured. For Domestic, please enter Australia.'></a>
                        </label>
                        <div class='where-div'>
                            <input type='text' id='where' value="">
                            <input type='hidden' class='where-values' value='<?php echo $countries?>'>
                        </div>
                    </div>
                    <div class='when-inp inp main-when'>
                        <label>When?
                            <!-- placeholder <a href="#" data-toggle="tooltip" title='Start Date can be up to 365 days in the future starting from today for Single Trip & up to 45 days for Annual Multi-Trip<br/>
Travel Duration up to a max value of 365 days. Unless there is an Insured Person who is at least 70 years old in which case the max is 180 days.'></a>-->
<!--                            formatting-->
                            <a href="#" data-toggle="tooltip" title='Start Date can be up to 365 days in the future starting from today for Single Trip<br/>
Travel Duration up to a max value of 365 days. Unless there is an Insured Person who is at least 70 years old in which case the max is 180 days.'></a>
                        </label>
                        <input type='button' id='when' class='from step2-when-year' name='time-from-visit'>
                        <i class='arr-r'></i>
                        <!-- placeholder: fix end date to start date + 1 year for annual multi-trip -->
                        <input type='button' class="to step2-when-year" name='time-to-visit'>
                    </div>
                    <div class='age-inp inp'>
                        <label for='age' class="children-tooltip">Adult ages?
<!--                            formatting-->
                            <a href="#" data-toggle="tooltip" title='In order to purchase a Policy You must be eighteen (18) years of age when You apply.<br/>
Insured Persons must be under 90 years of age, must have a permanent residence in Australia, unrestricted right of entry into Australia as well as access to long-term medical care in Australia and:
<ul>
    <li>be an Australian resident; or</li>
    <li>be on a skilled working visa (e.g. a 457 visa), but not a working holiday visa; or</li>
    <li>have a partner/spouse visa which allows you to stay in Australia for at least 2 years; or</li>
    <li>have a New Zealand passport.</li></ul>'></a>
                        </label>
                        <input type='text' id='age' class='main-page-age' value="<?php echo $age ?>">
                        <input type='text' id='age2' class='main-page-age' value="<?php echo $age2 ?>">
                    </div>
                    <?php echo $form->hiddenField($order_model, 'countries'); ?>
                    <?php echo $form->hiddenField($order_model, 'start_traveling', array('value' => $start_traveling, 'class' => 'order-start-traveling')); ?>
                    <?php echo $form->hiddenField($order_model, 'end_traveling', array('value' => $end_traveling, 'class' => 'order-end-traveling')); ?>
                    <?php echo $form->hiddenField($order_model, 'age', array('value' => $age, 'class'=>'order-age')); ?>
                    <?php echo $form->hiddenField($order_model, 'age2', array('value' => $age2, 'class'=>'order-age2')); ?>

                    <div class="quote-button-wrapper">
                        <?php if(Yii::app()->user->hasFlash('orders_base_error')):?>
                            <div class="errorMessage">
                                <?php echo Yii::app()->user->getFlash('orders_base_error'); ?>
                            </div>
                        <?php endif; ?>
                        <?php echo CHtml::errorSummary($order_model) ?>
                        <?php echo $form->error($order_model,'countries', array('style'=>'color: red; margin-bottom: 20px; font-weight: bold; font-size: 30px;')); ?>
                        <?php echo $form->error($order_model,'age', array('style'=>'color: red; margin-bottom: 20px; font-weight: bold; font-size: 30px;')); ?>
                        <?php echo CHtml::submitButton('Quote', array('class' => 'quote-button', 'name' => 'quote-button')); ?>
                    </div>
                <?php $this->endWidget(); ?>
<!--                </form>-->
            </div>
        </div>
    </div>
</div>

<div class="content-wrapper">
    <div class="content-bl">
        <div class="benefits-bl-wrapper">
            <h1>Our Benefits</h1>
            <p class='benefits-top-p'>Whether you are an occasional or frequent traveller, Mytripinsurance provides travel insurance cover
                directly to individuals and families. There are options to choose the benefits and level of cover you
                need to suit your travel itinerary. There are also add-on options so you also have the flexibility to
                select only the benefits you need for your future trip.
            </p>
            <div class="benefits-bl" data-columns>
                <div class="benefits-block">
                    <img src="<?php echo Yii::app()->theme->baseUrl; ?>/img/profile-bg.png" alt="">
                    <h3> Personal Profile</h3>
                    <p> Travellers can create their own profile to store policy details, personal details and emergency
                        contacts with confidence.</p>
                </div>
                <div class="benefits-block">
                    <div class="ben-img-bl">
                        <img src="<?php echo Yii::app()->theme->baseUrl; ?>/img/pre-exist-bg.png" alt="">
                    </div>
                    <h3> Pre-Existing Medical </h3>
                    <p> We understand that travellers don’t want to be restricted by their medical circumstances.</p>
                    <p>This is why 54 approved medical conditions are automatically covered within our policies with no
                        additional premium.</p>
                    <p><a placeholder href="/source/20161222%20-%20Mytrip%20-%20Approved%20Medical%20Conditions%20(ST).pdf" target="_blank">Proof
                            Link</a></p>
                </div>
                <div class="benefits-block">
                    <div class="ben-img-bl">
                        <img src="<?php echo Yii::app()->theme->baseUrl; ?>/img/glob-supp-bg.png" alt="">
                    </div>
                    <h3> Global support </h3>
                    <p> Mytripinsurance is supported by Chubb Insurance, which has local operations in 54 countries and
                        territories. We have a global footprint our services complemented by local experts who know the
                        country you are in.</p>
                </div>
                <div class="benefits-block">
                    <div class="ben-img-bl">
                        <img src="<?php echo Yii::app()->theme->baseUrl; ?>/img/travel-ind-bg.png" alt="">
                    </div>
                    <h3 class='two-row-header'> Travel Industry knowledge</h3>
                    <p>The world is getting smaller and people can now book direct, via online channels or through their
                        chosen travel agent.</p>
                    <p>MytripInsurance was designed by the travel industry for travellers.</p>
                </div>
            </div>
        </div>
        <div class="blocks-group">
            <div class="blocks-group-bl">
                <div class="blocks-group-bl1">
                    <h2>Emergency Assistance</h2>
                    <a class="blocks-number" href='tel:+61289075666'>+61 2 8907 5666</a>
                    <p class="blocks-text blocks-text1">Available to assist you every hour of every day as part of your cover.</p>
                </div>
                <div class="blocks-group-bl2">
                    <h2>Lodge A claim</h2>
                    <p class="blocks-text blocks-text1">If something goes wrong on your trip and you need to lodge a claim, begin the process by submitting your details here.</p>
                    <a href="/pages/claim" class='group-btn get-quote'>MAKE A CLAIM</a>
                </div>
<!--                <div class="blocks-group-bl3">-->
<!--                    <h2>FAQs</h2>-->
<!--                    <p class="blocks-text blocks-text1"> If you have any questions, please look at our FAQs page.</p>-->
<!--                    <a href="/site/page/view/about" class='group-btn find-out'>Find out more</a>-->
<!--                </div>-->
            </div>
        </div>
        <div class="single-trip-disclosure">
            <p>
            A detailed description of the cover is set out in the benefits sections of the <a href="/source/My%20Trip%20Insurance%20Australia%20-%20Single%20Trip%20PDS.pdf" target="_blank">Mytripinsurance Single
                Trip Product Disclosure Document (PDS)</a> and the cover provided is subject to the terms, conditions and exclusions contained.
            Maximum benefit limits and sub limits apply as set out in the Benefits and Maximum Benefit Limits table.
            Amounts as set out in the table are the amounts payable per Insured Person. Policy terms do however apply
            limits on what We will pay for such benefits. The information provided does not take into account Your
            individual objectives, financial situation or needs. You need to decide if the limits, type and level of
                cover are appropriate for You. You should read the <a href="/source/My%20Trip%20Insurance%20Australia%20-%20Single%20Trip%20PDS.pdf" target="_blank">PDS</a> carefully to understand what’s covered, what’s not
            covered, the eligibility criteria, terms, conditions, exclusions and limits of cover. Do not rely on
            assumptions of what should be covered under travel insurance.
            </p>
        </div>
    </div>
</div>