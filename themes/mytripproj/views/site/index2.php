<?php
/* @var $this SiteController */

$this->pageTitle=Yii::app()->name;
?>


    <div class="benefits-bl-wrapper">
        <h1>Our Benefits</h1>
        <div class="benefits-bl" data-columns>
            <div class="benefits-block">
                <img src="<?php echo Yii::app()->theme->baseUrl; ?>/img/benefits-img.png" alt="">
                <h3>International Traveller</h3>
                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the.</p>
            </div>
            <div class="benefits-block">
                <img src="<?php echo Yii::app()->theme->baseUrl; ?>/img/benefits-img.png" alt="">
                <h3>International Traveller</h3>
                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the.</p>
            </div>
            <div class="benefits-block">
                <img src="<?php echo Yii::app()->theme->baseUrl; ?>/img/benefits-img.png" alt="">
                <h3>International Traveller</h3>
                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the.</p>
            </div>
            <div class="benefits-block">
                <img src="<?php echo Yii::app()->theme->baseUrl; ?>/img/benefits-img.png" alt="">
                <h3>International Traveller</h3>
                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the.</p>
            </div>
        </div>
    </div>
    <div class="blocks-group">
        <div class="blocks-group-bl">
            <div class="blocks-group-bl1">
                <h2>Emergency Assistance</h2>
                <a class="blocks-number" href='tel:60327725592'>+60 3 2772 5592</a>
                <p class="blocks-text blocks-text1">Available to assist you every hour of every day as part of your cover.</p>
            </div>
            <div class="blocks-group-bl2">
                <h2>Lodge A claim</h2>
                <p class="blocks-text blocks-text1">If something goes wrong on your trip and you need to lodge a claim, begin the process by submitting your details here.</p>
                <a href="https://mytripinsurance.com.au/pages/claim" class='group-btn get-quote'>MAKE A CLAIM</a>
            </div>
            <div class="blocks-group-bl3">
                <h2>Faqs</h2>
                <p class="blocks-text blocks-text1"> If you have any questions, please look at our FAQ’s page.</p>
                <a href="#" class='group-btn find-out'>Find out more</a>
            </div>
        </div>
    </div>

<?php
foreach(Yii::app()->user->getFlashes() as $key => $message) {
    echo '<div class="flash-' . $key . '">' . $message . "</div>\n";
    if ($key == 'success_confirm' && Yii::app()->user->isGuest) {
        echo CHtml::link('Login', array('site/login'));
    }
}
?>
