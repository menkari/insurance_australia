<div class="contact-wrapper">
    <div class="contact-content">
        <h1 class='pages-header'>Contact Us</h1>
        <h2>Emergency, Claim, Question?</h2>
        <div class="contact-text">
<!--            placeholder -->
            <a href="tel:1300 666 000" class='contact-tel'>Call : <span>1800 061 568</span></a>
            <p>Claims and sales enquiries <a href="mailto:mytripinsurance.au@chubb.com">mytripinsurance.au@chubb.com</a></p>
            <p>Group and office contact <a href="mailto:info@mytripinsurance.com.au">info@mytripinsurance.com.au</a></p>
            <div class="contact-us-butt-wrapper">
                <form action="#" name='contact-us-form'>
<!--                    <button class='contact-us-butt' name='contact-us-butt'>Claim Form</button>-->
                </form>
            </div>
        </div>
    </div>
</div>